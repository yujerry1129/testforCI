
insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (1, N'Home', N'menu_home', NULL, NULL, N'Home', N'Index', 1);

insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (2, N'Device', N'menu_device', NULL, NULL, N'Device', N'Index', 2);


insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (3, N'Media Library', N'menu_media_library', NULL, NULL, N'Library', N'Index', 3);

insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (4, N'Campaign', N'menu_campaign', NULL, NULL, N'Campaign', N'Index', 4);

insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (5, N'Account', N'menu_account', NULL, NULL, N'Account', N'Index', 5);

insert into `digitalsignagedb`.`SiteMenu` (`Id`, `Name`, `LanKey`, `Url`, `Icon`, `ControllerName`, `ActionName`, `MenuOrder`) VALUES (6, N'LogInfo', N'menu_log_info', NULL, NULL, N'LogInfo', N'Index', 6);

insert into `digitalsignagedb`.`SiteRole` (`Id`, `Name`, `LanKey`) VALUES (1, N'admin', N'role_admin');

insert into `digitalsignagedb`.`SiteRole` (`Id`, `Name`, `LanKey`) VALUES (2, N'report', N'role_report');

insert into `digitalsignagedb`.`SiteRole` (`Id`, `Name`, `LanKey`) VALUES (3, N'content_manager', N'role_content_manager');



insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (1, 1, 1);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (2, 1, 2);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (3, 1, 3);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (4, 1, 4);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (5, 1, 5);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (6, 1, 6);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (7, 2, 1);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (8, 2, 6);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (9, 3, 1);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (10, 3, 2);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (11, 3, 3);

insert into `digitalsignagedb`.`SiteRoleToMenu` (`Id`, `SiteRole_Id`, `SiteMenu_Id`) VALUES (12, 3, 4);


insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (1, N'Home', N'menu_home', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (2, N'首頁', N'menu_home', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (3, N'Device', N'menu_device', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (4, N'裝置', N'menu_device', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (5, N'Library', N'menu_media_library', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (6, N'素材庫', N'menu_media_library', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (7, N'Campaign', N'menu_campaign', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (8, N'播放活動', N'menu_campaign', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (9, N'Administrator', N'role_admin', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (10, N'管理員', N'role_admin', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (11, N'Content Manager', N'role_content_manager', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (12, N'內容管理', N'role_content_manager', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (13, N'Account', N'menu_account', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (14, N'帳戶', N'menu_account', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (15, N'LogInfo', N'menu_log_info', N'en-us');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (16, N'紀錄報表', N'menu_log_info', N'zh-tw');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (17, N'首页', N'menu_home', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (18, N'装置', N'menu_device', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (19, N'素材库', N'menu_media_library', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (20, N'播放活动', N'menu_campaign', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (21, N'管理员', N'role_admin', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (22, N'内容管理', N'role_content_manager', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (23, N'账户', N'menu_account', N'zh-cn');

insert into `digitalsignagedb`.`SiteText` (`Id`, `LanText`, `LanKey`, `CultureCode`) VALUES (24, N'纪录报表', N'menu_log_info', N'zh-cn');

INSERT INTO `digitalsignagedb`.`agerangedefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`, `AgeId`) VALUES (1, N'兒童', 0, 13, 1, 0);
INSERT INTO `digitalsignagedb`.`agerangedefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`, `AgeId`) VALUES(2, N'青年', 13, 18, 1,1);
INSERT INTO `digitalsignagedb`.`agerangedefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`, `AgeId`) VALUES (3,N'壯年', 18, 35, 1,2);
INSERT INTO `digitalsignagedb`.`agerangedefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`, `AgeId`) VALUES (4, N'老年', 35, 65, 1,3);


insert into `digitalsignagedb`.`DistanceRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (1, N'1公尺以下', 0, 100, 1);
insert into `digitalsignagedb`.`DistanceRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (2, N'1~2公尺', 100, 200, 1);
insert into `digitalsignagedb`.`DistanceRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (3, N'2~3公尺', 200, 300, 1);
insert into `digitalsignagedb`.`DistanceRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (4, N'3公尺以上', 300, 99999999, 1);


insert into `digitalsignagedb`.`LooktimeRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (1, N'1秒以下', 0, 10, 1);
insert into `digitalsignagedb`.`LooktimeRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (2, N'1~5秒', 10, 50, 1);
insert into `digitalsignagedb`.`LooktimeRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (3, N'5~10秒', 50, 100, 1);
insert into `digitalsignagedb`.`LooktimeRangeDefinition` (`Id`, `Description`, `MinValue`, `MaxValue`, `UserGroup_Id`) VALUES (4, N'10秒以上', 100, 99999999, 1);
