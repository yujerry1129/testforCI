USE [DigitalSignageDB]
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (1, N'Home', N'menu_home', NULL, NULL, N'Home', N'Index', 1)
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (2, N'Device', N'menu_device', NULL, NULL, N'Device', N'Index', 2)
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (3, N'Media Library', N'menu_media_library', NULL, NULL, N'Library', N'Index', 3)
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (4, N'Campaign', N'menu_campaign', NULL, NULL, N'Campaign', N'Index', 4)
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (5, N'Account', N'menu_account', NULL, NULL, N'Account', N'Index', 5)
GO
INSERT [dbo].[SiteMenu] ([Id], [Name], [LanKey], [Url], [Icon], [ControllerName], [ActionName], [MenuOrder]) VALUES (6, N'LogInfo', N'menu_log_info', NULL, NULL, N'LogInfo', N'Index', 6)
GO
INSERT [dbo].[SiteRole] ([Id], [Name], [LanKey]) VALUES (1, N'admin', N'role_admin')
GO
INSERT [dbo].[SiteRole] ([Id], [Name], [LanKey]) VALUES (2, N'report', N'role_report')
GO
INSERT [dbo].[SiteRole] ([Id], [Name], [LanKey]) VALUES (3, N'content_manager', N'role_content_manager')
GO
SET IDENTITY_INSERT [dbo].[SiteRoleToMenu] ON 

GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (1, 1, 1)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (2, 1, 2)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (3, 1, 3)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (4, 1, 4)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (5, 1, 5)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (6, 1, 6)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (7, 2, 1)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (8, 2, 6)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (9, 3, 1)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (10, 3, 2)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (11, 3, 3)
GO
INSERT [dbo].[SiteRoleToMenu] ([Id], [SiteRole_Id], [SiteMenu_Id]) VALUES (12, 3, 4)


GO
SET IDENTITY_INSERT [dbo].[SiteRoleToMenu] OFF
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (1, N'Home', N'menu_home', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (2, N'首頁', N'menu_home', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (3, N'Device', N'menu_device', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (4, N'裝置', N'menu_device', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (5, N'Library', N'menu_media_library', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (6, N'素材庫', N'menu_media_library', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (7, N'Campaign', N'menu_campaign', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (8, N'播放活動', N'menu_campaign', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (9, N'Administrator', N'role_admin', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (10, N'管理員', N'role_admin', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (11, N'Content Manager', N'role_content_manager', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (12, N'內容管理', N'role_content_manager', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (13, N'Account', N'menu_account', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (14, N'帳戶', N'menu_account', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (15, N'LogInfo', N'menu_log_info', N'en-us')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (16, N'紀錄報表', N'menu_log_info', N'zh-tw')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (17, N'首页', N'menu_home', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (18, N'装置', N'menu_device', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (19, N'素材库', N'menu_media_library', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (20, N'播放活动', N'menu_campaign', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (21, N'管理员', N'role_admin', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (22, N'内容管理', N'role_content_manager', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (23, N'账户', N'menu_account', N'zh-cn')
GO
INSERT [dbo].[SiteText] ([Id], [LanText], [LanKey], [CultureCode]) VALUES (24, N'纪录报表', N'menu_log_info', N'zh-cn')
GO

INSERT [dbo].[AdminUser] ( [LoginAccount], [LoginPassword], [Email], [CreateDate], [IsEnabled]) VALUES (N'ECS', N'1', N'ECS@ace.com', 2017-10-25, 1)
GO

--SET IDENTITY_INSERT [dbo].[AgeRangeDefinition] ON 

--INSERT [dbo].[AgeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id],[AgeId]) VALUES (1, N'兒童', 0, 13, 1,0)
--INSERT [dbo].[AgeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id],[AgeId]) VALUES (2, N'青年', 13, 18, 1,1)
--INSERT [dbo].[AgeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id],[AgeId]) VALUES (3, N'壯年', 18, 35, 1,2)
--INSERT [dbo].[AgeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id],[AgeId]) VALUES (4, N'老年', 35, 65, 1,3)
--SET IDENTITY_INSERT [dbo].[AgeRangeDefinition] OFF
--SET IDENTITY_INSERT [dbo].[DistanceRangeDefinition] ON 

--INSERT [dbo].[DistanceRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (1, N'1公尺以下', 0, 100, 1)
--INSERT [dbo].[DistanceRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (2, N'1~2公尺', 100, 200, 1)
--INSERT [dbo].[DistanceRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (3, N'2~3公尺', 200, 300, 1)
--INSERT [dbo].[DistanceRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (4, N'3公尺以上', 300, 99999999, 1)
--SET IDENTITY_INSERT [dbo].[DistanceRangeDefinition] OFF
--SET IDENTITY_INSERT [dbo].[LooktimeRangeDefinition] ON 

--INSERT [dbo].[LooktimeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (1, N'1秒以下', 0, 10, 1)
--INSERT [dbo].[LooktimeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (2, N'1~5秒', 10, 50, 1)
--INSERT [dbo].[LooktimeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (3, N'5~10秒', 50, 100, 1)
--INSERT [dbo].[LooktimeRangeDefinition] ([Id], [Description], [MinValue], [MaxValue], [UserGroup_Id]) VALUES (4, N'10秒以上', 100, 99999999, 1)
--SET IDENTITY_INSERT [dbo].[LooktimeRangeDefinition] OFF