﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DsDbLib.Models;

namespace DsDbLib
{
    public class DsDb : DbContext
    {
        public DsDb() : base("DigitalSignageDB")
        {

        }

        public DbSet<AdminUser> AdminUser { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<EmailService> EmailService { get; set; }
        public DbSet<ProductRule> ProductRule { get; set; }
        public DbSet<DMIDictionary> DMIDictionay { get; set; }
        public DbSet<DeviceScreen> DeviceScreen { get; set; }
        public DbSet<Device> Device { get; set; }
        public DbSet<FileUpload> FileUpload { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<DeviceGroup> DeviceGroup { get; set; }
        public DbSet<LibraryFolder> LibraryFolder { get; set; }
        public DbSet<LayoutTemplate> LayoutTemplate { get; set; }
        public DbSet<ResolutionConfig> ResolutionConfig { get; set; }
        public DbSet<LayoutTemplateRegion> LayoutTemplateRegion { get; set; }
        public DbSet<RegionFrame> RegionFrame { get; set; }
        //public DbSet<AgeSetting> AgeSetting { get; set; }
        public DbSet<Restriction> Restriction { get; set; }
        public DbSet<FaceDetectionLog> FaceDetectionLog { get; set; }
        public DbSet<DisplayLog> DisplayLog { get; set; }
        public DbSet<UserActionLog> UserActionLog { get; set; }
        public DbSet<FileDownloadLog> FileDownloadLog { get; set; }
        public DbSet<PlayerInformationLog> PlayerInformationLog { get; set; }
        public DbSet<Campaign> Campaign { get; set; }
        public DbSet<CampaignToDeviceScreen> CampaignToDeviceScreen { get; set; }

        public DbSet<Playlist> Playlist { get; set; }
        public DbSet<CampaignRegion> CampaignRegion { get; set; }
        public DbSet<CampaignToPlaylist> CampaignToPlaylist { get; set; }
        public DbSet<TextAsset> TextAsset { get; set; }

        public DbSet<Publication> Publication { get; set; }
        public DbSet<PublishDevice> PublishDevice { get; set; }
        public DbSet<PublishDeviceGroup> PublishDeviceGroup { get; set; }
        public DbSet<PublishCampaign> PublishCampaign { get; set; }
        public DbSet<PublishPlaylist> PublishPlaylist { get; set; }
        public DbSet<LooktimeRangeDefinition> LooktimeRangeDefinition { get; set; }
        public DbSet<DistanceRangeDefinition> DistanceRangeDefinition { get; set; }
        public DbSet<AgeRangeDefinition> AgeRangeDefinition { get; set; }
        public DbSet<AgeToRestriction> AgeToRestriction { get; set; }
        public DbSet<SiteText> SiteText { get; set; }
        public DbSet<SiteMenu> SiteMenu { get; set; }
        public DbSet<SiteRole> SiteRole { get; set; }
        public DbSet<SiteUserToRole> SiteUserToRole { get; set; }
        public DbSet<SiteRoleToMenu> SiteRoleToMenu { get; set; }
        public DbSet<ScheduleSummary> ScheduleSummary { get; set; }
        public DbSet<MediaLog> MediaLog { get; set; }
        public DbSet<DefalutScreenLog> DefalutScreenLog { get; set; }
        public DbSet<CampaignLog> CampaignLog { get; set; }
        public DbSet<PublishLog> PublishLog { get; set; }
        public DbSet<ScreenGroup> ScreenGroup { get; set; }
        public DbSet<DeviceTag> DeviceTag { get; set; }
        public DbSet<CampaignTag> CampaignTag { get; set; }
        public DbSet<VersionFile> VersionFile { get; set; }
        public DbSet<WeatherData> WeatherData { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //}
    }
}
