﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DsDbLib.Models
{
    [Table("EmailService")]
    public class EmailService
    {
        [Key]
        public int Id { get; set; }
        public string SenderAddress { get; set; }
        public string SenderName { get; set; }
        public bool EnableSSL { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpServerAddress { get; set; }
        public bool AllowAnonymous { get; set; }
        public string SmtpAccount { get; set; }
        public string SmtpPassword { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        //[JsonIgnore]
        //[ForeignKey("UserGroupId")]
        //public virtual UserGroup UserGroup { get; set; }
        //public int UserGroupId { get; set; }

        public EmailService()
        {
            EnableSSL = false;
            AllowAnonymous = true;
            SmtpPort = 25;
        }
    }
}
