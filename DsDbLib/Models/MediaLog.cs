﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("MediaLog")]
    public class MediaLog
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// 0: nothing
        /// 1: image
        /// 2: video
        /// 3: audio
        /// 9: flash
        /// 10:Weaher
        /// 11:clock
        /// 12:Menu
        /// 13:NullImage
        /// 14:player msi
        /// </summary>
        public int FileType { get; set; }

        [MaxLength(10)]
        public string ExtensionName { get; set; }

        public long? Size { get; set; }
        public int? MediaDuration { get; set; }

        public string TextAssetContent { get; set; }

        public int UserId { get; set; }

        public int UserGroupId { get; set; }

        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// 1: Add
        /// 2: Edit
        /// 3: Delete
        /// </summary>
        public int Action { get; set; }


    }
}
