﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("FileUpload")]
    public class FileUpload
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [MaxLength(10)]
        public string ExtensionName { get; set; }

        public long Size { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [MaxLength(100)]
        public string MimeType { get; set; }

        /// <summary>
        /// 0: nothing
        /// 1: image
        /// 2: video
        /// 3: audio
        /// 9: flash
        /// 10:Weaher
        /// 11:clock
        /// 12:Menu
        /// 13:NullImage
        /// 14:player msi
        /// </summary>
        public int FileType { get; set; }

        public Guid HashCode { get; set; }

        public int? Width { get; set; }
        public int? Height { get; set; }

        [JsonIgnore]
        [ForeignKey("FolderId")]
        public virtual LibraryFolder Folder { get; set; }
        public int? FolderId { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int UserGroupId { get; set; }

        [JsonIgnore]
        [InverseProperty("MediaFile")]
        public virtual ICollection<RegionFrame> Frames { get; set; }

        public int? VideoDuration { get; set; }

        /// <summary>
        /// 提供某些特殊目的的檔案使用(Menu excel)
        /// </summary>
        [MaxLength]
        public string Content { get; set; }

    }
}
