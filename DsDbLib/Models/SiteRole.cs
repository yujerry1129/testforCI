﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace DsDbLib.Models
{
    [Table("SiteRole")]
    public class SiteRole
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LanKey { get; set; }

        [JsonIgnore]
        [InverseProperty("SiteRole")]
        public virtual IEquatable<SiteUserToRole> Users { get; set; }

        [JsonIgnore]
        [InverseProperty("Role")]
        public virtual IEquatable<SiteRoleToMenu> Menus { get; set; }

    }
}
