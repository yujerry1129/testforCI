﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("CampaignToDeviceScreen")]
    public class CampaignToDeviceScreen
    {
        [Key]
        public int Id { get; set; }
        //public DateTime? DefferedTime { get; set; }

        public int UserGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }
        public int? CampaignId { get; set; }

        [JsonIgnore]
        [ForeignKey("PlaylistId")]
        public virtual Playlist Playlist { get; set; }
        public int? PlaylistId { get; set; }

        public int PlayOrder { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }
        public int? DeviceId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceGroupId")]
        public virtual DeviceGroup DeviceGroup { get; set; }
        public int? DeviceGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("RestrictionId")]
        public virtual Restriction Restriction { get; set; }
        public int? RestrictionId { get; set; }

        public int? PublicationId { get; set; }
        [JsonIgnore]
        [ForeignKey("PublicationId")]
        public virtual Publication Publication { get; set; }
    }
}
