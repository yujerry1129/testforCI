﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace DsDbLib.Models
{
    [Table("DeviceGroup")]
    public class DeviceGroup
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [Index("IX_DeviceGroupName_UserGroup_Unique", 1, IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// 0: Group First, 優先撥放群組內容
        /// 1: Group Only, 只播放群組內容
        /// 2: Device First, 優先撥放裝置本身的內容
        /// 3: Device Only, 只播放裝置本身的內容
        /// </summary>
        public int PriorityType { get; set; }

        [Index]
        [Index("IX_DeviceGroupName_UserGroup_Unique", 2, IsUnique = true)]
        public int UserGroup_Id { get; set; }

        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }

        [JsonIgnore]
        [InverseProperty("DeviceGroup")]
        public virtual ICollection<Device> Devices { get; set; }

        [JsonIgnore]
        [InverseProperty("DeviceGroup")]
        public virtual ICollection<CampaignToDeviceScreen> PublishPlans { get; set; }
    }
}
