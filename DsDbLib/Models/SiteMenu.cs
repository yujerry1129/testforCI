﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("SiteMenu")]
    public class SiteMenu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LanKey { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? MenuOrder { get; set; }

        [JsonIgnore]
        [InverseProperty("Menu")]
        public virtual IEquatable<SiteRoleToMenu> Roles { get; set; }
    }
}
