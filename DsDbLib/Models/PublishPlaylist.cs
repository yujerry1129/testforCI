﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("PublishPlaylist")]
    public class PublishPlaylist
    {
        [Key]
        public int Id { get; set; }

        public int PublicationId { get; set; }
        [JsonIgnore]
        [ForeignKey("PublicationId")]
        public virtual Publication Publication { get; set; }

        public int PlaylistId { get; set; }
        [JsonIgnore]
        [ForeignKey("PlaylistId")]
        public virtual Playlist Playlist { get; set; }
    }
}
