﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("LooktimeRangeDefinition")]
    public class LooktimeRangeDefinition
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int UserGroup_Id { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }
    }
}
