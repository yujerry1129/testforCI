﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("UserActionLog")]
    public class UserActionLog
    {
        [Key]
        public long Id { get; set; }

        public int UserId { get; set; }

        [Index]
        public int UserGroupId { get; set; }

        /// <summary>
        /// 0:登入
        /// 1:登出
        /// 2:忘記密碼
        /// 3:修改密碼
        /// 4:遭遇Exception
        /// </summary>
        public int LogType { get; set; }
        
        [MaxLength(500)]
        public string LogMessage { get; set; }

        public DateTime LogTime { get; set; }
    }
}
