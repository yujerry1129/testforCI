﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("DefalutScreenLog")]
    public class DefalutScreenLog
    {
        [Key]
        public long Id { get; set; }
        public int DeviceId { get; set; }
        public int? DefaultScreenFileId { get; set; }
        public int UserId { get; set; }
        public int UserGroupId { get; set; }
        public DateTime TimeStamp { get; set; }
        /// <summary>
        /// 1: Add
        /// 2: Edit
        /// 3: Delete
        /// </summary>
        public int Action { get; set; }
    }
}
