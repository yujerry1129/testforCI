﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("VersionFile")]
    public class VersionFile
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Version Number
        /// </summary>
        [MaxLength(50)]
        public string Version { get; set; }

        public DateTime CreateDate { get; set; }

        public long UpdateFileSize { get; set; }

        public string UpdateFileMD5Hash { get; set; }

        /// <summary>
        /// 版本異動項目
        /// </summary>
        public string ChangeLog { get; set; }
    }
}
