﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    public class CampaignToPlaylist
    {
        [Key]
        public int Id { get; set; }

        public int UserGroup_Id { get; set; }

        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }

        public int Playlist_Id { get; set; }

        [ForeignKey("Playlist_Id")]
        public virtual Playlist Playlist { get; set; }

        public int Campaign_Id { get; set; }

        [ForeignKey("Campaign_Id")]
        public virtual Campaign Campaign { get; set; }

        public int PlayOrder { get; set; }
    }
}
