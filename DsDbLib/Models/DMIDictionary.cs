﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("DMIDictionary")]
    public class DMIDictionary
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string DMIKey { get; set; }

        [MaxLength(100)]
        public string DMIValue { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByAdminUserId")]
        public virtual AdminUser CreateBy { get; set; }
        public int CreateByAdminUserId { get; set; }

        [ForeignKey("UpdateByAdminUserId")]
        public virtual AdminUser UpdateBy { get; set; }
        public int? UpdateByAdminUserId { get; set; }
        
        [JsonIgnore]
        [ForeignKey("ProductRuleId")]
        public virtual ProductRule ProductRule { get; set; }
        public int ProductRuleId { get; set; }
    }
}
