﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("ScreenGroup")]
    public class ScreenGroup
    {
        [Key]
        public long Id { get; set; }

        [Index("IX_ScreenGroupName_UserGroup_Unique", 1, IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// 總寬度
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 總高度
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 作為校時基準的裝置ID
        /// </summary>
        public int TimeServerId { get; set; }
        //[JsonIgnore]
        //[ForeignKey("TimeServerId")]
        //public virtual Device TimeServer { get; set; }

        /// <summary>
        /// 水平螢幕個數
        /// </summary>
        public int HorizontallyScreenCount { get; set; }
        /// <summary>
        /// 垂直螢幕個數
        /// </summary>
        public int VerticallyScreenCount { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [Index("IX_ScreenGroupName_UserGroup_Unique", 2, IsUnique = true)]
        public int UserGroup_Id { get; set; }
        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }

        /// <summary>
        /// timeServer 判定幾台裝置不需等就可以開始執行 (預設 0)
        /// </summary>
        public int DisconnectCount { get; set; }

    }
}
