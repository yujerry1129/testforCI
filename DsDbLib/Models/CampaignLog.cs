﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("CampaignLog")]
    public class CampaignLog
    {

        [Key]
        public long Id { get; set; }
        public int CampaignId { get; set; }
        public String CampaignName { get; set; }
        public int CampaignDuration { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Regions { get; set; }
        public int UserId { get; set; }
        public int UserGroupId { get; set; }
        public DateTime TimeStamp { get; set; }
        /// <summary>
        /// 1: Add
        /// 2: Edit
        /// 3: Delete
        /// </summary>
        public int Action { get; set; }
    }
}
