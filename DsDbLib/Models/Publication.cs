﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("Publication")]
    public class Publication
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        #region Basic Informations
        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int UserGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        #endregion
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int RestrictionId { get; set; }
        [JsonIgnore]
        [ForeignKey("RestrictionId")]
        public virtual Restriction Restriction { get; set; }

        [InverseProperty("Publication")]
        public virtual IEnumerable<PublishDevice> Devices { get; set; }

        [InverseProperty("Publication")]
        public virtual IEnumerable<PublishDeviceGroup> DeviceGroups { get; set; }

        [InverseProperty("Publication")]
        public virtual IEnumerable<PublishCampaign> Campaigns { get; set; }

        [InverseProperty("Publication")]
        public virtual IEnumerable<PublishPlaylist> Playlists { get; set; }

        [InverseProperty("Publication")]
        public virtual IEnumerable<CampaignToDeviceScreen> CampaignToDeviceScreens { get; set; }
    }
}
