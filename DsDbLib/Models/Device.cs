﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("Device")]
    public class Device
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string DomainName { get; set; }

        [MaxLength(100)]
        public string MachineName { get; set; }

        [MaxLength(200)]
        public string IpAddressList { get; set; }

        [Index("IX_DeviceName_UserGroup_Unique", 1, IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        public DateTime? LastConnectionTime { get; set; }

        public string MacAddress { get; set; }

        public long MainDiskCapacity { get; set; }

        public long MainDiskUsage { get; set; }

        public TimeSpan? DailyShutDownTime { get; set; }

        [InverseProperty("Device")]
        public ICollection<DeviceScreen> Screens { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public int? LocationId { get; set; }

        [Index]
        [Index("IX_DeviceName_UserGroup_Unique", 2, IsUnique = true)]
        public int UserGroup_Id { get; set; }

        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }

        public DateTime? CreateDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int? CreateByUserId { get; set; }

        public DateTime? UpdateDate { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }

        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceGroupId")]
        public virtual DeviceGroup DeviceGroup { get; set; }
        public int? DeviceGroupId { get; set; }

        public bool FaceDetectionReportEnabled { get; set; }

        /// <summary>
        /// Null: 臉部辨識關閉
        /// 0: Single
        /// 1: Multiple
        /// </summary>
        public int? FaceDetectionType { get; set; }

        [MaxLength(50)]
        public string OperationSystem { get; set; }

        /// <summary>
        /// 0: 沒內容可派送
        /// 1: 所有內容皆以派送至客戶端
        /// 2: 有新內容可以派送，但尚未開始派送
        /// 3: 內容派送中
        /// 4: 派送失敗
        /// </summary>
        public int DispatchStatus { get; set; }

        /// <summary>
        /// 0: 無修改
        /// 1: 修改過      
        /// </summary>
        public int ContentStatus { get; set; }

        [ForeignKey("DefaultScreenFileId")]
        public virtual FileUpload DefaultScreenFile { get; set; }
        public int? DefaultScreenFileId { get; set; }

        //

        [InverseProperty("Device")]
        [JsonIgnore]
        public virtual ICollection<CampaignToDeviceScreen> PublishPlans { get; set; }

        public Device()
        {
            MainDiskCapacity = 0;
            MainDiskUsage = 0;
            FaceDetectionReportEnabled = false;
            FaceDetectionType = null;
            DispatchStatus = 0;
        }

        /// <summary>
        /// 解析度寬度
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 解析度高度
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Screen Group
        /// </summary>
        [JsonIgnore]
        [ForeignKey("ScreenGroupId")]
        public virtual ScreenGroup ScreenGroup { get; set; }
        public long? ScreenGroupId { get; set; }

        /// <summary>
        /// X軸位置索引
        /// </summary>

        public int? OffsetX { get; set; }

        /// <summary>
        /// Y軸位置索引
        /// </summary>
        public int? OffsetY { get; set; }



        [JsonIgnore]
        [ForeignKey("VersionId")]
        public virtual VersionFile Version { get; set; }
        public int VersionId { get; set; }

        public bool AutoUpdateEnabled { get; set; }
        public bool IsNeedUpdate { get; set; }

        /// <summary>
        /// device config 資料改變
        /// </summary>
        public bool ConfigStatus { get; set; }

        public Guid DeviceToken { get; set; }

        public TimeSpan? AutoUpdateTime { get; set; }

        /// <summary>
        /// player 使用的 版本預計更新時間
        /// </summary>
        public TimeSpan? VersionUpdateTime { get; set; }

        public bool IsNeedRestart { get; set; }
    }
}
