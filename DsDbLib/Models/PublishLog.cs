﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("PublishLog")]

    public class PublishLog
    {
        [Key]
        public long Id { get; set; }

        public long ScheduleSummaryId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string SelectType { get; set; }
        public string Campaigns { get; set; }
        public string Screens { get; set; }
        public string FaceRestriction { get; set; }
        public string TimeRestriction { get; set; }
        public int UserId { get; set; }

        public int UserGroupId { get; set; }

        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// 1: Add
        /// 2: Edit
        /// 3: Delete
        /// </summary>
        public int Action { get; set; }
    }
}
