﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("Restriction")]
    public class Restriction
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int UserGroupId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        public bool HasCalenderRule { get; set; }
        public bool HasFaceDetectionRule { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? RepeatMonday { get; set; }
        public bool? RepeatTuesday { get; set; }
        public bool? RepeatWednesday { get; set; }
        public bool? RepeatThurday { get; set; }
        public bool? RepeatFriday { get; set; }
        public bool? RepeatSaturday { get; set; }
        public bool? RepeatSunday { get; set; }
        public TimeSpan? DailyStartTime { get; set; }
        public TimeSpan? DailyEndTime { get; set; }
        public bool? IsCrossDay { get; set; }

        /// <summary>
        /// 性別
        /// 0: 不限
        /// 1: 男性
        /// 2: 女性
        /// </summary>
        public int? Gender { get; set; }
        
        [InverseProperty("AgeToRestriction")]
        public virtual IEnumerable<AgeToRestriction> AgeToRestrictions { get; set; }

        //[ForeignKey("AgeSettingId")]
        //public virtual AgeSetting Age { get; set; }
        //public int? AgeSettingId { get; set; }

    }
}
