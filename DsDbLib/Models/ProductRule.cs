﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("ProductRule")]
    public class ProductRule
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByAdminUserId")]
        public virtual AdminUser CreateBy { get; set; }
        public int CreateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByAdminUserId")]
        public virtual AdminUser UpdateBy { get; set; }
        public int? UpdateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeleteByAdminUserId")]
        public virtual AdminUser DeleteBy { get; set; }
        public int? DeleteByAdminUserId { get; set; }

        
        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int UserGroupId { get; set; }

        [InverseProperty("ProductRule")]
        [JsonIgnore]
        public virtual ICollection<DMIDictionary> DMIDictionaries { get; set; }
    }
}
