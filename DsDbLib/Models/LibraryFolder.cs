﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("LibraryFolder")]
    public class LibraryFolder
    {
        [Key]
        public int Id { get; set; }

        [InverseProperty("Folder")]
        public virtual ICollection<FileUpload> Files { get; set; }

        [Index]
        [Index("IX_FlolderName_UserGroup_Unique", 2, IsUnique = true)]
        public int UserGroup_Id { get; set; }

        [ForeignKey("UserGroup_Id")]
        public virtual UserGroup UserGroup { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [MaxLength(100)]
        [Index("IX_FlolderName_UserGroup_Unique", 1, IsUnique = true)]
        public string Name { get; set; }

        public int? ParentFolderId { get; set; }
    }
}
