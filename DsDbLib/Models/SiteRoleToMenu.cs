﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DsDbLib.Models
{
    [Table("SiteRoleToMenu")]
    public class SiteRoleToMenu
    {
        [Key]
        public int Id { get; set; }

        public int SiteRole_Id { get; set; }

        [JsonIgnore]
        [ForeignKey("SiteRole_Id")]
        public virtual SiteRole Role { get; set; }


        public int? SiteMenu_Id { get; set; }

        [JsonIgnore]
        [ForeignKey("SiteMenu_Id")]
        public virtual SiteMenu Menu { get; set; }
    }
}
