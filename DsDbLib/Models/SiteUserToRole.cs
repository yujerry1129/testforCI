﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("SiteUserToRole")]
    public class SiteUserToRole
    {
        [Key]
        public int Id { get; set; }
        public int User_Id { get; set; }

        [JsonIgnore]
        [ForeignKey("User_Id")]
        public virtual User User { get; set; }

        public int? Role_Id { get; set; }

        [JsonIgnore]
        [ForeignKey("Role_Id")]
        public virtual SiteRole SiteRole { get; set; }
    }
}
