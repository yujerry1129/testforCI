﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("PlayerInformationLog")]
    public class PlayerInformationLog
    {
        [Key]
        public long Id { get; set; }

        [Index]
        public int UserGroupId { get; set; }

        public int DeviceId { get; set; }
        public int LogType { get; set; }
        public string LogMessage { get; set; }
        public DateTime LogTime { get; set; }
    }
}
