﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("CampaignTag")]

    public class CampaignTag
    {
        [Key]
        public long Id { get; set; }
        public int CampaignId { get; set; }
        [MaxLength(100)]
        public String TagName { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
