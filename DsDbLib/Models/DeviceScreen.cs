﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("DeviceScreen")]
    public class DeviceScreen
    {
        [Key]
        public int Id { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        
        public bool IsPrimary { get; set; }

        public int ScreenIndex { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }
        public int DeviceId { get; set; }

        public DeviceScreen()
        {
            IsPrimary = true;
            ScreenIndex = 0;
        }
    }
}
