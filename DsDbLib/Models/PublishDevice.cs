﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("PublishDevice")]
    public class PublishDevice
    {
        [Key]
        public int Id { get; set; }

        public int PublicationId { get; set; }
        [JsonIgnore]
        [ForeignKey("PublicationId")]
        public virtual Publication Publication { get; set; }

        public int DeviceId { get; set; }
        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }
    }
}
