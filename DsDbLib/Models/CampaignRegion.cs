﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("CampaignRegion")]
    public class CampaignRegion
    {
        [Key]
        public int Id { get; set; }

        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int LayerOrder { get; set; }

        [Required]
        [JsonIgnore]
        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }
        public int CampaignId { get; set; }

        [InverseProperty("Region")]
        [JsonIgnore]
        public virtual ICollection<RegionFrame> Frames { get; set; }
    }
}
