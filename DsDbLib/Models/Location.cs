﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("Location")]
    public class Location
    {
        [Key]
        public int Id { get; set; }

        [JsonIgnore]
        [InverseProperty("Location")]
        public ICollection<Device> Devices { get; set; }

        [MaxLength(200)]
        [Index("IX_LocationName_UserGroup_Unique", 1, IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }
        public string Address { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        [Index]
        [Index("IX_LocationName_UserGroup_Unique", 2, IsUnique = true)]
        public int UserGroupId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }
    }
}
