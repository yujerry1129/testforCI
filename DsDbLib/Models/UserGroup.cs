﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("UserGroup")]
    public class UserGroup
    {

        [Key]
        public int Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }
        public int DeviceLimit { get; set; }
        public long StorageLimit { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(100)]
        public string SystemKey { get; set; }
        public bool EnableFaceDetection { get; set; }
        public int? WatchDogShutDownTime { get; set; }
        public bool EnableWatchDogLog { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByAdminUserId")]
        public virtual AdminUser CreateBy { get; set; }
        public int CreateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByAdminUserId")]
        public virtual AdminUser UpdateBy { get; set; }
        public int? UpdateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeleteByAdminUserId")]
        public virtual AdminUser DeleteBy { get; set; }
        public int? DeleteByAdminUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        /// <summary>
        /// 客戶契約起始日
        /// </summary>
        public DateTime? EffectiveStartDate { get; set; }

        /// <summary>
        /// 客戶契約結束日
        /// </summary>
        public DateTime? EffectiveEndDate { get; set; }


        //[InverseProperty("UserGroup")]
        //[JsonIgnore]
        //public virtual ICollection<EmailService> EmailServer { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<ProductRule> ProductRules { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<Device> Devices { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<DeviceGroup> DeviceGroups { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<FileUpload> UploadFiles { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<LibraryFolder> LibraryFolders { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<Location> Locations { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<LayoutTemplate> LayoutTemplates { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<ResolutionConfig> ResolutionConfigs { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<CampaignToDeviceScreen> PublishPlans { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<Playlist> Playlists { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<CampaignToPlaylist> CampaignsToPlaylists { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<DistanceRangeDefinition> DistanceRangeDefinitions { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<LooktimeRangeDefinition> LooktimeRangeDefinitions { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<AgeRangeDefinition> AgeRangeDefinitions { get; set; }

        [InverseProperty("UserGroup")]
        [JsonIgnore]
        public virtual ICollection<User> User { get; set; }
        

        public UserGroup()
        {
            //預設關閉臉部辨識及watch dog
            EnableFaceDetection = false;
            EnableWatchDogLog = false;
        }
    }
}
