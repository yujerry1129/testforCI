﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("AgeToRestriction")]
    public class AgeToRestriction
    {
        [Key]
        public int Id { get; set; }

        public int Age_Id { get; set; }

        [ForeignKey("Age_Id")]
        [JsonIgnore]
        public virtual AgeRangeDefinition Age { get; set; }
        public int Restriction_id { get; set; }

        [ForeignKey("Restriction_id")]
        [JsonIgnore]
        public virtual Restriction Restriction { get; set; }
    }
}
