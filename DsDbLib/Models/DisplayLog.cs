﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("DisplayLog")]
    public class DisplayLog
    {
        [Key]
        public long Id { get; set; }

        //public int DeviceScreenId { get; set; }
        public int DeviceId { get; set; }

        [Index]
        public int UserGroupId { get; set; }
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }

        /// <summary>
        /// 0: Defalut screen
        /// 1: General
        /// 2: Face Detect
        /// 3: Spot       
        /// </summary>
        public int DisplayType { get; set; }

        /// <summary>
        /// 0: error
        /// 1: image
        /// 2: video
        /// 3: audio
        /// 4: text
        /// 5: web page
        /// 6: marquee
        /// 7: rss
        /// 8: youtube
        /// 9: flash
        /// </summary>
        public int FileType { get; set; }
        public int? FileId { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }

        [Index]
        public DateTime LogTime { get; set; }

        /// <summary>
        /// 開始撥放時間
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 結束撥放時間
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 0: Default screen
        /// 1: Face detection triggered
        /// </summary>
        //public int DisplayReason { get; set; }
    }
}
