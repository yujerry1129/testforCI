﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("FaceDetectionLog")]
    public class FaceDetectionLog
    {
        [Key]
        public long Id { get; set; }

        [Index]
        public int UserGroupId { get; set; }

        [Index]
        public DateTime LogTime { get; set; }

        public int DeviceId { get; set; }
        public int? LocationId { get; set; }
        public int AgeId { get; set; }
        public int? AgeBottom { get; set; }
        public int? AgeTop { get; set; }
        public int Gender { get; set; }
        public int LookTime { get; set; }
        public int Distance { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }

    }
}
