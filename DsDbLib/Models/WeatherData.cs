﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    public class WeatherData
    {
        [Key]
        public long Id { get; set; }
        public int LocationId { get; set; }
        public int DataUnixTime { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public float Temp { get; set; }
        public float TempMax { get; set; }
        public float TempMin { get; set; }
        public int Humidity { get; set; }
        public float Pressure { get; set; }
        public float? WindDeg { get; set; }
        public float WindSpeed { get; set; }
        public int Clouds { get; set; }
        public int SunriseUnixTime { get; set; }
        public int SunsetUnixTime { get; set; }
    }
}
