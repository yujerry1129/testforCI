﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DsDbLib.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        [MaxLength(100)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }
        public int UserGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByAdminUserId")]
        public virtual AdminUser CreateBy { get; set; }
        public int CreateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByAdminUserId")]
        public virtual AdminUser UpdateBy { get; set; }
        public int? UpdateByAdminUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeleteByAdminUserId")]
        public virtual AdminUser DeleteBy { get; set; }
        public int? DeleteByAdminUserId { get; set; }

        [MaxLength(10)]
        public string TempPassword { get; set; }
        public DateTime? TempPasswordExpiredTime { get; set; }

        [JsonIgnore]
        [InverseProperty("User")]
        public virtual IEquatable<SiteUserToRole> Roles { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User UserCreateBy { get; set; }
        public int? CreateByUserId { get; set; }
        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User UserDeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UserUpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }

    }
}
