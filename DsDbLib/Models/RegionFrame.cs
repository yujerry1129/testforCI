﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("RegionFrame")]
    public class RegionFrame
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("DeleteByUserId")]
        public virtual User DeleteBy { get; set; }
        public int? DeleteByUserId { get; set; }

        [ForeignKey("RegionId")]
        public virtual CampaignRegion Region { get; set; }
        public int RegionId { get; set; }

        public int Offset { get; set; }
        public int Length { get; set; }
        public int Order { get; set; }

        /// <summary>
        /// 0:error
        /// 1:image
        /// 2:video
        /// 3:audio
        /// 4:text
        /// 5:web page
        /// 6:marquee
        /// 7:rss
        /// 8:youtube
        /// 9:flash
        /// </summary>
        public int FrameType { get; set; }

        [Index(IsUnique = true)]
        public Guid DataId { get; set; }

        [ForeignKey("FileId")]
        public virtual FileUpload MediaFile { get; set; }
        public int? FileId { get; set; }

        public string ThumbnameUrl { get; set; }
        public string ContentUrl { get; set; }
        public string ContentMeta { get; set; }
        public string Content { get; set; }

        public int? TextAssetId { get; set; }

        [JsonIgnore]
        [ForeignKey("TextAssetId")]
        public virtual TextAsset TextAsset { get; set; }

    }
}
