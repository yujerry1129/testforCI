﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("AdminUser")]
    public class AdminUser
    {
        [Key]
        public int Id { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(200)]
        public string LoginAccount { get; set; }
        public string LoginPassword { get; set; }

        [MaxLength(200)]
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }

        [MaxLength(200)]
        public string MisRemark { get; set; }
        public bool IsEnabled { get; set; }

        public AdminUser()
        {
            IsEnabled = true;
        }
    }
}
