﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Models
{
    [Table("ScheduleSummary")]
    public class ScheduleSummary
    {
        [Key]
        public long Id { get; set; }

        public int UserGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("UserGroupId")]
        public virtual UserGroup UserGroup { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        [JsonIgnore]
        [ForeignKey("CreateByUserId")]
        public virtual User CreateBy { get; set; }
        public int CreateByUserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UpdateByUserId")]
        public virtual User UpdateBy { get; set; }
        public int? UpdateByUserId { get; set; }


        [JsonIgnore]
        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }
        public int? CampaignId { get; set; }

        [JsonIgnore]
        [ForeignKey("PlaylistId")]
        public virtual Playlist Playlist { get; set; }
        public int? PlaylistId { get; set; }

        public int PlayOrder { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }
        public int? DeviceId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceGroupId")]
        public virtual DeviceGroup DeviceGroup { get; set; }
        public int? DeviceGroupId { get; set; }

        [JsonIgnore]
        [ForeignKey("ScreenGroupId")]
        public virtual ScreenGroup ScreenGroup { get; set; }
        public long? ScreenGroupId { get; set; }


        public int? PublicationId { get; set; }
        [JsonIgnore]
        [ForeignKey("PublicationId")]
        public virtual Publication Publication { get; set; }

        public bool HasCalenderRule { get; set; }
        public bool HasFaceDetectionRule { get; set; }
        public bool Spot { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? RepeatMonday { get; set; }
        public bool? RepeatTuesday { get; set; }
        public bool? RepeatWednesday { get; set; }
        public bool? RepeatThurday { get; set; }
        public bool? RepeatFriday { get; set; }
        public bool? RepeatSaturday { get; set; }
        public bool? RepeatSunday { get; set; }

        public TimeSpan? DailyStartTime { get; set; }
        public TimeSpan? DailyEndTime { get; set; }

        [MaxLength(200)]
        public string AgeRestriction { get; set; }
        public bool PlayAllDay { get; set; }

        public int? Gender { get; set; }

        public bool HasMonthCalenderRule { get; set; }
        public int MonthDay { get; set; }
        public bool Event { get; set; }

    }
}
