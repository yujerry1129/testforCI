﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DsDbLib.Models
{
    [Table("FileDownloadLog")]
    public class FileDownloadLog
    {
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// player 以此作搜尋識別
        /// </summary>
        public Guid DownLoadToken { get; set; }
        public int DeviceId { get; set; }
        [Index]
        public int UserGroupId { get; set; }

        public int FileId { get; set; }
        /// <summary>
        /// 0: nothing
        /// 1: image
        /// 2: video
        /// 3: audio
        /// 9: flash
        /// 10:Weaher
        /// 11:clock
        /// 12:Menu
        /// 13:NullImage
        /// 14:player msi
        /// </summary>
        public int FileType { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public long TransferedFileSize { get; set; }
        public DateTime StartDownloadTime { get; set; }
        public DateTime? EndDownloadTime { get; set; }
        public bool DownloadSuccess { get; set; }
        /// <summary>
        /// 下載百分比
        /// </summary>
        public int DownLoadPercentage { get; set; }
        /// <summary>
        /// 0: Browser
        /// 1: Player
        /// 2: Direct API call
        /// </summary>
        public int DownloadType { get; set; }
    }
}
