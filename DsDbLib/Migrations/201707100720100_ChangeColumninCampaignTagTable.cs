namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeColumninCampaignTagTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignTag", "CampaignId", c => c.Int(nullable: false));
            DropColumn("dbo.CampaignTag", "DeviceId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CampaignTag", "DeviceId", c => c.Int(nullable: false));
            DropColumn("dbo.CampaignTag", "CampaignId");
        }
    }
}
