namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddNullableVersionIdinDeviceTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "VersionId", c => c.Int());
            AddColumn("dbo.Device", "AutoUpdateEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Device", "IsNeedUpdate", c => c.Boolean(nullable: false));
            // ADD THIS BY HAND
            Sql(@"UPDATE dbo.Device SET VersionId = 1
              where VersionId IS NULL");
        }

        public override void Down()
        {
            DropColumn("dbo.Device", "IsNeedUpdate");
            DropColumn("dbo.Device", "AutoUpdateEnabled");
            DropColumn("dbo.Device", "VersionId");
        }
    }
}
