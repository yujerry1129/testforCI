namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFileTypeinDisplayLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DisplayLog", "FileType", c => c.Int(nullable: false));
            DropColumn("dbo.DisplayLog", "DeviceScreenId");
            DropColumn("dbo.DisplayLog", "DisplayReason");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DisplayLog", "DisplayReason", c => c.Int(nullable: false));
            AddColumn("dbo.DisplayLog", "DeviceScreenId", c => c.Int(nullable: false));
            DropColumn("dbo.DisplayLog", "FileType");
        }
    }
}
