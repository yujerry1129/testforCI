namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnAgeIdinAgeRangeDefinition : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AgeRangeDefinition", "AgeId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AgeRangeDefinition", "AgeId");
        }
    }
}
