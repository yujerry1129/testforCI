namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColAgeRestrictionColPlayAllDay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduleSummary", "AgeRestriction", c => c.String(maxLength: 200));
            AddColumn("dbo.ScheduleSummary", "PlayAllDay", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduleSummary", "PlayAllDay");
            DropColumn("dbo.ScheduleSummary", "AgeRestriction");
        }
    }
}
