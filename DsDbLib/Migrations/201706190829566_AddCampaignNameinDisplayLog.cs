namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCampaignNameinDisplayLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DisplayLog", "CampaignName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DisplayLog", "CampaignName");
        }
    }
}
