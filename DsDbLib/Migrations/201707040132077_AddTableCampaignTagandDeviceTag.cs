namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableCampaignTagandDeviceTag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampaignTag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagName = c.String(maxLength: 100),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeviceTag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagName = c.String(maxLength: 100),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DeviceTag");
            DropTable("dbo.CampaignTag");
        }
    }
}
