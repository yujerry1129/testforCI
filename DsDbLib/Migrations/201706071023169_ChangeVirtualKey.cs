namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeVirtualKey : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.CampaignRegion", name: "Campaign_Id", newName: "CampaignId");
            RenameColumn(table: "dbo.Device", name: "Location_Id", newName: "LocationId");
            RenameIndex(table: "dbo.Device", name: "IX_Location_Id", newName: "IX_LocationId");
            RenameIndex(table: "dbo.CampaignRegion", name: "IX_Campaign_Id", newName: "IX_CampaignId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.CampaignRegion", name: "IX_CampaignId", newName: "IX_Campaign_Id");
            RenameIndex(table: "dbo.Device", name: "IX_LocationId", newName: "IX_Location_Id");
            RenameColumn(table: "dbo.Device", name: "LocationId", newName: "Location_Id");
            RenameColumn(table: "dbo.CampaignRegion", name: "CampaignId", newName: "Campaign_Id");
        }
    }
}
