namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddVersionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VersionFile",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Version = c.String(maxLength: 50),
                    CreateDate = c.DateTime(nullable: false),
                    UpdateFileSize = c.Long(nullable: false),
                    UpdateFileMD5Hash = c.String(),
                    ChangeLog = c.String(),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.DeviceTag", "ScreenGroupId", c => c.Long());
            AlterColumn("dbo.DeviceTag", "DeviceId", c => c.Int());
            // ADD THIS BY HAND
            Sql(@"Insert into dbo.VersionFile(Version, CreateDate,UpdateFileSize,ChangeLog) VALUES ('1.0.0.0', convert(datetime,'2017-07-07 12:00'),1,'first version')");
        }

        public override void Down()
        {
            AlterColumn("dbo.DeviceTag", "DeviceId", c => c.Int(nullable: false));
            DropColumn("dbo.DeviceTag", "ScreenGroupId");
            DropTable("dbo.VersionFile");
        }
    }
}
