namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableScheduleSummary : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScheduleSummary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        CampaignId = c.Int(),
                        PlaylistId = c.Int(),
                        PlayOrder = c.Int(nullable: false),
                        DeviceId = c.Int(),
                        DeviceGroupId = c.Int(),
                        PublicationId = c.Int(),
                        HasCalenderRule = c.Boolean(nullable: false),
                        HasFaceDetectionRule = c.Boolean(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        RepeatMonday = c.Boolean(),
                        RepeatTuesday = c.Boolean(),
                        RepeatWednesday = c.Boolean(),
                        RepeatThurday = c.Boolean(),
                        RepeatFriday = c.Boolean(),
                        RepeatSaturday = c.Boolean(),
                        RepeatSunday = c.Boolean(),
                        DailyStartTime = c.Time(precision: 7),
                        DailyEndTime = c.Time(precision: 7),
                        Gender = c.Int(),
                        HasMonthCalenderRule = c.Boolean(nullable: false),
                        MonthDay = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaign", t => t.CampaignId)
                .ForeignKey("dbo.User", t => t.CreateByUserId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.Device", t => t.DeviceId)
                .ForeignKey("dbo.DeviceGroup", t => t.DeviceGroupId)
                .ForeignKey("dbo.Playlist", t => t.PlaylistId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId, cascadeDelete: true)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.CampaignId)
                .Index(t => t.PlaylistId)
                .Index(t => t.DeviceId)
                .Index(t => t.DeviceGroupId)
                .Index(t => t.PublicationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleSummary", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.ScheduleSummary", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.ScheduleSummary", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.ScheduleSummary", "PlaylistId", "dbo.Playlist");
            DropForeignKey("dbo.ScheduleSummary", "DeviceGroupId", "dbo.DeviceGroup");
            DropForeignKey("dbo.ScheduleSummary", "DeviceId", "dbo.Device");
            DropForeignKey("dbo.ScheduleSummary", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.ScheduleSummary", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.ScheduleSummary", "CampaignId", "dbo.Campaign");
            DropIndex("dbo.ScheduleSummary", new[] { "PublicationId" });
            DropIndex("dbo.ScheduleSummary", new[] { "DeviceGroupId" });
            DropIndex("dbo.ScheduleSummary", new[] { "DeviceId" });
            DropIndex("dbo.ScheduleSummary", new[] { "PlaylistId" });
            DropIndex("dbo.ScheduleSummary", new[] { "CampaignId" });
            DropIndex("dbo.ScheduleSummary", new[] { "DeleteByUserId" });
            DropIndex("dbo.ScheduleSummary", new[] { "UpdateByUserId" });
            DropIndex("dbo.ScheduleSummary", new[] { "CreateByUserId" });
            DropIndex("dbo.ScheduleSummary", new[] { "UserGroupId" });
            DropTable("dbo.ScheduleSummary");
        }
    }
}
