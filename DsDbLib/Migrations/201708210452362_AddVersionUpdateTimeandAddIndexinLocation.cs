namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVersionUpdateTimeandAddIndexinLocation : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Location", new[] { "Name" });
            DropIndex("dbo.Location", new[] { "UserGroupId" });
            AddColumn("dbo.Device", "AutoUpdateTime", c => c.Time(precision: 7));
            AddColumn("dbo.Device", "VersionUpdateTime", c => c.Time(precision: 7));
            CreateIndex("dbo.Location", new[] { "Name", "UserGroupId" }, unique: true, name: "IX_LocationName_UserGroup_Unique");
            CreateIndex("dbo.Location", "UserGroupId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Location", new[] { "UserGroupId" });
            DropIndex("dbo.Location", "IX_LocationName_UserGroup_Unique");
            DropColumn("dbo.Device", "VersionUpdateTime");
            DropColumn("dbo.Device", "AutoUpdateTime");
            CreateIndex("dbo.Location", "UserGroupId");
            CreateIndex("dbo.Location", "Name", unique: true);
        }
    }
}
