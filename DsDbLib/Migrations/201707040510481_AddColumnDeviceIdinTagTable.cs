namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnDeviceIdinTagTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampaignTag", "DeviceId", c => c.Int(nullable: false));
            AddColumn("dbo.DeviceTag", "DeviceId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeviceTag", "DeviceId");
            DropColumn("dbo.CampaignTag", "DeviceId");
        }
    }
}
