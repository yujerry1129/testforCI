// <auto-generated />
namespace DsDbLib.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddColumnAgeIdinAgeRangeDefinition : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddColumnAgeIdinAgeRangeDefinition));
        
        string IMigrationMetadata.Id
        {
            get { return "201708070826517_AddColumnAgeIdinAgeRangeDefinition"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
