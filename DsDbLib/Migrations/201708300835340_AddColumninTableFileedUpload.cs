namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumninTableFileedUpload : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileUpload", "Content", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileUpload", "Content");
        }
    }
}
