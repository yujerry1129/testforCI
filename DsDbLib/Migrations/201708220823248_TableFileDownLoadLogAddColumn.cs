namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableFileDownLoadLogAddColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDownloadLog", "DownLoadToken", c => c.Guid(nullable: false));
            AddColumn("dbo.FileDownloadLog", "DeviceId", c => c.Int(nullable: false));
            AddColumn("dbo.FileDownloadLog", "FileType", c => c.String());
            AddColumn("dbo.FileDownloadLog", "DownLoadPercentage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileDownloadLog", "DownLoadPercentage");
            DropColumn("dbo.FileDownloadLog", "FileType");
            DropColumn("dbo.FileDownloadLog", "DeviceId");
            DropColumn("dbo.FileDownloadLog", "DownLoadToken");
        }
    }
}
