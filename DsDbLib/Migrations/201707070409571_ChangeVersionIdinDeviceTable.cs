namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeVersionIdinDeviceTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Device", "VersionId", c => c.Int(nullable: false));
            CreateIndex("dbo.Device", "VersionId");
            AddForeignKey("dbo.Device", "VersionId", "dbo.VersionFile", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Device", "VersionId", "dbo.VersionFile");
            DropIndex("dbo.Device", new[] { "VersionId" });
            AlterColumn("dbo.Device", "VersionId", c => c.Int());
        }
    }
}
