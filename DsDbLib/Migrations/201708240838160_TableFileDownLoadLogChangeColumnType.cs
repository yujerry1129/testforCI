namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class TableFileDownLoadLogChangeColumnType : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [dbo].[FileDownloadLog] SET FileType = 0");
            AlterColumn("dbo.FileDownloadLog", "FileType", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            AlterColumn("dbo.FileDownloadLog", "FileType", c => c.String());
        }
    }
}
