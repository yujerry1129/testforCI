namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnConfigStatusinTableDevice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "ConfigStatus", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Device", "ConfigStatus");
        }
    }
}
