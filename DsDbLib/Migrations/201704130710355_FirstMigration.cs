namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginAccount = c.String(maxLength: 200),
                        LoginPassword = c.String(),
                        Email = c.String(maxLength: 200),
                        CreateDate = c.DateTime(nullable: false),
                        MisRemark = c.String(maxLength: 200),
                        IsEnabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.LoginAccount, unique: true);
            
            CreateTable(
                "dbo.AgeRangeDefinition",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        MinValue = c.Int(nullable: false),
                        MaxValue = c.Int(nullable: false),
                        UserGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.UserGroup_Id);
            
            CreateTable(
                "dbo.UserGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                        DeviceLimit = c.Int(nullable: false),
                        StorageLimit = c.Long(nullable: false),
                        SystemKey = c.String(maxLength: 100),
                        EnableFaceDetection = c.Boolean(nullable: false),
                        WatchDogShutDownTime = c.Int(),
                        EnableWatchDogLog = c.Boolean(nullable: false),
                        CreateByAdminUserId = c.Int(nullable: false),
                        UpdateByAdminUserId = c.Int(),
                        DeleteByAdminUserId = c.Int(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        EffectiveStartDate = c.DateTime(),
                        EffectiveEndDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.CreateByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.DeleteByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.UpdateByAdminUserId)
                .Index(t => t.SystemKey, unique: true)
                .Index(t => t.CreateByAdminUserId)
                .Index(t => t.UpdateByAdminUserId)
                .Index(t => t.DeleteByAdminUserId);
            
            CreateTable(
                "dbo.Campaign",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Duration = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Name = c.String(maxLength: 200),
                        UserGroup_Id = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_CampaignName_UserGroup_Unique")
                .Index(t => t.UserGroup_Id)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                        Email = c.String(maxLength: 100),
                        HashedPassword = c.String(),
                        LastLoginTime = c.DateTime(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        UserGroupId = c.Int(nullable: false),
                        CreateByAdminUserId = c.Int(nullable: false),
                        UpdateByAdminUserId = c.Int(),
                        DeleteByAdminUserId = c.Int(),
                        TempPassword = c.String(maxLength: 10),
                        TempPasswordExpiredTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.CreateByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.DeleteByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.UpdateByAdminUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.Email, unique: true)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByAdminUserId)
                .Index(t => t.UpdateByAdminUserId)
                .Index(t => t.DeleteByAdminUserId);
            
            CreateTable(
                "dbo.CampaignToDeviceScreen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        CampaignId = c.Int(),
                        PlaylistId = c.Int(),
                        PlayOrder = c.Int(nullable: false),
                        DeviceId = c.Int(),
                        DeviceGroupId = c.Int(),
                        RestrictionId = c.Int(),
                        PublicationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.DeviceGroup", t => t.DeviceGroupId)
                .ForeignKey("dbo.Device", t => t.DeviceId)
                .ForeignKey("dbo.Playlist", t => t.PlaylistId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .ForeignKey("dbo.Restriction", t => t.RestrictionId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.Campaign", t => t.CampaignId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.CampaignId)
                .Index(t => t.PlaylistId)
                .Index(t => t.DeviceId)
                .Index(t => t.DeviceGroupId)
                .Index(t => t.RestrictionId)
                .Index(t => t.PublicationId);
            
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DomainName = c.String(maxLength: 100),
                        MachineName = c.String(maxLength: 100),
                        IpAddressList = c.String(maxLength: 200),
                        Name = c.String(maxLength: 100),
                        LastConnectionTime = c.DateTime(),
                        MacAddress = c.String(),
                        MainDiskCapacity = c.Long(nullable: false),
                        MainDiskUsage = c.Long(nullable: false),
                        DailyShutDownTime = c.Time(precision: 7),
                        UserGroup_Id = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateByUserId = c.Int(),
                        UpdateDate = c.DateTime(),
                        UpdateByUserId = c.Int(),
                        DeleteDate = c.DateTime(),
                        DeleteByUserId = c.Int(),
                        DeviceGroupId = c.Int(),
                        FaceDetectionReportEnabled = c.Boolean(nullable: false),
                        FaceDetectionType = c.Int(),
                        OperationSystem = c.String(maxLength: 50),
                        DispatchStatus = c.Int(nullable: false),
                        DefaultScreenFileId = c.Int(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.FileUpload", t => t.DefaultScreenFileId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.DeviceGroup", t => t.DeviceGroupId)
                .ForeignKey("dbo.Location", t => t.Location_Id)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_DeviceName_UserGroup_Unique")
                .Index(t => t.UserGroup_Id)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.DeviceGroupId)
                .Index(t => t.DefaultScreenFileId)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.FileUpload",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ExtensionName = c.String(maxLength: 10),
                        Size = c.Long(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        MimeType = c.String(maxLength: 100),
                        FileType = c.Int(nullable: false),
                        HashCode = c.Guid(nullable: false),
                        Width = c.Int(),
                        Height = c.Int(),
                        FolderId = c.Int(),
                        UserGroupId = c.Int(nullable: false),
                        VideoDuration = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.LibraryFolder", t => t.FolderId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.FolderId)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.LibraryFolder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroup_Id = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        Name = c.String(maxLength: 100),
                        ParentFolderId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.UserGroup_Id)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_FlolderName_UserGroup_Unique")
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.RegionFrame",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        RegionId = c.Int(nullable: false),
                        Offset = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        FrameType = c.Int(nullable: false),
                        DataId = c.Guid(nullable: false),
                        FileId = c.Int(),
                        ThumbnameUrl = c.String(),
                        ContentUrl = c.String(),
                        ContentMeta = c.String(),
                        Content = c.String(),
                        TextAssetId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.CampaignRegion", t => t.RegionId)
                .ForeignKey("dbo.TextAsset", t => t.TextAssetId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.FileUpload", t => t.FileId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.RegionId)
                .Index(t => t.DataId, unique: true)
                .Index(t => t.FileId)
                .Index(t => t.TextAssetId);
            
            CreateTable(
                "dbo.CampaignRegion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OffsetX = c.Int(nullable: false),
                        OffsetY = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        LayerOrder = c.Int(nullable: false),
                        Campaign_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaign", t => t.Campaign_Id)
                .Index(t => t.Campaign_Id);
            
            CreateTable(
                "dbo.TextAsset",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        AssetType = c.Int(nullable: false),
                        Content = c.String(),
                        UserGroupId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.DeviceGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        Name = c.String(maxLength: 100),
                        PriorityType = c.Int(nullable: false),
                        UserGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_DeviceGroupName_UserGroup_Unique")
                .Index(t => t.UserGroup_Id);
            
            CreateTable(
                "dbo.Location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Description = c.String(),
                        Address = c.String(),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.DeviceScreen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        IsPrimary = c.Boolean(nullable: false),
                        ScreenIndex = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Device", t => t.DeviceId)
                .Index(t => t.DeviceId);
            
            CreateTable(
                "dbo.Playlist",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                        UserGroup_Id = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_Playlist_UserGroup_Unique")
                .Index(t => t.UserGroup_Id)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.Publication",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserGroupId = c.Int(nullable: false),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        RestrictionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.Restriction", t => t.RestrictionId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.RestrictionId);
            
            CreateTable(
                "dbo.Restriction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        HasCalenderRule = c.Boolean(nullable: false),
                        HasFaceDetectionRule = c.Boolean(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        RepeatMonday = c.Boolean(),
                        RepeatTuesday = c.Boolean(),
                        RepeatWednesday = c.Boolean(),
                        RepeatThurday = c.Boolean(),
                        RepeatFriday = c.Boolean(),
                        RepeatSaturday = c.Boolean(),
                        RepeatSunday = c.Boolean(),
                        DailyStartTime = c.Time(precision: 7),
                        DailyEndTime = c.Time(precision: 7),
                        IsCrossDay = c.Boolean(),
                        Gender = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            CreateTable(
                "dbo.CampaignToPlaylists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroup_Id = c.Int(nullable: false),
                        Playlist_Id = c.Int(nullable: false),
                        Campaign_Id = c.Int(nullable: false),
                        PlayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaign", t => t.Campaign_Id)
                .ForeignKey("dbo.Playlist", t => t.Playlist_Id)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.UserGroup_Id)
                .Index(t => t.Playlist_Id)
                .Index(t => t.Campaign_Id);
            
            CreateTable(
                "dbo.DistanceRangeDefinition",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        MinValue = c.Int(nullable: false),
                        MaxValue = c.Int(nullable: false),
                        UserGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.UserGroup_Id);
            
            CreateTable(
                "dbo.LayoutTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        UserGroupId = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.LayoutTemplateRegion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        OffsetX = c.Int(nullable: false),
                        OffsetY = c.Int(nullable: false),
                        LayerOrder = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.LayoutTemplate", t => t.TemplateId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "dbo.LooktimeRangeDefinition",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        MinValue = c.Int(nullable: false),
                        MaxValue = c.Int(nullable: false),
                        UserGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id)
                .Index(t => t.UserGroup_Id);
            
            CreateTable(
                "dbo.ProductRule",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByAdminUserId = c.Int(nullable: false),
                        UpdateByAdminUserId = c.Int(),
                        DeleteByAdminUserId = c.Int(),
                        UserGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.CreateByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.DeleteByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.UpdateByAdminUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.CreateByAdminUserId)
                .Index(t => t.UpdateByAdminUserId)
                .Index(t => t.DeleteByAdminUserId)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.DMIDictionary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DMIKey = c.String(maxLength: 100),
                        DMIValue = c.String(maxLength: 100),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        CreateByAdminUserId = c.Int(nullable: false),
                        UpdateByAdminUserId = c.Int(),
                        ProductRuleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUser", t => t.CreateByAdminUserId)
                .ForeignKey("dbo.AdminUser", t => t.UpdateByAdminUserId)
                .ForeignKey("dbo.ProductRule", t => t.ProductRuleId)
                .Index(t => t.CreateByAdminUserId)
                .Index(t => t.UpdateByAdminUserId)
                .Index(t => t.ProductRuleId);
            
            CreateTable(
                "dbo.ResolutionConfig",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        UserGroupId = c.Int(nullable: false),
                        Name = c.String(maxLength: 50),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroupId)
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.AgeToRestriction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Age_Id = c.Int(nullable: false),
                        Restriction_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AgeRangeDefinition", t => t.Age_Id)
                .ForeignKey("dbo.Restriction", t => t.Restriction_id)
                .Index(t => t.Age_Id)
                .Index(t => t.Restriction_id);
            
            CreateTable(
                "dbo.DisplayLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DeviceScreenId = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        CampaignId = c.Int(nullable: false),
                        DisplayType = c.Int(nullable: false),
                        FileId = c.Int(),
                        FileName = c.String(),
                        Content = c.String(),
                        LogTime = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        DisplayReason = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserGroupId)
                .Index(t => t.LogTime);
            
            CreateTable(
                "dbo.EmailService",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderAddress = c.String(),
                        SenderName = c.String(),
                        EnableSSL = c.Boolean(nullable: false),
                        SmtpPort = c.Int(nullable: false),
                        SmtpServerAddress = c.String(),
                        AllowAnonymous = c.Boolean(nullable: false),
                        SmtpAccount = c.String(),
                        SmtpPassword = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FaceDetectionLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        LogTime = c.DateTime(nullable: false),
                        DeviceId = c.Int(nullable: false),
                        LocationId = c.Int(),
                        AgeId = c.Int(nullable: false),
                        AgeBottom = c.Int(),
                        AgeTop = c.Int(),
                        Gender = c.Int(nullable: false),
                        LookTime = c.Int(nullable: false),
                        Distance = c.Int(nullable: false),
                        Year = c.Int(),
                        Month = c.Int(),
                        Day = c.Int(),
                        Hour = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserGroupId)
                .Index(t => t.LogTime);
            
            CreateTable(
                "dbo.FileDownloadLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        FileId = c.Int(nullable: false),
                        FileName = c.String(),
                        FileSize = c.Long(nullable: false),
                        TransferedFileSize = c.Long(nullable: false),
                        StartDownloadTime = c.DateTime(nullable: false),
                        EndDownloadTime = c.DateTime(),
                        DownloadSuccess = c.Boolean(nullable: false),
                        DownloadType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.PlayerInformationLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                        LogType = c.Int(nullable: false),
                        LogMessage = c.String(),
                        LogTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserGroupId);
            
            CreateTable(
                "dbo.PublishCampaign",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicationId = c.Int(nullable: false),
                        CampaignId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaign", t => t.CampaignId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .Index(t => t.PublicationId)
                .Index(t => t.CampaignId);
            
            CreateTable(
                "dbo.PublishDevice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicationId = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Device", t => t.DeviceId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .Index(t => t.PublicationId)
                .Index(t => t.DeviceId);
            
            CreateTable(
                "dbo.PublishDeviceGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicationId = c.Int(nullable: false),
                        DeviceGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviceGroup", t => t.DeviceGroupId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .Index(t => t.PublicationId)
                .Index(t => t.DeviceGroupId);
            
            CreateTable(
                "dbo.PublishPlaylist",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicationId = c.Int(nullable: false),
                        PlaylistId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Playlist", t => t.PlaylistId)
                .ForeignKey("dbo.Publication", t => t.PublicationId)
                .Index(t => t.PublicationId)
                .Index(t => t.PlaylistId);
            
            CreateTable(
                "dbo.SiteMenu",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        LanKey = c.String(),
                        Url = c.String(),
                        Icon = c.String(),
                        ControllerName = c.String(),
                        ActionName = c.String(),
                        MenuOrder = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteRole",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        LanKey = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteRoleToMenu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SiteRole_Id = c.Int(nullable: false),
                        SiteMenu_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SiteMenu", t => t.SiteMenu_Id)
                .ForeignKey("dbo.SiteRole", t => t.SiteRole_Id)
                .Index(t => t.SiteRole_Id)
                .Index(t => t.SiteMenu_Id);
            
            CreateTable(
                "dbo.SiteText",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        LanText = c.String(),
                        LanKey = c.String(),
                        CultureCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteUserToRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User_Id = c.Int(nullable: false),
                        Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SiteRole", t => t.Role_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.UserActionLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        LogType = c.Int(nullable: false),
                        LogMessage = c.String(maxLength: 500),
                        LogTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SiteUserToRole", "User_Id", "dbo.User");
            DropForeignKey("dbo.SiteUserToRole", "Role_Id", "dbo.SiteRole");
            DropForeignKey("dbo.SiteRoleToMenu", "SiteRole_Id", "dbo.SiteRole");
            DropForeignKey("dbo.SiteRoleToMenu", "SiteMenu_Id", "dbo.SiteMenu");
            DropForeignKey("dbo.PublishPlaylist", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.PublishPlaylist", "PlaylistId", "dbo.Playlist");
            DropForeignKey("dbo.PublishDeviceGroup", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.PublishDeviceGroup", "DeviceGroupId", "dbo.DeviceGroup");
            DropForeignKey("dbo.PublishDevice", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.PublishDevice", "DeviceId", "dbo.Device");
            DropForeignKey("dbo.PublishCampaign", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.PublishCampaign", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.AgeToRestriction", "Restriction_id", "dbo.Restriction");
            DropForeignKey("dbo.AgeToRestriction", "Age_Id", "dbo.AgeRangeDefinition");
            DropForeignKey("dbo.FileUpload", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.UserGroup", "UpdateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.ResolutionConfig", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.ResolutionConfig", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.ResolutionConfig", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.ResolutionConfig", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.ProductRule", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.ProductRule", "UpdateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.DMIDictionary", "ProductRuleId", "dbo.ProductRule");
            DropForeignKey("dbo.DMIDictionary", "UpdateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.DMIDictionary", "CreateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.ProductRule", "DeleteByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.ProductRule", "CreateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.Playlist", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.LooktimeRangeDefinition", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.Location", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.LibraryFolder", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.LayoutTemplate", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.LayoutTemplate", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.LayoutTemplateRegion", "TemplateId", "dbo.LayoutTemplate");
            DropForeignKey("dbo.LayoutTemplateRegion", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.LayoutTemplateRegion", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.LayoutTemplateRegion", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.LayoutTemplate", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.LayoutTemplate", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.DistanceRangeDefinition", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.Device", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.DeviceGroup", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.UserGroup", "DeleteByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.UserGroup", "CreateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.CampaignToPlaylists", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.CampaignToPlaylists", "Playlist_Id", "dbo.Playlist");
            DropForeignKey("dbo.CampaignToPlaylists", "Campaign_Id", "dbo.Campaign");
            DropForeignKey("dbo.Campaign", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.Campaign", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignRegion", "Campaign_Id", "dbo.Campaign");
            DropForeignKey("dbo.CampaignToDeviceScreen", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.CampaignToDeviceScreen", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "RestrictionId", "dbo.Restriction");
            DropForeignKey("dbo.CampaignToDeviceScreen", "PublicationId", "dbo.Publication");
            DropForeignKey("dbo.Publication", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.Publication", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.Publication", "RestrictionId", "dbo.Restriction");
            DropForeignKey("dbo.Restriction", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.Restriction", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.Restriction", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Restriction", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.Publication", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Publication", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "PlaylistId", "dbo.Playlist");
            DropForeignKey("dbo.Playlist", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.Playlist", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Playlist", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.Device", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.DeviceScreen", "DeviceId", "dbo.Device");
            DropForeignKey("dbo.CampaignToDeviceScreen", "DeviceId", "dbo.Device");
            DropForeignKey("dbo.Location", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.Device", "Location_Id", "dbo.Location");
            DropForeignKey("dbo.Location", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Location", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.DeviceGroup", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "DeviceGroupId", "dbo.DeviceGroup");
            DropForeignKey("dbo.Device", "DeviceGroupId", "dbo.DeviceGroup");
            DropForeignKey("dbo.DeviceGroup", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.DeviceGroup", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.Device", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Device", "DefaultScreenFileId", "dbo.FileUpload");
            DropForeignKey("dbo.FileUpload", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.RegionFrame", "FileId", "dbo.FileUpload");
            DropForeignKey("dbo.RegionFrame", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.RegionFrame", "TextAssetId", "dbo.TextAsset");
            DropForeignKey("dbo.TextAsset", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.TextAsset", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.TextAsset", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.TextAsset", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.RegionFrame", "RegionId", "dbo.CampaignRegion");
            DropForeignKey("dbo.RegionFrame", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.RegionFrame", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.LibraryFolder", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.FileUpload", "FolderId", "dbo.LibraryFolder");
            DropForeignKey("dbo.LibraryFolder", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.LibraryFolder", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.FileUpload", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.FileUpload", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.Device", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.CampaignToDeviceScreen", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.Campaign", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.Campaign", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.User", "UserGroupId", "dbo.UserGroup");
            DropForeignKey("dbo.User", "UpdateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.User", "DeleteByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.User", "CreateByAdminUserId", "dbo.AdminUser");
            DropForeignKey("dbo.AgeRangeDefinition", "UserGroup_Id", "dbo.UserGroup");
            DropIndex("dbo.UserActionLog", new[] { "UserGroupId" });
            DropIndex("dbo.SiteUserToRole", new[] { "Role_Id" });
            DropIndex("dbo.SiteUserToRole", new[] { "User_Id" });
            DropIndex("dbo.SiteRoleToMenu", new[] { "SiteMenu_Id" });
            DropIndex("dbo.SiteRoleToMenu", new[] { "SiteRole_Id" });
            DropIndex("dbo.PublishPlaylist", new[] { "PlaylistId" });
            DropIndex("dbo.PublishPlaylist", new[] { "PublicationId" });
            DropIndex("dbo.PublishDeviceGroup", new[] { "DeviceGroupId" });
            DropIndex("dbo.PublishDeviceGroup", new[] { "PublicationId" });
            DropIndex("dbo.PublishDevice", new[] { "DeviceId" });
            DropIndex("dbo.PublishDevice", new[] { "PublicationId" });
            DropIndex("dbo.PublishCampaign", new[] { "CampaignId" });
            DropIndex("dbo.PublishCampaign", new[] { "PublicationId" });
            DropIndex("dbo.PlayerInformationLog", new[] { "UserGroupId" });
            DropIndex("dbo.FileDownloadLog", new[] { "UserGroupId" });
            DropIndex("dbo.FaceDetectionLog", new[] { "LogTime" });
            DropIndex("dbo.FaceDetectionLog", new[] { "UserGroupId" });
            DropIndex("dbo.DisplayLog", new[] { "LogTime" });
            DropIndex("dbo.DisplayLog", new[] { "UserGroupId" });
            DropIndex("dbo.AgeToRestriction", new[] { "Restriction_id" });
            DropIndex("dbo.AgeToRestriction", new[] { "Age_Id" });
            DropIndex("dbo.ResolutionConfig", new[] { "UserGroupId" });
            DropIndex("dbo.ResolutionConfig", new[] { "DeleteByUserId" });
            DropIndex("dbo.ResolutionConfig", new[] { "UpdateByUserId" });
            DropIndex("dbo.ResolutionConfig", new[] { "CreateByUserId" });
            DropIndex("dbo.DMIDictionary", new[] { "ProductRuleId" });
            DropIndex("dbo.DMIDictionary", new[] { "UpdateByAdminUserId" });
            DropIndex("dbo.DMIDictionary", new[] { "CreateByAdminUserId" });
            DropIndex("dbo.ProductRule", new[] { "UserGroupId" });
            DropIndex("dbo.ProductRule", new[] { "DeleteByAdminUserId" });
            DropIndex("dbo.ProductRule", new[] { "UpdateByAdminUserId" });
            DropIndex("dbo.ProductRule", new[] { "CreateByAdminUserId" });
            DropIndex("dbo.LooktimeRangeDefinition", new[] { "UserGroup_Id" });
            DropIndex("dbo.LayoutTemplateRegion", new[] { "TemplateId" });
            DropIndex("dbo.LayoutTemplateRegion", new[] { "DeleteByUserId" });
            DropIndex("dbo.LayoutTemplateRegion", new[] { "UpdateByUserId" });
            DropIndex("dbo.LayoutTemplateRegion", new[] { "CreateByUserId" });
            DropIndex("dbo.LayoutTemplate", new[] { "UserGroupId" });
            DropIndex("dbo.LayoutTemplate", new[] { "DeleteByUserId" });
            DropIndex("dbo.LayoutTemplate", new[] { "UpdateByUserId" });
            DropIndex("dbo.LayoutTemplate", new[] { "CreateByUserId" });
            DropIndex("dbo.DistanceRangeDefinition", new[] { "UserGroup_Id" });
            DropIndex("dbo.CampaignToPlaylists", new[] { "Campaign_Id" });
            DropIndex("dbo.CampaignToPlaylists", new[] { "Playlist_Id" });
            DropIndex("dbo.CampaignToPlaylists", new[] { "UserGroup_Id" });
            DropIndex("dbo.Restriction", new[] { "DeleteByUserId" });
            DropIndex("dbo.Restriction", new[] { "UpdateByUserId" });
            DropIndex("dbo.Restriction", new[] { "CreateByUserId" });
            DropIndex("dbo.Restriction", new[] { "UserGroupId" });
            DropIndex("dbo.Publication", new[] { "RestrictionId" });
            DropIndex("dbo.Publication", new[] { "DeleteByUserId" });
            DropIndex("dbo.Publication", new[] { "UpdateByUserId" });
            DropIndex("dbo.Publication", new[] { "CreateByUserId" });
            DropIndex("dbo.Publication", new[] { "UserGroupId" });
            DropIndex("dbo.Playlist", new[] { "DeleteByUserId" });
            DropIndex("dbo.Playlist", new[] { "UpdateByUserId" });
            DropIndex("dbo.Playlist", new[] { "CreateByUserId" });
            DropIndex("dbo.Playlist", new[] { "UserGroup_Id" });
            DropIndex("dbo.Playlist", "IX_Playlist_UserGroup_Unique");
            DropIndex("dbo.DeviceScreen", new[] { "DeviceId" });
            DropIndex("dbo.Location", new[] { "DeleteByUserId" });
            DropIndex("dbo.Location", new[] { "UpdateByUserId" });
            DropIndex("dbo.Location", new[] { "CreateByUserId" });
            DropIndex("dbo.Location", new[] { "UserGroupId" });
            DropIndex("dbo.Location", new[] { "Name" });
            DropIndex("dbo.DeviceGroup", new[] { "UserGroup_Id" });
            DropIndex("dbo.DeviceGroup", "IX_DeviceGroupName_UserGroup_Unique");
            DropIndex("dbo.DeviceGroup", new[] { "DeleteByUserId" });
            DropIndex("dbo.DeviceGroup", new[] { "UpdateByUserId" });
            DropIndex("dbo.DeviceGroup", new[] { "CreateByUserId" });
            DropIndex("dbo.TextAsset", new[] { "DeleteByUserId" });
            DropIndex("dbo.TextAsset", new[] { "UpdateByUserId" });
            DropIndex("dbo.TextAsset", new[] { "CreateByUserId" });
            DropIndex("dbo.TextAsset", new[] { "UserGroupId" });
            DropIndex("dbo.CampaignRegion", new[] { "Campaign_Id" });
            DropIndex("dbo.RegionFrame", new[] { "TextAssetId" });
            DropIndex("dbo.RegionFrame", new[] { "FileId" });
            DropIndex("dbo.RegionFrame", new[] { "DataId" });
            DropIndex("dbo.RegionFrame", new[] { "RegionId" });
            DropIndex("dbo.RegionFrame", new[] { "DeleteByUserId" });
            DropIndex("dbo.RegionFrame", new[] { "UpdateByUserId" });
            DropIndex("dbo.RegionFrame", new[] { "CreateByUserId" });
            DropIndex("dbo.LibraryFolder", new[] { "DeleteByUserId" });
            DropIndex("dbo.LibraryFolder", new[] { "UpdateByUserId" });
            DropIndex("dbo.LibraryFolder", new[] { "CreateByUserId" });
            DropIndex("dbo.LibraryFolder", "IX_FlolderName_UserGroup_Unique");
            DropIndex("dbo.LibraryFolder", new[] { "UserGroup_Id" });
            DropIndex("dbo.FileUpload", new[] { "UserGroupId" });
            DropIndex("dbo.FileUpload", new[] { "FolderId" });
            DropIndex("dbo.FileUpload", new[] { "DeleteByUserId" });
            DropIndex("dbo.FileUpload", new[] { "UpdateByUserId" });
            DropIndex("dbo.FileUpload", new[] { "CreateByUserId" });
            DropIndex("dbo.Device", new[] { "Location_Id" });
            DropIndex("dbo.Device", new[] { "DefaultScreenFileId" });
            DropIndex("dbo.Device", new[] { "DeviceGroupId" });
            DropIndex("dbo.Device", new[] { "DeleteByUserId" });
            DropIndex("dbo.Device", new[] { "UpdateByUserId" });
            DropIndex("dbo.Device", new[] { "CreateByUserId" });
            DropIndex("dbo.Device", new[] { "UserGroup_Id" });
            DropIndex("dbo.Device", "IX_DeviceName_UserGroup_Unique");
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "PublicationId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "RestrictionId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "DeviceGroupId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "DeviceId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "PlaylistId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "CampaignId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "DeleteByUserId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "UpdateByUserId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "CreateByUserId" });
            DropIndex("dbo.CampaignToDeviceScreen", new[] { "UserGroupId" });
            DropIndex("dbo.User", new[] { "DeleteByAdminUserId" });
            DropIndex("dbo.User", new[] { "UpdateByAdminUserId" });
            DropIndex("dbo.User", new[] { "CreateByAdminUserId" });
            DropIndex("dbo.User", new[] { "UserGroupId" });
            DropIndex("dbo.User", new[] { "Email" });
            DropIndex("dbo.Campaign", new[] { "DeleteByUserId" });
            DropIndex("dbo.Campaign", new[] { "UpdateByUserId" });
            DropIndex("dbo.Campaign", new[] { "CreateByUserId" });
            DropIndex("dbo.Campaign", new[] { "UserGroup_Id" });
            DropIndex("dbo.Campaign", "IX_CampaignName_UserGroup_Unique");
            DropIndex("dbo.UserGroup", new[] { "DeleteByAdminUserId" });
            DropIndex("dbo.UserGroup", new[] { "UpdateByAdminUserId" });
            DropIndex("dbo.UserGroup", new[] { "CreateByAdminUserId" });
            DropIndex("dbo.UserGroup", new[] { "SystemKey" });
            DropIndex("dbo.AgeRangeDefinition", new[] { "UserGroup_Id" });
            DropIndex("dbo.AdminUser", new[] { "LoginAccount" });
            DropTable("dbo.UserActionLog");
            DropTable("dbo.SiteUserToRole");
            DropTable("dbo.SiteText");
            DropTable("dbo.SiteRoleToMenu");
            DropTable("dbo.SiteRole");
            DropTable("dbo.SiteMenu");
            DropTable("dbo.PublishPlaylist");
            DropTable("dbo.PublishDeviceGroup");
            DropTable("dbo.PublishDevice");
            DropTable("dbo.PublishCampaign");
            DropTable("dbo.PlayerInformationLog");
            DropTable("dbo.FileDownloadLog");
            DropTable("dbo.FaceDetectionLog");
            DropTable("dbo.EmailService");
            DropTable("dbo.DisplayLog");
            DropTable("dbo.AgeToRestriction");
            DropTable("dbo.ResolutionConfig");
            DropTable("dbo.DMIDictionary");
            DropTable("dbo.ProductRule");
            DropTable("dbo.LooktimeRangeDefinition");
            DropTable("dbo.LayoutTemplateRegion");
            DropTable("dbo.LayoutTemplate");
            DropTable("dbo.DistanceRangeDefinition");
            DropTable("dbo.CampaignToPlaylists");
            DropTable("dbo.Restriction");
            DropTable("dbo.Publication");
            DropTable("dbo.Playlist");
            DropTable("dbo.DeviceScreen");
            DropTable("dbo.Location");
            DropTable("dbo.DeviceGroup");
            DropTable("dbo.TextAsset");
            DropTable("dbo.CampaignRegion");
            DropTable("dbo.RegionFrame");
            DropTable("dbo.LibraryFolder");
            DropTable("dbo.FileUpload");
            DropTable("dbo.Device");
            DropTable("dbo.CampaignToDeviceScreen");
            DropTable("dbo.User");
            DropTable("dbo.Campaign");
            DropTable("dbo.UserGroup");
            DropTable("dbo.AgeRangeDefinition");
            DropTable("dbo.AdminUser");
        }
    }
}
