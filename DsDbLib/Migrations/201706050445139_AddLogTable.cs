namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScheduleSummary", "DeleteByUserId", "dbo.User");
            DropIndex("dbo.ScheduleSummary", new[] { "DeleteByUserId" });
            DropPrimaryKey("dbo.ScheduleSummary");
            CreateTable(
                "dbo.CampaignLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CampaignId = c.Int(nullable: false),
                        CampaignName = c.String(),
                        CampaignDuration = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Regions = c.String(),
                        UserId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Action = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DefalutScreenLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DeviceId = c.Int(nullable: false),
                        DefaultScreenFileId = c.Int(),
                        UserId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Action = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MediaLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        FileType = c.Int(nullable: false),
                        ExtensionName = c.String(maxLength: 10),
                        Size = c.Long(),
                        MediaDuration = c.Int(),
                        TextAssetContent = c.String(),
                        UserId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Action = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PublishLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ScheduleSummaryId = c.Long(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        SelectType = c.String(),
                        Campaigns = c.String(),
                        Screens = c.String(),
                        FaceRestriction = c.String(),
                        TimeRestriction = c.String(),
                        UserId = c.Int(nullable: false),
                        UserGroupId = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Action = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.ScheduleSummary", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.ScheduleSummary", "StartDate", c => c.DateTime(nullable: false));
            AddPrimaryKey("dbo.ScheduleSummary", "Id");
            DropColumn("dbo.ScheduleSummary", "DeleteByUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ScheduleSummary", "DeleteByUserId", c => c.Int());
            DropPrimaryKey("dbo.ScheduleSummary");
            AlterColumn("dbo.ScheduleSummary", "StartDate", c => c.DateTime());
            AlterColumn("dbo.ScheduleSummary", "Id", c => c.Int(nullable: false, identity: true));
            DropTable("dbo.PublishLog");
            DropTable("dbo.MediaLog");
            DropTable("dbo.DefalutScreenLog");
            DropTable("dbo.CampaignLog");
            AddPrimaryKey("dbo.ScheduleSummary", "Id");
            CreateIndex("dbo.ScheduleSummary", "DeleteByUserId");
            AddForeignKey("dbo.ScheduleSummary", "DeleteByUserId", "dbo.User", "Id");
        }
    }
}
