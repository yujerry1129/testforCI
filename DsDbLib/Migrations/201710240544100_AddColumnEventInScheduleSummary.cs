namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnEventInScheduleSummary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduleSummary", "Event", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduleSummary", "Event");
        }
    }
}
