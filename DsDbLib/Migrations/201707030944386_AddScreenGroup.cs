namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScreenGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScreenGroup",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        TimeServerId = c.Int(nullable: false),
                        HorizontallyScreenCount = c.Int(nullable: false),
                        VerticallyScreenCount = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateByUserId = c.Int(nullable: false),
                        UpdateByUserId = c.Int(),
                        DeleteByUserId = c.Int(),
                        UserGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreateByUserId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.DeleteByUserId)
                .ForeignKey("dbo.User", t => t.UpdateByUserId)
                .ForeignKey("dbo.UserGroup", t => t.UserGroup_Id, cascadeDelete: true)
                .Index(t => new { t.Name, t.UserGroup_Id }, unique: true, name: "IX_ScreenGroupName_UserGroup_Unique")
                .Index(t => t.CreateByUserId)
                .Index(t => t.UpdateByUserId)
                .Index(t => t.DeleteByUserId);
            
            AddColumn("dbo.User", "CreateByUserId", c => c.Int());
            AddColumn("dbo.User", "DeleteByUserId", c => c.Int());
            AddColumn("dbo.User", "UpdateByUserId", c => c.Int());
            AddColumn("dbo.Device", "Width", c => c.Int(nullable: false));
            AddColumn("dbo.Device", "Height", c => c.Int(nullable: false));
            AddColumn("dbo.Device", "ScreenGroupId", c => c.Long());
            AddColumn("dbo.Device", "OffsetX", c => c.Int());
            AddColumn("dbo.Device", "OffsetY", c => c.Int());
            AddColumn("dbo.ScheduleSummary", "ScreenGroupId", c => c.Long());
            CreateIndex("dbo.User", "CreateByUserId");
            CreateIndex("dbo.User", "DeleteByUserId");
            CreateIndex("dbo.User", "UpdateByUserId");
            CreateIndex("dbo.Device", "ScreenGroupId");
            CreateIndex("dbo.ScheduleSummary", "ScreenGroupId");
            AddForeignKey("dbo.User", "CreateByUserId", "dbo.User", "Id");
            AddForeignKey("dbo.User", "DeleteByUserId", "dbo.User", "Id");
            AddForeignKey("dbo.User", "UpdateByUserId", "dbo.User", "Id");
            AddForeignKey("dbo.Device", "ScreenGroupId", "dbo.ScreenGroup", "Id");
            AddForeignKey("dbo.ScheduleSummary", "ScreenGroupId", "dbo.ScreenGroup", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleSummary", "ScreenGroupId", "dbo.ScreenGroup");
            DropForeignKey("dbo.Device", "ScreenGroupId", "dbo.ScreenGroup");
            DropForeignKey("dbo.ScreenGroup", "UserGroup_Id", "dbo.UserGroup");
            DropForeignKey("dbo.ScreenGroup", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.ScreenGroup", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.ScreenGroup", "CreateByUserId", "dbo.User");
            DropForeignKey("dbo.User", "UpdateByUserId", "dbo.User");
            DropForeignKey("dbo.User", "DeleteByUserId", "dbo.User");
            DropForeignKey("dbo.User", "CreateByUserId", "dbo.User");
            DropIndex("dbo.ScheduleSummary", new[] { "ScreenGroupId" });
            DropIndex("dbo.ScreenGroup", new[] { "DeleteByUserId" });
            DropIndex("dbo.ScreenGroup", new[] { "UpdateByUserId" });
            DropIndex("dbo.ScreenGroup", new[] { "CreateByUserId" });
            DropIndex("dbo.ScreenGroup", "IX_ScreenGroupName_UserGroup_Unique");
            DropIndex("dbo.Device", new[] { "ScreenGroupId" });
            DropIndex("dbo.User", new[] { "UpdateByUserId" });
            DropIndex("dbo.User", new[] { "DeleteByUserId" });
            DropIndex("dbo.User", new[] { "CreateByUserId" });
            DropColumn("dbo.ScheduleSummary", "ScreenGroupId");
            DropColumn("dbo.Device", "OffsetY");
            DropColumn("dbo.Device", "OffsetX");
            DropColumn("dbo.Device", "ScreenGroupId");
            DropColumn("dbo.Device", "Height");
            DropColumn("dbo.Device", "Width");
            DropColumn("dbo.User", "UpdateByUserId");
            DropColumn("dbo.User", "DeleteByUserId");
            DropColumn("dbo.User", "CreateByUserId");
            DropTable("dbo.ScreenGroup");
        }
    }
}
