namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableWeatherData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WeatherDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        DataUnixTime = c.Int(nullable: false),
                        Description = c.String(),
                        Icon = c.String(),
                        Temp = c.Single(nullable: false),
                        TempMax = c.Single(nullable: false),
                        TempMin = c.Single(nullable: false),
                        Humidity = c.Int(nullable: false),
                        Pressure = c.Single(nullable: false),
                        WindDeg = c.Single(),
                        WindSpeed = c.Single(nullable: false),
                        Clouds = c.Int(nullable: false),
                        SunriseUnixTime = c.Int(nullable: false),
                        SunsetUnixTime = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Location", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WeatherDatas", "LocationId", "dbo.Location");
            DropIndex("dbo.WeatherDatas", new[] { "LocationId" });
            DropTable("dbo.WeatherDatas");
        }
    }
}
