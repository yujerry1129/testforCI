namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnDeviceTokeninTableDevice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "DeviceToken", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Device", "DeviceToken");
        }
    }
}
