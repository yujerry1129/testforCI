namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveFKinWeatherDataTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WeatherDatas", "LocationId", "dbo.Location");
            DropIndex("dbo.WeatherDatas", new[] { "LocationId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.WeatherDatas", "LocationId");
            AddForeignKey("dbo.WeatherDatas", "LocationId", "dbo.Location", "Id", cascadeDelete: true);
        }
    }
}
