namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumninTableScreenGroup : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.CampaignTag");
            DropPrimaryKey("dbo.DeviceTag");
            AddColumn("dbo.ScreenGroup", "DisconnectCount", c => c.Int(nullable: false));
            AlterColumn("dbo.CampaignTag", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.DeviceTag", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.CampaignTag", "Id");
            AddPrimaryKey("dbo.DeviceTag", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.DeviceTag");
            DropPrimaryKey("dbo.CampaignTag");
            AlterColumn("dbo.DeviceTag", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.CampaignTag", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.ScreenGroup", "DisconnectCount");
            AddPrimaryKey("dbo.DeviceTag", "Id");
            AddPrimaryKey("dbo.CampaignTag", "Id");
        }
    }
}
