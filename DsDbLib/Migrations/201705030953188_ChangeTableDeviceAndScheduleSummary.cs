namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTableDeviceAndScheduleSummary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "ContentStatus", c => c.Int(nullable: false));
            AddColumn("dbo.ScheduleSummary", "Spot", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduleSummary", "Spot");
            DropColumn("dbo.Device", "ContentStatus");
        }
    }
}
