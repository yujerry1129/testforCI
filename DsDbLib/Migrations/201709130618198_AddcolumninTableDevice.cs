namespace DsDbLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddcolumninTableDevice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Device", "IsNeedRestart", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Device", "IsNeedRestart");
        }
    }
}
