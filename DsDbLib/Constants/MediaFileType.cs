﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsDbLib.Constants
{
    public class MediaFileType
    {
        public static int? GetType(string FileExtensionName)
        {
            if (string.IsNullOrEmpty(FileExtensionName))
            {
                return null;
            }

            switch (FileExtensionName.ToLower().Trim())
            {
                //image
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".gif":
                case ".png":
                    return 1;

                //video
                case ".avi":
                case ".wmv":
                case ".mp4":
                    return 2;

                //music
                case ".mp3":
                    return 3;

                //flash
                case ".swf":
                    return 9;

                //excel
                case ".xls":
                    return 12;

                //xps
                case ".xps":
                    return 17;

                //pdf
                case ".pdf":
                    return 18;

                //file type not support
                default:
                    return 0;
            }
        }
    }
}
