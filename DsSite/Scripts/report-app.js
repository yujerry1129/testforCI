﻿var __queryData = []//此變數儲存全部資料

var app = new Vue({
    el: '#app',
    data: {
        api: {
            getInputParams: '/ace/api/report/inputparams',
            query: '/ace/api/report/query'
        },
        isLoading: false,
        errMsg: '',
        showMenu: true,
        document: $(document),
        dateType: 'day',
        genders: [
            { id: 1, text: 'Male', value: 1 },//男性
            { id: 2, text: 'Female', value: 2 }//女性
        ],
        ages: [
            { id: 1, text: '兒童', value: 1 },//
            { id: 2, text: '青少年', value: 2 },
            { id: 3, text: '成年', value: 3 },
            { id: 4, text: '老年', value: 4 },
        ],
        looktimes: [
            //{ id: 1, text: '1秒以下', value: 1 },
        ],
        distances: [
            //{ id: 1, text: '1以下', value: 1 },
        ],
        inputParam: {
            sex: [],
            age: [],
            looktime: [],
            distance: []
        },
        inputStartDate: '',
        inputEndDate: '',
        queryDataUpdated: false,
        queryStartDate: null,
        queryEndDate: null
    },
    //end data
    computed: {
        sexCheckAll: {
            get: function() {
                return this.inputParam.sex.length === this.genders.length
            },
            set: function(val) {
                this.inputParam.sex = []
                if (val) {
                    this.inputParam.sex = this.genders
                }
            }
        },
        //end sexCheckAll
        ageCheckAll: {
            get: function() {
                return this.inputParam.age.length === this.ages.length
            },
            set: function(val) {
                this.inputParam.age = []
                if (val) {
                    this.inputParam.age = this.ages
                }
            }
        },
        //end ageCheckAll
        looktimeCheckAll: {
            get: function() {
                return this.inputParam.looktime.length === this.looktimes.length
            },
            set: function(val) {
                this.inputParam.looktime = []
                if (val) {
                    this.inputParam.looktime = this.looktimes
                }
            }
        },
        //end looktimeCheckAll
        distanceCheckAll: {
            get: function() {
                return this.inputParam.distance.length === this.distances.length
            },
            set: function(val) {
                this.inputParam.distance = []
                if (val) {
                    this.inputParam.distance = this.distances
                }
            }
        },
    },
    mounted: function() {
        var vm = this
        this.document.click(function() {
            vm.showMenu = false
        })

        this.setDateInputFormat()
        this.sexCheckAll = true
        this.ageCheckAll = true
        this.looktimeCheckAll = true
        this.distanceCheckAll = true

        //初始化資料
        this.isLoading = true

        var api = $.get(vm.api.getInputParams)

        api.done(function(res) {
            if (res.Result) {
                //初始化looktime
                vm.looktimes = []
                $.each(res.LooktimeRangeDefinition, function(index, obj) {
                    vm.looktimes.push({
                        id: obj.Id,
                        text: obj.Description,
                        value: obj.Id
                    })
                })
                vm.looktimeCheckAll = true

                //初始化distance
                vm.distances = []
                $.each(res.DistanceRangeDefinition, function(index, obj) {
                    vm.distances.push({
                        id: obj.Id,
                        text: obj.Description,
                        value: obj.Id
                    })
                })
                vm.distanceCheckAll = true
            }
        })
        api.always(function() {
            vm.isLoading = false
        })
    },
    //end mounted
    watch: {
        dateType: function (val) {
            this.setDateInputFormat()
        },
        isLoading: function (val) {
            if (val) {
                this.showLoadingModal()
            } else {
                this.hideLoadingModal()
            }
        },
        'inputParam.sex': function() {
            this.updateReport()
        },
        'inputParam.age': function () {
            this.updateReport()
        },
        'inputParam.looktime': function () {
            this.updateReport()
        },
        'inputParam.distance': function () {
            this.updateReport()
        },
    },
    //end watch
    methods: {
        queryClick: function() {

            this.errMsg = ''

            var data = {
                TimeType: this.dateType,
                StartDateString: this.inputStartDate,
                EndDateString: this.inputEndDate
            }

            if (!data.StartDateString || !data.EndDateString || !this.dateType) {
                this.errMsg = 'Please Enter Start Time and End Time'//請輸入開始及結束時間
                return
            }

            this.isLoading = true

            var vm = this

            var api = $.post(vm.api.query, data)

            api.done(function(res) {

                if (res.Result) {

                    __queryData = res.Data

                    vm.queryStartDate = res.DataStartDate
                    vm.queryEndDate = res.DataEndDate

                    vm.updateReport()

                } else {
                    vm.errMsg = res.Message
                }
            })
            api.fail(function(err) {
                vm.errMsg = err
            })
            api.always(function() {
                vm.isLoading = false
            })
        },
        showLoadingModal: function() {
            $("#loadingModal").modal({
                backdrop: 'static',
                show: true
            })
        },
        hideLoadingModal: function() {
            $("#loadingModal").modal('hide')
        },
        collapseClick: function() {
            this.showMenu = !this.showMenu
        },
        //
        setDateInputFormat: function() {
            var format = ''
            switch (this.dateType) {
                case 'year':
                    format = 'YYYY'
                    break;

                case 'month':
                    format = 'YYYY-MM'
                    break;

                case 'day':
                    format = 'YYYY-MM-DD'
                    break;
            }

            if ($('#start_date').data("DateTimePicker")) {
                $('#start_date').data("DateTimePicker").destroy()
            }
            if ($('#end_date').data("DateTimePicker")) {
                $('#end_date').data("DateTimePicker").destroy()
            }

            var options = {
                format: format,
                showClose: true,
                showTodayButton: true,
                showClear: true,
                locale: 'en-us',//設定語言zh-tw 繁中
            }

            $('#start_date').datetimepicker(options)
            $('#end_date').datetimepicker(options)

            var vm = this

            $("#start_date,#end_date").on("dp.change", function (e) {
                vm.inputStartDate = $("#start_date").val()
                vm.inputEndDate = $("#end_date").val()
            });

            //設定當前時間
            $('#start_date').data("DateTimePicker").date(moment())
            $('#end_date').data("DateTimePicker").date(moment())

            this.inputStartDate = $('#start_date').val()
            this.inputEndDate = $('#end_date').val()
        },
        //end setDateInputFormat()
        updateReport: function() {
            if (!(__queryData && __queryData.length)) {
                return
            }

            var drawData = this.countDrawSpecData()

            drawReports(drawData)
        },
        countDrawSpecData: function() {
            var vm = this

            var sexCounter = []
            var ageCounter = []
            var looktimeCounter = []
            var distanceCounter = []

            $.each(vm.inputParam.sex, function(i, o) { sexCounter.push({ text: o.text, value: o.value, count: 0 }) })
            $.each(vm.inputParam.age, function(i, o) { ageCounter.push({ text: o.text, value: o.value, count: 0 }) })
            $.each(vm.inputParam.looktime, function(i, o) { looktimeCounter.push({ text: o.text, value: o.value, count: 0 }) })
            $.each(vm.inputParam.distance, function(i, o) { distanceCounter.push({ text: o.text, value: o.value, count: 0 }) })
            
            //過濾__queryData
            var filteredQueryData = __queryData

            //filter sex
            if (!vm.sexCheckAll) {
                filteredQueryData = _.filter(filteredQueryData, function (o) {
                    for (var i = 0; i < sexCounter.length; i++) {
                        if (sexCounter[i].value === o.Gender) {
                            return true
                        }
                    }
                    return false
                })
            }

            //filter age
            if (!vm.ageCheckAll) {
                filteredQueryData = _.filter(filteredQueryData, function (o) {
                    for (var i = 0; i < ageCounter.length; i++) {
                        if (ageCounter[i].value === o.Age) {
                            return true
                        }
                    }
                    return false
                })
            }

            //filter looktime
            if (!vm.looktimeCheckAll) {
                filteredQueryData = _.filter(filteredQueryData, function (o) {
                    for (var i = 0; i < looktimeCounter.length; i++) {
                        if (looktimeCounter[i].value === o.Lid) {
                            return true
                        }
                    }
                    return false
                })
            }

            //filter distance
            if (!vm.distanceCheckAll) {
                filteredQueryData = _.filter(filteredQueryData, function (o) {
                    for (var i = 0; i < distanceCounter.length; i++) {
                        if (distanceCounter[i].value === o.Did) {
                            return true
                        }
                    }
                    return false
                })
            }

            //統計年齡 性別 注視時間 注視距離
            $.each(filteredQueryData, function (index, data) {

                var t;

                //sex
                t = _.find(sexCounter, { value: data.Gender })
                if (t) {
                    t.count += data.Count
                }

                //age
                t = _.find(ageCounter, { value: data.Age })
                if (t) {
                    t.count += data.Count
                }

                //looktime
                t = _.find(looktimeCounter, { value: data.Lid })
                if (t) {
                    t.count += data.Count
                }

                //distance
                t = _.find(distanceCounter, { value: data.Did })
                if (t) {
                    t.count += data.Count
                }
            })
            
            var sexSpec = {
                type: "sex",
                data: [{
                    name: "性別人數統計",
                    key: _.map(sexCounter, 'text'),
                    value: _.map(sexCounter, 'count'),
                }]
            }

            var ageSpec = {
                type: "age",
                data: [{
                    name: "年齡統計",
                    key: _.map(ageCounter, 'text'),
                    value: _.map(ageCounter, 'count'),
                }]
            }

            var looktimeSpec = {
                type: "looktime",
                data: [{
                    name: "Looktime統計",
                    key: _.map(looktimeCounter, 'text'),
                    value: _.map(looktimeCounter, 'count'),
                }]
            }

            var distanceSpec = {
                type: "distance",
                data: [{
                    name: "Distance統計",
                    key: _.map(distanceCounter, 'text'),
                    value: _.map(distanceCounter, 'count'),
                }]
            }
            
            //每年與每年總人數統計
            var startDate = moment(vm.queryStartDate)
            var endDate = moment(vm.queryEndDate)

            var startYear = startDate.year()
            var startMonth = startDate.month()
            var startDay = startDate.date()

            var endYear = endDate.year()
            var endMonth = endDate.month()
            var endtDay = endDate.date()

            var peopleBarSpec = {
                type: "people_bar",
                data: []
            }

            var peopleLineSpecData = {
                type: "people_line",
                data: []
            }

            if (vm.dateType === 'year') {

                var years = []
                var yearMonths = []

                for (var y = startYear; y < endYear; y++) {
                    years.push({
                        year: y,
                        count: 0
                    })
                    for (var m = 1; m <= 12; m++) {
                        yearMonths.push({
                            year: y,
                            month: m,
                            count: 0
                        })
                    }
                }

                //統計year month
                $.each(filteredQueryData, function (index, data) {
                    var t = _.find(yearMonths, { year: data.Year, month: data.Month })
                    if (t) {
                        t.count += data.Count
                    }
                })

                //統計years
                $.each(yearMonths, function(index, data) {
                    var t = _.find(years, { year: data.year })
                    if (t) {
                        t.count += data.count
                    }
                })

                //製作bar spec
                peopleBarSpec.data.push({
                    name: "Year",
                    key: _.map(years, 'year'),
                    value: _.map(years, 'count')
                })

                //製作line spec
                _.forEach(years, function(obj) {
                    var data = _.filter(yearMonths, { year: obj.year })
                    peopleLineSpecData.data.push({
                        name: obj.year.toString(),
                        key: _.map(data, 'month'),
                        value: _.map(data, 'count')
                    })
                })

            } else if (vm.dateType === 'month') {

                var start = moment([startYear, startMonth])
                var end = moment([endYear, endMonth])

                var months = []
                var monthDays = []

                for (var m = start; m < end; m = m.add(1, 'months')) {
                    months.push({
                        year: m.year(),
                        month: m.month() + 1,
                        count: 0
                    })

                    for (var d = 1; d <= m.daysInMonth() ; d++) {
                        monthDays.push({
                            year: m.year(),
                            month: m.month() + 1,
                            day: d,
                            count: 0
                        })
                    }
                }

                //統計month days
                $.each(filteredQueryData, function (index, data) {
                    var t = _.find(monthDays, { year: data.Year, month: data.Month, day: data.Day })
                    if (t) {
                        t.count += data.Count
                    }
                })

                //統計months
                $.each(monthDays, function(index, data) {
                    var t = _.find(months, { year: data.year, month: data.month })
                    if (t) {
                        t.count += data.count
                    }
                })

                //製作bar spec
                peopleBarSpec.data.push({
                    name: "Month",
                    key: _.map(months, function (o) { return o.year + '/' + o.month }),
                    value: _.map(months, 'count')
                })

                //製作line spec
                _.forEach(months, function(obj) {
                    var data = _.filter(monthDays, { year: obj.year, month: obj.month })
                    peopleLineSpecData.data.push({
                        name: obj.year + '/' + obj.month,
                        key: _.map(data, 'day'),
                        value: _.map(data, 'count')
                    })
                })

            } else if (vm.dateType === 'day') {

                var start = moment([startYear, startMonth, startDay])
                var end = moment([endYear, endMonth, endtDay])

                var days = []
                var dayHours = []

                for (var d = start; d < end; d = d.add(1, 'days')) {

                    days.push({
                        year: d.year(),
                        month: d.month() + 1,
                        day: d.date(),
                        count: 0
                    })

                    for (var h = 0; h <= 23 ; h++) {
                        dayHours.push({
                            year: d.year(),
                            month: d.month() + 1,
                            day: d.date(),
                            hour: h,
                            count: 0
                        })
                    }
                }

                //統計day hours
                $.each(filteredQueryData, function (index, data) {
                    var t = _.find(dayHours, { year: data.Year, month: data.Month, day: data.Day, hour: data.Hour })
                    if (t) {
                        t.count += data.Count
                    }
                })

                //months
                $.each(dayHours, function(index, data) {
                    var t = _.find(days, { year: data.year, month: data.month, day: data.day })
                    if (t) {
                        t.count += data.count
                    }
                })

                //製作bar spec
                peopleBarSpec.data.push({
                    name: "Day",
                    key: _.map(days, function (o) { return o.year + '/' + o.month + '/' + o.day }),
                    value: _.map(days, 'count')
                })

                //製作line spec
                _.forEach(days, function(obj) {
                    var data = _.filter(dayHours, { year: obj.year, month: obj.month, day: obj.day })
                    peopleLineSpecData.data.push({
                        name: obj.year + '/' + obj.month + '/' + obj.day,
                        key: _.map(data, 'hour'),
                        value: _.map(data, 'count')
                    })
                })
            }

            var specData = []
            specData.push(peopleBarSpec)
            specData.push(peopleLineSpecData)
            specData.push(sexSpec)
            specData.push(ageSpec)
            specData.push(looktimeSpec)
            specData.push(distanceSpec)

            return specData

        }
    },
    //end methods
})