﻿if (!_d_s) {

    var _d_s = function () { }

    //enableDropUpload
    _d_s.prototype.enableDropUpload = function (selector, defaultUploadPostUrl, callbacks) {

        
        //Check if browser support FormData
        if (!window.FormData) {
            if (callbacks && callbacks.error) {
                callbacks.error(null, {
                    success: false,
                    message: 'browser does not support drag and drop upload'
                }, null);
            }
            return;
        }
        
        var self = this;

        $(selector).each(function (index, dropbox) {
            
            //prevent drag enter default behavior
            dropbox.addEventListener("dragenter", function (e) {
                e.stopPropagation();
                e.preventDefault();

                if (callbacks && callbacks.enter) {
                    callbacks.enter(e);
                }
            }, false);

            //prevent drag over default behavior
            dropbox.addEventListener("dragover", function (e) {
                e.stopPropagation();
                e.preventDefault();

                if (callbacks && callbacks.over) {
                    callbacks.over(e);
                }
            }, false);

            dropbox.addEventListener("drop", function (e) {

                //prevent default behavior
                e.stopPropagation();
                e.preventDefault();

                if (callbacks && callbacks.drop) {
                    callbacks.drop(e);
                }

                //get files
                var dt = e.dataTransfer;
                var files = dt.files;

                for (var i = 0; i < files.length; i++) {

                    //Upload file
                    self.uploadFile(defaultUploadPostUrl, files[i], callbacks, e);
                }

            }, false);
        });

    };

    //uploadFile
    _d_s.prototype.uploadFile = function (defaultUploadPostUrl, file, callbacks, e,menuData) {

        var fd = new FormData();
        var xhr = new XMLHttpRequest();
        var fname = file.name;
        var fsize = file.size;
        var key = null;
        var uploadUrl = null;
        
        //begin upload
        if (callbacks && callbacks.begin) {
            
            var result = callbacks.begin(file, e, xhr);

            if (result === false) {
                console.log('cancel upload');
                return;
            }

            if (result && result.key) {
                key = result.key;//識別每一次上傳的key
            }

            if (result && result.uploadUrl) {
                uploadUrl = result.uploadUrl;//上傳網址
            }

        }

        //on upload progress update
        xhr.upload.onprogress = function (evt) {
            if (evt.lengthComputable) {
                var progress = (evt.loaded / evt.total * 100 | 0);
                if (callbacks && callbacks.progress) {
                    callbacks.progress(file, progress, key);
                }
            }
        }

        //on server response
        xhr.onload = function (res) {
            if (callbacks && callbacks.onResponse) {
                callbacks.onResponse(file, res.target, key);
            }
        };

        //on error
        xhr.onerror = function (err) {
            if (callbacks && callbacks.error) {
                callbacks.error(file, err.target, key);
            }
        }

        //Put file into form data
        key ? fd.append(key, file) : fd.append(fname, file);

        //上傳(若在begin事件有返回上傳網址，就以此網址上傳；反之，以defaultUploadPostUrl上傳)
        xhr.open('POST', uploadUrl || defaultUploadPostUrl);
        if (menuData) {
            xhr.setRequestHeader("menuData", menuData);
        }

        xhr.send(fd);
    };

    //根據file size(bytes)取得描述的檔案大小
    _d_s.prototype.getFileSizeString = function (size, fix) {

        if (!size) {
            size = 0;
        }

        if (!fix) {
            fix = 2;
        }

        //byte
        if (size < 1024) {
            return size + ' byte(s)';
        }

        //kb
        size = (size / 1024).toFixed(2);
        if (size < 1024) {
            return size + ' KB';
        }

        //mb
        size = (size / 1024).toFixed(2);
        if (size < 1024) {
            return size + ' MB';
        }

        //gb
        size = (size / 1024).toFixed(2);
        return size + ' GB';
    };
}