﻿var __culture = __getCookie("lan")

if (!__culture) {
    __culture = 'en-us'
}

function LText(lanId) {
    if (!lanId) {
        return ''
    }
    var lanSet = _.find(__languages, ['id', lanId])
    if (lanSet) {
        var lan = _.find(lanSet.lan, ['code', __culture])
        if (lan) {
            return lan.text
        }
    }
    return ''
}

function __getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}