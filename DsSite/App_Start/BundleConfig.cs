﻿using System.Web;
using System.Web.Optimization;

namespace DsSite
{
    public class BundleConfig
    {
        // 如需「搭配」的詳細資訊，請瀏覽 http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //jquery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            //jquery validation js
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            //modernizr js
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bootstrap js
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //bootstrap css
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-superhero.css",
                      "~/Content/font-awesome.css",
                      "~/Content/animate.css",
                      "~/Content/_Layout/style.css",
                      "~/Content/site.css"));

            //lodash
            bundles.Add(new ScriptBundle("~/bundles/lodash").Include(
                      "~/Scripts/lodash.min.js"));

            //多語
            bundles.Add(new ScriptBundle("~/bundles/language").Include(
                      "~/lan.js",
                      "~/Scripts/l-text.js"));
        }
    }
}
