﻿using System.Web;
using System.Web.Mvc;

namespace DsSite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new Filters.DsAuthFilter());
        }
    }
}
