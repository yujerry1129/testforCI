﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DsSite.Models;
using DsDbLib.Models;
using System.Globalization;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class DeviceController : Base.DsBaseController
    {
        /// <summary>
        /// 列出所有裝置(圖示、列表顯示)
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            DeviceModels DeviceModels = new DeviceModels();
            return View();
        }

        /// <summary>
        /// 裝置詳細資訊頁面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(int id)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var DeviceInfo = DeviceModels.FindDevice(id, _UserInfo);
            if (DeviceInfo != null)
                return View();
            else
                return RedirectToAction("");

        }

        /// <summary>
        /// Screen Group 詳細資訊頁面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ScreenGroup(long? id)
        {
            DeviceModels DeviceModels = new DeviceModels();
            if (id != null)
            {
                var ScreenGroupInfo = DeviceModels.FindScreenGroup(id.Value, _UserInfo);
                if (ScreenGroupInfo == null)
                    return RedirectToAction("");
                else
                    return View();
            }
            else
            {
                return RedirectToAction("");
            }

        }

    }
}