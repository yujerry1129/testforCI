﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DsSite.Controllers
{
    public class ErrorsController : Controller
    {
        //public ActionResult General(Exception exception)
        //{
        //    return Content("General failure", "text/plain");
        //}

        //public ActionResult Http404()
        //{
        //    return Content("Not found", "text/plain");
        //}

        //public ActionResult Http403()
        //{

        //    return Content("Forbidden", "text/plain");
        //}
        public ActionResult AccessDeny()
        {
            return Content("<div style='text-align: center; margin-top: 40px'>Access denied</div>");
        }
        public ActionResult InternalError()
        {
            //return View(); 
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            //若已經沒有session
            if (context.Session != null)
            {
                return new RedirectToRouteResult(new RouteValueDictionary {
                        { "controller", "Home" },
                        { "action", "Index" }
                    });
            }
            else
            {
                return new RedirectToRouteResult(new RouteValueDictionary {
                        { "controller", "Account" },
                        { "action", "Login" }
                    });
            }
        }
        public ActionResult LicenseError()
        {
            return View();
        }
    }
}