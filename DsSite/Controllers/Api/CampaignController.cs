﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    /// <summary>
    /// Campaign 所有API
    /// </summary>
    [Authorize]
    [RoutePrefix("api/campaign")]
    public class CampaignController : Base.DsBaseApiController
    {

        CampaignModels CampaignModels = new CampaignModels();
        PublishModels PublishModels = new PublishModels();

        /// <summary>
        /// 取得當前使用者的Campagin資訊(同UserGroup也可看到)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("all")]
        public ApiResult GetAllCampaign(CampaignApiModel CampaignApiModel)
        {
            var data = CampaignModels.GetAllCampaign(_UserInfo, CampaignApiModel.Page,
                CampaignApiModel.PageRows, CampaignApiModel.SearchText, CampaignApiModel.SearchType);
            return data;
        }


        /// <summary>
        /// 取得Campaign Detail
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("detail")]
        public ApiResult GetCampaign(CampaignApiModel Model)
        {
            var data = CampaignModels.GetCampaign(_UserInfo, Model);
            return data;
        }


        /// <summary>
        /// 取得 Camapign meta 資訊
        /// </summary>
        /// <param name="Model">CampaignApiModel</param>
        /// <returns>回傳取得資料或是失敗無資料</returns>
        [HttpPost]
        [Route("info")]
        public ApiResult GetCampaignMetaInformation(CampaignApiModel Model)
        {
            var data = CampaignModels.GetCampaignMetaInformation(_UserInfo, Model);
            return data;
        }


        /// <summary>刪除Camapign</summary>
        /// <param name="Model">CampaignApiModel</param>
        /// <returns>回傳Campaign刪除的結果</returns>
        [HttpPost]
        [Route("delete")]
        public ApiResult DeleteCamapign(CampaignApiModel Model)
        {
            var data = CampaignModels.DeleteCamapign(_UserInfo, Model);
            return data;
        }

        /// <summary>
        /// 新增Camapign
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public ApiResult Add(CampaignApiModel Model)
        {
            var data = CampaignModels.Add(_UserInfo, Model);
            return data;
        }

        /// <summary>
        /// Edit Campaign
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("edit")]
        public ApiResult Edit(CampaignApiModel Model)
        {
            var data = CampaignModels.Edit(_UserInfo, Model);
            return data;
        }

        /// <summary>
        /// 被Device使用的Campaign清單
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("usedcampaign")]
        public ApiResult CampaignUsedList(CampaignApiModel Model)
        {
            var data = CampaignModels.CampaignUsedList(_UserInfo, Model);
            return data;
        }

        /// <summary>
        /// insert campaign tag 
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertcampaigntag")]
        public ApiResult InsertCampaignTag(CampaignTagApiModel CampaignTagApiModel)
        {
            var data = CampaignModels.InsertTag(_UserInfo, CampaignTagApiModel.CampaignId, CampaignTagApiModel.TagName);
            return data;
        }

        /// <summary>
        /// delete campaign tag 
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletecampaigntag")]
        public ApiResult DeleteCampaignTag(CampaignTagApiModel CampaignTagApiModel)
        {
            var data = CampaignModels.DeleteTag(_UserInfo, CampaignTagApiModel.CampaignId, CampaignTagApiModel.TagName);
            return data;
        }


        /// <summary>
        /// save as campaign
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addother")]
        public ApiResult AddAnotherCampaign(CampaignApiModel Model)
        {
            var data = CampaignModels.AddOther(_UserInfo, Model);
            return data;
        }

        /// <summary>
        /// replace campaign
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("replacecampaign")]
        public ApiResult ReplaceCampaign(CampaignSaveAsApiModel Model)
        {
            var data = CampaignModels.ReplaceCampaign(_UserInfo, Model);
            return data;
        }



        [HttpPost]
        [Route("getallcampaigninfo")]
        public ApiResult GetAllCampaignInfo(CampaignApiModel Model)
        {
            var data = CampaignModels.GetAllCampaignInfo(_UserInfo, Model.SearchText);
            return data;
        }
    }
}
