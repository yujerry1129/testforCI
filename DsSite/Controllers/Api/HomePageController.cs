﻿using DsSite.Controllers.Api.Models;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/home")]
    public class HomePageController : Base.DsBaseApiController
    {

        /// <summary>
        /// 取得home info 資訊
        /// </summary>
        /// <returns>檔案下載</returns>
        [HttpPost]
        [Route("gethomeinfo")]
        public ApiResult GetHomeInfo()
        {
            HomePageModels HomePageModels = new HomePageModels();
            var homeInfo = HomePageModels.GetHomeInfo(_UserInfo);

            return homeInfo;
        }


        /// <summary>
        /// get player的zip檔
        /// </summary>
        /// <returns>檔案下載</returns>
        [HttpGet]
        [Route("getplayerfile")]
        public HttpResponseMessage GetPlayerFile()
        {
            var FilePath = WebConfigurationManager.AppSettings["UploadFolder"] + "/AcePlayerSetup.zip";
            var stream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "AcePlayerSetup.zip"
            };
            return response;
        }


    }
}