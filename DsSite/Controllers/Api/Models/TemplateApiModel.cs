﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class TemplateApiModel
    {
        public int TemplateId { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public List<TemplateRegionApiModel> Regions { get; set; }
    }

    public class TemplateRegionApiModel
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Order { get; set; }
    }
}