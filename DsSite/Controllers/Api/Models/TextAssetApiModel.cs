﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class TextAssetApiModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int Type { get; set; }
        public List<string> RssData { get; set; }
    }

    /// <summary>
    /// RSS
    /// </summary>
    public class Rss
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public int FontSize { get; set; }
        public string Color { get; set; }
        public string BackgroundColor { get; set; }
        public bool Transparent { get; set; }
    }
}