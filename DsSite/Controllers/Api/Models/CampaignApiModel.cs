﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class CampaignApiModel
    {
        public int CampaignId { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public List<RegionApiModel> Regions { get; set; }
        //public int UserGroup_Id { get; set; }
        //public DateTime CreateDate { get; set; }
        //public DateTime? UpdateDate { get; set; }
        //public DateTime? DeleteDate { get; set; }
        //public int CreateByUserId { get; set; }
        //public int? UpdateByUserId { get; set; }
        //public int? DeleteByUserId { get; set; }

        public string CheckModel()
        {
            if (string.IsNullOrEmpty(Name))
            {
                return "Campaign name is required";
            }

            if (Duration <= 0)
            {
                return "Duration must be at least 1 second";
            }

            if (Width <= 0)
            {
                return "Width must be at least 1 pixel";
            }

            if (Height <= 0)
            {
                return "Height must be at least 1 pixel";
            }

            if (Regions == null || Regions.Count == 0)
            {
                return "Campaign must has 1 region";
            }

            return null;
        }

        public int Page { get; set; }
        public int PageRows { get; set; }
        public string SearchText { get; set; }
        public string SearchType { get; set; }
    }

    public class RegionApiModel
    {
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int LayerOrder { get; set; }
        public List<FrameApiModel> Frames { get; set; }
        public int RegionId { get; set; }
    }

    public class FrameApiModel
    {
        //public int Offset { get; set; }
        public int Length { get; set; }
        public int Order { get; set; }

        /// <summary>
        /// 0:error
        /// 1:image
        /// 2:video
        /// 3:audio
        /// 4:text
        /// 5:web page
        /// 6:marquee
        /// 9.flash
        /// 10.clock
        /// 11.weather
        /// 12.menu
        /// 13.null image
        /// </summary>
        public int FrameType { get; set; }
        public int? FileId { get; set; }
        public int FrameId { get; set; }
        public int ClockStyle { get; set; }
    }
    public class CampaignTagApiModel
    {
        public List<int> CampaignId { get; set; }
        public List<String> TagName { get; set; }
        public DateTime CreateDate { get; set; }
    }

    /// <summary>
    /// campaign replace api model
    /// </summary>
    public class CampaignSaveAsApiModel
    {
        public int ReplaceCampaignId { get; set; }
        public int SaveAsCampaignId { get; set; }
        public string SaveAsCampaignName { get; set; }
    }

    public class CampaignListInfo
    {
        public class CampaignList
        {
            public int CampaignId { get; set; }
            public string CampaignName { get; set; }

            public int Duration { get; set; }
            public DateTime? UpdateDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public string UpdatebyUserName { get; set; }
            public string CreatebyUserName { get; set; }
        }
        public List<CampaignListInfo.CampaignList> CampaignsList { get; set; }

    }


}