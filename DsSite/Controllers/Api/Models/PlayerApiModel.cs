﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;

namespace DsSite.Controllers.Api.Models
{
    #region 接受參數型態定義

    public class UserGroupData
    {
        public string SystemKey { get; set; }
        public int? UserGroupId { get; set; }
        public string UserGroupName { get; set; }
    }

    public class DeviceData : UserGroupData
    {
        public int? DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceToken { get; set; }
    }
    public class ErrorLog
    {
        public System.DateTime LogTime { get; set; }
        public int ErrorType { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class MiddlewareLogData
    {
        public DateTime? LogTime { get; set; }
        public int? Gender { get; set; }
        public int? Age { get; set; }
        public int? LookTime { get; set; }
        public int? Distance { get; set; }
    }

    public class ErrorMiddlewareLog : DeviceData
    {
        public List<ErrorLog> MiddlewareErrorLogList { get; set; }
    }
    public class InsertMiddlewareLogData : DeviceData
    {
        public List<MiddlewareLogData> MiddlewareLogList { get; set; }
    }

    public class DownloadLog
    {
        public Guid DownloadToken { get; set; }
        public int Progress { get; set; }
        public DateTime DownloadTime { get; set; }
        public int FileType { get; set; }
        public int FileId { get; set; }
    }

    public class DownloadLogData : DeviceData
    {
        public List<DownloadLog> Logs { get; set; }
    }

    public class InsertDisplayLogData : DeviceData
    {
        public List<DisplayLog> Logs { get; set; }
    }

    public class DeviceFaceSetting
    {
        public int DeviceId { get; set; }
        public int FaceDetection { get; set; }
        public int FaceDetectionType { get; set; }
        public int FaceReport { get; set; }

    }
    public class WatchDogSetting
    {
        public bool IsOn { get; set; }
        public int? Time { get; set; }
        public bool Log { get; set; }
    }
    public class MachineSetting
    {
        public WatchDogSetting WatchDogSetting { get; set; }
        public DeviceFaceSetting DeviceFaceSetting { get; set; }
        public AutoShutdown ShutDownSetting { get; set; }
    }
    public class AutoShutdown
    {
        public TimeSpan? AutoShutDownTime { get; set; }
        public DateTime? LastShutDownTime { get; set; }
    }

    //含1~多組screen資訊註冊的device data
    public class MachineData : DeviceData
    {
        public string DomainName { get; set; }
        public string MachineName { get; set; }
        public string IpAddress { get; set; }
        public List<DeviceScreen> ScreenList { get; set; }
        public string MacAddress { get; set; }
        public string OperationSystem { get; set; }
        public long DiskUsage { get; set; }
        public long DiskCapacity { get; set; }
        public string DeviceVersion { get; set; }
    }

    public class DispatchedData : DeviceData
    {
        public bool SignageUpdated { get; set; }
        public bool ConfigUpdated { get; set; }
    }

    public class DispatchData
    {
        public bool HasNewSignage { get; set; }

        public bool IsNeedUpdate { get; set; }
        public TimeSpan? VersionUpdateTime { get; set; }
        public string Version { get; set; }
        public long UpdateFileSize { get; set; }
        public string UpdateFileMD5Hash { get; set; }

        public bool ConfigUpdate { get; set; }

        public bool IsNeedRestart { get; set; }

    }

    public class ConfigData
    {
        public string DeviceName { get; set; }
        public ScreenGroupDetail ScreenGroup { get; set; }
        public TimeServer TimeServer { get; set; }
    }

    public class ScreenGroupDetail
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int MaxX { get; set; }
        public int MaxY { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public int DisconnectCount { get; set; }

        public ScreenGroupDetail(ScreenGroup sg, int offsetx, int offsety)
        {
            Id = sg.Id;
            Name = sg.Name;
            MaxX = sg.HorizontallyScreenCount;
            MaxY = sg.VerticallyScreenCount;
            OffsetX = offsetx;
            OffsetY = offsety;
            DisconnectCount = sg.DisconnectCount;
        }
    }

    public class TimeServer
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string Ip { get; set; }

        public TimeServer(DsDbLib.Models.Device device)
        {
            DeviceId = device.Id;
            DeviceName = device.Name;
            Ip = device.IpAddressList;
        }
    }

    public class MenuContent : DsSite.Models.LibraryModels.MenuData
    {
        //繼承MenuData之類別
    }

    public class CountdownTimerContent : DsSite.Models.LibraryModels.TimerData
    {
        //繼承TimerData之類別
    }

    #endregion

    #region 舊版資料格式
    public class BaseSchedule
    {
        public long Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public int Duration { get; set; }
        public int? CampaignId { get; set; }
        public int Priority { get; set; }

        //新增至舊版資料格式，以配合新版之邏輯運算
        public bool HasCalenderRule { get; set; }
        public bool? RepeatMonday { get; set; }
        public bool? RepeatTuesday { get; set; }
        public bool? RepeatWednesday { get; set; }
        public bool? RepeatThurday { get; set; }
        public bool? RepeatFriday { get; set; }
        public bool? RepeatSaturday { get; set; }
        public bool? RepeatSunday { get; set; }
        public int PlayOrder { get; set; }

        public bool HasMonthCalenderRule { get; set; }
        public int MonthDay { get; set; }

        public BaseSchedule(ScheduleSummary ss, int duration)
        {
            Id = ss.Id;
            StartDate = ss.StartDate;
            StartTime = ss.DailyStartTime.Value;
            Duration = duration;
            EndDate = ss.EndDate;

            CampaignId = ss.CampaignId.Value;

            Priority = 0;
            PlayOrder = ss.PlayOrder;
            HasCalenderRule = ss.HasCalenderRule;
            RepeatMonday = ss.RepeatMonday;
            RepeatTuesday = ss.RepeatTuesday;
            RepeatWednesday = ss.RepeatWednesday;
            RepeatThurday = ss.RepeatThurday;
            RepeatFriday = ss.RepeatFriday;
            RepeatSaturday = ss.RepeatSaturday;
            RepeatSunday = ss.RepeatSunday;
            HasMonthCalenderRule = ss.HasMonthCalenderRule;
            MonthDay = ss.MonthDay;
        }
    }

    public class Schedule : BaseSchedule
    {
        public TimeSpan DailyEndTime { get; set; }
        public Schedule(ScheduleSummary ss, int duration) : base(ss, duration)
        {
            DailyEndTime = ss.DailyEndTime.Value;
        }
    }

    public class RuleFace
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
        public int DetectionType { get; set; }
        public int UserGroupId { get; set; }
        public int? CampaignId { get; set; }
        public int Duration { get; set; }

        public RuleFace(ScheduleSummary ss, int duration, int age)
        {
            Id = ss.Id;
            Age = age;
            //Age=_SS.AgeRestriction,
            Gender = ss.Gender.Value;
            Duration = duration;
            CampaignId = ss.CampaignId;
        }
    }


    public class RuleSpot : BaseSchedule
    {
        public RuleSpot(ScheduleSummary ss, int duration) : base(ss, duration)
        {
        }
    }

    public class CampaignLayout
    {
        public int Id { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public CampaignLayout(Campaign cp)
        {
            Id = cp.Id;
            Height = cp.Height;
            Width = cp.Width;
        }
    }

    public partial class Region
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int LayerOrder { get; set; }
        public decimal OffsetX { get; set; }
        public decimal OffsetY { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }

        public Region(Campaign cp, CampaignRegion cr)
        {
            Id = cr.Id;
            CampaignId = cp.Id;
            LayerOrder = cr.LayerOrder;
            Height = cr.Height;
            Width = cr.Width;
            OffsetX = cr.OffsetX;
            OffsetY = cr.OffsetY;
        }
    }
    public class Segment
    {
        public int Id { get; set; }
        public int RegionId { get; set; }
        public int? MediaId { get; set; }
        public int? MediaType { get; set; }
        public int Sequence { get; set; }
        public int? Duration { get; set; }
    }
    public partial class Media
    {
        public int Id { get; set; }
        public int? MediaType { get; set; }
        public string Name { get; set; }
        public string FileExtensionName { get; set; }
        public string OriginalName { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string ContentDescription { get; set; }
        public Guid? HashCode { get; set; }
        public System.DateTime? DeletedTime { get; set; }
    }
    #endregion

}