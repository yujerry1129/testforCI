﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class DeviceApiModel
    {
        public int Id { get; set; }
        public string SelectedDate { get; set; }
        public string Month { get; set; }
        public int Num { get; set; }
        public int? LocationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int? DefaultScreenId { get; set; }
        public string AutoShutDownTime { get; set; }
        public int? FaceDetectionType { get; set; }
        public bool FaceDetectionReportEnabled { get; set; }
        public List<long> ScheduleSummaryIds { get; set; }
        public int? DeviceId { get; set; }
        public long? ScreenGroupId { get; set; }
        public long? scheduleSummaryId { get; set; }
        public bool AutoUpdateEnabled { get; set; }
        public string AutoUpdateTime { get; set; }
    }

    public class DeviceTagApiModel
    {
        public List<int> DeviceId { get; set; }
        public List<long> ScreenGroupId { get; set; }
        public List<String> TagName { get; set; }
        public int Page { get; set; }
        public int PageRows { get; set; }
        public string SearchText { get; set; }
        public int SearchType { get; set; }

    }

    public class ScreenGroupApiModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int TimeServerId { get; set; }
        public int HorizontallyScreenCount { get; set; }
        public int VerticallyScreenCount { get; set; }
        public List<DeviceOffsetInfo> Devices { get; set; }
        public bool IsNeedUpdate { get; set; }
        public bool AutoUpdateEnabled { get; set; }
        public string AutoUpdateTime { get; set; }
        public int DisconnectCount { get; set; }
    }

    public class BatchChangeDeviceApiModel
    {
        public List<int> DeviceList { get; set; }
        public List<long> ScreenGroupList { get; set; }
        public string AutoUpdateTime { get; set; }
        public int Type { get; set; }
    }

    public class DeviceOffsetInfo
    {
        public int Id { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
    }

    public class UserGroupInfo
    {
        public int DeviceLimit { get; set; }
        public int DeviceCount { get; set; }
        public int DeviceRemaining { get; set; }
        public string UsageRate { get; set; }
    }

    public class DeviceInfo
    {
        public int Id { get; set; }
        public int DispatchStatus { get; set; }
        public string DeviceName { get; set; }
        public bool ConnectionStatus { get; set; }
        public string Resolution { get; set; }

        public string DiskCapacity { get; set; }
        public string DiskUsage { get; set; }
        public string DiskUsageRate { get; set; }
        public string MachineName { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public string OperationSystem { get; set; }
        public int? OffsetX { get; set; }
        public int? OffsetY { get; set; }
        public int? LocationId { get; set; }
        public bool EnableFaceDetection { get; set; }
        public int? FaceDetectionType { get; set; }
        public bool FaceDetectionReportEnabled { get; set; }
        public TimeSpan? DailyShutDownTime { get; set; }
        public string DefaultScreenUrl { get; set; }
        public int? DefaultScreenId { get; set; }
        public List<string> TagsName { get; set; }
        public string VersionNumber { get; set; }
        public bool AutoUpdateEnabled { get; set; }
        public bool IsLatestVersion { get; set; }
        public bool IsNeedUpdate { get; set; }
        public bool IsNeedRestart { get; set; }
        public TimeSpan? AutoUpdateTime { get; set; }
    }

    public class ScreenGroupInfo
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<DeviceInfo> Devices { get; set; }
        public int TimeServerId { get; set; }
        public string TimeServerName { get; set; }
        public int HorizontallyScreenCount { get; set; }
        public int VerticallyScreenCount { get; set; }
        public List<string> TagsName { get; set; }
        public int DisconnectCount { get; set; }
    }
    public class DispatchedCampaignInfo
    {
        public long ScheduleSummaryId { get; set; }
        public int Playorder { get; set; }
        public int? CampaignId { get; set; }
        public string CampaignName { get; set; }
        public bool HasCalendarRule { get; set; }
        /// <summary>
        /// 紀錄該廣告每周幾要播放
        /// </summary>
        public List<string> RepeatWeekDay { get; set; }
        public bool HasFaceDetectionRule { get; set; }
        public bool Spot { get; set; }
        public bool Event { get; set; }
        public string Startdate { get; set; }
        public string Enddate { get; set; }
        public TimeSpan? DailyStartTime { get; set; }
        public TimeSpan? DailyEndTime { get; set; }
        public string Gender { get; set; }
        public string AgeRestriction { get; set; }
        public bool HasMonthCalenderRule { get; set; }
        public int MonthDay { get; set; }
        public class CalendarDateInfo
        {
            public int DateNumber { get; set; }
            public int[] CampaignCount { get; set; }
        }
    }

    public class DeviceTagInfo
    {
        public int DeviceId { get; set; }
        public string Name { get; set; }
        public List<string> Tag { get; set; }
        public long ScreenGroupId { get; set; }
        public string TempTag { get; set; }
    }
}