﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class ApiResult
    {
        public bool Result { get; set; }
        public int? ResultCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }

    public class SuccessResult : ApiResult
    {
        public SuccessResult(object Data = null)
        {
            this.Result = true;
            this.Data = Data;
        }
    }

    public class NotFoundResult : ApiResult
    {
        public NotFoundResult(object Data = null)
        {
            this.Result = false;
            this.ResultCode = 404;
            this.Message = "Data not found";
            this.Data = Data;
        }
    }

    public class ExceptionResult : ApiResult
    {
        public ExceptionResult(object Data = null, string Message = "Exception occurs")
        {
            this.Result = false;
            this.Data = Data;
            this.Message = Message;
        }
    }
}