﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class ResolutionApiModel
    {
        public int ResolutionId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }

        public string CheckModel()
        {
            if (string.IsNullOrEmpty(Name))
            {
                return "Resolution name is required";
            }

            if (Width <= 0)
            {
                return "Width must be at least 1 pixel";
            }

            if (Height <= 0)
            {
                return "Height must be at least 1 pixel";
            }


            return null;
        }
    }

}