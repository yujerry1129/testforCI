﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class AccountApiModel
    {
        public string Email { get; set; }
        public string TempPassword { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
    }

    public class _User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string LastLoginTime { get; set; }
        public string CreateDate { get; set; }
        public List<string> SiteRole { get; set; }
        public string HashedPassword { get; set; }
        public int PageCount { get; set; }

    }

    public class ErrorMsg
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool status { get; set; }
    }

    public class SetRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string Role { get; set; }
        public List<User> Users { get; set; }
        public List<string> Roles { get; set; }
        public int Page { get; set; }
        public int PageRows { get; set; }
        public string DeviceName { get; set; }

    }
}