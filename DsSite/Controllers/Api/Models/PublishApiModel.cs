﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class PublishApiModel
    {
        public List<PublishCampaignApiModel> Campaigns { get; set; }
        public List<PublishDeviceApiModel> Screens { get; set; }
        public List<PublishScreenGroupApiModel> ScreenGroups { get; set; }
        public List<PublishAgeRange> AgesRanges { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SelectedTypeApiModel SelectedType { get; set; }
        public TimeRestrictionApiModel TimeRestriction { get; set; }
        public FaceRestrictionApiModel FaceRestriction { get; set; }
        public long ScheduleSummaryId { get; set; }

    }



    public class PublishCampaignApiModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public List<string> Tag { get; set; }
    }


    public class PublishDeviceApiModel
    {
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public List<string> Tag { get; set; }
        public int ResolutionWidth { get; set; }
        public int ResolutionHeight { get; set; }
        //public int DeviceId { get; set; }
        //public int ScreenIndex { get; set; }
    }

    public class PublishScreenGroupApiModel
    {
        public long Id { get; set; }
        public string ScreenGroupName { get; set; }
        public List<string> Tag { get; set; }
        public int HorizontallyScreenCount { get; set; }
        public int VerticallyScreenCount { get; set; }
    }

    public class TimeRestrictionApiModel
    {
        public bool? RepeatMon { get; set; }
        public bool? RepeatTue { get; set; }
        public bool? RepeatWed { get; set; }
        public bool? RepeatThu { get; set; }
        public bool? RepeatFri { get; set; }
        public bool? RepeatSat { get; set; }
        public bool? RepeatSun { get; set; }
        public TimeSpan? DailyStartTime { get; set; }
        public TimeSpan? DailyEndTime { get; set; }
    }

    public class FaceRestrictionApiModel
    {
        public bool Male { get; set; }
        public bool Female { get; set; }
        public List<int> AgeIdList { get; set; }

    }

    public class PublishAgeRange
    {
        //public int Id { get; set; }
        public int AgeId { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public string Descripiton { get; set; }
    }

    public class SelectedTypeApiModel
    {
        //public int Id { get; set; }
        public bool General { get; set; }
        public bool Face { get; set; }
        public bool Spot { get; set; }
        public bool Event { get; set; }
    }
    public class PublishUsedModel
    {

        public int Id { get; set; }
        public string Name { get; set; }


        public int UserGroupId { get; set; }


        public int CreateByUserId { get; set; }


        public int? UpdateByUserId { get; set; }


        public int? DeleteByUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }


        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int RestrictionId { get; set; }

        public Restriction Restriction { get; set; }


        public IEnumerable<PublishDevice> Devices { get; set; }


        public IEnumerable<PublishScreenGroupApiModel> ScreenGroups { get; set; }


        public IEnumerable<PublishCampaign> Campaigns { get; set; }

        public IEnumerable<CampaignToDeviceScreen> CampaignToDeviceScreens { get; set; }
    }



}