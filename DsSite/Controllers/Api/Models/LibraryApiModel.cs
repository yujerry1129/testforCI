﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class LibraryApiModel
    {
        public string FolderName { get; set; }
        public int? FolderId { get; set; }
        public int? ParentFolderId { get; set; }
        public int? FileId { get; set; }

        public List<int> FolderIdList { get; set; }
        public List<int> FileIdList { get; set; }
        public string FileName { get; set; }
        public string TargetName { get; set; }
        public int TargetId { get; set; }
        public bool Action { get; set; }
    }


    public class LibraryFolderData
    {
        public List<LibraryFolder> Folders { get; set; }
        public List<FileUpload> Files { get; set; }
        public LibraryFolder Folder { get; set; }
        public List<LibraryApiModel> Parents { get; set; }
        public List<itemFolders> ItemFolders { get; set; }
        public List<itemFiles> ItemFiles { get; set; }
    }

    public class itemFolders
    {
        public string Name { get; set; }
        public int? ParentFolderId { get; set; }
        public int FolderId { get; set; }
        public long FolderFileSize { get; set; }
        public string UpdateDate { get; set; }
    }

    public class itemFiles
    {
        public int FileId { get; set; }
        public string Name { get; set; }
        public int FileType { get; set; }
        public long Size { get; set; }
        public string ExtensionName { get; set; }
        public DateTime? UploadDate { get; set; }
        public string UpdateDate { get; set; }
        public string MimeType { get; set; }
        public int? VideoDuration { get; set; }
    }

    public class MenuInfo
    {
        public int MenuId { get; set; }
        public int AssetId { get; set; }
        public string Name { get; set; }
        public int BackgroudFileId { get; set; }
        public int TextEffect { get; set; }
    }

    public class TimerInfo
    {
        public int AssetId { get; set; }
        public string Name { get; set; }
        public string TimerDetail { get; set; }
    }
}