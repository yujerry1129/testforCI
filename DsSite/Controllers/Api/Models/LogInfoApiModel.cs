﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class LogInfoDeviceApiModel
    {
        public int Page { get; set; }
        public int PageRows { get; set; }
        public string DeviceName { get; set; }

    }

    public class FaceLogApiModel
    {
        public int DeviceId { get; set; }
        public int Page { get; set; }
        public int PageRows { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class DisplayLogApiModel
    {
        public int DeviceId { get; set; }
        public int Page { get; set; }
        public string CampaignName { get; set; }
        public int PageRows { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }
}