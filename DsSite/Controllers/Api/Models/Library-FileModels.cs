﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Controllers.Api.Models
{
    public class Library_FileModels
    {

        public class FilesInfo
        {
            public int FileId { get; set; }
            public int FileType { get; set; }
            public int Index { get; set; }
            public int FrameIndex { get; set; }
            public string Content { get; set; }
            public List<string> RssContent { get; set; }


        }
        public class Files
        {
            public int FileId { get; set; }
            public int FileType { get; set; }
            public int Index { get; set; }
            public int FrameIndex { get; set; }

        }
        public List<Files> AllFile { get; set; }
        // public List<FilesInfo> ReturnFileInfo { get; set; }

    }
}