﻿using DsDbLib;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Security;

namespace DsSite.Controllers.Api
{
    public class AccountController : Base.DsBaseApiController
    {
        /// <summary>
        /// 忘記密碼
        /// </summary>
        /// <param name="Model"></param>
        /// <returns>回傳忘記密碼成功或錯誤訊息</returns>
        [HttpPost]
        [Route("api/forgetpassword")]
        public ApiResult ForgetPassword(AccountApiModel Model)
        {
            // AccountModels AccountModels = new AccountModels();
            var result = new ApiResult();
            EmailServiceModels EmailService = new EmailServiceModels();
            AccountModels AccountModels = new AccountModels();

            var user = AccountModels.FindUser(Model.Email);

            if (user != null)
            {
                //若user的暫時密碼尚未過期，就不產生新的密碼，也不會寄送email
                //if (user.TempPasswordExpiredTime != null && user.TempPasswordExpiredTime < DateTime.Now)
                //{
                //    result.Result = true;
                //    result.ResultCode = 1;
                //    return result;
                //}

                //產生隨機五位數  儲存至temp password        
                string tmpPwd = RandomHelper.GetRandomNumberString(5);
                AccountModels.setTempPassword(user, tmpPwd, 10);

                //將暫時密碼寄送給user
                var subject = "ACE system password reset message";
                var body = "Temp password:<br/><b>" + tmpPwd + "</b>, expires at 10 minutes later.";

                //查詢email service 
                var emailService = EmailService.FindEmailService();

                if (emailService != null && !string.IsNullOrEmpty(Model.Email))
                {
                    var email = new MailMessage();
                    email.From = new MailAddress(emailService.SenderAddress, emailService.SenderName);
                    email.To.Add(Model.Email);

                    email.Subject = subject;
                    email.Body = body;
                    email.IsBodyHtml = true;

                    try
                    {
                        emailService.CreateSmtpClient().Send(email);

                        result.Result = true;
                    }
                    catch (Exception ex)
                    {
                        result.Result = false;
                        result.Message = "Send system email fail: " + ex.Message;
                    }
                }
                else
                {
                    result.Result = false;
                    result.Message = "No available email service or user email is empty";
                }
            }
            else
            {
                //User不存在
                result.Result = false;
                result.Message = "Invalid email address. Please check you email again.";
            }

            return result;
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="Model"> AccountAPI viewModel</param>
        /// <returns>回傳修改密碼結果</returns>
        [HttpPost]
        [Route("api/changepassword")]
        public ApiResult ChangePassword(AccountApiModel Model)
        {
            var result = new ApiResult();
            EmailServiceModels EmailService = new EmailServiceModels();
            AccountModels AccountModels = new AccountModels();

            var user = AccountModels.FindUser(Model.Email);

            if (user == null || user.TempPasswordExpiredTime == null)
            {
                //User不存在或沒有更改密碼的需求
                result.Result = false;
                result.Message = "Change failed";
                return result;
            }
            else if (user.TempPasswordExpiredTime < DateTime.Now)
            {
                result.Result = false;
                result.Message = "Check code is expired. Please try again.";
                return result;
            }
            else if (Model.Password != Model.RepeatPassword)
            {
                result.Result = false;
                result.Message = "Password inputs are not the same.";
                return result;
            }
            else if (user.TempPassword != Model.TempPassword)
            {
                result.Result = false;
                result.Message = "Check code is not correct.";
                return result;
            }
            else
            {
                AccountModels.updatePassword(user, Model.Password);
                result.Result = true;

                //寄送密碼成功變更信件
                //查詢email service 
                var emailService = EmailService.FindEmailService();
                if (emailService != null)
                {
                    var email = new MailMessage();
                    email.From = new MailAddress(emailService.SenderAddress, emailService.SenderName);
                    email.To.Add(user.Email);
                    email.Subject = "ACE system password is changed successfully!";
                    email.Body = "ACE system password is changed successfully!";

                    try
                    {
                        emailService.CreateSmtpClient().Send(email);
                    }
                    catch { }
                }

                return result;
            }

        }

        /// <summary>
        /// 新增帳戶
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/add")]
        [Route("api/account/edit")]
        public ApiResult AddEdit(SetRole roleData)
        {
            var errMsg = new ErrorMsg();
            errMsg.status = true;
            var result = new ApiResult();
            if (roleData.Name == null)
            {
                errMsg.status = false;
                errMsg.Name = "Please enter account name.";
            }

            if (roleData.Email == null)
            {
                errMsg.status = false;
                errMsg.Email = "Please enter account email.";
            }

            //新增才要檢查密碼
            if (roleData.Id == 0)
            {
                if (roleData.HashedPassword == null)
                {
                    errMsg.status = false;
                    errMsg.Password = "Please enter account password.";
                }
            }

            if (!errMsg.status)
            {
                result.Result = false;
                result.Data = errMsg;
                return result;
            }

            AccountModels models = new AccountModels();
            var checkUser = models.AddEditAccount(_UserInfo, roleData);

            if (checkUser.Result == false)
            {
                result.Result = false;
                result.Message = checkUser.Message;
                return result;
            }

            return new SuccessResult();
        }

        /// <summary>
        /// 取得所有帳戶資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/getAllAccounts")]
        public ApiResult GetAllAccounts(SetRole SetRole)
        {
            AccountModels models = new AccountModels();
            var result = models.GetAllAccounts(SetRole.Page, SetRole.PageRows, SetRole.DeviceName, _UserInfo);

            return result;
        }

        /// <summary>
        /// 刪除裝置及角色
        /// </summary>
        /// /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/delete")]
        public ApiResult Delete(SetRole roleData)
        {
            AccountModels models = new AccountModels();
            var result = models.Delete(_UserInfo, roleData);

            return result;
        }

        /// <summary>
        /// 刪除帳戶角色
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/deleteRole")]
        public ApiResult DeleteRole(SetRole roleData)
        {
            AccountModels models = new AccountModels();
            var result = models.DeleteRole(_UserInfo, roleData);

            return result;
        }

        /// <summary>
        /// 轉移帳戶為Admin
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/transfer")]
        public ApiResult Transfer(SetRole roleData)
        {
            AccountModels models = new AccountModels();
            var result = models.Transfer(_UserInfo, roleData.Id);

            return result;
        }

        /// <summary>
        /// 取得所有角色資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/getAllRoles")]
        public ApiResult GetAllRoles()
        {
            AccountModels models = new AccountModels();
            var result = models.GetAllRoles(_UserInfo);

            return result;
        }

        /// <summary>
        /// 取得角色對應表(Menu)清單
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/getRolesInfo")]
        public ApiResult GetRolesInfo()
        {
            AccountModels models = new AccountModels();
            var result = models.GetRolesInfo(_UserInfo);

            return result;
        }

        /// <summary>
        /// 指派帳戶
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/account/setRoleToUser")]
        public ApiResult SetRoleToUser(SetRole roleData)
        {
            AccountModels models = new AccountModels();
            var result = models.SetRoleToUser(_UserInfo, roleData);
            return result;
        }

        [HttpGet]
        [Route("api/account/userinfo")]
        public Object GetUserInfo()
        {
            AccountModels models = new AccountModels();
            var result = models.GetUserInfo(_UserInfo);

            return result;
        }

        [HttpPost]
        [Route("api/account/logout")]
        public ApiResult Logout()
        {
            FormsAuthentication.SignOut();

            return new SuccessResult();
        }
    }
}
