﻿using DsDbLib;
using DsSite.Controllers.Api.Models;
using DsSite.Models.ViewModels;
using DsSite.Models.ViewModels.Modals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    public class ReportApiInputModel
    {
        /// <summary>
        /// year
        /// month
        /// day
        /// </summary>
        public string TimeType { get; set; }
        //public List<DateTime> Dates { get; set; }
        public string StartDateString { get; set; }
        public string EndDateString { get; set; }
    }

    [Authorize]
    [RoutePrefix("api/report")]
    public class ReportController : Base.DsBaseApiController
    {
        //ApiResult _TimeErrorResult = new ApiResult() { Result = false, Message = "時間格式錯誤" };
        //ApiResult _EndDateErrorResult = new ApiResult() { Result = false, Message = "結束時間不得早於開始時間" };

        [HttpGet]
        [Route("echo")]
        public string Echo()
        {
            return "hello world";
        }
        /// <summary>
        /// 取得LooktimeRange、DistanceRange定義
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("inputparams")]
        public object InputParams()
        {
            ReportModels ReportModels = new ReportModels();
            var result = ReportModels.InputParams(_UserInfo);
            return result;

        }

        /// <summary>
        /// 呼叫篩選資料
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("query")]
        public object Query(ReportApiInputModel Model)
        {
            ReportModels ReportModels = new ReportModels();
            var result = ReportModels.Query(Model, _UserInfo);
            return result;
        }
    }
}