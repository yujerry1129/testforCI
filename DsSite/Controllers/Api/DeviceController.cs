﻿using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using DsSite.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/device")]
    public class DeviceController : Base.DsBaseApiController
    {
        /// <summary>
        /// 傳入DeviceIds，將未註冊的裝置註冊
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("registerdevice")]
        public ApiResult RegisterDevice(BatchChangeDeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();

            var result = DeviceModels.RegisteDevice(_UserInfo, DeviceModel.DeviceList);
            return result;
        }
        /// <summary>
        /// 傳入DeviceIds，可移除被拒絕註冊的裝置
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("refuseregisterdevice")]
        public ApiResult RefuseRegisterDevice(BatchChangeDeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            DeviceModels.RefuseRegisterDevice(_UserInfo, DeviceModel.DeviceList);
            return new SuccessResult();
        }


        /// <summary>
        /// Get Device Information and Setting data
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getdevicedetail")]
        public ApiResult GetDeviceDetail(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var DeviceInfo = DeviceModels.GetDeviceDetailInfo(_UserInfo, DeviceModel.Id);

            return DeviceInfo;
        }

        /// <summary>
        /// (裝置與螢幕群組共用)傳入DeviceId/ScreenGroupId及name，重新命名裝置名稱
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("rename")]
        public ApiResult RenameDevice(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            if (DeviceModel.Name == null || string.IsNullOrEmpty(DeviceModel.Name))
            {
                return new ExceptionResult(Message: "Error_0004");
            }
            else
            {
                if (DeviceModel.DeviceId.HasValue)
                {
                    var result = DeviceModels.RenameDevice(_UserInfo, DeviceModel.DeviceId.Value, DeviceModel.Name);
                    return result;
                }
                else
                {
                    var result = DeviceModels.RenameScreenGroup(_UserInfo, DeviceModel.ScreenGroupId.Value, DeviceModel.Name);
                    return result;
                }
            }
        }

        /// <summary>
        /// 取出所有位置資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getlocationlist")]
        public ApiResult GetLocationList()
        {
            DeviceModels DeviceModels = new DeviceModels();
            var LocationInfo = DeviceModels.GetLocationList(_UserInfo);

            return LocationInfo;
        }

        /// <summary>
        /// 傳入deviceId, locationId，將裝置位置設定到傳入的位置上
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("setdevicelocation")]
        public ApiResult SetDeviceLocation(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var changeresult = DeviceModels.SetDeviceLocation(_UserInfo, DeviceModel);
            return changeresult;
        }

        /// <summary>
        /// 傳入 deviceId, name, description, address, Latitude, Longitude，將傳入的經緯度資訊建立一個新地點，並將裝置位置設定為新增的地點
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addlocation")]
        public ApiResult AddLocation(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var addresult = DeviceModels.AddLocation(_UserInfo, DeviceModel);

            return addresult;
        }


        /// <summary>
        /// 傳入 deviceId, locationId, name, description, address, Latitude, Longitude，更新locationId對應到的地點資訊，並將裝置位置設定為更新後的地點
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatelocationinfo")]
        public ApiResult UpdateLocationInfo(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var Updateresult = DeviceModels.UpdateLocationInfo(_UserInfo, DeviceModel);
            return Updateresult;
        }

        /// <summary>
        /// 傳入 locationId，刪除該位置
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletelocation")]
        public ApiResult DeleteLocation(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            DeviceModels.DeleteLocation(_UserInfo, DeviceModel);
            return new SuccessResult();
        }


        /// <summary>
        /// 取出所有圖片檔
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getallimage")]
        public ApiResult GetImageInfo()
        {
            DeviceModels DeviceModels = new DeviceModels();
            var ImageInfo = DeviceModels.GetImageInfo(_UserInfo);

            return ImageInfo;
        }

        /// <summary>
        /// 傳入id,DefaultScreen，設定裝置的預設畫面
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("settingdefaultscreen")]
        public ApiResult SettingDefaultScreen(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.SettingDefaultScreen(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.DefaultScreenId);
            return result;
        }

        /// <summary>
        /// 傳入deviceId，刪除該裝置
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletedevice")]
        public ApiResult DeleteDevice(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            PublishModels PublishModels = new PublishModels();
            PublishModels.AdjustbyDeleteDevice(DeviceModel.Id);
            DeviceModels.DeleteDevice(_UserInfo, DeviceModel.Id);

            return new SuccessResult();
        }

        /// <summary>
        /// 傳入id, AutoShutDownTime，設定裝置的每日關機時間
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("setdailyshutdowntime")]
        public ApiResult SetDailyShutDownTime(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var changeresult = DeviceModels.SetDailyShutDownTime(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.AutoShutDownTime);

            return changeresult;
        }

        /// <summary>
        /// 傳入id, FaceDetectionType，臉部辨識設定: 是否開啟臉部辨識切換、偵測模式為單一或多數的設定
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("facedetectionchange")]
        public ApiResult FaceDetectionChange(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var changeresult = DeviceModels.FaceDetectionChange(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.FaceDetectionType);
            return changeresult;
        }

        /// <summary>
        /// 傳入id, FaceDetectionReportEnabled，臉部辨識設定：是否開啟臉部辨識報表的設定
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("facedetectionreportenabledchange")]
        public ApiResult FaceDetectionReportEnabledChange(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var changeresult = DeviceModels.FaceDetectionReportEnabledChange(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.FaceDetectionReportEnabled);
            return changeresult;
        }

        /// <summary>
        /// Get Device Campaign list order by playorder
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getdevicecampaignlist")]
        public ApiResult GetDeviceCampaignList(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var DeviceCampaignList = DeviceModels.GetDispatchedCampaignList(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId);

            return DeviceCampaignList;
        }

        /// <summary>
        /// 傳入DeviceId或ScreenGroupId跟Month(當前頁面顯示的月份)及Num(上個月帶-1，下個月帶1，當月帶0)，回傳對應月份預計播放的CampaignCalendarList
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getdevicecampaigncalendar")]
        public ApiResult GetDeviceCampaignCalendar(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var DeviceCampaignCalendar = DeviceModels.SetCalendar(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.Month, DeviceModel.Num);

            return DeviceCampaignCalendar;
        }

        /// <summary>
        /// 傳入deviceId跟selectDate，回傳該日預計播放的Campaign
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getdeviceseleteddatecampaign")]
        public ApiResult GetDeviceSeletedDateCampaign(DeviceApiModel DeviceModel) // assumes the enum is typeof EmployeeStatus[FromBody]int id, [FromBody] string selectDate 
        {
            DeviceModels DeviceModels = new DeviceModels();
            var campaign = DeviceModels.GetSeletedDateDispatchedCampaign(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.SelectedDate);

            return new SuccessResult()
            {
                Data = campaign
            };
        }

        /// <summary>
        /// 新增裝置標籤(傳入TagName, DeviceId, ScreenGroupId)
        /// </summary>
        /// <param name="deviceTagApiModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertdevicetag")]
        public ApiResult InsertDeviceTag(DeviceTagApiModel deviceTagApiModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.InsertTag(deviceTagApiModel.TagName, deviceTagApiModel.DeviceId, deviceTagApiModel.ScreenGroupId);
            return result;
        }

        /// <summary>
        /// 刪除裝置標籤(傳入TagName, DeviceId, ScreenGroupId)
        /// </summary>
        /// <param name="deviceTagApiModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletedevicetag")]
        public ApiResult DeleteDeviceTag(DeviceTagApiModel deviceTagApiModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.DeleteTag(deviceTagApiModel.TagName, deviceTagApiModel.DeviceId, deviceTagApiModel.ScreenGroupId);
            return result;
        }

        /// <summary>
        /// 取得裝置標籤，Page, PageRows, SearchText, SearchType
        /// </summary>
        /// <param name="deviceTagApiModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getdevicetag")]
        public ApiResult GetDeviceTag(DeviceTagApiModel deviceTagApiModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.GetDeviceTag(deviceTagApiModel.Page, deviceTagApiModel.PageRows, _UserInfo, deviceTagApiModel.SearchText, deviceTagApiModel.SearchType);
            return result;
        }

        /// <summary>
        /// 取得所有群組與設備
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getalldevice")]
        public ApiResult GetAllDevice()
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.GetAllDevice(_UserInfo);

            return result;
        }

        /// <summary>
        /// 顯示群組底下的裝置
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getscreengroupdetail")]
        public ApiResult GetScreenGroupDetail(ScreenGroupApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.GetScreenGroupDetailInfo(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 新增/編輯群組(裝置資訊)
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("editscreengroup")]
        public ApiResult EditScreenGroup(ScreenGroupApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.EditScreenGroup(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 刪除(解散裝置)群組
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletescreengroup")]
        public ApiResult DeleteScreenGroup(ScreenGroupApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.DeleteScreenGroup(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 多筆刪除群組和裝置
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("batchdeletedevices")]
        public ApiResult BatchDeleteDevices(BatchChangeDeviceApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.BatchDeleteDevices(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 由裝置或螢幕群組刪除單一或多筆派送紀錄
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteschedulesummarys")]
        public ApiResult DeleteScheduleSummarys(DeviceApiModel DeviceModel)
        {
            PublishModels PublishModels = new PublishModels();
            if (DeviceModel.ScreenGroupId != null)
            {
                if (DeviceModel.ScheduleSummaryIds != null)
                {
                    //由螢幕群組中刪除多個廣告派送
                    var data = PublishModels.AdjustbyBatchDeleteScheduleSummaryIdinScreenGroup(DeviceModel.ScheduleSummaryIds, DeviceModel.ScreenGroupId.Value, _UserInfo);
                    return data;
                }
                else
                {
                    //由螢幕群組中刪除單一廣告派送
                    var data = PublishModels.AdjustbyDeleteScheduleSummaryIdinScreenGroup(DeviceModel.scheduleSummaryId.Value, DeviceModel.ScreenGroupId.Value, _UserInfo);
                    return data;
                }

            }
            else
            {
                if (DeviceModel.ScheduleSummaryIds != null)
                {
                    //由個別裝置中刪除多個廣告派送
                    var data = PublishModels.AdjustbyBatchDeleteScheduleSummaryIdinDevice(DeviceModel.ScheduleSummaryIds, DeviceModel.DeviceId.Value, _UserInfo);
                    return data;
                }
                else
                {
                    //由個別裝置中刪除單一廣告派送
                    var data = PublishModels.AdjustbyDeleteScheduleSummaryIdinDevice(DeviceModel.scheduleSummaryId.Value, DeviceModel.DeviceId, _UserInfo);
                    return data;
                }
            }
        }

        /// <summary>
        /// 傳入id, ScreenGroupId,autoUpdateEnabled，自動更新播放器設定：是否開啟自動更新播放器功能
        /// </summary>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("autoupdateenabledchange")]
        public ApiResult AutoUpdateEnabledChange(DeviceApiModel DeviceModel)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var changeresult = DeviceModels.AutoUpdateEnabledChange(_UserInfo, DeviceModel.DeviceId, DeviceModel.ScreenGroupId, DeviceModel.AutoUpdateEnabled, DeviceModel.AutoUpdateTime);
            return changeresult;
        }

        /// <summary>
        /// 傳入Groups, Devices, SelectedDevice, SelectedScreenGroup, IsBatch，批次或個別更新裝置的Player version
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("batchupdatedevicesversion")]
        public ApiResult BatchUpdateDevicesVersion(BatchChangeDeviceApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.BatchUpdateDevicesVersion(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 取得所有未分組裝置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getungroupeddevice")]
        public ApiResult GetUngroupedDevice()
        {
            DeviceModels DeviceModels = new DeviceModels();

            var ungroupedDevice = DeviceModels.GetWholeDeviceList(_UserInfo, null); //Null表示未分組裝置

            //無任何設備或群組
            if (ungroupedDevice == null)
            {
                return new NotFoundResult();
            }

            return new SuccessResult(Data: new
            {
                UngroupedDevice = ungroupedDevice,
            });
        }

        /// <summary>
        /// 傳入Device Id，指定該台裝置進行重啟OS
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("restartdevice")]
        public ApiResult RestartDevice(DeviceApiModel Model)
        {
            DeviceModels DeviceModels = new DeviceModels();
            var result = DeviceModels.RestartDeviceOS(_UserInfo, Model.Id);

            return result;
        }
    }
}