﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/publish")]
    public class PublishController : Base.DsBaseApiController
    {
        /// <summary>
        /// 取得 UserGroup 下面的播放及設備相關資訊
        /// </summary>
        /// <returns>回傳UserGroup下面的 device / device group / campaigns / AgeRangeDefinition</returns>
        [HttpGet]
        [Route("assets")]
        public ApiResult GetAvailablePublishAssets()
        {
            //Thread.Sleep(6000);   
            PublishModels PublishModels = new PublishModels();
            var result = PublishModels.GetAvailablePublishAssets(_UserInfo);
            return result;
        }


        /// <summary>
        /// 取得 Campaigns
        /// </summary>
        /// <returns>回傳可用的Campaigns資訊</returns>
        [HttpGet]
        [Route("campaigns")]
        public ApiResult GetAvailableCampaigns()
        {
            PublishModels PublishModels = new PublishModels();
            var result = PublishModels.GetAvailableCampaigns(_UserInfo);
            return result;
        }


        /// <summary>
        /// 儲存 Publication 資訊
        /// </summary>
        /// <param name="Model">PublishApiModel Model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("save")]
        public ApiResult AddPublication(PublishApiModel Model)
        {

            PublishModels PublishModels = new PublishModels();
            var result = PublishModels.AddPublication(_UserInfo, Model);
            return result;
        }

        /// <summary>
        /// 取得 需要維護的派送資料
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getMaintainData")]
        public ApiResult GetMaintainData(ScheduleSummary Model)
        {

            PublishModels PublishModels = new PublishModels();
            var result = PublishModels.GetMaintainData(Model, _UserInfo);
            return result;
        }

        [HttpPost]
        [Route("updateMaintainData")]
        public ApiResult UpdateMaintainData(PublishApiModel Model)
        {
            PublishModels PublishModels = new PublishModels();
            var result = PublishModels.UpdateMaintainData(Model, _UserInfo);
            //將修改Publish資訊存入log
            PublishLogModels PublishLogModels = new PublishLogModels();
            PublishLogModels.PublishLogEdit(Model.ScheduleSummaryId, Model, _UserInfo, 2);
            return result;
        }
    }
}
