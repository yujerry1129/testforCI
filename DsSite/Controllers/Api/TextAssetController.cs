﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models.Modals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/text")]
    public class TextAssetController : Base.DsBaseApiController
    {
        /// <summary>
        /// 取得當前所有文字資源(含文字、跑馬燈、網址)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public ApiResult GetAllTextAsset()
        {
            TextAssetModels TextAssetModels = new TextAssetModels();
            var result = TextAssetModels.GetAllTextAsset(_UserInfo);
            return result;

        }

        /// <summary>
        /// 新增或編輯後儲存文字資源
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        [Route("edit")]
        public ApiResult SaveTextAsset(TextAssetApiModel Model)
        {
            TextAssetModels TextAssetModels = new TextAssetModels();
            var result = TextAssetModels.SaveTextAsset(Model, _UserInfo);
            return result;
        }

        /// <summary>
        /// 刪除文字資源
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("delete")]
        public ApiResult DeleteTextAsset(TextAssetApiModel Model)
        {
            TextAssetModels TextAssetModels = new TextAssetModels();
            var result = TextAssetModels.DeleteTextAsset(Model, _UserInfo);
            return result;
        }

        [HttpPost]
        [Route("usedFile")]
        public ApiResult FileUsedList(TextAssetApiModel Model)
        {

            TextAssetModels TextAssetModels = new TextAssetModels();
            var result = TextAssetModels.FindCampaignUsedTextAssetFile(Model.Id, _UserInfo);
            return result;
        }
    }
}
