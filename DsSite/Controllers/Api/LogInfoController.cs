﻿using DsSite.Controllers.Api.Models;
using DsSite.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/LogInfo")]

    public class LogInfoController : Base.DsBaseApiController
    {
        /// <summary>
        /// 取得所有已註冊Device
        /// </summary>
        /// <param name="LogInfoModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllDeviceInfo")]
        public ApiResult GetAllDeviceInfo(LogInfoDeviceApiModel LogInfoDeviceApiModel)
        {

            LogInfoModels LogInfoModels = new LogInfoModels();
            var Devices = LogInfoModels.GetWholeDeviceInfo(LogInfoDeviceApiModel.Page, LogInfoDeviceApiModel.PageRows, _UserInfo, LogInfoDeviceApiModel.DeviceName);
            return Devices;
        }

        /// <summary>
        /// Get Device Log list order by Logtime
        /// </summary>
        /// <param name="FaceLogApiModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDeviceFaceLogList")]
        public ApiResult GetDeviceFaceLogList(FaceLogApiModel FaceLogApiModel)
        {
            LogInfoModels LogInfoModels = new LogInfoModels();
            var FaceLogList = LogInfoModels
                .GetDeviceFaceLogList(FaceLogApiModel.DeviceId, FaceLogApiModel.Page, FaceLogApiModel.PageRows, _UserInfo, FaceLogApiModel.StartDate, FaceLogApiModel.EndDate);
            return FaceLogList;
        }

        /// <summary>
        /// Get Device DisplayLog list order by Logtime
        /// </summary>
        /// <param name="DisplayLogApiModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDeviceDisplayLogList")]
        public ApiResult GetDeviceDisplayLogList(DisplayLogApiModel DisplayLogApiModel)
        {
            LogInfoModels LogInfoModels = new LogInfoModels();
            var DisplayLogList = LogInfoModels.GetDeviceDisplayLogList(DisplayLogApiModel.DeviceId, DisplayLogApiModel.Page, DisplayLogApiModel.PageRows, _UserInfo, DisplayLogApiModel.StartDate, DisplayLogApiModel.EndDate);

            return DisplayLogList;
        }

        [HttpGet]
        [Route("GetCSVDownload/{DeviceId:int}/{Type}")]
        public HttpResponseMessage GetCSVDownload(int DeviceId,String Type)
        {
            LogInfoModels LogInfoModels = new LogInfoModels();
            var CSVString = LogInfoModels.ExportCSV(DeviceId, Type, _UserInfo);
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8);
            writer.Write(CSVString);
            writer.Flush();
            stream.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Export.csv" };
            return result;

        }


    }
}