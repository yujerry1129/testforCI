﻿using DsSite.Controllers.Api.Models;
using DsSite.Models;
using System.Net.Http;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [RoutePrefix("api/player")]
    public class Player_1_0_Controller : ApiController
    {
        /// <summary>
        /// 驗證System Key是否是有效的
        /// </summary>
        /// <param name="data">UserGroupData</param>
        /// <returns>帶有UserGroupData的ApiResult</returns>
        [HttpPost]
        [Route("checksystemkey")]
        public ApiResult CheckSystemKey([FromBody] UserGroupData data)
        {
            ApiResult result = new PlayerModels().CheckSystemKey(data);
            return result;
        }

        /// <summary>
        /// 檢查device name是否可用於註冊新的device
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("checkdevicename")]
        public ApiResult CheckDeviceName([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().CheckDeviceName(data);
            return result;
        }

        /// <summary>
        /// 提供device的註冊，並加入screen至DB
        /// </summary>
        /// <param name="data">MachineData</param>
        /// <returns>帶有新裝置資訊的ApiResult</returns>
        [HttpPost]
        [Route("registedevice")]
        public ApiResult RegisteDevice([FromBody] MachineData data)
        {
            ApiResult result = new PlayerModels().RegisteDevice(data);
            return result;
        }

        /// <summary>
        /// 更新裝置的最後連線時間
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("updatemachinestatus")]
        public ApiResult UpdateMachineStatus([FromBody]DeviceData data)
        {
            ApiResult result = new PlayerModels().UpdateMachineStatus(data);
            return result;
        }

        /// <summary>
        /// 取得該使用者群組的產品規則
        /// </summary>
        /// <param name="data">UserGroupData</param>
        /// <returns>帶有產品規則資訊的ApiResult</returns>
        [HttpPost]
        [Route("getproductrules")]
        public ApiResult GetProductRules([FromBody]UserGroupData data)
        {
            var result = new PlayerModels().GetProductRules(data);
            return result;
        }

        /// <summary>
        /// 取得新的Signage資料
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>帶有Signage資料的ApiResult</returns>
        [HttpPost]
        [Route("getsignagedata")]
        public ApiResult GetSignageData([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().GetSignageData(data);
            return result;
        }

        /// <summary>
        /// 取得新的Config資訊
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>帶有Config資訊的ApiResult</returns>
        [HttpPost]
        [Route("getconfigupdate")]
        public ApiResult GetConfigUpdate([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().GetConfigUpdate(data);
            return result;
        }

        /// <summary>
        /// 檢查device的update status
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>帶有DispatchData的ApiResult</returns>
        [HttpPost]
        [Route("checknewdispatch")]
        public ApiResult CheckNewDispatch([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().CheckNewDispatch(data);
            return result;
        }

        /// <summary>
        /// 裝置已取得資訊後的狀態設定
        /// </summary>
        /// <param name="data">DispatchedData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("setdispatched")]
        public ApiResult SetDispatched([FromBody] DispatchedData data)
        {
            ApiResult result = new PlayerModels().SetDispatched(data);
            return result;
        }

        [HttpPost]
        [Route("setrestarted")]
        public ApiResult SetRestarted([FromBody]DeviceData data)
        {
            ApiResult result = new PlayerModels().SetRestarted(data);
            return result;
        }


        /// <summary>
        /// 裝置已取得資訊後的狀態設定
        /// </summary>
        /// <param name="data">DispatchedData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("removedevice")]
        public ApiResult RemoveDevice([FromBody]DeviceData data)
        {
            ApiResult result = new PlayerModels().RemoveDevice(data);
            return result;
        }

        /// <summary>
        /// 定時更新當下裝置的狀態及版本資訊
        /// </summary>
        /// <param name="data">MachineData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("updatedeviceinfo")]
        public ApiResult UpdateDeviceInfo([FromBody] MachineData data)
        {
            ApiResult result = new PlayerModels().UpdateDeviceInfo(data);
            return result;
        }

        /// <summary>
        /// 上傳播放紀錄
        /// </summary>
        /// <param name="data">InsertDisplayLogData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("uploaddisplaylog")]
        public ApiResult UploadDisplayLog([FromBody] InsertDisplayLogData data)
        {
            ApiResult result = new PlayerModels().UploadDisplayLog(data);
            return result;
        }

        /// <summary>
        /// 上傳臉部辨識紀錄
        /// </summary>
        /// <param name="data">InsertMiddlewareLogData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("uploadmiddlewarelog")]
        public ApiResult UploadMiddlewareLog([FromBody] InsertMiddlewareLogData data)
        {
            ApiResult result = new PlayerModels().UploadMiddlewareLog(data);
            return result;
        }

        /// <summary>
        /// 上傳裝置異常紀錄
        /// </summary>
        /// <param name="data">ErrorMiddlewareLog</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("uploadmiddlewareerrorlog")]
        public ApiResult UploadMiddlewareErrorLog([FromBody] ErrorMiddlewareLog data)
        {
            ApiResult result = new PlayerModels().UploadMiddlewareErrorLog(data);
            return result;
        }

        /// <summary>
        /// 取得WatchDog、臉部辨識、自動關機等設定資訊
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>帶有MachineSetting的ApiResult</returns>
        [HttpPost]
        [Route("getmachinesetting")]
        public ApiResult GetMachineSetting([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().GetMachineSetting(data);
            return result;
        }

        /// <summary>
        /// 回傳上傳資料夾內的檔案
        /// </summary>
        /// <param name="deviceToken">Device Token</param>
        /// <param name="mediaId">Media Id</param>
        /// <param name="downloadToken">Download Token</param>
        /// <returns>媒體檔案</returns>
        [HttpGet]
        [Route("getfile/{deviceToken}/{mediaId}/{downloadToken}")]
        public HttpResponseMessage GetFile(string deviceToken, int mediaId, string downloadToken)
        {
            HttpResponseMessage result = new PlayerModels().GetFile(deviceToken, mediaId, downloadToken);
            return result;
        }

        /// <summary>
        /// 取得最新的更新檔(兼容舊版Player的URL)
        /// </summary>
        /// <returns>更新檔</returns>
        [HttpGet]
        [Route("getupdatefile")]
        public HttpResponseMessage GetUpdateFile()
        {
            HttpResponseMessage result = new PlayerModels().GetUpdateFile("00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000000");
            return result;
        }

        /// <summary>
        /// 取得最新的更新檔
        /// </summary>
        /// <param name="deviceToken">Device Token</param>
        /// <param name="downloadToken">Download Token</param>
        /// <returns>更新檔</returns>
        [HttpGet]
        [Route("getupdatefile/{deviceToken}/{downloadToken}")]
        public HttpResponseMessage GetUpdateFile(string deviceToken, string downloadToken)
        {
            HttpResponseMessage result = new PlayerModels().GetUpdateFile(deviceToken, downloadToken);
            return result;
        }

        /// <summary>
        /// 更新檔案下載的紀錄
        /// </summary>
        /// <param name="data">DownloadLogData</param>
        /// <returns>ApiResult</returns>
        [HttpPost]
        [Route("uploaddownloadlog")]
        public ApiResult UploadDownloadLog([FromBody] DownloadLogData data)
        {
            ApiResult result = new PlayerModels().UploadDownloadLog(data);
            return result;
        }

        /// <summary>
        /// 更新檔案下載的紀錄
        /// </summary>
        /// <param name="data">DeviceData</param>
        /// <returns>帶有天氣資訊的ApiResult</returns>
        [HttpPost]
        [Route("getweather")]
        public ApiResult GetWeather([FromBody] DeviceData data)
        {
            ApiResult result = new PlayerModels().GetWeather(data);
            return result;
        }
    }
}