﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using DsSite.Models.Modals;


namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/resolution")]
    public class ResolutionController : Base.DsBaseApiController
    {
        /// <summary>
        /// 取得當前所有預設Resolution資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public ApiResult GetAllResolution()
        {
            ResolutionModels ResolutionModels = new ResolutionModels();
            var result = ResolutionModels.GetAllResolution(_UserInfo);
            return result;

        }

        /// <summary>
        /// 新增解析度模版
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public ApiResult AddResolution(ResolutionApiModel Model)
        {
            ResolutionModels ResolutionModels = new ResolutionModels();
            var result = ResolutionModels.AddResolution(Model, _UserInfo);
            return result;

        }

        /// <summary>
        /// 刪除當前選擇該筆解析度資料
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("delete")]
        public ApiResult RemoveResolution(ResolutionApiModel Model)
        {
            ResolutionModels ResolutionModels = new ResolutionModels();
            var result = ResolutionModels.RemoveResolution(Model);
            return result;
        }
    }
}
