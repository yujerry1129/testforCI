﻿using DsDbLib;
using DsDbLib.Models;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DsSite.Controllers.Api.Base
{
    public class DsBaseApiController : ApiController
    {
        protected DsDb _DsDb = new DsDb();

        protected UserInfo _UserInfo;

        protected UserGroup _UserGroup;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _DsDb.Dispose();
            }

            base.Dispose(disposing);
        }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            SettingModels SettingModels = new SettingModels();

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                AccountModels AccountModels = new AccountModels();
                var user = AccountModels.FindUser(HttpContext.Current.User.Identity.Name);

                if (user != null)
                {
                    //取語言
                    var lanCookie = HttpContext.Current.Request.Cookies["lan"];
                    var lanCode = "en-us";

                    if (lanCookie != null)
                    {

                        lanCode = SettingModels.CheckLanguage(lanCookie.Value);
                    }
                    else if (HttpContext.Current.Request.UserLanguages != null)
                    {
                        lanCode = SettingModels.CheckLanguage(HttpContext.Current.Request.UserLanguages[0]);

                        //save language code to cookie
                        HttpContext.Current.Response.Cookies.Set(new HttpCookie("lan", lanCode)
                        {
                            Expires = DateTime.Now.AddYears(10)
                        });
                    }
                    _UserInfo = new UserInfo()
                    {
                        Email = user.Email,
                        Name = user.Name,
                        LastLoginTime = user.LastLoginTime,
                        Id = user.Id,
                        UserGroupId = user.UserGroup.Id,
                        LanguageCode = lanCode
                    };

                    _UserGroup = AccountModels.FindUserGroupId(user.UserGroup.Id);
                }
            }
        }
    }
}