﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models.Modals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/template")]
    public class TemplateController : Base.DsBaseApiController
    {
        /// <summary>
        /// 讀取所有Template模版
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public ApiResult GetAllTemplate()
        {
            TemplateModels TemplateModels = new TemplateModels();
            var result = TemplateModels.GetAllTemplate(_UserInfo);
            return result;

        }

        /// <summary>
        /// 新增當前Template模版
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public ApiResult AddTemplate(TemplateApiModel Model)
        {
            TemplateModels TemplateModels = new TemplateModels();
            var result = TemplateModels.AddTemplate(Model, _UserInfo);
            return result;

        }

        /// <summary>
        /// 刪除當前所選擇Template模版
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("delete")]
        public ApiResult RemoveTemplate(TemplateApiModel Model)
        {
            TemplateModels TemplateModels = new TemplateModels();
            var result = TemplateModels.RemoveTemplate(Model, _UserInfo);
            return result;
        }
    }
}
