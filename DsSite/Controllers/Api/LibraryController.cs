﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using DsSite.Models;
using System.Diagnostics;
using DsSite.Models.Modals;
namespace DsSite.Controllers.Api
{
    /// <summary>
    /// 新增資料夾
    /// </summary>
    /// <returns>回傳新增成功或錯誤訊息</returns>
    [Authorize]
    [RoutePrefix("api/library")]
    public class LibraryController : Base.DsBaseApiController
    {
        LibraryModels LibraryModels = new LibraryModels();

        /// <summary>
        /// 新增資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("folder/add")]
        public ApiResult AddFolder(LibraryApiModel Model)
        {
            var result = new ApiResult();

            if (Model == null || string.IsNullOrEmpty(Model.FolderName))
            {
                return new ApiResult() { Result = false };
            }

            result = LibraryModels.AddFolder(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 取得指定資料夾階層的物件。是資料夾與檔案的結合。
        /// </summary>
        [HttpGet]
        [Route("folder/items/{FolderId:int?}")]
        public ApiResult GetLibraryFolderItems(int? FolderId = null)
        {
            //Check user group
            if (_UserGroup == null)
            {
                return new ApiResult() { Result = false };
            }

            var data = LibraryModels.GetLibraryFolderItems(FolderId, _UserInfo);

            var folderName = data.Folder?.Name;
            var folderId = data.Folder?.Id;

            //return result
            var items = new
            {
                Folders = data.ItemFolders,
                Files = data.ItemFiles,
                Parents = data.Parents,

                //Current folder information
                FolderId = folderId,
                FolderName = folderName,
            };

            return new ApiResult()
            {
                Result = true,
                Data = items
            };
        }

        /// <summary>
        /// 取得圖片
        /// </summary>
        /// <param name="FileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("file-image/{FileId:int}")]
        public HttpResponseMessage GetImage(int FileId)
        {
            //get file data
            var file = LibraryModels.FindFile(FileId, _UserInfo);

            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var path = "";

            //判斷是否為影片
            if (file.FileType == 2)
            {
                path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.HashCode.ToString() + ".png");
            }
            else
            {
                path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.GetSavedFileName());
            }

            //file path
            if (File.Exists(path) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            //設定FileShare以允許多個Client同時對相同檔案存取
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            fs.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fs);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + file.Name + "\"");

            return result;
        }

        /// <summary>
        /// 取圖片縮圖
        /// </summary>
        /// <param name="FileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("file-thumbnail/{FileId:int}")]
        public HttpResponseMessage GetThumbnail(int FileId)
        {
            //get file data
            var file = LibraryModels.FindFile(FileId, _UserInfo);

            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var path = "";

            path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "thumbnail", file.GetSavedFileName());

            //file path
            if (File.Exists(path) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            //設定FileShare以允許多個Client同時對相同檔案存取
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            fs.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fs);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + file.Name + "\"");

            return result;
        }

        /// <summary>
        /// 取得影片
        /// </summary>
        /// <param name="FileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("file-video/{FileId:int}")]
        public HttpResponseMessage GetVideo(int FileId)
        {
            //get file data
            var file = LibraryModels.FindFile(FileId, _UserInfo);

            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.GetSavedFileName());

            //file path
            if (File.Exists(path) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            //設定FileShare以允許多個Client同時對相同檔案存取
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            fs.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fs);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + file.Name + "\"");

            return result;
        }

        /// <summary>
        /// 取得flash
        /// </summary>
        /// <param name="FileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("file-flash/{FileId:int}")]
        public HttpResponseMessage Getflash(int FileId)
        {
            //get file data
            var file = LibraryModels.FindFile(FileId, _UserInfo);

            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.GetSavedFileName());

            if (File.Exists(path) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            //設定FileShare以允許多個Client同時對相同檔案存取
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            fs.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fs);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;

        }

        /// <summary>
        /// 取得PDF
        /// </summary>
        /// <param name="FileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("file-pdf/{FileId:int}")]
        public HttpResponseMessage GetPDF(int FileId)
        {
            //get file data
            var file = LibraryModels.FindFile(FileId, _UserInfo);

            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var path = Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.GetSavedFileName());

            if (File.Exists(path) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            //設定FileShare以允許多個Client同時對相同檔案存取
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)
            {
                Position = 0
            };

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fs);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + file.Name + "\"");

            return result;

        }

        [HttpPost]
        [Route("all-file-path")]
        public List<Library_FileModels.FilesInfo> AllFilePath(Library_FileModels Model)
        {
            var Result = new List<Library_FileModels.FilesInfo>();
            if (Model == null)
            {
                return Result;
            }
            else
            {
                foreach (var f in Model.AllFile)
                {

                    var content = "";
                    bool flag = true;
                    string mediaFile = WebConfigurationManager.AppSettings["SiteRoot"] + "/api/library/file-video/";
                    string flashFile = WebConfigurationManager.AppSettings["SiteRoot"] + "/api/library/file-flash/";
                    string imageFile = WebConfigurationManager.AppSettings["SiteRoot"] + "/api/library/file-image/";
                    string pdfFile = WebConfigurationManager.AppSettings["SiteRoot"] + "/api/library/file-pdf/";
                    TextAsset textAssetFile = new TextAsset() { };
                    TextAssetApiModel TextAssetApiModel = new TextAssetApiModel();
                    TextAssetModels TextAssetModels = new TextAssetModels();

                    //確認檔案是否存在
                    if (f.FileType == 1 || f.FileType == 2 || f.FileType == 3 || f.FileType == 9 || f.FileType==17 ||f.FileType == 18)
                    {

                        var file = LibraryModels.FindFile(f.FileId, _UserInfo);
                        //判別圖檔取縮圖
                        var path = f.FileType == 1 ? Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "thumbnail", file.GetSavedFileName())
                            : Path.Combine(WebConfigurationManager.AppSettings["UploadFolder"], _UserInfo.UserGroupId.ToString(), "origin-files", file.GetSavedFileName());
                        if (File.Exists(path) == true)
                        {
                            //設定FileShare以允許多個Client同時對相同檔案存取
                            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                        }
                        else
                        {
                            flag = false;
                        }
                        if(f.FileType == 2 && file.Size >= 20971520)
                            flag = false;

                    }
                    else if (f.FileType == 4 || f.FileType == 5 || f.FileType == 6 || f.FileType == 8)
                    {
                        textAssetFile = TextAssetModels.FindTextAssetFile(f.FileId, _UserInfo);
                        if (textAssetFile == null)
                        {
                            flag = false;
                        }

                    }
                    else if (f.FileType == 7)
                    {
                        textAssetFile = TextAssetModels.FindTextAssetFile(f.FileId, _UserInfo);
                        if (textAssetFile == null)
                        {
                            flag = false;
                        }
                    }

                    //End 確認檔案是否存在

                    switch (f.FileType)
                    {

                        case 1:  //image  
                            if (flag)
                                content = imageFile + f.FileId;
                            Result.Add(new Library_FileModels.FilesInfo()
                            {

                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });
                            break;
                        case 2:  //video  
                        case 3:  //audio                                
                            if (flag)
                                content = mediaFile + f.FileId;
                            Result.Add(new Library_FileModels.FilesInfo()
                            {

                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });
                            break;
                        case 4: //text
                        case 5: //web page
                        case 6: //marquee
                        case 8: //youtube
                            if (flag)
                            {
                                content = textAssetFile.Content;

                                if (f.FileType == 4)
                                    content = HttpUtility.HtmlDecode(content);
                            }

                            Result.Add(new Library_FileModels.FilesInfo()
                            {
                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });
                            break;
                        case 7: //rss
                            if (flag)
                            {
                                TextAssetApiModel.RssData = TextAssetModels.GetRssData(f.FileId).RssData;
                                content = textAssetFile.Content;
                            }

                            Result.Add(new Library_FileModels.FilesInfo()
                            {
                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = flag ? TextAssetApiModel.RssData : null
                            });
                            break;
                        case 9:  //flash                   
                            if (flag)
                                content = flashFile + f.FileId;
                            Result.Add(new Library_FileModels.FilesInfo()
                            {

                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });
                            break;
                        case 17:
                            Result.Add(new Library_FileModels.FilesInfo()
                            {
                                FileId = f.FileId,
                                Content = null,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });
                            break;
                        case 18:
                            if (flag)
                                content = pdfFile + f.FileId;
                            Result.Add(new Library_FileModels.FilesInfo()
                            {
                                FileId = f.FileId,
                                Content = content,
                                Index = f.Index,
                                FrameIndex = f.FrameIndex,
                                FileType = f.FileType,
                                RssContent = null
                            });

                            break;
                    }

                }

                return Result;
            }
        }


        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("file/delete")]
        public ApiResult DeleteFile(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteFile(Model.FileId, _UserInfo);

            return result;
        }

        /// <summary>
        /// 檔案被使用清單
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("file/usedfile")]
        public ApiResult FileUsedList(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.FileUsedList(Model.FileId, _UserInfo);

            return result;
        }

        /// <summary>
        /// 刪除資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("folder/delete")]
        public ApiResult DeleteFolder(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteFolder(Model.FolderId, _UserInfo);

            return result;
        }

        [HttpPost]
        [Route("folder/usedfolder")]
        public ApiResult FolderUsedList(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.FolderUsedList(Model.FolderId, _UserInfo);

            return result;
        }

        /// <summary>
        /// 刪除多選資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("delete-all")]
        public ApiResult DeleteAll(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteAll(Model, _UserInfo);

            return result;
        }

        [HttpPost]
        [Route("delete-all-fileList")]
        public ApiResult DeleteAllFileList(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteAllFileList(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 取得資料夾底下的檔案或資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("folder-files")]
        public ApiResult GetFolderFiles(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.GetFolderFiles(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 編輯檔案
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("file/edit")]
        public ApiResult EditFile(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.EditFile(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 編輯資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("folder/edit")]
        public ApiResult EditFolder(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.EditFolder(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 拖曳檔案至資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("file/dragFile")]
        public ApiResult DragFile(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DragFile(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 移動檔案(資料樹)
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("file/moveFile")]
        public ApiResult moveFile(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.MoveFile(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 移動資料夾(資料樹)
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("folder/moveFolders")]
        public ApiResult moveFolders(LibraryApiModel Model)
        {
            //判斷FolderList, FoleIdList是否為空
            if (Model.FolderIdList == null && Model.FileIdList == null)
            {
                return new NotFoundResult();
            }

            var result = new ApiResult();

            result = LibraryModels.MoveFolder(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 貼上檔案或資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("pastefoldersorfiles")]
        public ApiResult PasteFoldersorFiles(LibraryApiModel Model)
        {
            var result = new ApiResult();

            result = LibraryModels.PasteFoldersorFiles(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 取得所有Menu
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getallmenu")]
        public ApiResult GetAllMenu()
        {
            var result = new ApiResult();

            result = LibraryModels.GetAllMenu(_UserInfo);

            return result;
        }

        /// <summary>
        /// 刪除菜單
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletemenu")]
        public ApiResult DeleteMenu(MenuInfo Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteMenu(Model, _UserInfo);

            return result;
        }

        /// <summary>
        /// 取得所有倒數計時器
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getalltimer")]
        public ApiResult GetAllTimer()
        {
            var result = new ApiResult();

            result = LibraryModels.GetAllTimer(_UserInfo);

            return result;
        }

        /// <summary>
        /// 新增倒數計時器
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addtimer")]
        public ApiResult AddTimer(TimerInfo Model)
        {
            var result = new ApiResult();

            result = LibraryModels.AddTimer(Model,_UserInfo);

            return result;
        }

        [HttpPost]
        [Route("deletetimer")]
        public ApiResult DeleteTimer(TimerInfo Model)
        {
            var result = new ApiResult();

            result = LibraryModels.DeleteTimer(Model, _UserInfo);

            return result;
        }
    }
}

