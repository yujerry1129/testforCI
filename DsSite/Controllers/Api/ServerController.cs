﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DsSite.Controllers.Api
{
    [RoutePrefix("api/server")]
    public class ServerController : ApiController
    {
        [Route("echo")]
        [HttpGet]
        public string Echo()
        {
            return "ok";
        }
    }
}
