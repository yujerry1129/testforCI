﻿using DsDbLib;
using DsDbLib.Constants;
using DsDbLib.Models;
using DsKit.Extensions;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using DsSite.Models;
using System.Diagnostics;
using DsSite.Models.Modals;
using DsSite.Models.ViewModels.Modals;
using System.Web.Security;
using WebApi.OutputCache.V2;

namespace DsSite.Controllers.Api
{
    [Authorize]
    public class SettingController : Base.DsBaseApiController
    {
        [HttpPost]
        [Route("api/savesetting")]
        public ApiResult Save(SettingModalViewModel setting)
        {
            SettingModels SettingModels = new SettingModels();
            //save language setting to cookie
            HttpCookie cookie = new HttpCookie("lan", SettingModels.CheckLanguage(setting.LanguageCode));
            cookie.Expires = DateTime.Now.AddYears(10);
            HttpContext.Current.Response.Cookies.Set(cookie);

            //Update password
            if (string.IsNullOrEmpty(setting.Password) == false)
            {
                if (!setting.Password.Equals(setting.RepeatPasssword))
                {
                    return new ExceptionResult();
                }
                //使用者是否登入完成
                if (User.Identity.IsAuthenticated)
                {
                    // Get user email from cookie
                    var userEmail = (User.Identity as FormsIdentity).Ticket.Name;
                    AccountModels AccountModels = new AccountModels();
                    var user = AccountModels.FindUser(userEmail);
                    if (user != null)
                    {
                        AccountModels.updatePassword(user, setting.Password);

                    }
                }
            }

            return new SuccessResult();
        }
    }
}