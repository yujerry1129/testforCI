﻿using DsSite.App_GlobalResources;
using DsSite.Controllers.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Resources;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using WebApi.OutputCache.V2;

namespace DsSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/lan")]
    //server and client cache 120 sec
    [CacheOutput(ServerTimeSpan = 120, ClientTimeSpan = 120)]
    public class TranslateController : Base.DsBaseApiController
    {
        [HttpGet]
        [Route("get")]
        public IHttpActionResult GetResources()
        {
            string[] lanCode = { "en-us", "zh-tw", "zh-cn" };

            var en = new Dictionary<string, string>();
            var zhTW = new Dictionary<string, string>();
            var zhCN = new Dictionary<string, string>();

            List<Dictionary<string, string>> multiLanResult = new List<Dictionary<string, string>>();
            foreach (var lan in lanCode)
            {
                var resourceSet = MultiLan.ResourceManager.GetResourceSet(new CultureInfo(lan), true, true);
                var errorMsgResourceSet = MultiLanErrorMsg.ResourceManager.GetResourceSet(new CultureInfo(lan), true, true);
                IDictionaryEnumerator enumerator = resourceSet.GetEnumerator();
                IDictionaryEnumerator errorMsgEnumerator = errorMsgResourceSet.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (lan == "en-us")
                        en.Add(enumerator.Key.ToString(), enumerator.Value.ToString());
                    else if (lan == "zh-tw")
                        zhTW.Add(enumerator.Key.ToString(), enumerator.Value.ToString());
                    else if (lan == "zh-cn")
                        zhCN.Add(enumerator.Key.ToString(), enumerator.Value.ToString());
                }
                while (errorMsgEnumerator.MoveNext())
                {
                    if (lan == "en-us")
                        en.Add(errorMsgEnumerator.Key.ToString(), errorMsgEnumerator.Value.ToString());
                    else if (lan == "zh-tw")
                        zhTW.Add(errorMsgEnumerator.Key.ToString(), errorMsgEnumerator.Value.ToString());
                    else if (lan == "zh-cn")
                        zhCN.Add(errorMsgEnumerator.Key.ToString(), errorMsgEnumerator.Value.ToString());
                }
            }

            multiLanResult.Add(en);
            multiLanResult.Add(zhTW);
            multiLanResult.Add(zhCN);


            //return new SuccessResult(Data: multiLanResult);
            return Ok(multiLanResult);
        }
    }
}
