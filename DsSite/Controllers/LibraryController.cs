﻿using DsDbLib.Constants;
using DsSite.Controllers.Api.Models;
using DsSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using DsSite.Models;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class LibraryController : Base.DsBaseController
    {
        private const string _FFmpegPath = "C:/DsUpload/ffmpeg.exe";

        // GET: Library
        public ActionResult Index()
        {
            var model = new LibraryViewModel()
            {
                FileSizeLimit = int.Parse(WebConfigurationManager.AppSettings["FileSizeLimit"]),
                SupportFileExt = "['.mp4','.wmv','.avi','.bmp','.gif','.jpg','.jpeg','.png','.mp3','.swf']"
            };

            return View(model);
        }

        /// <summary>
        /// 檔案上傳
        /// </summary>
        /// <param name="FolderId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadAjax(int? FolderId)
        {
            LibraryModels LibraryModels = new LibraryModels();
            var result = LibraryModels.UploadAjax(FolderId, _UserInfo, Request);
            // Checking no of files injected in Request object  
            return result;
        }

    }
}