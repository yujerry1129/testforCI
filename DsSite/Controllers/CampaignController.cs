﻿using DsSite.Models;
using DsSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class CampaignController : Base.DsBaseController
    {
        // GET: Campaign
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Editor(int? id)
        {
            CampaignModels CampaignModels = new CampaignModels();
            var model = CampaignModels.CampaignEditor(id, _UserInfo);
            if (model.HideEditor)
                return RedirectToAction("Index");
            else
            {
                return View(model);
            }
        }

    }
}