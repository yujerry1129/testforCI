﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Controllers
{

    [Authorize]
    [Filters.DsAuthFilter]

    public class LogInfoController : Base.DsBaseController
    {
        // GET: LogInfo
        public ActionResult Index()
        {
            return View();
        }
    }
}