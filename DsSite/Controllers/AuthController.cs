﻿using DsSite.Models.ViewModels;
using DsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class AuthController : Base.DsBaseController
    {
        /// <summary>
        /// 限制使用者權限是否可以看到的內容
        /// </summary>
        /// <returns></returns>
        // GET: Auth
        public ActionResult Index()
        {
            AuthModels AuthModels = new AuthModels();
            var FilterResult = AuthModels.AuthFilter(_UserInfo);
            return View(FilterResult);
        }

        //public ActionResult AccessDeny()
        //{
        //    return Content("<div style='text-align: center; margin-top: 40px'>Access denied</div>");
        //}
    }
}