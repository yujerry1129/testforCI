﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class ChartController : Base.DsBaseController
    {
        // GET: Chart
        public ActionResult Index()
        {
            return View();
        }
    }
}