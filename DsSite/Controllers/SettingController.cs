﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DsSite.Models;
namespace DsSite.Controllers
{
    [Authorize]
    public class SettingController : Controller
    {
        /// <summary>
        /// setting 更換密碼及語系設定
        /// </summary>
        /// <param name="language"></param>
        /// <param name="Password"></param>
        /// <param name="PasswordConfirm"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(string language, string Password, string PasswordConfirm)
        {
            SettingModels SettingModels = new SettingModels();
            //save language setting to cookie
            HttpCookie cookie = new HttpCookie("lan", SettingModels.CheckLanguage(language));

            cookie.Expires = DateTime.Now.AddYears(10);

            Response.SetCookie(cookie);

            //Update password
            if (string.IsNullOrEmpty(Password) == false)
            {
                if (!Password.Equals(PasswordConfirm))
                {
                    //TempData["HelloWorld"] = "hello world";
                    TempData["SETTING_ERROR_MESSAGE"] = "密碼不一致，請重新輸入";
                }
                else
                {
                    //使用者是否登入完成
                    if (User.Identity.IsAuthenticated)
                    {
                        //Get user email from cookie
                        var userEmail = (User.Identity as FormsIdentity).Ticket.Name;

                        AccountModels AccountModels = new AccountModels();
                        var user = AccountModels.FindUser(userEmail);

                        if (user != null)
                        {
                            AccountModels.updatePassword(user, Password);
                            TempData["SETTING_ERROR_MESSAGE"] = "密碼已更新";
                        }
                    }
                }
            }

            return Redirect(Request.UrlReferrer.PathAndQuery);
        }
    }
}