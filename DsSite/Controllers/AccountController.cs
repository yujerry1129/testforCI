﻿using DsDbLib;
using DsKit.Extensions;
using DsSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DsKit.Helpers;
using DsSite.Models;
using System.Web.Routing;

namespace DsSite.Controllers
{
    public class AccountController : Base.DsBaseController
    {

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        [Filters.DsAuthFilter]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Login page
        /// </summary>
        /// <returns>轉換頁面</returns>
        [HttpGet]
        public ActionResult LogIn()
        {
            License License = new License();
            var clientValidated = License.CheckClient();
            if (clientValidated.Result)
            {
                return View();
            }
            else
            {
                return new RedirectToRouteResult(new RouteValueDictionary {
                        { "controller", "Errors" },
                        { "action", "LicenseError" }
                    });
            }
        }

        /// <summary>
        /// 使用者登入驗證
        /// </summary>
        /// <param name="Account">帳戶</param>
        /// <param name="Password">密碼</param>
        /// <param name="ReturnUrl">回傳url</param>
        /// <returns>轉換至主頁面</returns>
        [HttpPost]
        public ActionResult LogIn(string Account, string Password, string ReturnUrl)
        {
            AccountModels AccountModels = new AccountModels();
            var user = AccountModels.FindUser(Account);

            if (user != null && Password == user.HashedPassword)
            {
                //Login success
                AccountModels.updateLoginInfo(user);

                //Create cookie token
                var ticket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, DateTime.Now.Add(FormsAuthentication.Timeout), true, "user_data");
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
                {
                    HttpOnly = true,
                };

                Response.Cookies.Add(cookie);

                if (!string.IsNullOrEmpty(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            //Login fail
            ViewBag.ErrorMessage = "Login failed.";

            return View();
        }

    }
}