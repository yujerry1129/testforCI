﻿using DsDbLib;
using DsSite.Models;
using DsSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace DsSite.Controllers.Base
{
    public class DsBaseController : Controller
    {
        /// <summary>
        /// Digital signage database
        /// </summary>
        protected DsDb _DsDb = new DsDb();

        /// <summary>
        /// 登入者基本資訊
        /// </summary>
        protected UserInfo _UserInfo;

        public int UserId
        {
            get
            {
                return _UserInfo != null ? _UserInfo.Id : 0;
            }
        }

        public DsDb Db
        {
            get { return _DsDb; }
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            //取得登入者基本資訊
            _UserInfo = this.GetUserInfo();

            //把登入者基本資訊存入ViewBag
            ViewBag.UserInfo = _UserInfo;

            //Get Controller Name
            ViewBag.ControllerName = requestContext.RouteData.Values["controller"];

            //取得user的menu
            ViewBag.Menu = GetUserMenu();

            ViewBag.signOut = _UserInfo.LanguageCode == "en-us" ? "Sign out" : _UserInfo.LanguageCode == "zh-tw" ? "登出" : "登出";
            ViewBag.setting = _UserInfo.LanguageCode == "en-us" ? "Setting" : _UserInfo.LanguageCode == "zh-tw" ? "設定" : "设定";
        }

        /// <summary>
        /// 主頁面初始時需要有的 menu 清單
        /// </summary>
        /// <returns>回傳含語系的menu 清單</returns>
        private IEnumerable<MenuViewModel> GetUserMenu()
        {
            if (_UserInfo.IsLogin)
            {
                DsBaseModels DsBaseModels = new DsBaseModels();

                //取使用者所有可用menu
                var userMenu = DsBaseModels.GetUserMenu(_UserInfo);

                return userMenu;
            }

            return null;
        }

        /// <summary>
        /// 取得使用者的資訊並儲存使用者語系及設定cookie
        /// </summary>
        /// <returns>回傳使用者的資訊並儲存使用者語系及設定cookie</returns>
        private UserInfo GetUserInfo()
        {
            var info = new UserInfo();
            SettingModels SettingModels = new SettingModels();

            //使用者是否登入完成

            if (User.Identity.IsAuthenticated)
            {
                //Get user email from cookie
                var userEmail = (User.Identity as FormsIdentity).Ticket.Name;

                AccountModels AccountModels = new AccountModels();

                //Get user information from database
                var user = AccountModels.FindUser(userEmail);
                //var user = _DsDb.User.FirstOrDefault(o => o.Email == userEmail);

                if (user != null)
                {
                    info = new UserInfo()
                    {
                        Email = user.Email,
                        Id = user.Id,
                        IsLogin = true,
                        LastLoginTime = user.LastLoginTime,
                        Name = user.Name,
                        UserGroupId = user.UserGroupId
                    };
                }
            }

            //取語言
            var lanCookie = Request.Cookies["lan"];
            var lanCode = "en-us";

            if (lanCookie != null)
            {

                lanCode = SettingModels.CheckLanguage(lanCookie.Value);
            }
            else if (Request.UserLanguages != null)
            {
                lanCode = SettingModels.CheckLanguage(Request.UserLanguages[0]);

                //save language code to cookie
                Response.Cookies.Set(new HttpCookie("lan", lanCode)
                {
                    Expires = DateTime.Now.AddYears(10)
                });
            }

            info.LanguageCode = lanCode;

            return info;
        }


        protected override void Dispose(bool disposing)
        {
            _DsDb.Dispose();
            base.Dispose(disposing);
        }
    }
}