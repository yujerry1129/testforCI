﻿using DsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Controllers
{
    [Authorize]
    public class MultiLanController : Base.DsBaseController
    {
        // GET: MultiLan
        public ActionResult Index()
        {
            SettingModels SettingModels = new SettingModels();
            string controllerText = SettingModels.LText(_UserInfo, "save");
            ViewBag.ControllerText = controllerText;
            return View();
        }

        /// <summary>
        /// 將語系碼儲存至cookie中(目前未有參考使用)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveLan(string code)
        {
            //將語系設定存入cookie
            HttpCookie cookie = new HttpCookie("lan", code);

            cookie.Expires = DateTime.Now.AddYears(10);

            Response.SetCookie(cookie);

            return RedirectToAction("Index");
        }
    }
}