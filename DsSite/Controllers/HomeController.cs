﻿using DsSite.Models.Modals;
using System.Web.Mvc;

namespace DsSite.Controllers
{
    [Authorize]
    [Filters.DsAuthFilter]
    public class HomeController : Base.DsBaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}