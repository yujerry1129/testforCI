require('es6-promise').polyfill()
import Vue from 'vue'
import MediaLibrary from './components/media-library/media-library-index.vue'
import DeviceIndex from './components/device/device-index.vue'
import DeviceDetail from './components/device/device-detail.vue'
// import CampaignEditor from './components/campaign/editor.vue'
import CampaignIndex from './components/campaign/campaign-index.vue'
import TemplateEditor from './components/campaign/template-editor.vue'
import LogInfoIndex from './components/log-info/log-info-index.vue'
import AccountManagement from './components/account/account-management.vue'
import ScreenGroupDetail from './components/device/screen-group/screen-group-detail.vue'
import HomeIndex from './components/home/home-index.vue'
import SettingModal from './components/controls/modal/setting-modal.vue'
import LogoutModal from './components/controls/modal/logout-modal.vue'
import axios from 'axios'
import VueI18n from 'vue-i18n'
import ToolLibrary from './components/media-library/tool-library-index.vue'

Vue.config.silent = true
Vue.use(VueI18n)

var en,
    zhTW,
    zhCN

var p1 = new Promise((resolve, reject) => {
    axios.get('/ace/api/lan/get')
        .then(function (response) {
            if (!response.data)
                reject('translate file loading fail')
            resolve(response.data)
        })
}).then(values => {
    let messages = {
        "en-us": {
            message: values[0]
        },
        "zh-tw": {
            message: values[1]
        },
        "zh-cn": {
            message: values[2]
        }
    }
    // Create VueI18n instance with options
    const i18n = new VueI18n({
        locale: __culture, // set locale
        fallbackLocale: 'en-us',
        messages, // set locale messages
    })
    // Create a Vue instanc with `i18n` option
    new Vue({
        el: '#app',
        i18n,
        // render: h => h(App)
        components: {
            MediaLibrary,
            TemplateEditor,
            CampaignIndex,
            DeviceDetail,
            DeviceIndex,
            LogInfoIndex,
            AccountManagement,
            ScreenGroupDetail,
            SettingModal,
            HomeIndex,
            ToolLibrary,
            LogoutModal
        }
    }).$mount('#app')

    new Vue({
        el: '#settingVue',
        i18n,
        components: {
            SettingModal,
            LogoutModal
        }
    }).$mount('#settingVue')
}).catch(reason => {
    console.log(reason)
});