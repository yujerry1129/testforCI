﻿using DsSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json;
using DsSite.Controllers;
using DsKit.Helpers;

namespace DsSite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //讀取語言檔
            _InitLanguageData();
        }
        protected void Application_Error()
        {
            var exception = Server.GetLastError();
            var httpException = exception as HttpException;
            Response.Clear();
            Server.ClearError();
            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = "InternalError";
            routeData.Values["exception"] = exception;
            Response.StatusCode = 500;
            //if (httpException != null)
            //{
            //    Response.StatusCode = httpException.GetHttpCode();
            //    switch (Response.StatusCode)
            //    {
            //        case 403:
            //            routeData.Values["action"] = "Http403";
            //            break;
            //        case 404:
            //            routeData.Values["action"] = "Http404";
            //            break;
            //    }
            //}

            IController errorsController = new ErrorsController();
            var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            errorsController.Execute(rc);
        }

        private void _InitLanguageData()
        {
            //讀取lan.js語言設定檔
            var str = File.ReadAllText(Server.MapPath("lan.js"));

            //lan.js中，語言設定字串的開始位置
            var start = str.IndexOf('[');

            //結束位置
            var end = str.LastIndexOf(']');

            //將字串轉換成LanSet串列物件
            var lanJSON = JsonConvert.DeserializeObject<List<LanSet>>(str.Substring(start, end - start + 1));

            //把所有文字放進字典中(以id做鍵)
            var allLang = new Dictionary<string, Lan[]>();

            foreach (var lan in lanJSON)
            {
                allLang.Add(lan.id, lan.lan);
            }

            //把文字資料存入Application全域變數中
            HttpContext.Current.Application.Lock();

            HttpContext.Current.Application["lan_data"] = allLang;

            HttpContext.Current.Application.UnLock();
        }
    }
}
