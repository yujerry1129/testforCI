﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DsSite.Controllers.Base;
using DsSite.Models;
using DsKit.Helpers;
using System.Web.Security;

namespace DsSite.Filters
{
    /// <summary>
    /// 檢查user是否有權限存取controller，若不通過就進入 AccessDeny
    /// </summary>
    public class DsAuthFilter : ActionFilterAttribute, IActionFilter
    {

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                DsAuthFilterModels DsAuthFilterModels = new DsAuthFilterModels();
                var c = ((DsBaseController)filterContext.Controller);
                var uid = c.UserId;


                //檢查user是否有權限存取controller
                var controllerName = filterContext.RouteData.Values["controller"].ToString();
                var check = DsAuthFilterModels.CheckAuth(uid, controllerName);


                if (!check)
                {
                    //Rediect to error page
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "controller", "Errors" },
                        { "action", "InternalError" }
                    });
                }

                //檢查驗證license檔案合法性
                License License = new License();
                var licenseVerified = License.CheckClient();
                if (!licenseVerified.Result)
                {
                    FormsAuthentication.SignOut();
                    //Rediect to LicenseError page
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "controller", "Errors" },
                        { "action", "LicenseError" }
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}