﻿using DsDbLib.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace DsSite.Models
{
    public class WeatherModel : Controllers.Base.DsBaseController
    {
        public WeatherData CheckAndGetWeatherData(int locationId)
        {
            WeatherData weatherData = _DsDb.WeatherData.SingleOrDefault(o => o.LocationId == locationId);

            //若無資料則新增，若資料過期則以新的覆蓋
            if (weatherData == null)
            {
                weatherData = GetNewWeatherData(locationId);

                _DsDb.WeatherData.Add(weatherData);
                _DsDb.SaveChanges();
            }
            else if (DateTime.Now > UnixTimeToDateTime(weatherData.DataUnixTime).AddHours(2))
            {
                WeatherData newData = GetNewWeatherData(locationId);
                
                weatherData.DataUnixTime = newData.DataUnixTime;
                weatherData.Description = newData.Description;
                weatherData.Icon = newData.Icon;
                weatherData.Temp = newData.Temp;
                weatherData.TempMax = newData.TempMax;
                weatherData.TempMin = newData.TempMin;
                weatherData.Humidity = newData.Humidity;
                weatherData.Pressure = newData.Pressure;
                weatherData.WindDeg = newData.WindDeg;
                weatherData.WindSpeed = newData.WindSpeed;
                weatherData.Clouds = newData.Clouds;
                weatherData.SunriseUnixTime = newData.SunriseUnixTime;
                weatherData.SunsetUnixTime = newData.SunsetUnixTime;
                _DsDb.SaveChanges();
            }

            return weatherData;
        }

        private WeatherData GetNewWeatherData(int locationId)
        {
            Location location = _DsDb.Location.SingleOrDefault(o => o.Id == locationId);
            if (location == null)
            {
                location = new Location { Id = -1, Latitude = 20.05f, Longitude = 121.53f }; //預設座標為台北
            }

            WeatherOriginData originData = GetWeatherOriginData(location.Latitude, location.Longitude);
            WeatherData result = ConvertToWeatherData(location.Id, originData);
            return result;
        }

        private WeatherOriginData GetWeatherOriginData(float lat, float lon)
        {
            string apiKey = "71f3f765eee8c3543558c4ae7fdf317a"; //測試用的私人Key
            WeatherOriginData result = new HttpGetWeatherOriginData(lat, lon, apiKey).Request().Data;
            if (result == null)
            {
                WeatherOriginData default_weatherData = new WeatherOriginData()
                {
                    weather = new Weather[] { new Weather() { description = "No found", icon = "01d" } },
                    main = new Main { temp = 0, temp_max = 0, temp_min = 0, humidity = 0, pressure = 0 },
                    wind = new Wind { deg = null, speed = 0 },
                    clouds = new Clouds { all = 0 },
                    dt = 0,
                    sys = new Sys { sunrise = 0, sunset = 0 }
                };
                result = default_weatherData;
            }

            return result;
        }

        private WeatherData ConvertToWeatherData(int locationId, WeatherOriginData originData)
        {
            WeatherData result = new WeatherData
            {
                LocationId = locationId,
                DataUnixTime = originData.dt,
                Description = originData.weather[0].description,
                Icon = originData.weather[0].icon,
                Temp = originData.main.temp,
                TempMax = originData.main.temp_max,
                TempMin = originData.main.temp_min,
                Humidity = originData.main.humidity,
                Pressure = originData.main.pressure,
                WindDeg = originData.wind.deg,
                WindSpeed = originData.wind.speed,
                Clouds = originData.clouds.all,
                SunriseUnixTime = originData.sys.sunrise,
                SunsetUnixTime = originData.sys.sunset
            };

            return result;
        }

        private DateTime UnixTimeToDateTime(long unixTime)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTime).ToLocalTime();
        }
    }

    #region WeatherOriginData Class
    public class WeatherOriginData
    {
        public Coord coord { get; set; }
        public Weather[] weather { get; set; }
        public string _base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Rain rain { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public Sys sys { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }

    public class Coord
    {
        public float lon { get; set; }
        public float lat { get; set; }
    }

    public class Main
    {
        public float temp { get; set; }
        public float pressure { get; set; }
        public int humidity { get; set; }
        public float temp_min { get; set; }
        public float temp_max { get; set; }
        public float sea_level { get; set; }
        public float grnd_level { get; set; }
    }

    public class Wind
    {
        public float speed { get; set; }
        public float? deg { get; set; }
    }

    public class Rain
    {
        public float _3h { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class Sys
    {
        public float message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
    #endregion

    #region Http Call Api
    public class ApiResult<T>
    {
        public bool Result { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }

    /// <summary>
    /// Http溝通的基礎設定
    /// </summary>
    public abstract class HttpBase
    {
        protected const int HTTP_TIMEOUT = 10 * 1000;
    }

    /// <summary>
    /// Http 的 Get函數與設定
    /// </summary>
    public abstract class HttpGet : HttpBase
    {
        protected async Task<ApiResult<T>> Get<T>(Uri api, bool isAsync = true)
        {
            //呼叫測試連線API
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            ApiResult<T> apiResult = new ApiResult<T>();

            try
            {
                request = (HttpWebRequest)WebRequest.Create(api);
                request.Method = "GET";
                request.Timeout = HTTP_TIMEOUT;
                if (isAsync)
                {
                    response = (HttpWebResponse)await request.GetResponseAsync();
                }
                else
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                Stream responseStream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream);
                apiResult.Data = JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd());
                apiResult.Result = true;
            }
            catch
            {
                apiResult.Result = false;
            }
            finally
            {
                if (request is HttpWebRequest)
                {
                    request.Abort();
                }

                if (response is HttpWebResponse)
                {
                    response.Close();
                    response.Dispose();
                }
            }

            return apiResult;
        }
    }

    public class HttpGetWeatherOriginData : HttpGet
    {
        Uri API;
        public HttpGetWeatherOriginData(float lat, float lon, string apiKey)
        {
            API = new Uri("http://api.openweathermap.org/data/2.5/weather?"
                + "lat=" + lat + "&lon=" + lon
                + "&cluster=yes&format=json"
                + "&units=metric"
                + "&APPID=" + apiKey);
        }

        public ApiResult<WeatherOriginData> Request()
        {
            return Get<WeatherOriginData>(API, false).Result;
        }
    }
    #endregion
}