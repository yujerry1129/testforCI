﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Models
{
    public class SettingModels : Controller
    {
        /// <summary>
        /// 確認語系
        /// </summary>
        /// <param name="language">語言碼</param>
        /// <returns>回傳語系</returns>
        public string CheckLanguage(string language)
        {
            if (string.IsNullOrEmpty(language))
            {
                return "en-us";
            }

            var lan = language.Trim().ToLower();

            switch (lan)
            {
                case "en-us":
                    return "en-us";
                case "zh-tw":
                    return "zh-tw";
                case "zh-cn":
                    return "zh-cn";
            }

            return "en-us";
        }

        /// <summary>
        /// 用Culture篩選出指定文字
        /// </summary>
        /// <param name="LanId">language code</param>
        /// <returns>回傳對應的文字</returns>
        public string LText(UserInfo _UserInfo, string LanId)
        {
            try
            {
                //依照LanId找到文字組
                var s = HttpContext.Application["lan_data"] as Dictionary<string, Lan[]>;

                //用Culture篩選出指定文字
                return s[LanId].FirstOrDefault(o => o.code == _UserInfo.LanguageCode).text;
            }
            catch
            {
                return "";
            }
        }
    }
}