﻿using DsDbLib.Models;
using DsSite.Models.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class AuthModels : Controllers.Base.DsBaseController
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        public AuthorizationManagementViewModel AuthFilter(UserInfo _UserInfo)
        {
            var roles = from role in _DsDb.SiteRole
                        join text in _DsDb.SiteText on role.LanKey equals text.LanKey
                        where text.CultureCode == _UserInfo.LanguageCode
                        select new SiteRoleViewModel()
                        {
                            Id = role.Id,
                            Name = role.Name,
                            Description = text.LanText,
                            Menus = _DsDb.SiteRoleToMenu.Where(o => o.SiteRole_Id == role.Id).Select(o => new MenuViewModel()
                            {
                                Action = o.Menu.ActionName,
                                Controller = o.Menu.ControllerName
                            }).ToList()
                        };

            var users = _DsDb.User.Where(o => o.DeleteByAdminUserId == null).Select(o => new SiteUserViewModel()
            {
                Name = o.Name,
                Email = o.Email,
                UserId = o.Id,
                Roles = _DsDb.SiteUserToRole.Where(p => p.User_Id == o.Id).Select(p => new SiteRoleViewModel()
                {
                    Id = p.SiteRole.Id,
                    Name = p.SiteRole.Name
                }).ToList()
            });

            return new AuthorizationManagementViewModel()
            {
                Roles = roles.ToList(),
                Users = users.ToList()
            };

        }
    }
}