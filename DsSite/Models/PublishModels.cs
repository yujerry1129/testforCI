﻿using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class PublishModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得 UserGroup 下面的播放及設備相關資訊
        /// </summary>
        /// <returns>回傳UserGroup下面的 device / device group / campaigns / AgeRangeDefinition</returns>
        public ApiResult GetAvailablePublishAssets(UserInfo _UserInfo)
        {
            //取得 UserGroup下面的 device//取得 device group //取得 campaigns//取得年齡定義

            //取得device 及其tag           
            var findDevices = from d in _DsDb.Device
                              where d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteByUserId == null && d.CreateByUserId != null && d.ScreenGroupId == null
                              join ds in _DsDb.DeviceScreen on d.Id equals ds.DeviceId
                              select new
                              {
                                  Id = d.Id,
                                  Name = d.Name,
                                  Width = ds.Width,
                                  Height = ds.Height
                              };

            List<PublishDeviceApiModel> devices = new List<PublishDeviceApiModel>();

            foreach (var item in findDevices.ToList())
            {
                //找出此 Device 有哪些 tag                
                var tagReslult = _DsDb.DeviceTag
                .Where(o => o.DeviceId == item.Id).Select(o => o.TagName).ToList();

                devices.Add(new PublishDeviceApiModel()
                {
                    Id = item.Id,
                    DeviceName = item.Name,
                    Tag = tagReslult,
                    ResolutionWidth = item.Width,
                    ResolutionHeight = item.Height
                });
            }


            //取得 screen group 
            var findScreenGroups = _DsDb.ScreenGroup
                .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();

            List<PublishScreenGroupApiModel> screenGroups = new List<PublishScreenGroupApiModel>();

            foreach (var item in findScreenGroups)
            {
                //找出 screen group 下面的 device 的tag 
                List<string> saveTempTag = new List<string>();
                var deviceTagReslult = _DsDb.Device.Where(o => o.ScreenGroupId == item.Id).Select(o => o.Id).Distinct().ToList();
                foreach (var d in deviceTagReslult)
                {
                    var TagReslutinDevice = _DsDb.DeviceTag.Where(o => o.DeviceId == d).Select(o => o.TagName).Distinct();

                    saveTempTag.AddRange(TagReslutinDevice);

                }
                //找出此 screen Group的tag 
                var screenGroupTagReslult = _DsDb.DeviceTag.Where(o => o.ScreenGroupId == item.Id).Select(o => o.TagName).Distinct().ToList();
                saveTempTag.AddRange(screenGroupTagReslult);
                screenGroups.Add(new PublishScreenGroupApiModel()
                {
                    ScreenGroupName = item.Name,
                    Id = item.Id,
                    Tag = saveTempTag,
                    HorizontallyScreenCount = item.HorizontallyScreenCount,
                    VerticallyScreenCount = item.VerticallyScreenCount
                });

            }

            //取得 campaigns
            var findCampaigns = _DsDb.Campaign
                .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId).ToList();

            List<PublishCampaignApiModel> campaigns = new List<PublishCampaignApiModel>();

            foreach (var item in findCampaigns)
            {
                //找出此 Device 有哪些 tag                
                var tagReslult = _DsDb.CampaignTag
                .Where(o => o.CampaignId == item.Id).Select(o => o.TagName).ToList();

                campaigns.Add(new PublishCampaignApiModel()
                {
                    Id = item.Id,
                    Duration = item.Duration,
                    Name = item.Name,
                    Tag = tagReslult,
                    Width = item.Width,
                    Height = item.Height
                });
            }

            //取得年齡定義
            var ageRangeDefinitions = _DsDb.AgeRangeDefinition
                .Where(o => o.UserGroup_Id == _UserInfo.UserGroupId)
                .OrderBy(o => o.MinValue)
                .Select(o => new PublishAgeRange()
                {
                    AgeId = o.AgeId,
                    Descripiton = o.Description,
                    MinValue = o.MinValue,
                    MaxValue = o.MaxValue
                })
                .ToList();

            //回傳資料
            var data = new PublishApiModel()
            {
                Screens = devices.ToList(),
                ScreenGroups = screenGroups,
                Campaigns = campaigns.ToList(),
                AgesRanges = ageRangeDefinitions
            };

            return new SuccessResult(Data: data);
        }


        /// <summary>
        /// 取得 Campaigns
        /// </summary>
        /// <returns>回傳可用的Campaigns資訊</returns>
        public ApiResult GetAvailableCampaigns(UserInfo _UserInfo)
        {
            var campaigns = _DsDb.Campaign
                .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId)
                .Select(o => new PublishCampaignApiModel()
                {
                    Id = o.Id,
                    Name = o.Name,
                    Duration = o.Duration
                });

            var data = new PublishApiModel()
            {
                Campaigns = campaigns.ToList()
            };

            return new SuccessResult(Data: data);
        }

        public ApiResult CheckData(PublishApiModel Model)
        {
            if ((Model.Campaigns == null || Model.Campaigns.Count == 0))
            {
                return new ExceptionResult(Message: "Please select campaign");
            }
            if ((Model.Screens == null || Model.Screens.Count == 0) && (Model.ScreenGroups == null && Model.ScreenGroups.Count == 0))
            {
                return new ExceptionResult(Message: "Please select device screen or device group");
            }
            if (Model.FaceRestriction != null)
            {
                if (Model.FaceRestriction.Male == false && Model.FaceRestriction.Female == false)
                {
                    return new ExceptionResult(Message: "Please select gender for face detection restriction");
                }
                if (Model.FaceRestriction.AgeIdList == null || Model.FaceRestriction.AgeIdList.Count == 0)
                {
                    return new ExceptionResult(Message: "Please select age for face detection restriction");
                }
            }
            return new SuccessResult(Data: true);
        }


        /// <summary>
        /// 回傳年齡定義限制
        /// </summary>
        /// <param name="restriction">Restriction</param>
        /// <param name="Model">PublishApiModel</param>
        /// <returns>回傳年齡定義限制</returns>
        public List<AgeToRestriction> ReturnAgeRestrication(Restriction restriction, PublishApiModel Model)
        {
            var face = Model.FaceRestriction;

            if (face.Male && face.Female)
            {
                //Both male & female
                restriction.Gender = 0;
            }
            else if (face.Male)
            {
                //Male
                restriction.Gender = 1;
            }
            else
            {
                //Female
                restriction.Gender = 2;
            }

            var ages = new List<AgeToRestriction>();
            ages.AddRange(Model.FaceRestriction.AgeIdList.Select(o => new AgeToRestriction()
            {
                Age_Id = o,
                Restriction = restriction
            }));
            return ages;
        }


        public Publication ReturnPublishAdd(Publication publication)
        {
            //save publication
            var DbPublish = _DsDb.Publication.Add(publication);

            //save published campaign
            if (publication.Campaigns != null && publication.Campaigns.Count() > 0)
            {
                _DsDb.PublishCampaign.AddRange(publication.Campaigns);
            }

            //save published device screens
            if (publication.Devices != null && publication.Devices.Count() > 0)
            {
                _DsDb.PublishDevice.AddRange(publication.Devices);
            }

            //save published device groups
            if (publication.DeviceGroups != null && publication.DeviceGroups.Count() > 0)
            {
                _DsDb.PublishDeviceGroup.AddRange(publication.DeviceGroups);
            }

            if (publication.Restriction.HasFaceDetectionRule)
            {
                _DsDb.AgeToRestriction.AddRange(publication.Restriction.AgeToRestrictions);
            }
            return DbPublish;
        }

        /// <summary>
        /// 取得此 Device最大的 play order
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="publishedScreen"></param>
        /// <returns></returns>
        public int ReturnDeviceCountbyDeviceId(UserInfo _UserInfo, PublishDevice publishedScreen)
        {
            var playorder = _DsDb.ScheduleSummary.Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeviceId == publishedScreen.DeviceId && o.ScreenGroupId == null && o.PlayOrder > 0)
                .OrderByDescending(o => o.PlayOrder).FirstOrDefault();
            if (playorder == null)
                return 0;
            else
            {
                return playorder.PlayOrder;
            }

        }


        /// <summary>
        /// 取得此screenGroup 最大的 play order
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="publishedGroup"></param>
        /// <returns></returns>
        public int ReturnDeviceCountbyGroupId(UserInfo _UserInfo, PublishScreenGroupApiModel publishedGroup)
        {
            var playorder = _DsDb.ScheduleSummary.Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.ScreenGroupId == publishedGroup.Id && o.DeviceId == null && o.PlayOrder > 0)
               .OrderByDescending(o => o.PlayOrder).FirstOrDefault();
            if (playorder == null)
                return 0;
            else
            {
                return playorder.PlayOrder;
            }
        }


        /// <summary>
        /// 儲存發布至device 或是 screen group 的 資訊(含時間及臉部辨識限制)
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult AddPublication(UserInfo _UserInfo, PublishApiModel Model)
        {
            //Thread.Sleep(2000);

            //check data
            var checkData = CheckData(Model);
            if (!checkData.Result)
            {
                return checkData;
            }
            else
            {

                var publication = ReturnPublication(_UserInfo, Model);

                var restriction = ReturnRestrication(_UserInfo, Model);

                var selectedType = ReturnSelectedType(_UserInfo, Model);


                //日期限制20170515 會議決定 HasCalenderRule false 的情況為全選周一到周日 其餘為true                
                TimeLimit(restriction, Model);

                //臉部辨識限制
                if (restriction.HasFaceDetectionRule)
                {
                    var ages = ReturnAgeRestrication(restriction, Model);
                    restriction.AgeToRestrictions = ages;
                }

                publication.Restriction = restriction;
                //
                //更新CampaignToDeviceScreen
                //
                var publishList = new List<ScheduleSummary>();

                //loop screen (device)
                if (publication.Devices != null)
                {
                    foreach (var publishedScreen in publication.Devices)
                    {
                        var order = ReturnDeviceCountbyDeviceId(_UserInfo, publishedScreen);

                        //assign campaign to screen
                        if (publication.Campaigns != null)
                        {
                            foreach (var publishedCampaign in publication.Campaigns)
                            {
                                ++order;
                                AddPublishListWithCampaigns(selectedType, publication, publishList, publishedCampaign, publishedScreen, _UserInfo, order);

                            }
                        }
                    }
                }

                //loop device group
                if (publication.ScreenGroups != null)
                {
                    foreach (var publishedGroup in publication.ScreenGroups)
                    {

                        var order = ReturnDeviceCountbyGroupId(_UserInfo, publishedGroup);

                        //assign campaign to screen
                        if (publication.Campaigns != null)
                        {
                            foreach (var publishedCampaign in publication.Campaigns)
                            {
                                ++order;
                                //可能要查詢device group 下面有哪些
                                AddPublishListWithScreenGroupbyGroupId(publication, publishList, publishedCampaign, publishedGroup, selectedType, _UserInfo, order);
                            }
                        }

                    }
                }

                try
                {
                    if (publishList != null && publishList.Count > 0)
                    {
                        _DsDb.ScheduleSummary.AddRange(publishList);
                    }

                    _DsDb.SaveChanges();

                    //將新增Publish資訊存入log
                    PublishLogModels PublishLogModels = new PublishLogModels();
                    PublishLogModels.PublishLogAdd(Model, _UserInfo, 1);
                    //PublishLogModels.PublishLogAdd(Model, _UserInfo, 1);

                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: ex.Message);
                }

                //修改內容狀態及派送狀態
                //針對 單一 device

                List<PublishDevice> publshDeviceList = new List<PublishDevice>();

                //針對 screen group
                if (publication.ScreenGroups != null && publication.ScreenGroups.Count() > 0)
                {
                    foreach (var screenGroup in publication.ScreenGroups)
                    {
                        var deviceList = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroup.Id)
                            .ToList();
                        var reslut = deviceList.Select(o => new PublishDevice()
                        {
                            DeviceId = o.Id
                        }).ToList();
                        publshDeviceList.AddRange(reslut);
                    }
                }
                var ContentStatus = ChangeContentStatus(null, publication.Devices != null && publication.Devices.Count() > 0 ? publication.Devices.Concat(publshDeviceList) : publshDeviceList);
                if (ContentStatus.Result)

                    return new SuccessResult(Data: publication.Id);
                else
                {
                    return ContentStatus;
                }

            }
        }

        public void AddPublishListWithCampaigns(SelectedTypeApiModel selectedType, PublishUsedModel publication, List<ScheduleSummary> publishList, PublishCampaign publishedCampaign,
           PublishDevice publishedScreen, UserInfo _UserInfo, int order)
        {
            var AgeRestriction = "";
            AgeRestriction = AgeRestrictionsplit(publication);


            publishList.Add(new ScheduleSummary()
            {
                CampaignId = publishedCampaign.CampaignId,
                CreateByUserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                CreateDate = DateTime.Now,
                DeviceId = publishedScreen.DeviceId,
                PlayOrder = selectedType.General ? order : -1,
                DeviceGroupId = null,
                HasCalenderRule = publication.Restriction.HasCalenderRule,
                HasFaceDetectionRule = publication.Restriction.HasFaceDetectionRule,
                StartDate = publication.StartDate,
                EndDate = publication.EndDate,
                RepeatMonday = publication.Restriction.RepeatMonday,
                RepeatTuesday = publication.Restriction.RepeatTuesday,
                RepeatWednesday = publication.Restriction.RepeatWednesday,
                RepeatThurday = publication.Restriction.RepeatThurday,
                RepeatFriday = publication.Restriction.RepeatFriday,
                RepeatSaturday = publication.Restriction.RepeatSaturday,
                RepeatSunday = publication.Restriction.RepeatSunday,
                DailyStartTime = publication.Restriction.DailyStartTime,
                DailyEndTime = publication.Restriction.DailyEndTime,
                Gender = publication.Restriction.Gender,
                Spot = selectedType.Spot,
                AgeRestriction = AgeRestriction == "" ? null : AgeRestriction,
                Event = selectedType.Event,
                //以後再使用月循環
                HasMonthCalenderRule = false,
                MonthDay = 0
            });
        }


        public void AddPublishListWithScreenGroupbyGroupId(PublishUsedModel publication, List<ScheduleSummary> publishList, PublishCampaign publishedCampaign,
         PublishScreenGroupApiModel publicationScreenGroup, SelectedTypeApiModel selectedType, UserInfo _UserInfo, int order)
        {
            // var devices = SearchDevicebyScreenGroupId(publicationScreenGroup.Id, _UserInfo.UserGroupId);
            //foreach (var device in devices)
            //{
            var AgeRestriction = "";
            AgeRestriction = AgeRestrictionsplit(publication);

            publishList.Add(new ScheduleSummary()
            {
                CampaignId = publishedCampaign.CampaignId,
                CreateByUserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                CreateDate = DateTime.Now,
                PlayOrder = selectedType.General ? order : -1,
                ScreenGroupId = publicationScreenGroup.Id,
                HasCalenderRule = publication.Restriction.HasCalenderRule,
                HasFaceDetectionRule = publication.Restriction.HasFaceDetectionRule,
                StartDate = publication.StartDate,
                EndDate = publication.EndDate,
                RepeatMonday = publication.Restriction.RepeatMonday,
                RepeatTuesday = publication.Restriction.RepeatTuesday,
                RepeatWednesday = publication.Restriction.RepeatWednesday,
                RepeatThurday = publication.Restriction.RepeatThurday,
                RepeatFriday = publication.Restriction.RepeatFriday,
                RepeatSaturday = publication.Restriction.RepeatSaturday,
                RepeatSunday = publication.Restriction.RepeatSunday,
                DailyStartTime = publication.Restriction.DailyStartTime,
                DailyEndTime = publication.Restriction.DailyEndTime,
                Gender = publication.Restriction.Gender,
                Spot = selectedType.Spot,
                AgeRestriction = AgeRestriction == "" ? null : AgeRestriction,
                Event = selectedType.Event,
                //以後再使用月循環
                HasMonthCalenderRule = false,
                MonthDay = 0
            });
            //}
        }

        /// <summary>
        /// 回傳 Publication meta data
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public PublishUsedModel ReturnPublication(UserInfo _UserInfo, PublishApiModel Model)
        {
            var publication = new PublishUsedModel()
            {
                UserGroupId = _UserInfo.UserGroupId,
                CreateByUserId = _UserInfo.Id,
                CreateDate = DateTime.Now,
                StartDate = Model.StartDate,
                EndDate = Model.EndDate,
                Campaigns = Model.Campaigns?.Select(o => new PublishCampaign() { CampaignId = o.Id }).ToList(),
                Devices = Model.Screens?.Select(o => new PublishDevice() { DeviceId = o.Id }).ToList(),
                ScreenGroups = Model.ScreenGroups?.Select(o => new PublishScreenGroupApiModel() { Id = o.Id }).ToList()
            };
            return publication;
        }

        /// <summary>
        /// 回傳 Restrication
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public Restriction ReturnRestrication(UserInfo _UserInfo, PublishApiModel Model)
        {
            var restriction = new Restriction()
            {
                UserGroupId = _UserInfo.UserGroupId,
                CreateByUserId = _UserInfo.Id,
                CreateDate = DateTime.Now,
                HasCalenderRule = CheckCalenderRule(Model),
                HasFaceDetectionRule = Model.FaceRestriction != null,
                StartDate = Model.StartDate,
                EndDate = Model.EndDate
            };
            return restriction;
        }

        /// <summary>
        /// 判別CalenderRule flag
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>

        public bool CheckCalenderRule(PublishApiModel Model)
        {
            if (Model != null)
            {
                //has repeatday and choose every day
                if ((Convert.ToInt32(Model.TimeRestriction.RepeatMon) + Convert.ToInt32(Model.TimeRestriction.RepeatTue) + Convert.ToInt32(Model.TimeRestriction.RepeatWed) +
                     Convert.ToInt32(Model.TimeRestriction.RepeatThu) + Convert.ToInt32(Model.TimeRestriction.RepeatFri) + Convert.ToInt32(Model.TimeRestriction.RepeatSat) + Convert.ToInt32(Model.TimeRestriction.RepeatSun)) == 7)
                    return false;
                else
                {
                    return true;
                }
            }
            else
            {
                //no repeat day 
                return false;
            }
        }

        public SelectedTypeApiModel ReturnSelectedType(UserInfo _UserInfo, PublishApiModel Model)
        {
            var SelectedType = new SelectedTypeApiModel()
            {
                Face = Model.SelectedType.Face,
                General = Model.SelectedType.General,
                Spot = Model.SelectedType.Spot,
                Event = Model.SelectedType.Event
            };
            return SelectedType;
        }

        /// <summary>
        /// 回傳時間限制條件
        /// </summary>
        /// <param name="restriction"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public Restriction TimeLimit(Restriction restriction, PublishApiModel Model)
        {
            var time = Model.TimeRestriction;

            restriction.RepeatMonday = time.RepeatMon;
            restriction.RepeatTuesday = time.RepeatTue;
            restriction.RepeatWednesday = time.RepeatWed;
            restriction.RepeatThurday = time.RepeatThu;
            restriction.RepeatFriday = time.RepeatFri;
            restriction.RepeatSaturday = time.RepeatSat;
            restriction.RepeatSunday = time.RepeatSun;

            restriction.DailyStartTime = time.DailyStartTime.HasValue ? time.DailyStartTime.Value : new TimeSpan(0, 0, 0);
            restriction.DailyEndTime = time.DailyEndTime.HasValue ? time.DailyEndTime.Value : new TimeSpan(23, 59, 59);

            //判斷是否跨日
            restriction.IsCrossDay = restriction.DailyEndTime <= restriction.DailyStartTime;
            return restriction;
        }

        /// <summary>
        /// Age restrication split
        /// </summary>
        /// <param name="publication"></param>
        /// <returns></returns>
        public String AgeRestrictionsplit(PublishUsedModel publication)
        {
            var AgeRestriction = "";
            if (publication.Restriction.AgeToRestrictions != null)
            {

                foreach (var age in publication.Restriction.AgeToRestrictions)
                {
                    AgeRestriction = AgeRestriction + age.Age_Id + ",";
                }
                AgeRestriction = AgeRestriction.Substring(0, AgeRestriction.Length - 1);
            }
            return AgeRestriction;
        }


        /// <summary>
        /// 搜尋此 screenGroup 含有哪些Device
        /// </summary>
        /// <param name="screenGroupId"></param>
        /// <param name="userGroupId"></param>
        /// <returns></returns>
        public List<Device> SearchDevicebyScreenGroupId(long screenGroupId, int userGroupId)
        {
            var devices = _DsDb.Device
                .Where(o => o.DeviceGroupId == screenGroupId && o.UserGroup_Id == userGroupId)
                .ToList();
            return devices;
        }

        /// <summary>
        /// Adjust ScheduleSummary when add device to deviceGroup 
        /// </summary>
        /// <param name="deviceId">deviceId</param>
        /// <param name="deviceGroupId">deviceGroupId</param>
        public void AdjustbyAddDevicetoDeviceGroup(int deviceId, long screenGroupId)
        {
            var publicationDevices = _DsDb.ScheduleSummary
                .Where(o => o.DeviceGroupId == screenGroupId)
                .ToList();
            if (publicationDevices.Count() > 0)
            {
                foreach (var d in publicationDevices)
                {
                    d.DeviceId = deviceId;
                }
                _DsDb.ScheduleSummary.AddRange(publicationDevices);
                _DsDb.SaveChanges();
            }

        }

        /// <summary>
        /// 調整 ScheduleSummary 刪除 device
        /// </summary>
        /// <param name="deviceId"></param>
        public void AdjustbyDeleteDevice(int deviceId)
        {
            var publicationDevices = _DsDb.ScheduleSummary
                .Where(o => o.DeviceId == deviceId)
                .ToList();

            _DsDb.ScheduleSummary.RemoveRange(publicationDevices);
            _DsDb.SaveChanges();
        }

        /// <summary>
        /// Adjust ScheduleSummary when delete campaignId
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="_UserInfo"></param>
        public ApiResult AdjustbyCampaignsOrder(int campaignId, UserInfo _UserInfo)
        {
            //列出被異動的那些 device  
            var deviceList = _DsDb.ScheduleSummary
               .Where(o => o.CampaignId == campaignId && o.DeviceId != null)
               .ToList();
            var screenList = _DsDb.ScheduleSummary
              .Where(o => o.CampaignId == campaignId && o.DeviceId == null)
              .ToList();
            deviceList.AddRange(screenList);

            foreach (var d in deviceList)
            {
                //列出被異動後須重新排列的 那些 campaigns  
                var changeCampaigns = _DsDb.ScheduleSummary
                                 .Where(o => o.CampaignId != campaignId && o.DeviceId == d.DeviceId && o.PlayOrder > 0)
                                 .OrderBy(o => o.PlayOrder)
                                 .ToList();
                var order = 1;
                foreach (var c in changeCampaigns)
                {
                    c.PlayOrder = order;
                    order++;
                };
                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: ex.Message);
                }
            }


            //列出被異動的那些 screen group              
            foreach (var sg in screenList)
            {
                //列出被異動後須重新排列的 那些 campaigns  
                var changeCampaigns = _DsDb.ScheduleSummary
                                 .Where(o => o.CampaignId != campaignId && o.ScreenGroupId == sg.ScreenGroupId && o.PlayOrder > 0)
                                 .OrderBy(o => o.PlayOrder)
                                 .ToList();

                var order = 1;
                foreach (var c in changeCampaigns)
                {
                    c.PlayOrder = order;
                    order++;
                };
                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: ex.Message);
                }
            }

            //修改內容狀態及派送狀態
            var ContentStatus = ChangeContentStatus(deviceList, null);
            if (!ContentStatus.Result)

                return ContentStatus;
            else
            {
                if (deviceList.Count() > 0)
                {
                    _DsDb.ScheduleSummary.RemoveRange(deviceList);
                    try
                    {

                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }
                }
                return new SuccessResult();
            }

        }

        /// <summary>
        /// 當維護的派送資料 selectType 調整時 需重新計算 play order
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="scheduleSummaryId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AdjustCampaignsOrderbySelectType(int deviceId, long screenGroupId, long scheduleSummaryId, UserInfo _UserInfo)
        {
            if (deviceId > 0)
            {
                //列出被異動的 device  
                var deviceList = _DsDb.ScheduleSummary
                   .Where(o => o.DeviceId == deviceId)
                   .ToList();

                foreach (var d in deviceList)
                {
                    //列出被異動後須重新排列的 那些 campaigns  List
                    var changeCampaigns = _DsDb.ScheduleSummary
                                     .Where(o => o.Id != scheduleSummaryId && o.DeviceId == d.DeviceId && o.PlayOrder > 0)
                                     .OrderBy(o => o.PlayOrder)
                                     .ToList();
                    var order = 1;
                    foreach (var c in changeCampaigns)
                    {
                        c.PlayOrder = order;
                        order++;
                    };
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }


                }

            }
            else
            {
                //列出被異動的 screenGroupList  多筆要異動的
                var screenGroupList = _DsDb.ScheduleSummary
                   .Where(o => o.ScreenGroupId == screenGroupId)
                   .ToList();

                foreach (var s in screenGroupList)
                {
                    //列出被異動後須重新排列的 那些 campaigns List 
                    var changeCampaigns = _DsDb.ScheduleSummary
                                     .Where(o => o.Id != scheduleSummaryId && o.ScreenGroupId == s.ScreenGroupId && o.PlayOrder > 0)
                                     .OrderBy(o => o.PlayOrder)
                                     .ToList();
                    var order = 1;
                    foreach (var c in changeCampaigns)
                    {
                        c.PlayOrder = order;
                        order++;
                    };
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }


                }

            }
            return new SuccessResult();


        }



        /// <summary>
        /// 移除device 指定的某個campaignId(針對從device 設備移除已派送的campaign)
        /// 主要將該device 上面的 campaignId 派送移除 (目前應該無使用)
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="deviceId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>

        //public ApiResult AdjustbyDeleteCampaigninDevice(int campaignId, int deviceId, UserInfo _UserInfo)
        //{
        //    //在ScheduleSummary 中要刪除的campaign
        //    var deleteCampaign = _DsDb.ScheduleSummary
        //                     .Where(o => o.CampaignId == campaignId && o.DeviceId == deviceId)
        //                     .ToList();

        //    //列出被異動後須重新排列的 那些 campaigns  
        //    var changeCampaigns = _DsDb.ScheduleSummary
        //                     .Where(o => o.CampaignId != campaignId && o.DeviceId == deviceId && o.PlayOrder > 0)
        //                     .OrderBy(o => o.PlayOrder)
        //                     .ToList();
        //    var order = 1;
        //    foreach (var c in changeCampaigns)
        //    {
        //        c.PlayOrder = order;
        //        order++;
        //    };
        //    try
        //    {
        //        _DsDb.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ExceptionResult(Message: ex.Message);
        //    }

        //    //要更改的deviceId
        //    List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
        //    deviceList.Add(new ScheduleSummary()
        //    {
        //        DeviceId = deviceId
        //    });

        //    //修改內容狀態及派送狀態
        //    var ContentStatus = ChangeContentStatus(deviceList, null);
        //    if (!ContentStatus.Result)

        //        return ContentStatus;
        //    else
        //    {
        //        if (deviceList.Count() > 0)
        //        {
        //            _DsDb.ScheduleSummary.RemoveRange(deleteCampaign);
        //            try
        //            {

        //                _DsDb.SaveChanges();
        //            }
        //            catch (Exception ex)
        //            {
        //                return new ExceptionResult(Message: ex.Message);
        //            }
        //        }
        //        return new SuccessResult();
        //    }

        //}

        /// <summary>
        /// 依device 刪除 ScheduleSummaryId (刪除整筆派送播放的紀錄) 
        /// </summary>
        /// <param name="scheduleSummaryId"></param>
        /// <param name="deviceId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AdjustbyDeleteScheduleSummaryIdinDevice(long scheduleSummaryId, int? deviceId, UserInfo _UserInfo)
        {
            //在ScheduleSummary 中要刪除的 scheduleSummaryId
            var deleteScheduleSummaryId = _DsDb.ScheduleSummary
                             .Where(o => o.Id == scheduleSummaryId && o.DeviceId == deviceId)
                             .ToList();

            //列出被異動後須重新排列的 那些 campaigns  
            var changeCampaigns = _DsDb.ScheduleSummary
                             .Where(o => o.Id != scheduleSummaryId && o.DeviceId == deviceId && o.PlayOrder > 0)
                             .OrderBy(o => o.PlayOrder)
                             .ToList();
            var order = 1;
            foreach (var c in changeCampaigns)
            {
                c.PlayOrder = order;
                order++;
            };
            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }

            //要更改的deviceId
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
            deviceList.Add(new ScheduleSummary()
            {
                DeviceId = deviceId
            });

            //修改內容狀態及派送狀態
            var ContentStatus = ChangeContentStatus(deviceList, null);
            if (!ContentStatus.Result)

                return ContentStatus;
            else
            {
                if (deviceList.Count() > 0)
                {
                    //將刪除Publish資訊存入log
                    PublishLogModels PublishLogModels = new PublishLogModels();
                    PublishLogModels.PublishLogDelete(scheduleSummaryId, deviceId, null, _UserInfo, 3);

                    _DsDb.ScheduleSummary.RemoveRange(deleteScheduleSummaryId);
                    try
                    {

                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }
                }
                return new SuccessResult();
            }

        }

        /// <summary>
        /// 刪除 派送到 screenGroup 的 scheduleSummaryId
        /// </summary>
        /// <param name="scheduleSummaryId"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AdjustbyDeleteScheduleSummaryIdinScreenGroup(long scheduleSummaryId, long screenGroupId, UserInfo _UserInfo)
        {
            //在ScheduleSummary 中要刪除的 scheduleSummaryId
            var deleteScheduleSummaryId = _DsDb.ScheduleSummary
                             .Where(o => o.Id == scheduleSummaryId && o.ScreenGroupId == screenGroupId)
                             .ToList();

            //列出被異動後須重新排列的 那些 campaigns  
            var changeCampaigns = _DsDb.ScheduleSummary
                             .Where(o => o.Id != scheduleSummaryId && o.ScreenGroupId == screenGroupId && o.PlayOrder > 0)
                             .OrderBy(o => o.PlayOrder)
                             .ToList();
            var order = 1;
            foreach (var c in changeCampaigns)
            {
                c.PlayOrder = order;
                order++;
            };
            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }

            var deviceinScreenGroup = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId).ToList();
            //要更改的 在 screenGroupId 下的 device             
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
            foreach (var d in deviceinScreenGroup)
            {
                deviceList.Add(new ScheduleSummary()
                {
                    DeviceId = d.Id
                });
            }

            //修改內容狀態及派送狀態
            var ContentStatus = ChangeContentStatus(deviceList, null);
            if (!ContentStatus.Result)

                return ContentStatus;
            else
            {
                if (deviceList.Count() > 0)
                {
                    //將刪除Publish資訊存入log
                    PublishLogModels PublishLogModels = new PublishLogModels();
                    PublishLogModels.PublishLogDelete(scheduleSummaryId, null, screenGroupId, _UserInfo, 3);

                    _DsDb.ScheduleSummary.RemoveRange(deleteScheduleSummaryId);
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }
                }
                return new SuccessResult();
            }

        }

        /// <summary>
        /// 依device批次刪除多筆ScheduleSummary派送紀錄
        /// </summary>
        /// <param name="ScheduleSummaryIds"></param>
        /// <param name="DeviceId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AdjustbyBatchDeleteScheduleSummaryIdinDevice(List<long> ScheduleSummaryIds, int DeviceId, UserInfo _UserInfo)
        {

            //取出此device所有有效廣告
            var deviceschedulesummary = _DsDb.ScheduleSummary
                             .Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeviceId == DeviceId && o.DeleteDate == null);

            //在ScheduleSummary 中要刪除的 scheduleSummaryId
            var deleteScheduleSummaryId = from Schedule in deviceschedulesummary
                                          where (ScheduleSummaryIds.Contains(Schedule.Id))
                                          select Schedule;

            //列出被異動後須重新排列的 那些 campaigns
            var changeCampaigns = (from Schedule in deviceschedulesummary
                                   where (!ScheduleSummaryIds.Contains(Schedule.Id) && Schedule.PlayOrder > 0)
                                   orderby Schedule.PlayOrder
                                   select Schedule).ToList();

            var order = 1;
            foreach (var c in changeCampaigns)
            {
                c.PlayOrder = order;
                order++;
            };
            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }

            //要更改的deviceId
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
            deviceList.Add(new ScheduleSummary()
            {
                DeviceId = DeviceId
            });

            //修改內容狀態及派送狀態
            var ContentStatus = ChangeContentStatus(deviceList, null);
            if (!ContentStatus.Result)

                return ContentStatus;
            else
            {
                if (deviceList.Count() > 0)
                {

                    foreach (var SummaryID in ScheduleSummaryIds)
                    {
                        //將刪除Publish資訊存入log
                        PublishLogModels PublishLogModels = new PublishLogModels();
                        PublishLogModels.PublishLogDelete(SummaryID, DeviceId, null, _UserInfo, 3);

                    }

                    _DsDb.ScheduleSummary.RemoveRange(deleteScheduleSummaryId);
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }
                }
                return new SuccessResult();
            }

        }

        /// <summary>
        /// 依照 screenGroup 批次刪除 scheduleSummary
        /// </summary>
        /// <param name="ScheduleSummaryIds"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AdjustbyBatchDeleteScheduleSummaryIdinScreenGroup(List<long> ScheduleSummaryIds, long? screenGroupId, UserInfo _UserInfo)
        {

            //取出此device所有有效廣告
            var deviceschedulesummary = _DsDb.ScheduleSummary
                             .Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.ScreenGroupId == screenGroupId && o.DeleteDate == null);

            //在ScheduleSummary 中要刪除的 scheduleSummaryId
            var deleteScheduleSummaryId = from Schedule in deviceschedulesummary
                                          where (ScheduleSummaryIds.Contains(Schedule.Id))
                                          select Schedule;

            //列出被異動後須重新排列的 那些 campaigns
            var changeCampaigns = (from Schedule in deviceschedulesummary
                                   where (!ScheduleSummaryIds.Contains(Schedule.Id) && Schedule.PlayOrder > 0)
                                   orderby Schedule.PlayOrder
                                   select Schedule).ToList();

            var order = 1;
            foreach (var c in changeCampaigns)
            {
                c.PlayOrder = order;
                order++;
            };
            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }

            var deviceinScreenGroup = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId).ToList();
            //要更改的 在 screenGroupId 下的 device             
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
            foreach (var d in deviceinScreenGroup)
            {
                deviceList.Add(new ScheduleSummary()
                {
                    DeviceId = d.Id
                });
            }

            //修改內容狀態及派送狀態
            var ContentStatus = ChangeContentStatus(deviceList, null);
            if (!ContentStatus.Result)

                return ContentStatus;
            else
            {
                if (deviceList.Count() > 0)
                {

                    foreach (var SummaryID in ScheduleSummaryIds)
                    {
                        //將刪除Publish資訊存入log
                        PublishLogModels PublishLogModels = new PublishLogModels();
                        PublishLogModels.PublishLogDelete(SummaryID, null, screenGroupId, _UserInfo, 3);

                    }

                    _DsDb.ScheduleSummary.RemoveRange(deleteScheduleSummaryId);
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }
                }
                return new SuccessResult();
            }

        }


        /// <summary>
        /// 改變device 派送的狀態及content 被異動的狀態
        /// </summary>
        /// <param name="deviceList"></param>
        /// <param name="PublicationDevice"></param>
        /// <returns></returns>
        public ApiResult ChangeContentStatus(List<ScheduleSummary> deviceList, IEnumerable<PublishDevice> PublicationDevice)
        {
            if (deviceList != null)
            {
                foreach (var device in deviceList)
                {
                    //列出被異動的device 並更新其狀態  
                    if (device.DeviceId != null)
                    {
                        var devices = _DsDb.Device
                                        .Where(o => o.Id == device.DeviceId && o.DeleteDate == null)
                                        .FirstOrDefault();
                        devices.ContentStatus = 1;
                        devices.DispatchStatus = 2;

                    }
                    else
                    {
                        var devices = _DsDb.Device
                                           .Where(o => o.ScreenGroupId == device.ScreenGroupId && o.DeleteDate == null)
                                           .ToList();
                        foreach (var d in devices)
                        {
                            d.ContentStatus = 1;
                            d.DispatchStatus = 2;
                        }

                    }

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }

                }

            }
            else
            {
                foreach (var d in PublicationDevice)
                {
                    //列出被異動的device 並更新其狀態  
                    var device = _DsDb.Device
                                     .Where(o => o.Id == d.DeviceId && o.DeleteDate == null)
                                     .FirstOrDefault();
                    device.ContentStatus = 1;
                    device.DispatchStatus = 2;
                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }

                }
            }

            return new SuccessResult();
        }

        /// <summary>
        /// 列出指定的ScheduleSummaryID 項目供前端修改
        /// </summary>
        /// <param name="SchduleSummaryId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetMaintainData(ScheduleSummary ScheduleSummary, UserInfo _UserInfo)
        {

            var maintainList = _DsDb.ScheduleSummary
               .Where(o => o.Id == ScheduleSummary.Id)
               .ToList();

            return new SuccessResult()
            {
                Result = true,
                Data = maintainList

            };
        }

        /// <summary>
        /// 針對指定的ScheduleSummaryID 由前端做編輯維護
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult UpdateMaintainData(PublishApiModel Model, UserInfo _UserInfo)
        {
            var publication = ReturnPublication(_UserInfo, Model);

            var restriction = ReturnRestrication(_UserInfo, Model);

            var selectedType = ReturnSelectedType(_UserInfo, Model);

            var publishDevice = new PublishDevice();
            var publishScreenGroup = new PublishScreenGroupApiModel();



            TimeLimit(restriction, Model);

            //臉部辨識限制
            if (restriction.HasFaceDetectionRule)
            {
                var ages = ReturnAgeRestrication(restriction, Model);
                restriction.AgeToRestrictions = ages;
            }

            publication.Restriction = restriction;

            //Event
            if (selectedType.Event)
            {
                restriction = null;
            }

            var maintainList = _DsDb.ScheduleSummary
               .Where(o => o.Id == Model.ScheduleSummaryId)
               .ToList();

            var AgeRestriction = "";
            AgeRestriction = AgeRestrictionsplit(publication);

            //device
            if (Model.Screens != null)
            {
                publishDevice.DeviceId = Model.Screens[0].Id;
                var deviceOrder = ReturnDeviceCountbyDeviceId(_UserInfo, publishDevice);

                foreach (var d in maintainList)
                {
                    if (selectedType.General)
                    {
                        //原本 play order 是 -1 
                        if (d.Event || d.Spot == true || d.HasFaceDetectionRule == true)
                        {
                            ++deviceOrder;
                            d.PlayOrder = deviceOrder;
                        }
                    }
                    else //選成event / spot / face
                    {
                        //原本是 general 則要調整 play order
                        if (d.Event == false && d.Spot == false && d.HasFaceDetectionRule == false)
                        {
                            AdjustCampaignsOrderbySelectType(publishDevice.DeviceId, 0, Model.ScheduleSummaryId, _UserInfo);
                            d.PlayOrder = -1;
                        }
                    }
                    d.Event = selectedType.Event;
                    d.UpdateByUserId = _UserInfo.Id;
                    d.HasCalenderRule = d.Event == true ? false : publication.Restriction.HasCalenderRule;
                    d.HasFaceDetectionRule = publication.Restriction.HasFaceDetectionRule;
                    d.StartDate = publication.StartDate;
                    d.EndDate = publication.EndDate;
                    d.RepeatMonday = d.Event == true ? true : publication.Restriction.RepeatMonday;
                    d.RepeatTuesday = d.Event == true ? true : publication.Restriction.RepeatTuesday;
                    d.RepeatWednesday = d.Event == true ? true : publication.Restriction.RepeatWednesday;
                    d.RepeatThurday = d.Event == true ? true : publication.Restriction.RepeatThurday;
                    d.RepeatFriday = d.Event == true ? true : publication.Restriction.RepeatFriday;
                    d.RepeatSaturday = d.Event == true ? true : publication.Restriction.RepeatSaturday;
                    d.RepeatSunday = d.Event == true ? true : publication.Restriction.RepeatSunday;
                    d.DailyStartTime = d.Event == true ? new TimeSpan(0, 0, 0) : publication.Restriction.DailyStartTime;
                    d.DailyEndTime = d.Event == true ? new TimeSpan(23, 59, 59) : publication.Restriction.DailyEndTime;
                    d.Gender = publication.Restriction.Gender;
                    d.Spot = selectedType.Spot;
                    d.AgeRestriction = AgeRestriction == "" ? null : AgeRestriction;
                    d.UpdateDate = DateTime.Now;
                    d.UpdateByUserId = _UserInfo.Id;
                }
                //修改內容狀態及派送狀態
                var tempDeviceId = Model.Screens[0].Id;
                var deviceList = _DsDb.ScheduleSummary
                   .Where(o => o.DeviceId == tempDeviceId)
                   .ToList();
                var ContentStatus = ChangeContentStatus(deviceList, null);
                if (!ContentStatus.Result)

                    return ContentStatus;
                else
                {
                    return new SuccessResult();
                }
            }
            else
            {
                //ScreenGroup
                publishScreenGroup.Id = Model.ScreenGroups[0].Id;
                var screenGroupOrder = ReturnDeviceCountbyGroupId(_UserInfo, publishScreenGroup);

                foreach (var s in maintainList)
                {
                    if (selectedType.General)
                    {
                        //原本是 play order 是 -1 
                        if (s.Event == true || s.Spot == true || s.HasFaceDetectionRule == true)
                        {
                            ++screenGroupOrder;
                            s.PlayOrder = screenGroupOrder;
                        }
                    }
                    else //選成event / spot / face
                    {
                        //原本是 general 則要調整 play order
                        if (s.Event == false && s.Spot == false && s.HasFaceDetectionRule == false)
                        {
                            AdjustCampaignsOrderbySelectType(0, publishScreenGroup.Id, Model.ScheduleSummaryId, _UserInfo);
                            s.PlayOrder = -1;
                        }
                    }
                    s.Event = selectedType.Event;
                    s.UpdateByUserId = _UserInfo.Id;
                    s.HasCalenderRule = s.Event == true ? false : publication.Restriction.HasCalenderRule;
                    s.HasFaceDetectionRule = publication.Restriction.HasFaceDetectionRule;
                    s.StartDate = publication.StartDate;
                    s.EndDate = publication.EndDate;
                    s.RepeatMonday = s.Event == true ? true : publication.Restriction.RepeatMonday;
                    s.RepeatTuesday = s.Event == true ? true : publication.Restriction.RepeatTuesday;
                    s.RepeatWednesday = s.Event == true ? true : publication.Restriction.RepeatWednesday;
                    s.RepeatThurday = s.Event == true ? true : publication.Restriction.RepeatThurday;
                    s.RepeatFriday = s.Event == true ? true : publication.Restriction.RepeatFriday;
                    s.RepeatSaturday = s.Event == true ? true : publication.Restriction.RepeatSaturday;
                    s.RepeatSunday = s.Event == true ? true : publication.Restriction.RepeatSunday;
                    s.DailyStartTime = s.Event == true ? new TimeSpan(0, 0, 0) : publication.Restriction.DailyStartTime;
                    s.DailyEndTime = s.Event == true ? new TimeSpan(23, 59, 59) : publication.Restriction.DailyEndTime;
                    s.Gender = publication.Restriction.Gender;
                    s.Spot = selectedType.Spot;
                    s.AgeRestriction = AgeRestriction == "" ? null : AgeRestriction;
                    s.UpdateDate = DateTime.Now;
                    s.UpdateByUserId = _UserInfo.Id;


                }
                List<PublishDevice> deviceinScreenGroup = new List<PublishDevice>();
                var tempScreenGroupId = Model.ScreenGroups[0].Id;
                //var deviceList = _DsDb.Device.Where(o => o.ScreenGroupId == tempScreenGroupId).Select(o => new PublishDevice()
                //{
                //    DeviceId = o.Id
                //}).ToList();
                var deviceList = _DsDb.Device.Where(o => o.ScreenGroupId == tempScreenGroupId).ToList();
                foreach (var d in deviceList)
                {
                    deviceinScreenGroup.Add(new PublishDevice()
                    {
                        DeviceId = d.Id
                    });
                }
                //修改內容狀態及派送狀態
                var ContentStatus = ChangeContentStatus(null, deviceinScreenGroup);
                if (!ContentStatus.Result)

                    return ContentStatus;
                else
                {
                    try
                    {
                        if (maintainList != null)
                        {
                            _DsDb.SaveChanges();

                            //將修改Publish資訊存入log
                            //PublishLogModels PublishLogModels = new PublishLogModels();
                            //PublishLogModels.PublishLogEdit(Model.ScheduleSummaryId, Model, _UserInfo, 2);


                        }
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: ex.Message);
                    }

                    return new SuccessResult()
                    {
                        Result = true,
                        Data = maintainList
                    };
                }

            }

        }
    }
}
