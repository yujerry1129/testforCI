﻿using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Models
{
    public class LogInfoModels : Controllers.Base.DsBaseController
    {

        /// <summary>
        /// get all device info + location for logInfo page
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageRows"></param>
        /// <param name="deviceName"></param>
        /// <param name="_UserInfo"></param>
        /// <returns>
        /// Device Infomation and UserGroup usage Information
        /// </returns>
        public ApiResult GetWholeDeviceInfo(int page, int pageRows, UserInfo _UserInfo, string deviceName)
        {
            int currentUserGroupId = _UserInfo.UserGroupId;
            int allPage = 0;
            var DeviceData = _DsDb.Device.Select(d => new LogViewModel.Device.Devices());
            var currentUserGroup = _DsDb.UserGroup.Where(o => o.Id == currentUserGroupId).SingleOrDefault();
            //string[] DispatchStatusNameList = new string[] { "No Data To Dispatch", "Dispatched", "Waiting For Update", "Updating", "Dispatch Fail" };
            if (deviceName != null)
            {
                allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(_DsDb.Device.Where(o => o.Name.Contains(deviceName) && o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.CreateByUserId != null).Count()) / pageRows));
                DeviceData = from d in _DsDb.Device.Where(o => o.Name.Contains(deviceName) && o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.CreateByUserId != null)
                             join l in _DsDb.Location on d.LocationId equals l.Id into ps
                             from dl in ps.DefaultIfEmpty()
                             orderby dl.Id descending
                             select new LogViewModel.Device.Devices()
                             {
                                 deviceId = d.Id,
                                 deviceName = d.Name,
                                 dispatchStatus = d.DispatchStatus,
                                 //dispatchStatusName = DispatchStatusNameList[d.DispatchStatus],
                                 locationId = d.LocationId,
                                 locationName = dl.Name,
                                 enableFaceDetection = currentUserGroup.EnableFaceDetection,
                             };
            }
            else
            {
                allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(_DsDb.Device.Where(o => o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.CreateByUserId != null).Count()) / pageRows));
                DeviceData = from d in _DsDb.Device.Where(o => o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.CreateByUserId != null)
                             join l in _DsDb.Location on d.LocationId equals l.Id into ps
                             from l in ps.DefaultIfEmpty()
                             orderby d.MachineName
                             select new LogViewModel.Device.Devices()
                             {
                                 deviceId = d.Id,
                                 deviceName = d.Name,
                                 dispatchStatus = d.DispatchStatus,
                                 //dispatchStatusName = DispatchStatusNameList[d.DispatchStatus],
                                 locationId = d.LocationId,
                                 locationName = l.Name,
                                 enableFaceDetection = currentUserGroup.EnableFaceDetection,
                             };
            }
            var model = new LogViewModel.Device();
            model.DeviceList = DeviceData.Skip((page - 1) * pageRows).Take(pageRows).ToList();
            model.Count = allPage;
            return new SuccessResult(Data: model);
        }

        /// <summary>
        /// get device's face logs
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageRows"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public ApiResult GetDeviceFaceLogList(int id, int page, int pageRows, UserInfo _UserInfo, String startDate, String endDate)
        {
            String temp = "1911-01-01 00:00:00";
            DateTime sd = Convert.ToDateTime(temp); ;
            DateTime ed = DateTime.Now;
            if (startDate != null)
            {
                sd = Convert.ToDateTime(startDate);
            }
            if (endDate != null)
            {
                ed = Convert.ToDateTime(endDate);
            }
            int currentUserGroupId = _UserInfo.UserGroupId;

            int allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(_DsDb.FaceDetectionLog
                .Where(o => o.DeviceId == id && o.UserGroupId == currentUserGroupId && o.LogTime >= sd && o.LogTime <= ed)
                .Count()) / pageRows));

            var faceLogs = from f in _DsDb.FaceDetectionLog
                           join a in _DsDb.AgeRangeDefinition on f.AgeId equals a.AgeId
                           where a.UserGroup_Id == currentUserGroupId && f.DeviceId == id && f.UserGroupId == currentUserGroupId && f.LogTime >= sd && f.LogTime <= ed
                           orderby f.LogTime descending
                           select new LogViewModel.FaceLogViewModel.FaceLogList()
                           {
                               Description = a.Description,
                               LogTime = f.LogTime,
                               Gender = f.Gender,
                               AgeId = f.AgeId,
                               LookTime = f.LookTime,
                               Distance = f.Distance
                           };
            faceLogs.Skip((page - 1) * pageRows).Take(pageRows).ToList();
            var model = new LogViewModel.FaceLogViewModel();
            model.FaceList = faceLogs.ToList().Skip((page - 1) * pageRows).Take(pageRows).ToList();
            model.Count = allPage;

            return new SuccessResult(Data: model);
        }

        /// <summary>
        /// get device's display logs
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageRows"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetDeviceDisplayLogList(int id, int page, int pageRows, UserInfo _UserInfo, String startDate, String endDate)
        {
            String temp = "1911-01-01 00:00:00";
            DateTime sd = Convert.ToDateTime(temp); ;
            DateTime ed = DateTime.Now;
            int currentUserGroupId = _UserInfo.UserGroupId;
            if (startDate != null)
            {
                sd = Convert.ToDateTime(startDate);
            }
            if (endDate != null)
            {
                ed = Convert.ToDateTime(endDate);
            }
            var sql = _DsDb.DisplayLog
                .Where(o => o.DeviceId == id && o.UserGroupId == currentUserGroupId && o.StartTime >= sd && o.StartTime <= ed).OrderByDescending(o => o.StartTime);
            int allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(sql.Count()) / pageRows));
            var displayLogs = sql
                .Select(p => new LogViewModel.DisplayLogViewModel.DisplayLogList()
                {
                    Id = p.Id,
                    StartTime = p.StartTime,
                    EndTime = p.EndTime,
                    FileType = p.FileType,
                    FileName = p.FileName,
                    DisplayType = p.DisplayType,
                    CampaignName = p.CampaignName
                })
                .Skip((page - 1) * pageRows).Take(pageRows).ToList();
            var model = new LogViewModel.DisplayLogViewModel();
            model.DisplayList = displayLogs;
            model.Count = allPage;
            return new SuccessResult(Data: model);
        }
        public String ExportCSV(int id, string type, UserInfo _UserInfo)
        {
            int currentUserGroupId = _UserInfo.UserGroupId;
            if (type == "face")
            {
                var result = _DsDb.FaceDetectionLog.Where(o => o.DeviceId == id && o.UserGroupId == currentUserGroupId)
                .Select(d => new LogViewModel.FaceLogViewModel.FaceLogList()
                {
                    Id = d.Id,
                    Gender = d.Gender,
                    AgeId = d.AgeId,
                    Distance = d.Distance,
                    LookTime = d.LookTime,
                    LogTime = d.LogTime,
                });

                //組字串
                string csv = "Gender,Age,Distance,Look Time,Log Time\r\n";
                foreach (var item in result)
                {
                    //處理Gender和AgeId輸出顯示資料
                    var Gender = "";
                    switch (item.Gender)
                    {
                        case 0:
                            Gender = "?";
                            break;
                        case 1:
                            Gender = "Male";
                            break;
                        case 2:
                            Gender = "Female";
                            break;

                    }
                    var AgeRange = "";
                    switch (item.AgeId)
                    {
                        case 0:
                            AgeRange = "Unrecognized";
                            break;
                        case 1:
                            AgeRange = "Child";
                            break;
                        case 2:
                            AgeRange = "Teenager";
                            break;
                        case 3:
                            AgeRange = "Adult";
                            break;
                        case 4:
                            AgeRange = "Elder";
                            break;

                    }

                    csv += Gender + ",";
                    csv += AgeRange + ",";
                    csv += item.Distance + ",";
                    csv += item.LookTime + ",";
                    csv += item.LogTime + ",";
                    csv += "\r\n";
                }
                return csv;
            }
            else
            {
                var result = _DsDb.DisplayLog.Where(o => o.DeviceId == id && o.UserGroupId == currentUserGroupId)
                .Select(d => new LogViewModel.DisplayLogViewModel.DisplayLogList()
                {
                    Id = d.Id,
                    LogTime = d.LogTime,
                    StartTime = d.StartTime,
                    EndTime = d.EndTime,
                    FileType = d.FileType,
                    FileName = d.FileName,
                    DisplayType = d.DisplayType,
                    CampaignName = d.CampaignName
                });

                //組字串
                string csv = "File Name,File Type,StartTime,EndTime,Campaign Name,Display Type,Date \r\n";
                foreach (var item in result)
                {
                    //處理FileType和DisplayType輸出顯示資料
                    var FileType = "";
                    switch (item.FileType)
                    {
                        case 0:
                            FileType = "Unknown";
                            break;
                        case 1:
                            FileType = "Image";
                            break;
                        case 2:
                            FileType = "Video";
                            break;
                        case 3:
                            FileType = "Audio";
                            break;
                        case 4:
                            FileType = "Text";
                            break;
                        case 5:
                            FileType = "Web";
                            break;
                        case 6:
                            FileType = "Marquee";
                            break;
                        case 7:
                            FileType = "Rss";
                            break;
                        case 8:
                            FileType = "Youtube";
                            break;
                        case 9:
                            FileType = "Flash";
                            break;

                    }
                    var DisplayType = "";
                    switch (item.DisplayType)
                    {
                        case 0:
                            DisplayType = "Unrecgonized";
                            break;
                        case 1:
                            DisplayType = "General";
                            break;
                        case 2:
                            DisplayType = "Face Detect";
                            break;
                        case 3:
                            DisplayType = "Spot";
                            break;


                    }

                    csv += item.FileName + ",";
                    csv += FileType + ",";
                    csv += item.StartTime + ",";
                    csv += item.EndTime + ",";
                    csv += item.CampaignName + ",";
                    csv += DisplayType + ",";
                    csv += item.LogTime + ",";
                    csv += "\r\n";
                }
                return csv;

            }
            //撈取 使用者資料 資料表 資訊



            //return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "FaceCSV.csv");

            //StringBuilder csv = new StringBuilder();
            //csv.AppendLine("Gender,Age,Distance,Look Time,Log Time");
            //foreach (var item in result)
            //{
            //    csv.Append(item.Gender + ",");
            //    csv.Append(item.AgeId + ",");
            //    csv.Append(item.Distance + ",");
            //    csv.Append(item.LookTime + ",");
            //    csv.Append(item.LogTime.ToShortDateString());
            //    csv.AppendLine();
            //}
            //csv.ToString();

        }
    }
}