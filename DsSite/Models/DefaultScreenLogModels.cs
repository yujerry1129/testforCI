﻿using DsDbLib.Models;
using System;


namespace DsSite.Models
{
    public class DefaultScreenLogModels : Controllers.Base.DsBaseController
    {
        public void ScreenLog(Device device, UserInfo _UserInfo, int action)
        {
             
                
            //將Screen資訊存入log
            var Screenlog = new DsDbLib.Models.DefalutScreenLog()
            {
                DeviceId =device.Id,
                DefaultScreenFileId = device.DefaultScreenFileId ,
                UserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                TimeStamp = DateTime.Now,
                Action = action
            };

            _DsDb.DefalutScreenLog.Add(Screenlog);
            _DsDb.SaveChanges();
           
        }
    }
}