﻿using DsSite.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class DsBaseModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得使用者的menu 並轉換成 使用者語系
        /// </summary>
        /// <param name="_UserInfo">使用者資訊</param>
        /// <returns>回傳轉換語系後的 menu list</returns>
        public IEnumerable<MenuViewModel> GetUserMenu(UserInfo _UserInfo)
        {
            //取使用者所有可用menu
            var userMenu = from user in _DsDb.SiteUserToRole
                           where user.User_Id == _UserInfo.Id
                           join role in _DsDb.SiteRoleToMenu on user.Role_Id equals role.SiteRole_Id
                           select role.Menu;

            if (userMenu != null)
            {
                //取menu文字
                var menu = from m in userMenu.ToList().Distinct()
                           join t in _DsDb.SiteText on m.LanKey equals t.LanKey
                           where t.CultureCode == _UserInfo.LanguageCode
                           select new MenuViewModel()
                           {
                               Text = t.LanText,
                               Action = m.ActionName,
                               Controller = m.ControllerName,
                               Order = m.MenuOrder
                           };

                if (menu != null)
                {
                    return menu.OrderBy(o => o.Order).ToList();
                }
            }
            return null;
        }
    }
}