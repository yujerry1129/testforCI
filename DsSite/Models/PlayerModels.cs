﻿using DsDbLib.Models;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Configuration;

namespace DsSite.Models
{
    public class PlayerModels : Controllers.Base.DsBaseController
    {
        #region Player API DB處理及回傳的方法

        public ApiResult CheckSystemKey(UserGroupData data)
        {
            if (data == null || string.IsNullOrEmpty(data.SystemKey))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get UserGroup
                UserGroup userGroup = _DsDb.UserGroup.SingleOrDefault(o => o.SystemKey == data.SystemKey);
                if (userGroup == null)
                {
                    return new ExceptionResult(Message: "Error user group");
                }
                else
                {
                    //return UserGroupData
                    UserGroupData userGroupData = new UserGroupData()
                    {
                        SystemKey = userGroup.SystemKey,
                        UserGroupId = userGroup.Id,
                        UserGroupName = userGroup.Name
                    };
                    return new SuccessResult(Data: userGroupData);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult CheckDeviceName(DeviceData data)
        {
            if (data == null || data.UserGroupId.HasValue == false || string.IsNullOrEmpty(data.DeviceName) || string.IsNullOrEmpty(data.DeviceName.Trim()))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check Device name duplicated
                if (CheckDeviceNameDuplicated(data.DeviceName, data.UserGroupId.Value))
                {
                    return new ExceptionResult(Message: "Device name is duplicated");
                }
                else
                {
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult RegisteDevice(MachineData data)
        {
            if (data == null || data.UserGroupId.HasValue == false || string.IsNullOrEmpty(data.SystemKey) || data.DeviceName == null)
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                int deviceLimitQuantity = 0;  //get device quantity limitation
                int deviceUsed = 0;  //check device count limit

                //check and get UserGroup
                UserGroup userGroup = _DsDb.UserGroup.SingleOrDefault(o => o.Id == data.UserGroupId);
                if (userGroup == null)
                {
                    return new ExceptionResult(Message: "Error user group");
                }
                else
                {
                    //check device amount limitaion
                    deviceLimitQuantity = userGroup.DeviceLimit;
                    deviceUsed = userGroup.Devices.Where(o => o.DeleteDate == null) is IEnumerable<Device> ? userGroup.Devices.Where(o => o.DeleteDate == null).Count() : 0;
                    if (deviceUsed >= deviceLimitQuantity)
                    {
                        return new ExceptionResult(Message: "Device amount reaches limitaion " + deviceLimitQuantity);
                    }

                    //check Device name duplicated
                    if (CheckDeviceNameDuplicated(data.DeviceName, data.UserGroupId.Value))
                    {
                        return new ExceptionResult(Message: "Device name is duplicated");
                    }

                    //check Device version
                    VersionFile version = _DsDb.VersionFile.FirstOrDefault(o => o.Version == data.DeviceVersion);
                    if (version == null)
                    {
                        return new ExceptionResult(Message: "Error player verion");
                    }

                    //create new device
                    Guid deviceToken = Guid.NewGuid();
                    Device newDevice = new Device()
                    {
                        DomainName = data.DomainName,
                        MachineName = data.MachineName,
                        Name = data.DeviceName,
                        MainDiskCapacity = data.DiskCapacity,
                        MainDiskUsage = data.DiskUsage,
                        IpAddressList = data.IpAddress,
                        MacAddress = data.MacAddress,
                        OperationSystem = data.OperationSystem,
                        DispatchStatus = 0,
                        UserGroup_Id = userGroup.Id,
                        FaceDetectionReportEnabled = true,
                        FaceDetectionType = userGroup.EnableFaceDetection ? 0 : new int?(),
                        VersionId = version.Id,
                        DeviceToken = deviceToken
                    };
                    _DsDb.Device.Add(newDevice);

                    //create device with 1~n screens
                    foreach (DeviceScreen screen in data.ScreenList)
                    {
                        _DsDb.DeviceScreen.Add(new DeviceScreen()
                        {
                            DeviceId = newDevice.Id,
                            Height = screen.Height,
                            Width = screen.Width,
                            IsPrimary = screen.IsPrimary,
                            ScreenIndex = screen.ScreenIndex
                        });
                    }

                    //save Db change
                    _DsDb.SaveChanges();

                    //將Device的資訊塞到result中的Data
                    var deviceData = new
                    {
                        DeviceId = newDevice.Id,
                        DeviceToken = deviceToken
                    };
                    return new SuccessResult(Data: deviceData);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult UpdateMachineStatus(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check UserGroup
                if (_DsDb.UserGroup.Any(o => o.Id == data.UserGroupId) == false)
                {
                    return new ExceptionResult(Message: "Error user group");
                }

                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "MACHINE_NOT_EXIST");
                }
                else
                {
                    //update LastConnectionTime and return result
                    device.LastConnectionTime = DateTime.Now;
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult GetProductRules(UserGroupData data)
        {
            if (data == null || string.IsNullOrEmpty(data.SystemKey))
            {
                return new ExceptionResult(Message: "Empty key");
            }

            try
            {
                //check and get UserGroup
                UserGroup userGroup = _DsDb.UserGroup.SingleOrDefault(o => o.SystemKey == data.SystemKey);
                if (userGroup == null)
                {
                    return new ExceptionResult(Message: "Error user group");
                }
                else
                {
                    //return ProductRules
                    var productRules = _DsDb.ProductRule.Where(o => o.UserGroupId == userGroup.Id).Select(o => new
                    {
                        RuleId = o.Id,
                        DMIDictionary = o.DMIDictionaries.ToList()
                    }).ToList();
                    return new SuccessResult(Data: productRules);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult GetSignageData(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //get all Schedules
                    List<ScheduleSummary> allSCS = new List<ScheduleSummary>();
                    if (device.ScreenGroupId.HasValue)
                    {
                        allSCS = _DsDb.ScheduleSummary.Where(o => (o.ScreenGroupId.Value == device.ScreenGroupId.Value) && (o.EndDate == null || o.EndDate >= DateTime.Now)).ToList();
                    }
                    else
                    {
                        allSCS = _DsDb.ScheduleSummary.Where(o => (o.DeviceId.Value == device.Id) && (o.EndDate == null || o.EndDate >= DateTime.Now)).ToList();
                    }

                    //get general Schedules
                    List<Schedule> schedules = new List<Schedule>();
                    List<ScheduleSummary> SCS = allSCS.Where(o => o.HasFaceDetectionRule == false && o.Spot == false).ToList();
                    foreach (ScheduleSummary sc in SCS)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == sc.CampaignId.Value);
                        schedules.Add(new Schedule(sc, campaign.Duration));
                    }

                    //get Rules
                    List<RuleFace> rules = new List<RuleFace>();
                    List<ScheduleSummary> RFS = allSCS.Where(o => o.HasFaceDetectionRule == true && o.Spot == false).ToList();
                    foreach (ScheduleSummary rf in RFS)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == rf.CampaignId.Value);
                        string[] ages = rf.AgeRestriction.Split(',');
                        foreach (string age in ages)
                        {
                            rules.Add(new RuleFace(rf, campaign.Duration, Int32.Parse(age)));
                        }
                    }

                    //get Spot
                    List<RuleSpot> spots = new List<RuleSpot>();
                    List<ScheduleSummary> SRS = allSCS.Where(o => o.HasFaceDetectionRule == false && o.Spot == true).ToList();
                    foreach (ScheduleSummary sr in SRS)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == sr.CampaignId.Value);
                        spots.Add(new RuleSpot(sr, campaign.Duration));
                    }

                    //get Campaigns
                    List<CampaignLayout> campaigns = new List<CampaignLayout>();
                    //get Campaigns from display task
                    foreach (Schedule d in schedules)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == d.CampaignId.Value);
                        campaigns.Add(new CampaignLayout(campaign));
                    }
                    //get Campaigns from rules
                    foreach (RuleFace r in rules)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == r.CampaignId);
                        campaigns.Add(new CampaignLayout(campaign));
                    }
                    //get Campaigns from spots
                    foreach (RuleSpot s in spots)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == s.CampaignId);
                        campaigns.Add(new CampaignLayout(campaign));
                    }
                    //distinct Campaigns
                    campaigns = campaigns.Distinct(o => o.Id).ToList();

                    //get Regions
                    List<Region> regions = new List<Region>();
                    //get Regions from Campaigns
                    foreach (CampaignLayout layout in campaigns)
                    {
                        Campaign campaign = _DsDb.Campaign.SingleOrDefault(o => o.Id == layout.Id);
                        foreach (CampaignRegion cr in campaign.Regions.ToList())
                        {
                            regions.Add(new Region(campaign, cr));
                        }
                    }
                    //distinct Regions
                    regions = regions.Distinct(o => o.Id).OrderBy(o => o.LayerOrder).ToList();

                    //get Segments and Media
                    List<Segment> segments = new List<Segment>(); //all Segments
                    List<Media> mediaList = new List<Media>();  //all Media
                    //get Segments and Media from Regions
                    foreach (Region region in regions)
                    {
                        List<RegionFrame> rfs = _DsDb.RegionFrame.Where(o => o.RegionId == region.Id).ToList();
                        foreach (RegionFrame rf in rfs)
                        {
                            int mediaId = 0; //Media Id
                            string name = ""; //Media name
                            int mediatype = 0; //Media type

                            //依照RegionFrame性質不同編輯Media內容
                            if (rf.FileId.HasValue) //擁有File的Media
                            {
                                mediaId = rf.FileId.Value;
                                FileUpload file = _DsDb.FileUpload.SingleOrDefault(o => o.Id == mediaId);
                                mediatype = file.FileType;

                                mediaList.Add(new Media()
                                {
                                    Id = mediaId,
                                    Name = file.Name,
                                    FileExtensionName = file.ExtensionName,
                                    OriginalName = file.Name,
                                    Size = Convert.ToInt32(file.Size),
                                    MediaType = file.FileType,
                                    HashCode = file.HashCode,
                                    ContentDescription = file.Content
                                });
                            }
                            else if (rf.TextAssetId.HasValue) //存有TextAsset資訊的Media
                            {
                                TextAsset ta = _DsDb.TextAsset.SingleOrDefault(o => o.Id == rf.TextAssetId.Value);
                                mediaId = ta.Id;
                                name = ta.Name;
                                mediatype = ta.AssetType;

                                if (rf.FrameType == 4) //Text
                                {
                                    ta.Content = "&lt;div style=&quot;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;&quot;&gt;" + ta.Content + "&lt;/div&gt;";

                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = ta.AssetType,
                                        Url = ta.Content,
                                        ContentDescription = ta is TextAsset ? ta.Content : ""
                                    });
                                }
                                else if (rf.FrameType == 7) //RSS
                                {
                                    Rss jsonToObject = JsonConvert.DeserializeObject<Rss>(ta.Content);

                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = ta.AssetType,
                                        Url = jsonToObject.Url,
                                        ContentDescription = ta.Content
                                    });
                                }
                                else if (rf.FrameType == 12) //Menu
                                {
                                    MenuContent mc = JsonConvert.DeserializeObject<MenuContent>(ta.Content);
                                    FileUpload excel = _DsDb.FileUpload.SingleOrDefault(o => o.Id == mc.MenuId);
                                    FileUpload bgFile = _DsDb.FileUpload.SingleOrDefault(o => o.Id == mc.BackgroudFileId);
                                    string bgFileName = "";
                                    string textEffect = mc.TextEffect.ToString();
                                    mediaId = excel.Id;

                                    if (bgFile is FileUpload)
                                    {
                                        mediaList.Add(new Media() //背景圖檔
                                        {
                                            Id = bgFile.Id,
                                            Name = bgFile.Name,
                                            FileExtensionName = bgFile.ExtensionName,
                                            OriginalName = bgFile.Name,
                                            Size = Convert.ToInt32(bgFile.Size),
                                            MediaType = bgFile.FileType,
                                            HashCode = bgFile.HashCode
                                        });

                                        bgFileName = bgFile.HashCode + bgFile.ExtensionName;
                                    }

                                    mediaList.Add(new Media() //Excel檔
                                    {
                                        Id = mediaId,
                                        Name = excel.Name,
                                        FileExtensionName = excel.ExtensionName,
                                        OriginalName = excel.Name,
                                        Size = Convert.ToInt32(excel.Size),
                                        MediaType = excel.FileType,
                                        ContentDescription = bgFileName + "," + textEffect,
                                        HashCode = excel.HashCode
                                    });
                                }
                                else if (rf.FrameType == 16) //Countdown Timer
                                {
                                    CountdownTimerContent ctc = JsonConvert.DeserializeObject<CountdownTimerContent>(ta.Content);
                                    string contentSrting = ctc.DateTime + "," +
                                        ctc.Color + "," +
                                        ctc.Settings.Year.ToString() + "," +
                                        ctc.Settings.Month.ToString() + "," +
                                        ctc.Settings.Day.ToString() + "," +
                                        ctc.Settings.Hour.ToString() + "," +
                                        ctc.Settings.Minute.ToString() + "," +
                                        ctc.Settings.Second.ToString() + "," +
                                        ctc.Settings.Millisecond.ToString();

                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = rf.FrameType,
                                        ContentDescription = contentSrting
                                    });
                                }
                                else //其他TextAsset類的Media，例如:Web Page、Marquee、Youtube
                                {
                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = ta.AssetType,
                                        Url = ta.Content,
                                        ContentDescription = ta is TextAsset ? ta.Content : ""
                                    });
                                }
                            }
                            else //單以Content帶資訊的Media
                            {
                                mediaId = rf.Id;
                                name = "Widget";
                                mediatype = rf.FrameType;

                                if (rf.FrameType == 10) //Weather
                                {
                                    name += "_Weather";

                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = rf.FrameType,
                                    });
                                }
                                else if (rf.FrameType == 11) //Clock
                                {
                                    name += "_Clock";

                                    mediaList.Add(new Media()
                                    {
                                        Id = mediaId,
                                        Name = name,
                                        MediaType = rf.FrameType,
                                        ContentDescription = rf.Content //0:傳統時鐘 1:電子時鐘
                                    });
                                }
                            }

                            //編輯Segments
                            segments.Add(new Segment()
                            {
                                Id = rf.Id,
                                RegionId = region.Id,
                                MediaId = mediaId,
                                MediaType = mediatype,
                                Sequence = rf.Order,
                                Duration = rf.Length
                            });
                        }
                    }

                    //check and get DefaultImage
                    int? defaultImageId = _DsDb.Device.SingleOrDefault(o => o.Id == data.DeviceId)?.DefaultScreenFileId;
                    if (defaultImageId.HasValue)
                    {
                        FileUpload defaultMedia = _DsDb.FileUpload.SingleOrDefault(o => o.Id == defaultImageId.Value);
                        if (defaultMedia is FileUpload && mediaList.Any(o => o.Id == defaultImageId) == false)
                        {
                            mediaList.Add(new Media()
                            {
                                Id = defaultImageId.Value,
                                Name = defaultMedia.Name,
                                FileExtensionName = defaultMedia.ExtensionName,
                                OriginalName = defaultMedia.Name,
                                Size = Convert.ToInt32(defaultMedia.Size),
                                MediaType = defaultMedia.FileType,
                                HashCode = defaultMedia.HashCode
                            });
                        }
                    }

                    //return SignageData
                    var signageData = new
                    {
                        Schedules = schedules,
                        Campaigns = campaigns,
                        Regions = regions,
                        Segments = segments.Distinct(o => o.Id).ToList(),
                        Medias = mediaList.Distinct(o => o.Id + "_" + o.MediaType).ToList(),
                        Spots = spots,
                        Rules = rules,
                        DefaultScreen = defaultImageId
                    };
                    return new SuccessResult(Data: signageData);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult GetConfigUpdate(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check ane get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    ScreenGroupDetail screenGroup = null;
                    TimeServer timeServer = null;

                    //如果是ScreenGroup，則取得相對應的ScreenGroupDetail及TimeServer
                    if (device.ScreenGroup is ScreenGroup)
                    {
                        Device timeServerDevice = device.ScreenGroup == null ? null : _DsDb.Device.SingleOrDefault(o => o.Id == device.ScreenGroup.TimeServerId);
                        screenGroup = new ScreenGroupDetail(device.ScreenGroup, device.OffsetX.Value, device.OffsetY.Value);
                        timeServer = new TimeServer(timeServerDevice);
                    }

                    //return ConfigData
                    ConfigData cd = new ConfigData()
                    {
                        DeviceName = device.Name,
                        ScreenGroup = screenGroup,
                        TimeServer = timeServer
                    };
                    return new SuccessResult(Data: cd);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult CheckNewDispatch(DeviceData data)
        {

            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    DispatchData dd = new DispatchData();
                    bool versionUpdate = false;
                    string tokenMessage = "";

                    //check Signage
                    if (device.DispatchStatus == 2)
                    {
                        dd.HasNewSignage = true;
                    }

                    dd.IsNeedRestart = device.IsNeedRestart;

                    //若為screengroup，檢查其中的Device版本有沒有不一樣的，不一樣則升級
                    if (device.ScreenGroupId.HasValue)
                    {
                        int targetVersionId = device.VersionId;
                        ScreenGroup screenGroup = device.ScreenGroup;
                        versionUpdate = _DsDb.Device.Any(o => o.ScreenGroupId == screenGroup.Id && o.VersionId != targetVersionId);
                    }

                    //若需要升級，設定升級的資訊
                    if (device.IsNeedUpdate || versionUpdate)
                    {
                        //取得最新的版本號
                        ConvertHelper convertHelper = new ConvertHelper();
                        string MaxVersion = convertHelper.CompareVersion(_DsDb.VersionFile.Select(o => o.Version).ToList());
                        VersionFile newVersionFile = _DsDb.VersionFile.Where(o => o.Version == MaxVersion).FirstOrDefault();
                        //若現在device的版本不是最新的，才更新，避免已經是最新版了依舊需要更新
                        if (newVersionFile.Id != device.VersionId)
                        {
                            TimeSpan? versionUpdateTime = device.VersionUpdateTime;
                            string version = newVersionFile.Version;
                            long updateFileSize = newVersionFile.UpdateFileSize;
                            string updateFileMD5Hash = newVersionFile.UpdateFileMD5Hash;

                            dd.IsNeedUpdate = true;
                            dd.VersionUpdateTime = versionUpdateTime;
                            dd.Version = version;
                            dd.UpdateFileSize = updateFileSize;
                            dd.UpdateFileMD5Hash = updateFileMD5Hash;
                        }
                    }

                    //check ConfigStatus update
                    if (device.ConfigStatus)
                    {
                        dd.ConfigUpdate = true;
                    }

                    //check and set DeviceToken message
                    if (device.DeviceToken.Equals(new Guid("00000000-0000-0000-0000-000000000000")))
                    {
                        Guid deviceToken = Guid.NewGuid();
                        tokenMessage = "new Guid: " + deviceToken.ToString();
                        device.DeviceToken = deviceToken;
                        _DsDb.SaveChanges();
                    }

                    //retuen result
                    return new SuccessResult(Data: dd) { Message = tokenMessage };
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult SetDispatched(DispatchedData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    int deviceId = device.Id;
                    long? screenGroupId = device.ScreenGroupId;

                    //set ConfigStatus
                    if (data.ConfigUpdated)
                    {
                        device.ConfigStatus = false;
                    }

                    //set DispatchStatus and ContentStatuss
                    if (data.SignageUpdated && device.DispatchStatus == 2 && device.ContentStatus == 1)
                    {
                        //check ScheduleSummary
                        ScheduleSummary SCS = _DsDb.ScheduleSummary.FirstOrDefault(o => (o.DeviceId.HasValue && o.DeviceId.Value == data.DeviceId) || (o.ScreenGroupId.HasValue && screenGroupId.HasValue && o.ScreenGroupId.Value == screenGroupId.Value));
                        if (SCS == null)
                        {
                            device.DispatchStatus = 0;
                        }
                        else
                        {
                            device.DispatchStatus = 1;
                        }

                        device.ContentStatus = 0;
                    }

                    //save Db change and retrun result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }
        public ApiResult SetRestarted(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    device.IsNeedRestart = false;
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }


        public ApiResult RemoveDevice(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get UserGroup
                UserGroup userGroup = _DsDb.UserGroup.SingleOrDefault(o => o.Id == data.UserGroupId);
                if (userGroup == null)
                {
                    return new ExceptionResult(Message: "Error user group");
                }
                else
                {
                    //check and get Device
                    Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                    if (device == null)
                    {
                        return new ExceptionResult(Message: "Error device");
                    }
                    else
                    {
                        //remove Device and related Schedule
                        new DsSite.Models.PublishModels().AdjustbyDeleteDevice(data.DeviceId.Value); ;

                        //remove DeviceScreens
                        _DsDb.DeviceScreen.RemoveRange(_DsDb.DeviceScreen.Where(o => o.DeviceId == device.Id));

                        //add DeleteDate to Device
                        device.DeleteDate = DateTime.Now;
                        device.DeleteByUserId = device.CreateByUserId;

                        //save Db change and return result
                        _DsDb.SaveChanges();
                        return new SuccessResult();
                    }
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult UpdateDeviceInfo(MachineData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check UserGroup
                if (_DsDb.UserGroup.Any(o => o.Id == data.UserGroupId) == false)
                {
                    return new ExceptionResult(Message: "Error user group");
                }

                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //update Device
                    device.MainDiskUsage = data.DiskUsage;
                    device.MainDiskCapacity = data.DiskCapacity;
                    device.IpAddressList = data.IpAddress;
                    device.MacAddress = data.MacAddress;

                    //get Device Version Id
                    VersionFile deviceVersionFile = _DsDb.VersionFile.FirstOrDefault(o => o.Version == data.DeviceVersion);
                    int versionId = deviceVersionFile is VersionFile ? deviceVersionFile.Id : -1;
                    //set Device Version Id
                    device.VersionId = versionId;
                    //check Version update
                    ConvertHelper convertHelper = new ConvertHelper();
                    string MaxVersion = convertHelper.CompareVersion(_DsDb.VersionFile.Select(o => o.Version).ToList());
                    VersionFile newVersionFile = _DsDb.VersionFile.Where(o => o.Version == MaxVersion).FirstOrDefault();
                    if (versionId == newVersionFile.Id)
                    {
                        device.IsNeedUpdate = false;
                        device.VersionUpdateTime = device.AutoUpdateTime;
                    }
                    else if (device.AutoUpdateEnabled)
                    {
                        device.IsNeedUpdate = true;
                    }

                    var screen = data.ScreenList.First();
                    var deviceScreen = Db.DeviceScreen.Where(o => o.DeviceId == data.DeviceId).Single();
                    deviceScreen.Width = screen.Width;
                    deviceScreen.Height = screen.Height;

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult UploadDisplayLog(InsertDisplayLogData data)
        {
            if (RequestDataNotValid(data) ||
                data.Logs == null || data.Logs.Count == 0)
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check UserGroup
                if (_DsDb.UserGroup.Any(o => o.Id == data.UserGroupId) == false)
                {
                    return new ExceptionResult(Message: "Error user group");
                }

                //check Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    device.LastConnectionTime = DateTime.Now;
                    //add DisplayLog
                    foreach (DisplayLog log in data.Logs)
                    {
                        Campaign campaignname = _DsDb.Campaign.SingleOrDefault(o => o.Id == log.CampaignId);
                        log.CampaignName = campaignname.Name;
                    }
                    _DsDb.DisplayLog.AddRange(data.Logs);

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult UploadMiddlewareLog(InsertMiddlewareLogData data)
        {
            if (RequestDataNotValid(data) ||
                data.MiddlewareLogList == null || data.MiddlewareLogList.Count == 0)
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check UserGroup
                if (_DsDb.UserGroup.Any(o => o.Id == data.UserGroupId) == false)
                {
                    return new ExceptionResult(Message: "Error user group");
                }

                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //set age range
                    Dictionary<int, Tuple<int?, int?>> ageRange = new Dictionary<int, Tuple<int?, int?>>();
                    ageRange.Add(0, new Tuple<int?, int?>(0, 17));
                    ageRange.Add(1, new Tuple<int?, int?>(18, 30));
                    ageRange.Add(2, new Tuple<int?, int?>(31, 59));
                    ageRange.Add(3, new Tuple<int?, int?>(60, null));
                    ageRange.Add(9, new Tuple<int?, int?>(null, null));
                    //若之後改為Age區間改為動態，則將上方定義區間設定移除掉。
                    //var dbAge = db.AgeRangeDefinition.ToList();
                    //foreach (var age in dbAge)
                    //{
                    //    ageRange.Add(age.Id, new Tuple<int?, int?>(age.MinValue, age.MaxValue));
                    //}

                    //add FaceDetectionLog
                    List<FaceDetectionLog> fdLog = new List<FaceDetectionLog>();
                    foreach (MiddlewareLogData log in data.MiddlewareLogList)
                    {
                        int? ageBottom = null;
                        int? ageTop = null;

                        if (log.Age.HasValue)
                        {
                            ageBottom = ageRange[log.Age.Value].Item1;
                            ageTop = ageRange[log.Age.Value].Item2;
                        }

                        fdLog.Add(new FaceDetectionLog()
                        {
                            DeviceId = device.Id,
                            UserGroupId = data.UserGroupId.Value,
                            LocationId = device.Location is Location ? device.Location.Id : new int?(),
                            LookTime = log.LookTime.HasValue ? log.LookTime.Value : 0,
                            Gender = log.Gender.HasValue ? log.Gender.Value : 0,
                            AgeId = log.Age.HasValue ? log.Age.Value : 9,
                            AgeBottom = ageBottom,
                            AgeTop = ageTop,
                            Distance = log.Distance.HasValue ? log.Distance.Value : 0,
                            LogTime = log.LogTime.HasValue ? log.LogTime.Value : new DateTime(0),
                            Year = log.LogTime.HasValue ? log.LogTime.Value.Year : new int?(),
                            Month = log.LogTime.HasValue ? log.LogTime.Value.Month : new int?(),
                            Day = log.LogTime.HasValue ? log.LogTime.Value.Day : new int?(),
                            Hour = log.LogTime.HasValue ? log.LogTime.Value.Hour : new int?()
                        });
                    }
                    _DsDb.FaceDetectionLog.AddRange(fdLog);

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult UploadMiddlewareErrorLog(ErrorMiddlewareLog data)
        {
            int ExceptionInLogType = 17; //Exception的Type被定義為17。

            if (RequestDataNotValid(data) ||
                data.MiddlewareErrorLogList == null || data.MiddlewareErrorLogList.Count == 0)
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check UserGroup
                if (_DsDb.UserGroup.Any(o => o.Id == data.UserGroupId) == false)
                {
                    return new ExceptionResult(Message: "Error user group");
                }

                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //add PlayerInformationLog
                    List<PlayerInformationLog> pLog = new List<PlayerInformationLog>();
                    foreach (ErrorLog log in data.MiddlewareErrorLogList)
                    {
                        pLog.Add(new PlayerInformationLog()
                        {
                            DeviceId = data.DeviceId.Value,
                            UserGroupId = device.UserGroup_Id,
                            LogType = ExceptionInLogType, //Exception
                            LogTime = log.LogTime,
                            LogMessage = log.ErrorType + "," + log.ErrorMessage,
                        });
                    }
                    _DsDb.PlayerInformationLog.AddRange(pLog);

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult GetMachineSetting(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check and get Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //set UserGroup and DeviceFaceSetting
                    UserGroup group = device.UserGroup;
                    DeviceFaceSetting faceSetting = new DeviceFaceSetting()
                    {
                        DeviceId = device.Id,
                        FaceDetection = device.FaceDetectionType.HasValue ? 1 : 0,
                        FaceDetectionType = device.FaceDetectionType.HasValue ? device.FaceDetectionType.Value : 0,
                        FaceReport = device.FaceDetectionReportEnabled ? 1 : 0
                    };

                    //return MachineSetting
                    MachineSetting machineSetting = new MachineSetting()
                    {
                        WatchDogSetting = new WatchDogSetting()
                        {
                            IsOn = group.EnableWatchDogLog,
                            Time = group.WatchDogShutDownTime.HasValue ? group.WatchDogShutDownTime.Value : group.WatchDogShutDownTime,
                            Log = group.EnableWatchDogLog
                        },
                        DeviceFaceSetting = faceSetting,
                        ShutDownSetting = new AutoShutdown()
                        {
                            AutoShutDownTime = device.DailyShutDownTime,
                            LastShutDownTime = null
                        }
                    };
                    return new SuccessResult(Data: machineSetting);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public HttpResponseMessage GetFile(string deviceToken, int mediaId, string downloadToken)
        {
            if (deviceToken == null || mediaId == 0 || downloadToken == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            try
            {
                //check and get Media File
                FileUpload media = _DsDb.FileUpload.SingleOrDefault(m => m.Id == mediaId);
                if (media == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                //check and get Device
                Device device = _DsDb.Device.SingleOrDefault(o => o.DeviceToken.Equals(new Guid(deviceToken)));
                if (device == null || media.UserGroupId != device.UserGroup_Id)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                //check and get File path
                string mediaUploadPath = WebConfigurationManager.AppSettings["UploadFolder"] + "/" + media.UserGroupId.ToString() + "/" + "origin-files" + "/";
                if (media.FileType == 12)
                {
                    mediaUploadPath += "menu" + "/";
                }
                string filePath = mediaUploadPath + media.HashCode.ToString() + media.ExtensionName;
                if (System.IO.File.Exists(filePath) == false)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
                else
                {
                    //設定FileShare以允許多個Client同時對相同檔案存取
                    FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    fs.Position = 0;

                    //設定回傳的Response
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StreamContent(fs);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + media.Name + "\"");

                    //add FileDownloadLog
                    _DsDb.FileDownloadLog.Add(new FileDownloadLog()
                    {
                        DownLoadToken = new Guid(downloadToken),
                        DeviceId = device.Id,
                        UserGroupId = media.UserGroupId,
                        FileId = media.Id,
                        FileType = media.FileType,
                        FileName = media.Name + media.ExtensionName,
                        FileSize = media.Size,
                        DownloadType = 1, //player
                        StartDownloadTime = DateTime.Now,
                    });

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return result;
                }
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        public HttpResponseMessage GetUpdateFile(string deviceToken, string downloadToken)
        {
            if (deviceToken == null || downloadToken == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            try
            {
                //check and get Device
                Device device = _DsDb.Device.SingleOrDefault(o => o.DeviceToken == new Guid(deviceToken));
                if (device == null && !deviceToken.Equals("00000000-0000-0000-0000-000000000000"))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                //check and get Version File
                VersionFile newVersionFile = _DsDb.VersionFile.OrderByDescending(o => o.CreateDate).FirstOrDefault();
                if (newVersionFile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                //check and get File path
                string versionFileUploadPath = WebConfigurationManager.AppSettings["UploadFolder"] + "/VersionRecord/AcePlayerSetup-" + newVersionFile.Version + "/";
                string filePath = versionFileUploadPath + "AcePlayerSetup-" + newVersionFile.Version + ".msi";
                if (System.IO.File.Exists(filePath) == false)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
                else
                {
                    //設定FileShare以允許多個Client同時對相同檔案存取
                    FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    fs.Position = 0;

                    //設定回傳的Response
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StreamContent(fs);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.Add("content-disposition", "attachment; filename=\"" + newVersionFile.Version + ".msi\"");

                    //add FileDownloadLog,由於Player不需要知道Version在資料庫中的ID，因此下載PlayerMSI的時候，FileId為0
                    _DsDb.FileDownloadLog.Add(new FileDownloadLog()
                    {
                        DownLoadToken = new Guid(downloadToken),
                        DeviceId = device.Id,
                        UserGroupId = device.UserGroup_Id,
                        FileId = 0,
                        FileType = 14,//PlayerMSI的Type號碼為14
                        FileName = newVersionFile.Version,
                        FileSize = newVersionFile.UpdateFileSize,
                        DownloadType = 1, //player
                        StartDownloadTime = DateTime.Now,
                    });

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return result;
                }
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        public ApiResult UploadDownloadLog(DownloadLogData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //update FileDownloadLog
                    foreach (DownloadLog dl in data.Logs)
                    {
                        //由於Player不需要知道Version在資料庫中的ID，因此下載PlayerMSI的時候，FileId為0
                        FileDownloadLog fdl = _DsDb.FileDownloadLog.SingleOrDefault(o => o.DownLoadToken == dl.DownloadToken && o.FileType == dl.FileType && (o.FileId == 0 || o.FileId == dl.FileId));

                        if (dl.Progress > fdl.DownLoadPercentage)
                        {
                            if (dl.Progress == 100)
                            {
                                fdl.DownloadSuccess = true;
                            }
                            fdl.TransferedFileSize = (long)(fdl.FileSize * dl.Progress / 100);
                            fdl.DownLoadPercentage = dl.Progress;
                            fdl.EndDownloadTime = dl.DownloadTime;
                        }
                    }

                    //save Db change and return result
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        public ApiResult GetWeather(DeviceData data)
        {
            if (RequestDataNotValid(data))
            {
                return new ExceptionResult(Message: "Empty data");
            }

            try
            {
                //check Device
                Device device = CheckAndGetDevice(data.DeviceId.Value, data.UserGroupId.Value, data.DeviceToken);
                if (device == null)
                {
                    return new ExceptionResult(Message: "Error device");
                }
                else
                {
                    //get WeatherData
                    int locationId = device.LocationId.HasValue ? device.LocationId.Value : -1;
                    WeatherData weatherData = new WeatherModel().CheckAndGetWeatherData(locationId);

                    //return result
                    return new SuccessResult(Data: weatherData);
                }
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: ex.Message);
            }
        }

        #endregion

        #region 共用方法

        private bool CheckDeviceNameDuplicated(string name, int userGroupId)
        {
            return _DsDb.Device.Any(o => o.Name == name && o.UserGroup_Id == userGroupId);
        }

        private bool RequestDataNotValid(DeviceData data)
        {
            bool flag = false;
            if (data == null || data.DeviceToken == null || data.DeviceId.HasValue == false || data.UserGroupId.HasValue == false)
            {
                flag = true;
            }
            return flag;
        }

        private Device CheckAndGetDevice(int deviceId, int userGroupId, string deviceToken)
        {
            Device device = null;
            //（暫時容納以前沒有DeviceToken的player，一段時間後刪除這一段程式碼）
            if (deviceToken.Equals("00000000-0000-0000-0000-000000000000"))
            {
                device = _DsDb.Device.SingleOrDefault(o => o.Id == deviceId && o.UserGroup_Id == userGroupId && o.DeleteByUserId == null);
                if (device is Device && !device.DeviceToken.Equals(new Guid("00000000-0000-0000-0000-000000000000")))
                {
                    device = null;
                }
            }
            else
            {
                device = _DsDb.Device.SingleOrDefault(o => o.Id == deviceId && o.UserGroup_Id == userGroupId && o.DeleteByUserId == null && o.DeviceToken.Equals(new Guid(deviceToken)));
            }
            return device;
        }

        #endregion
    }

    #region 擴充方法

    /// <summary>
    /// 擴充方法:Linq針對class中的資料過濾相同資料。
    /// </summary>
    public static class EnumerableExtender
    {
        public static IEnumerable<TSource> Distinct<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                TKey elementValue = keySelector(element);
                if (seenKeys.Add(elementValue))
                {
                    yield return element;
                }
            }
        }
    }

    #endregion
}