﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DsDbLib;
using DsSite.Controllers.Api.Models;
using DsSite.Controllers.Api;

namespace DsSite.Models.ViewModels.Modals
{

    public class ReportModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得DB的LooktimeRange、DistanceRange定義
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public object InputParams(UserInfo _UserInfo)
        {
            return new
            {
                Result = true,
                LooktimeRangeDefinition = _DsDb.LooktimeRangeDefinition.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId).ToList(),
                DistanceRangeDefinition = _DsDb.DistanceRangeDefinition.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId).ToList(),
                AgeRangeDefinition = _DsDb.AgeRangeDefinition.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId).ToList()
            };
        }

        /// <summary>
        /// 挑選DectectionLog在篩選範圍內的所有資料
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult Query(ReportApiInputModel Model, UserInfo _UserInfo)
        {
            //Test delay
            //Thread.Sleep(1000);

            var startDate = new DateTime();
            var endDate = new DateTime();

            #region 檢查時間格式，並初始化開始/結束時間區間
            //
            var c = new char[2] { '-', '/' };

            if (string.IsNullOrEmpty(Model.StartDateString) || string.IsNullOrEmpty(Model.EndDateString) || string.IsNullOrEmpty(Model.TimeType))
            {
                return new ExceptionResult(Message: "Error_0058");
            }

            var start = Model.StartDateString.Split(c);
            var end = Model.EndDateString.Split(c);

            if (start == null || end == null)
            {
                return new ExceptionResult(Message: "Error_0058");
            }

            try
            {
                switch (Model.TimeType.ToLower())
                {
                    case "year":
                        startDate = DateTime.Parse(start[0] + "/1/1");
                        endDate = DateTime.Parse(end[0] + "/12/31").AddDays(1);
                        break;

                    case "month":
                        startDate = DateTime.Parse(start[0] + "/" + start[1] + "/1");
                        endDate = DateTime.Parse(end[0] + "/" + end[1] + "/" + DateTime.DaysInMonth(int.Parse(end[0]), int.Parse(end[1]))).AddDays(1);
                        break;

                    case "day":
                        startDate = DateTime.Parse(start[0] + "/" + start[1] + "/" + start[2]);
                        endDate = DateTime.Parse(end[0] + "/" + end[1] + "/" + end[2]).AddDays(1);
                        break;

                    default:
                        return new ExceptionResult(Message: "Error_0058");
                }
            }
            catch
            {
                return new ExceptionResult(Message: "Error_0058");
            }

            if (endDate <= startDate)
            {
                return new ExceptionResult(Message: "Error_0030");
            }
            //
            #endregion

            try
            {

                var data = _DsDb.FaceDetectionLog.Where(o => o.LogTime >= startDate && o.LogTime < endDate && o.UserGroupId == _UserInfo.Id)
                    .Select(o => new
                    {
                        //LooktimeRangeId
                        Lid = _DsDb.LooktimeRangeDefinition.FirstOrDefault(range => range.MinValue <= o.LookTime && o.LookTime < range.MaxValue && range.UserGroup_Id == _UserInfo.UserGroupId).Id,
                        //DistanceRangeId
                        Did = _DsDb.DistanceRangeDefinition.FirstOrDefault(range => range.MinValue <= o.Distance && o.Distance < range.MaxValue && range.UserGroup_Id == _UserInfo.UserGroupId).Id,
                        Age = o.AgeId,
                        Gender = o.Gender,
                        Year = o.Year,
                        Month = o.Month,
                        Day = o.Day,
                        Hour = o.Hour
                    })
                    .GroupBy(o => new { o.Lid, o.Did, o.Age, o.Gender, o.Year, o.Month, o.Day, o.Hour })
                    .Select(o => new
                    {
                        Lid = o.Key.Lid,
                        Did = o.Key.Did,
                        Age = o.Key.Age,
                        Gender = o.Key.Gender,
                        Year = o.Key.Year,
                        Month = o.Key.Month,
                        Day = o.Key.Day,
                        Hour = o.Key.Hour,
                        Count = o.Count()
                    })
                    .ToList();

                if (data.Count == 0)
                {
                    return new ExceptionResult(Message: "Error_0018");
                }

                return new SuccessResult(
                    Data: new
                    {
                        FaceDectionLog = data,
                        DataStartDate = startDate,
                        DataEndDate = endDate
                    });


            }
            catch (Exception ex)
            {
                if (ex is OutOfMemoryException)
                {
                    return new ExceptionResult(Message: "Error_0059");
                }
                else
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

            }
        }
    }
}