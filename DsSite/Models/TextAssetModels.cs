﻿using DsDbLib.Models;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Xml;

namespace DsSite.Models.Modals
{
    public class TextAssetModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得當前DB所有文字資源(含文字、跑馬燈、網址)
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllTextAsset(UserInfo _UserInfo)
        {
            var data = _DsDb.TextAsset
                .Where(o => o.DeleteByUserId == null && o.UserGroupId == _UserInfo.UserGroupId && (o.AssetType == 4 || o.AssetType == 5 || o.AssetType == 6 || o.AssetType == 7 || o.AssetType == 8))
                .OrderBy(o => o.AssetType)
                .ToList();

            var temp = new List<object>();
            foreach (var i in data)
            {
                temp.Add(new
                {
                    Id = i.Id,
                    Name = i.Name,
                    Type = i.AssetType, //MediaHelper.GetMediaTypeString(i.AssetType),
                    Text = i.AssetType == 4 ? HttpUtility.HtmlDecode(i.Content) : i.Content

                });
            }

            return new SuccessResult(Data: temp);
        }

        /// <summary>
        /// 儲存文字資源(含文字、跑馬燈、網址)
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult SaveTextAsset(TextAssetApiModel Model, UserInfo _UserInfo)
        {
            if (Model.Type == 4)
            {
                Model.Text = HttpUtility.HtmlEncode(Model.Text);
            }

            if (Model.Id == 0)
            {
                //add
                var asset = new TextAsset()
                {
                    Name = Model.Name.Trim(),
                    AssetType = Model.Type,
                    Content = Model.Text,
                    UserGroupId = _UserInfo.UserGroupId,
                    CreateDate = DateTime.Now,
                    CreateByUserId = _UserInfo.Id
                };

                _DsDb.TextAsset.Add(asset);
                _DsDb.SaveChanges();

                //將新增檔案資訊存入log
                MediaLogModels MediaLogModels = new MediaLogModels();
                MediaLogModels.MediaAssetLog(asset, _UserInfo, 1);

                if (asset.AssetType == 4)
                {
                    asset.Content = HttpUtility.HtmlDecode(asset.Content);
                }

                return new SuccessResult(Data: asset);
            }
            else
            {
                List<int> campaignTextAssetUsedList = new List<int>();

                //update
                var asset = _DsDb.TextAsset.FirstOrDefault(o => o.Id == Model.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);

                asset.Name = Model.Name;
                asset.Content = Model.Text;
                asset.UpdateByUserId = _UserInfo.Id;
                asset.UpdateDate = DateTime.Now;

                _DsDb.SaveChanges();


                //找出那些有引用的campaign region
                var regionId = FindRegionUsedwithTextAsset(Model.Id, _UserInfo);
                foreach (var campaignRegionId in regionId)
                {
                    int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                    campaignTextAssetUsedList.Add(campaignId);
                }

                //找出scheduleSummary 中有用到此campaignId 的DeviceId
                List<ScheduleSummary> DeviceIdList = new List<ScheduleSummary>();
                //在ScheduleSummary中全部的 Campaign List 
                var campaigninScheduleSummary = _DsDb.ScheduleSummary.Where(o => o.CampaignId.HasValue && o.UserGroupId == _UserInfo.UserGroupId).ToList();

                foreach (var campaignId in campaigninScheduleSummary)
                {
                    foreach (var usedId in campaignTextAssetUsedList)
                    {
                        //比較 是否有相同的 CampaignId
                        if (campaignId.CampaignId == usedId)
                        {
                            //若為 screenGroup
                            if (campaignId.DeviceId == null)
                            {
                                var dinSg = _DsDb.Device.Where(o => o.ScreenGroupId == campaignId.ScreenGroupId).Select(o => o.Id).Distinct().ToList();
                                foreach (var d in dinSg)
                                {
                                    DeviceIdList.Add(new ScheduleSummary()
                                    {
                                        DeviceId = d
                                    });
                                }
                            }
                            else
                            {
                                //若為 一般device
                                DeviceIdList.Add(new ScheduleSummary()
                                {
                                    DeviceId = campaignId.DeviceId.Value
                                });

                            }

                        }
                    }
                }
                //修改內容狀態及派送狀態
                PublishModels PublishModels = new PublishModels();
                var ContentStatus = PublishModels.ChangeContentStatus(DeviceIdList, null);

                //將修改檔案資訊存入log
                MediaLogModels MediaLogModels = new MediaLogModels();
                //var action = MediaLogModels.CheckAction(Model.Id);
                MediaLogModels.MediaAssetLog(asset, _UserInfo, 2);

                if (asset.AssetType == 4)
                {
                    asset.Content = HttpUtility.HtmlDecode(asset.Content);
                }

                return new SuccessResult(Data: asset);
            }
        }

        /// <summary>
        /// 刪除單筆文字資源(含文字、跑馬燈、網址)
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteTextAsset(TextAssetApiModel Model, UserInfo _UserInfo)
        {

            var asset = _DsDb.TextAsset.FirstOrDefault(o => o.Id == Model.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);

            if (asset != null)
            {
                _DeleteRegionFrameTextAsset(Model.Id);

                asset.DeleteDate = DateTime.Now;
                asset.DeleteByUserId = _UserInfo.Id;

                try
                {
                    //將刪除檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaAssetLog(asset, _UserInfo, 3);
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult(Data: asset);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        public void _DeleteRegionFrameTextAsset(int textId)
        {
            //要更改的deviceId
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();

            var deleteTextRegion = _DsDb.RegionFrame.Where(o => o.TextAssetId == textId).ToList();

            if (deleteTextRegion.Count > 0)
            {
                var regionIds = deleteTextRegion.Select(o => o.RegionId);
                foreach (var id in regionIds)
                {
                    var regionOrders = _DsDb.RegionFrame.Where(o => o.RegionId == id && o.TextAssetId != textId).OrderBy(o => o.Order);
                    //重新排序Order
                    int count = 1;
                    foreach (var order in regionOrders)
                    {
                        order.Order = count;
                        count++;
                    }

                    var cId = _DsDb.CampaignRegion.Where(o => o.Id == id).Select(o => o.Campaign.Id);

                    foreach (var i in cId)
                    {
                        //組成使用到此Campaign的Device
                        var devices = _DsDb.ScheduleSummary.Where(o => o.CampaignId == i && o.DeviceId != null).Select(o => o.DeviceId).Distinct().ToList();
                        var screenGroups = _DsDb.ScheduleSummary.Where(o => o.CampaignId == i && o.DeviceId == null).Select(o => o.ScreenGroupId).Distinct().ToList();

                        if (devices.Count > 0 || screenGroups.Count() > 0)
                        {
                            foreach (var d in devices)
                            {
                                deviceList.Add(new ScheduleSummary()
                                {
                                    DeviceId = d.Value
                                });
                            }

                            foreach (var s in screenGroups)
                            {
                                var deviceinScreenGroup = _DsDb.Device.Where(o => o.ScreenGroupId == s).Select(o => o.Id).Distinct().ToList();
                                foreach (var d in deviceinScreenGroup)
                                {
                                    deviceList.Add(new ScheduleSummary()
                                    {
                                        DeviceId = d
                                    });
                                }
                            }
                            PublishModels PublishModels = new PublishModels();
                            PublishModels.ChangeContentStatus(deviceList, null);
                        }
                    }
                }
            }
            try
            {
                _DsDb.RegionFrame.RemoveRange(deleteTextRegion);
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 取得Rss資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TextAssetApiModel GetRssData(int id)
        {
            List<string> data = new List<string>();
            var rssContent = _DsDb.TextAsset.Where(o => o.Id == id).Select(o => o.Content).SingleOrDefault();
            //Json轉Object
            var obj = JsonConvert.DeserializeObject<Rss>(rssContent);

            try
            {
                XmlReader reader = XmlReader.Create(obj.Url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                foreach (SyndicationItem item in feed.Items)
                {
                    data.Add(item.Title.Text);
                }
            }
            catch (Exception)
            {
                //
            }

            TextAssetApiModel model = new TextAssetApiModel();
            model.RssData = data;

            return model;
        }

        /// <summary>
        /// 找出TextAsset 內容
        /// </summary>
        /// <param name="FileId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public TextAsset FindTextAssetFile(int FileId, UserInfo _UserInfo)
        {
            var file = _DsDb.TextAsset.FirstOrDefault(o => o.Id == FileId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);
            return file;
        }

        /// <summary>
        /// 找出有使用此 textAsset 的 Campaign
        /// </summary>
        /// <param name="textId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult FindCampaignUsedTextAssetFile(int textId, UserInfo _UserInfo)
        {

            List<string> textAssetUsedNameList = new List<string>();
            //找出哪些 region 有用到
            var regionId = FindRegionUsedwithTextAsset(textId, _UserInfo);

            foreach (var campaignRegionId in regionId)
            {
                int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                string campaignName = _DsDb.Campaign.Where(o => o.Id == campaignId).Select(o => o.Name).SingleOrDefault();
                textAssetUsedNameList.Add(campaignName);
            }

            if (textAssetUsedNameList == null)
            {
                return new SuccessResult(Data: null);
            }
            else
            {
                return new SuccessResult(Data: new { textAssetUsedNameList = textAssetUsedNameList.Distinct() });
            }

        }

        public List<int> FindRegionUsedwithTextAsset(int textId, UserInfo _UserInfo)
        {

            //找到textAsset 的資訊
            var file = FindTextAssetFile(textId, _UserInfo);

            //找出哪些 region 有用到
            var regionId = _DsDb.RegionFrame.Where(o => o.TextAssetId == file.Id).Select(o => o.RegionId).Distinct().ToList();

            return regionId;
        }
    }
}