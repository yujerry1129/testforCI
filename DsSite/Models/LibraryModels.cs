﻿using DsDbLib.Constants;
using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using DsSite.Models.Modals;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using static DsSite.Controllers.Api.LibraryController;

namespace DsSite.Models
{
    public class LibraryModels : Controllers.Base.DsBaseController
    {
        private const string _FFmpegPath = "C:/DsUpload/ffmpeg.exe";
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// 新增資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AddFolder(LibraryApiModel Model, UserInfo _UserInfo)
        {
            if (_DsDb.LibraryFolder.Any(o => o.Name == Model.FolderName && o.UserGroup_Id == _UserInfo.UserGroupId))
            {
                return new ApiResult() { Result = false, Message = "Folder name " + Model.FolderName + " already exists." };
            }
            else
            {
                try
                {
                    //Add new folder

                    _DsDb.LibraryFolder.Add(new LibraryFolder()
                    {
                        Name = Model.FolderName,
                        CreateBy = _DsDb.User.Find(_UserInfo.Id),
                        CreateDate = DateTime.Now,
                        UserGroup = _DsDb.UserGroup.Find(_UserInfo.UserGroupId),
                        ParentFolderId = Model.ParentFolderId
                    });

                    _DsDb.SaveChanges();

                    return new ApiResult() { Result = true };
                }
                catch (Exception ex)
                {
                    return new ApiResult() { Result = false, Message = "Error_0044" };
                }
            }
        }

        /// <summary>
        /// 取得指定資料夾階層的物件。是資料夾與檔案的結合
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public LibraryFolderData GetLibraryFolderItems(int? FolderId, UserInfo _UserInfo)
        {
            var data = new LibraryFolderData();
            //Get folders
            var folders = _DsDb.LibraryFolder.Where(o => o.ParentFolderId == FolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();

            var files = _DsDb.FileUpload.Where(o => o.Folder.Id == FolderId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null && o.FileType != 12).ToList();

            //Current folder
            var folder = _DsDb.LibraryFolder.FirstOrDefault(o => o.Id == FolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);

            //Get parent folders
            var parents = new List<LibraryApiModel>();

            if (folder != null && folder.ParentFolderId != null)
            {
                //Get first parent folder
                var parentFolder = _DsDb.LibraryFolder.FirstOrDefault(o => o.Id == folder.ParentFolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);

                while (parentFolder != null)
                {
                    parents.Add(new LibraryApiModel() { FolderId = parentFolder.Id, FolderName = parentFolder.Name });

                    parentFolder = _DsDb.LibraryFolder.FirstOrDefault(o => o.Id == parentFolder.ParentFolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);
                }
            }

            var itemFolders = folders.Select(o => new itemFolders()
            {
                Name = o.Name,
                ParentFolderId = o.ParentFolderId,
                FolderId = o.Id,
                FolderFileSize = FolderSizeTotal(o.Id, _UserInfo),
                UpdateDate = o.UpdateDate == null ? o.CreateDate.ToString("yyyy" + "/" + "MM" + "/" + "dd" + " HH" + ":" + "mm") : o.UpdateDate.Value.ToString("yyyy" + "/" + "MM" + "/" + "dd" + " HH" + ":" + "mm")
            }).ToList();

            var itemFiles = files.Select(o => new itemFiles()
            {
                FileId = o.Id,
                Name = o.Name,
                FileType = o.FileType,
                Size = o.Size,
                ExtensionName = o.ExtensionName,
                UploadDate = o.UpdateDate,
                UpdateDate = o.UpdateDate == null ? o.CreateDate.ToString("yyyy" + "/" + "MM" + "/" + "dd" + " HH" + ":" + "mm") : o.UpdateDate.Value.ToString("yyyy" + "/" + "MM" + "/" + "dd" + " HH" + ":" + "mm"),
                MimeType = o.MimeType,
                VideoDuration = o.VideoDuration
            }).ToList();

            data.Folders = folders;
            data.Files = files;
            data.Folder = folder;
            data.Parents = parents;
            data.ItemFolders = itemFolders;
            data.ItemFiles = itemFiles;

            return data;
        }

        private long FolderSizeTotal(int folderId, UserInfo _UserInfo)
        {
            long total = 0;

            //當層資料夾的檔案大小加總
            total = _DsDb.FileUpload.Where(p => p.FolderId == folderId && p.UserGroupId == _UserInfo.UserGroupId && p.DeleteDate == null).ToList().Sum(o => o.Size);

            //呼叫計算資料夾中檔案大小的方法
            long subFolderSizeTotal = _CalculateFolderSize(folderId, 0, _UserInfo);

            return total + subFolderSizeTotal;
        }

        private long _CalculateFolderSize(int folderId, long subFoldertotal, UserInfo _UserInfo)
        {
            //計算子資料夾檔案總大小
            var subFolders = _DsDb.LibraryFolder.Where(o => o.ParentFolderId == folderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();

            //判斷是否有子資料夾
            if (subFolders.Count > 0)
            {
                long tempFilesTotal = 0; //所有檔案大小
                long tempToatl = 0; //所有資料夾與檔案加總

                foreach (var subFolder in subFolders)
                {
                    //子資料夾底下的所有檔案大小
                    tempFilesTotal = _DsDb.FileUpload.Where(p => p.FolderId == subFolder.Id && p.UserGroupId == _UserInfo.UserGroupId && p.DeleteDate == null).ToList().Sum(p => p.Size);
                    //呼叫計算方法
                    tempToatl = _CalculateFolderSize(subFolder.Id, subFoldertotal, _UserInfo);
                    //累加資料夾與所有檔案
                    subFoldertotal = subFoldertotal + tempFilesTotal;
                }

                return subFoldertotal + tempToatl;
            }
            else
            {
                //沒有子資料夾回傳0
                return 0;
            }

        }

        /// <summary>
        /// 取得使用者選取的檔案
        /// </summary>
        /// <param name="FileId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public FileUpload FindFile(int FileId, UserInfo _UserInfo)
        {
            var file = _DsDb.FileUpload.FirstOrDefault(o => o.Id == FileId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);
            return file;
        }

        /// <summary>
        /// 取得使用這所選取的資料夾
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public LibraryFolder FindFolder(int? FolderId, UserInfo _UserInfo)
        {
            var folder = _DsDb.LibraryFolder.FirstOrDefault(o => o.Id == FolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);
            return folder;
        }

        /// <summary>
        /// 刪除使用在RegionFrame的Media
        /// </summary>
        /// <param name="file"></param>
        /// <param name="_UserInfo"></param>
        public void _DeleteRegionFrameMedia(FileUpload file, UserInfo _UserInfo)
        {
            //要更改的deviceId
            List<ScheduleSummary> deviceList = new List<ScheduleSummary>();
            CampaignModels CampaignModel = new CampaignModels();
            var deleteFileRegion = _DsDb.RegionFrame.Where(o => o.FileId == file.Id).ToList();

            var defaultScreenIds = _DsDb.Device.Where(o => o.DefaultScreenFileId == file.Id && o.UserGroup_Id == _UserInfo.UserGroupId).ToList();
            if (defaultScreenIds.Count > 0)
            {
                foreach (var defaultScreenId in defaultScreenIds)
                {
                    defaultScreenId.DefaultScreenFileId = null;
                }
            }

            if (deleteFileRegion.Count > 0)
            {
                var regionIds = deleteFileRegion.Select(o => o.RegionId);
                foreach (var id in regionIds)
                {
                    var regionOrders = _DsDb.RegionFrame.Where(o => o.RegionId == id && o.FileId != file.Id).OrderBy(o => o.Order);
                    //重新排序Order
                    int count = 1;
                    foreach (var order in regionOrders)
                    {
                        order.Order = count;
                        count++;
                    }

                    var cId = _DsDb.CampaignRegion.Where(o => o.Id == id).Select(o => o.Campaign.Id);

                    foreach (var i in cId)
                    {
                        //組成使用到此Campaign的Device
                        var dList = _DsDb.ScheduleSummary.Where(o => o.CampaignId == i && o.UserGroupId == _UserInfo.UserGroupId && o.DeviceId != null).Select(o => o.DeviceId).Distinct().ToList();
                        var sgList = _DsDb.ScheduleSummary.Where(o => o.CampaignId == i && o.UserGroupId == _UserInfo.UserGroupId && o.DeviceId == null).Select(o => o.ScreenGroupId).Distinct().ToList();
                        var dinSgList = _DsDb.Device.Where(o => sgList.Contains(o.ScreenGroupId)).Select(o => o.Id).Distinct().ToList();
                        if (dList.Count != 0 || sgList.Count != 0)
                        {
                            foreach (var dId in dList)
                            {
                                deviceList.Add(new ScheduleSummary() { DeviceId = dId });
                            }
                            foreach (var d in dinSgList)
                            {

                                deviceList.Add(new ScheduleSummary() { DeviceId = d });

                            }
                            PublishModels PublishModels = new PublishModels();
                            PublishModels.ChangeContentStatus(deviceList, null);
                        }
                    }
                }
                try
                {
                    _DsDb.RegionFrame.RemoveRange(deleteFileRegion);
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="FileId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteFile(int? FileId, UserInfo _UserInfo)
        {
            var file = FindFile(FileId.Value, _UserInfo);

            if (file != null)
            {
                _DeleteRegionFrameMedia(file, _UserInfo);

                file.DeleteDate = DateTime.Now;
                file.DeleteByUserId = _UserInfo.Id;

                try
                {
                    _DsDb.SaveChanges();

                    //將刪除檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaLog(file, _UserInfo, 3);

                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 檔案被使用清單
        /// </summary>
        /// <param name="FileId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult FileUsedList(int? FileId, UserInfo _UserInfo)
        {
            List<string> fileUsedNameList = new List<string>();
            List<string> defaultScreenNameList = new List<string>();

            var file = FindFile(FileId.Value, _UserInfo);

            var regionId = _DsDb.RegionFrame.Where(o => o.FileId == file.Id).Select(o => o.RegionId).Distinct().ToList();

            foreach (var campaignRegionId in regionId)
            {
                int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                string campaignName = _DsDb.Campaign.Where(o => o.Id == campaignId && o.UserGroup_Id == _UserInfo.UserGroupId).Select(o => o.Name).SingleOrDefault();
                fileUsedNameList.Add(campaignName);
            }

            var devices = _DsDb.Device.Where(o => o.DefaultScreenFileId == FileId && o.UserGroup_Id == _UserInfo.UserGroupId).ToList();
            if (devices.Count > 0)
            {
                foreach (var device in devices)
                {
                    defaultScreenNameList.Add(device.Name);
                }
            }

            if (fileUsedNameList == null && defaultScreenNameList == null)
            {
                return new SuccessResult(Data: null);
            }
            else
            {
                return new SuccessResult(Data: new { FileUsedNameList = fileUsedNameList.Distinct(), DefaultScreenList = defaultScreenNameList.Distinct() });
            }
        }

        /// <summary>
        /// 刪除資料夾
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteFolder(int? FolderId, UserInfo _UserInfo)
        {
            var folder = FindFolder(FolderId, _UserInfo);

            if (folder != null)
            {
                //刪除所有子資料夾及檔案
                _RemoveFolder(folder, _UserInfo.Id, DateTime.Now, _UserInfo);

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }
            else
            {
                return new NotFoundResult();
            }
        }

        private void _RemoveFolder(LibraryFolder Folder, int UserId, DateTime DeleteDate, UserInfo _UserInfo)
        {
            if (Folder == null)
            {
                return;
            }

            //Remove files in folder
            foreach (var file in _DsDb.FileUpload.Where(o => o.FolderId == Folder.Id && o.DeleteDate == null && o.UserGroupId == _UserInfo.UserGroupId))
            {
                _DeleteRegionFrameMedia(file, _UserInfo);
                file.DeleteByUserId = UserId;
                file.DeleteDate = DeleteDate;
            }

            //Remove folder
            Folder.Name = Folder.Id.ToString() + DeleteDate.ToString();
            Folder.DeleteByUserId = UserId;
            Folder.DeleteDate = DeleteDate;

            //遞迴Remove sub folders
            var subFolders = _DsDb.LibraryFolder.Where(o => o.ParentFolderId == Folder.Id && o.DeleteDate == null && o.UserGroup_Id == _UserInfo.UserGroupId);

            foreach (var subFolder in subFolders)
            {
                _RemoveFolder(subFolder, UserId, DeleteDate, _UserInfo);
            }
        }

        /// <summary>
        /// 被使用的資料夾
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult FolderUsedList(int? FolderId, UserInfo _UserInfo)
        {
            var folder = FindFolder(FolderId, _UserInfo);
            List<DeleteAllUsedList> folderUsedNameList = new List<DeleteAllUsedList>();

            var files = findFileInFolders(folder, _UserInfo.Id, DateTime.Now, _UserInfo, folderUsedNameList);

            if (files == null)
            {
                return new SuccessResult(Data: null);
            }
            else
            {
                return new SuccessResult(Data: files);
            }
        }

        /// <summary>
        /// 找資料夾內的檔案
        /// </summary>
        /// <param name="Folder"></param>
        /// <param name="UserId"></param>
        /// <param name="DeleteDate"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="List"></param>
        /// <returns></returns>
        public List<DeleteAllUsedList> findFileInFolders(LibraryFolder Folder, int UserId, DateTime DeleteDate, UserInfo _UserInfo, List<DeleteAllUsedList> List)
        {
            List<string> folderList = new List<string>();

            //列出資料夾下的所有檔案
            foreach (var file in _DsDb.FileUpload.Where(o => o.FolderId == Folder.Id && o.DeleteDate == null && o.UserGroupId == _UserInfo.UserGroupId))
            {
                var regionIds = _DsDb.RegionFrame.Where(o => o.FileId == file.Id).Select(o => o.RegionId).Distinct().ToList();
                if (regionIds.Count != 0)
                {
                    foreach (var id in regionIds)
                    {
                        int cId = _DsDb.CampaignRegion.Where(o => o.Id == id).Select(o => o.Campaign.Id).SingleOrDefault();
                        string cName = _DsDb.Campaign.Where(o => o.Id == cId && o.UserGroup_Id == _UserInfo.UserGroupId).Select(o => o.Name).SingleOrDefault();
                        folderList.Add(cName);
                    }
                    List.Add(new DeleteAllUsedList() { FileName = file.Name, CampaignName = folderList.Distinct().ToList(), FolderName = file.Folder.Name });
                }
            }

            //遞迴尋找sub folders
            var subFolders = _DsDb.LibraryFolder.Where(o => o.ParentFolderId == Folder.Id && o.DeleteDate == null && o.UserGroup_Id == _UserInfo.UserGroupId);

            foreach (var subFolder in subFolders)
            {
                findFileInFolders(subFolder, UserId, DeleteDate, _UserInfo, List);
            }

            return List;
        }

        /// <summary>
        /// 刪除全部
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteAll(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var now = DateTime.Now;

            if (Model.FileIdList != null && Model.FileIdList.Count > 0)
            {
                //Delete files
                foreach (var file in _DsDb.FileUpload.Where(
                    o => Model.FileIdList.Contains(o.Id)
                    && o.UserGroupId == _UserInfo.UserGroupId
                    && o.DeleteDate == null))

                {
                    _DeleteRegionFrameMedia(file, _UserInfo);
                    file.DeleteByUserId = _UserInfo.Id;
                    file.DeleteDate = now;
                    //將多筆刪除檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaLog(file, _UserInfo, 3);
                }
            }

            if (Model.FolderIdList != null && Model.FolderIdList.Count > 0)
            {
                //Delete folders
                foreach (var folder in _DsDb.LibraryFolder.Where(
                    o => Model.FolderIdList.Contains(o.Id)
                    && o.UserGroup_Id == _UserInfo.UserGroupId
                    && o.DeleteDate == null))
                {
                    _RemoveFolder(folder, _UserInfo.Id, now, _UserInfo);
                }
            }

            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            return new SuccessResult();
        }

        public class DeleteAllUsedList
        {
            public string FileName { get; set; }
            public List<string> CampaignName { get; set; }
            public List<string> DefaultScreen { get; set; }
            public string FolderName { get; set; }
        }

        /// <summary>
        /// 多筆刪除
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteAllFileList(LibraryApiModel Model, UserInfo _UserInfo)
        {
            List<DeleteAllUsedList> fileUsedList = new List<DeleteAllUsedList>();
            List<DeleteAllUsedList> folderUsedList = new List<DeleteAllUsedList>();

            if (Model.FileIdList != null)
            {
                foreach (var usedFileId in Model.FileIdList)
                {
                    List<string> campaignNames = new List<string>();
                    List<string> defaultScreens = new List<string>();

                    var file = FindFile(usedFileId, _UserInfo);

                    var regionId = _DsDb.RegionFrame.Where(o => o.FileId == file.Id).Select(o => o.RegionId).Distinct().ToList();

                    foreach (var campaignRegionId in regionId)
                    {
                        int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                        string campaignName = _DsDb.Campaign.Where(o => o.Id == campaignId && o.UserGroup_Id == _UserInfo.UserGroupId).Select(o => o.Name).SingleOrDefault();
                        campaignNames.Add(campaignName);
                    }

                    var devices = _DsDb.Device.Where(o => o.DefaultScreenFileId == usedFileId && o.UserGroup_Id == _UserInfo.UserGroupId).ToList();
                    if (devices.Count > 0)
                    {
                        foreach (var device in devices)
                        {
                            defaultScreens.Add(device.Name);
                        }
                    }

                    if (regionId.Count != 0)
                    {
                        fileUsedList.Add(new DeleteAllUsedList { FileName = file.Name, CampaignName = campaignNames, DefaultScreen = defaultScreens });
                    }

                }
            }

            if (Model.FolderIdList != null)
            {
                foreach (var usedFolderId in Model.FolderIdList)
                {
                    var folder = FindFolder(usedFolderId, _UserInfo);

                    var files = findFileInFolders(folder, _UserInfo.Id, DateTime.Now, _UserInfo, folderUsedList);

                    if (files != null)
                    {
                        folderUsedList = files;
                    }
                }
            }

            if (fileUsedList == null && folderUsedList == null)
            {
                return new NotFoundResult();
            }

            return new SuccessResult(Data: new { File = fileUsedList, Folder = folderUsedList });

        }

        /// <summary>
        /// 取得所有資料夾底下的檔案
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetFolderFiles(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var files = _DsDb.FileUpload
                .Where(o =>
                       o.FolderId == Model.FolderId
                    && o.DeleteDate == null
                    && o.UserGroupId == _UserInfo.UserGroupId)
                .Select(o => new
                {
                    FileId = o.Id,
                    FileType = o.FileType,
                    Name = o.Name
                })
                .ToList();

            return new SuccessResult(Data: files);
        }

        /// <summary>
        /// 編輯檔案
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult EditFile(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var file = FindFile(Model.FileId.Value, _UserInfo);

            var now = DateTime.Now;

            if (file != null)
            {
                file.UpdateDate = now;
                file.UpdateByUserId = _UserInfo.Id;
                file.Name = Model.FileName;

                try
                {
                    _DsDb.SaveChanges();

                    //將修改檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaLog(file, _UserInfo, 2);
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult(Data: now.ToString("yyyy" + "/" + "MM" + "/" + "dd" + " HH" + ":" + "mm"));
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 編輯資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult EditFolder(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var folder = FindFolder(Model.FolderId, _UserInfo);

            if (folder != null)
            {
                folder.UpdateDate = DateTime.Now;
                folder.UpdateByUserId = _UserInfo.Id;
                folder.Name = Model.FolderName;

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult(Data: folder.UpdateDate);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 拖曳檔案或資料夾
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DragFile(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var fileIds = _DsDb.FileUpload.Where(o => Model.FileIdList.Contains(o.Id) && o.UserGroupId == _UserInfo.UserGroupId);
            var folderIds = _DsDb.LibraryFolder.Where(o => Model.FolderIdList.Contains(o.Id) && o.UserGroup_Id == _UserInfo.UserGroupId);

            if (fileIds != null || folderIds != null)
            {
                //被拖曳進去的目標Id
                var targetFolderId = _DsDb.LibraryFolder.Where(o => o.Name == Model.TargetName && o.UserGroup_Id == _UserInfo.UserGroupId).Select(o => o.Id).SingleOrDefault();

                if (Model.FileIdList != null && targetFolderId != 0)
                {
                    foreach (var fileId in fileIds)
                    {
                        fileId.FolderId = targetFolderId;
                    }
                }

                if (Model.FolderIdList != null && targetFolderId != 0)
                {
                    //判斷是否相同的資料夾拖到相同的資料夾(拖曳的檔案與被拖曳的目標相同)
                    if (Model.FolderIdList.Contains(targetFolderId))
                    {
                        return new NotFoundResult();
                    }

                    foreach (var folderId in folderIds)
                    {
                        folderId.ParentFolderId = targetFolderId;
                    }
                }

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }

            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 移動檔案(透過資料樹)
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult MoveFile(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var allFolder = _DsDb.LibraryFolder.Where(o => o.DeleteDate == null && o.UserGroup_Id == _UserInfo.UserGroupId)
                .Select(o => new
                {
                    id = o.Id,
                    text = o.Name,
                    parent = o.ParentFolderId.ToString() == "" ? "0" : o.ParentFolderId.ToString(),
                }).ToList();

            allFolder.Add(new { id = 0, text = "Home", parent = "#" });

            return new ApiResult()
            {
                Result = true,
                Data = allFolder
            };
        }

        /// <summary>
        /// 移動資料夾(透過資料樹)
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult MoveFolder(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var fileIds = _DsDb.FileUpload.Where(o => Model.FileIdList.Contains(o.Id) && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null);
            var folderIds = _DsDb.LibraryFolder.Where(o => Model.FolderIdList.Contains(o.Id) && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);

            if (folderIds != null || fileIds != null)
            {
                if (Model.FolderIdList != null)
                {
                    //檢查是否將檔案移至子資料夾
                    int targetId = Model.TargetId;
                    if (Model.FolderIdList.Contains(targetId))
                    {
                        return new NotFoundResult();
                    }
                    else
                    {
                        try
                        {
                            for (int? i = 0; i != null; i++)
                            {
                                var parentId = _DsDb.LibraryFolder.Where(o => o.Id == targetId && o.UserGroup_Id == _UserInfo.UserGroupId).Select(o => o.ParentFolderId).SingleOrDefault();
                                if (parentId == null)
                                {
                                    break;
                                }
                                if (Model.FolderIdList.Contains(parentId.Value))
                                {
                                    return new NotFoundResult();
                                }
                                else
                                {
                                    targetId = parentId.Value;
                                }
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }

                    if (Model.TargetId == 0)
                    {
                        foreach (var folderId in folderIds)
                        {
                            folderId.ParentFolderId = null;
                        }
                    }
                    else
                    {
                        foreach (var folderId in folderIds)
                        {
                            folderId.ParentFolderId = Model.TargetId;
                        }
                    }
                }

                if (Model.FileIdList != null)
                {
                    if (Model.TargetId == 0)
                    {
                        foreach (var fileId in fileIds)
                        {
                            fileId.FolderId = null;
                        }

                    }
                    else
                    {
                        foreach (var fileId in fileIds)
                        {
                            fileId.FolderId = Model.TargetId;
                        }
                    }
                }

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }

            else
            {
                return new NotFoundResult();
            }
        }

        public class MenuData
        {
            public int AssetId { get; set; }
            public int MenuId { get; set; }
            public string Name { get; set; }
            public int BackgroudFileId { get; set; }
            public int TextEffect { get; set; }
            public int MediaType { get; set; }
        }

        /// <summary>
        /// 上傳檔案
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="Request"></param>
        /// <returns></returns>
        public ActionResult UploadAjax(int? FolderId, UserInfo _UserInfo, System.Web.HttpRequestBase Request)
        {
            MediaLogModels MediaLogModels = new MediaLogModels();

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //Upload Folder Path
                    var uploadFolderPath = WebConfigurationManager.AppSettings["UploadFolder"] + "/" + _UserInfo.UserGroupId.ToString() + "/" + "origin-files";

                    //Upload Thumbnail Path
                    var thumbnailFolderPath = WebConfigurationManager.AppSettings["UploadFolder"] + "/" + _UserInfo.UserGroupId.ToString() + "/" + "thumbnail";

                    //Upload Menu Path
                    var MenuFolderPath = WebConfigurationManager.AppSettings["UploadFolder"] + "/" + _UserInfo.UserGroupId.ToString() + "/" + "origin-files/menu";

                    //If user upload folder not exists, create
                    if (Directory.Exists(uploadFolderPath) == false)
                    {
                        Directory.CreateDirectory(uploadFolderPath);
                    }

                    //If user thumbnail folder not exists, create
                    if (Directory.Exists(thumbnailFolderPath) == false)
                    {
                        Directory.CreateDirectory(thumbnailFolderPath);
                    }

                    //If user menu folder not exists, create
                    if (Directory.Exists(MenuFolderPath) == false)
                    {
                        Directory.CreateDirectory(MenuFolderPath);
                    }

                    //  Get all files from Request object  
                    var files = Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];

                        //產生一組GUID作為檔名
                        var hashCode = Guid.NewGuid();
                        //將檔案資訊存入資料庫
                        var fileData = new DsDbLib.Models.FileUpload()
                        {
                            Name = Path.GetFileNameWithoutExtension(file.FileName.Replace(",", "")),
                            Size = file.ContentLength,
                            MimeType = file.ContentType,
                            ExtensionName = Path.GetExtension(file.FileName),
                            HashCode = hashCode,
                            CreateByUserId = _UserInfo.Id,
                            CreateDate = DateTime.Now,
                            UserGroupId = _UserInfo.UserGroupId,
                        };

                        //設定檔案類型
                        var fileType = MediaFileType.GetType(fileData.ExtensionName);

                        if (fileType == null || fileType == 0)
                        {
                            throw new Exception("Error_0014");
                        }

                        fileData.FileType = fileType.Value;

                        string filePath = "";

                        if (fileData.FileType != 12) //不是Menu才做
                        {
                            filePath = Path.Combine(uploadFolderPath, hashCode.ToString() + fileData.ExtensionName);
                        }
                        else
                        {
                            filePath = Path.Combine(MenuFolderPath, hashCode.ToString() + fileData.ExtensionName);
                        }

                        //儲存檔案
                        file.SaveAs(filePath);

                        //若是圖檔，就要取縮圖
                        if (fileData.FileType == 1)
                        {
                            //儲存縮圖檔
                            SaveThumbnail(file, thumbnailFolderPath, hashCode);
                        }

                        //若是影片，就要取影片資訊
                        if (fileData.FileType == 2)
                        {
                            //取撥放時間
                            var videoInfo = GetVideoAudioLength(filePath, fileData.FileType);
                            if (videoInfo.Duration <= 0.0)
                            {
                                //影片格式錯誤
                                //todo: 錯誤處理
                            }

                            fileData.VideoDuration = (int)Math.Floor(videoInfo.Duration);
                            fileData.Content = videoInfo.Resolution;

                            //取影片縮圖
                            var saveResult = SaveVideoThumbnail(filePath, fileData.VideoDuration.Value * 0.33, _UserInfo);
                            if (saveResult)
                            {
                                //save縮圖 success
                            }
                        }

                        //若是音樂，就要擷取音樂資訊
                        if (fileData.FileType == 3)
                        {
                            //取播放時間
                            var audioInfo = GetVideoAudioLength(filePath, fileData.FileType);
                            if (audioInfo.Duration <= 0.0)
                            {
                                //影片格式錯誤
                                //todo: 錯誤處理
                            }

                            fileData.VideoDuration = (int)Math.Floor(audioInfo.Duration);
                        }

                        //若有指定存入的資料夾，檢查資料夾是否存在
                        if (FolderId != null && _DsDb.LibraryFolder.Any(o => o.Id == FolderId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null))
                        {
                            fileData.FolderId = FolderId;
                        }

                        _DsDb.FileUpload.Add(fileData);
                        _DsDb.SaveChanges();

                        //若是Excel，就要存到menu資料夾底下
                        if (fileData.FileType == 12)
                        {
                            var obj = JsonConvert.DeserializeObject<MenuData>(Request.Headers["menuData"]);

                            int originMenuId = obj.MenuId;

                            //New MenuId
                            obj.MenuId = _DsDb.FileUpload.Where(o => o.HashCode == fileData.HashCode).Select(o => o.Id).SingleOrDefault();

                            string menuContent = JsonConvert.SerializeObject(obj);

                            //表示新增
                            if (obj.AssetId == 0)
                            {
                                TextAsset textAsset = new TextAsset()
                                {
                                    Name = obj.Name,
                                    AssetType = 12,
                                    Content = menuContent,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    UserGroupId = _UserInfo.UserGroupId
                                };

                                _DsDb.TextAsset.Add(textAsset);
                            }
                            else //表示編輯
                            {
                                List<int> campaignTextAssetUsedList = new List<int>();

                                //刪除之前的Menu
                                var originMenu = _DsDb.FileUpload.Where(o => o.Id == originMenuId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();
                                _DsDb.FileUpload.Remove(originMenu);

                                var editTextAsset = _DsDb.TextAsset.Where(o => o.Id == obj.AssetId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();
                                editTextAsset.Name = obj.Name;
                                editTextAsset.Content = menuContent;
                                editTextAsset.UpdateByUserId = _UserInfo.UserGroupId;
                                editTextAsset.UpdateDate = DateTime.Now;

                                TextAssetModels textAsset = new TextAssetModels();

                                //找出那些有引用的campaign region
                                var regionId = textAsset.FindRegionUsedwithTextAsset(obj.AssetId, _UserInfo);
                                foreach (var campaignRegionId in regionId)
                                {
                                    int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                                    campaignTextAssetUsedList.Add(campaignId);
                                }

                                //找出scheduleSummary 中有用到此campaignId 的DeviceId
                                List<ScheduleSummary> DeviceIdList = new List<ScheduleSummary>();
                                //在ScheduleSummary中全部的 Campaign List 
                                var campaigninScheduleSummary = _DsDb.ScheduleSummary.Where(o => o.CampaignId.HasValue && o.UserGroupId == _UserInfo.UserGroupId).ToList();

                                foreach (var campaignId in campaigninScheduleSummary)
                                {
                                    foreach (var usedId in campaignTextAssetUsedList)
                                    {
                                        //比較 是否有相同的 CampaignId
                                        if (campaignId.CampaignId == usedId)
                                        {
                                            //若為 screenGroup
                                            if (campaignId.DeviceId == null)
                                            {
                                                var dinSg = _DsDb.Device.Where(o => o.ScreenGroupId == campaignId.ScreenGroupId).Select(o => o.Id).Distinct().ToList();
                                                foreach (var d in dinSg)
                                                {
                                                    DeviceIdList.Add(new ScheduleSummary()
                                                    {
                                                        DeviceId = d
                                                    });
                                                }
                                            }
                                            else
                                            {
                                                //若為 一般device
                                                DeviceIdList.Add(new ScheduleSummary()
                                                {
                                                    DeviceId = campaignId.DeviceId.Value
                                                });

                                            }

                                        }
                                    }
                                }

                                //修改內容狀態及派送狀態
                                PublishModels PublishModels = new PublishModels();
                                var ContentStatus = PublishModels.ChangeContentStatus(DeviceIdList, null);

                                //將修改檔案資訊存入log
                                MediaLogModels.MediaAssetLog(editTextAsset, _UserInfo, 2);
                            }

                            _DsDb.SaveChanges();
                        }

                        //將新增檔案資訊存入log
                        MediaLogModels.MediaLog(fileData, _UserInfo, 1);

                    }

                    return Json(new
                    {
                        Success = true,
                        Message = "ok"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "Error_0044"
                    });
                }
            }
            else
            {
                return Json(new
                {
                    Success = false,
                    Message = "Error_0044"
                });
            }
        }

        private void SaveThumbnail(HttpPostedFileBase file, string thumbnailFolderPath, Guid hashCode)
        {
            //製作縮圖
            int thumbnailMax = 150;
            //Loads original image from file
            Image imgOriginal = Image.FromStream(file.InputStream);
            float originalHeight = imgOriginal.Height;
            float originalWidth = imgOriginal.Width;
            //Finds height and width of resized image
            int thumbnailWidth = 0;
            int thumbnailHeight = 0;
            if (originalHeight > originalWidth)
            {
                thumbnailHeight = thumbnailMax;
                thumbnailWidth = (int)((originalHeight / originalWidth) * (float)thumbnailMax);
            }
            else
            {
                thumbnailWidth = thumbnailMax;
                thumbnailHeight = (int)((originalHeight / originalWidth) * (float)thumbnailMax);
            }

            //Create new bitmap that will be used for thumbnail
            Bitmap thumbnailBitmap = new Bitmap(thumbnailWidth, thumbnailHeight);
            Graphics resizedImg = Graphics.FromImage(thumbnailBitmap);

            //Resized image will have best possible quality
            resizedImg.InterpolationMode = InterpolationMode.HighQualityBicubic;
            resizedImg.CompositingQuality = CompositingQuality.HighQuality;
            resizedImg.SmoothingMode = SmoothingMode.HighQuality;

            //Draw resized image
            resizedImg.DrawImage(imgOriginal, 0, 0, thumbnailWidth, thumbnailHeight);

            //Save thumbnail to file
            var thumbnailPath = Path.Combine(thumbnailFolderPath, hashCode.ToString() + Path.GetExtension(file.FileName));
            thumbnailBitmap.Save(thumbnailPath);
        }

        private class VideoAudioInfo
        {
            public double Duration { get; set; }
            public string Resolution { get; set; }
        }

        private class VideoResolution
        {
            public string Width { get; set; }
            public string Height { get; set; }
        }

        private VideoAudioInfo GetVideoAudioLength(string VideoPath, int fileType)
        {
            VideoAudioInfo videoAudioInfo = new VideoAudioInfo();
            string result = "";
            string durationInfo = "";
            string[] duration;

            //影片解析度區域變數
            string videoResolutioin = "";
            string resolutionToJson = "";
            VideoResolution resolution = new VideoResolution();

            using (Process ffmpeg = new System.Diagnostics.Process())
            {
                //fmmpeg輸出訊息的設定預設值直接為error Message，必須要用error的stream來讀取。
                StreamReader errorreader;

                ffmpeg.StartInfo.UseShellExecute = false;
                ffmpeg.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                ffmpeg.StartInfo.RedirectStandardError = true;
                ffmpeg.StartInfo.FileName = _FFmpegPath;//ffmpeg 執行檔完整路徑
                ffmpeg.StartInfo.Arguments = "-i " + VideoPath;
                ffmpeg.Start();

                errorreader = ffmpeg.StandardError;

                ffmpeg.WaitForExit();

                result = errorreader.ReadToEnd();
                try
                {
                    durationInfo = result.Substring(result.IndexOf("Duration: ") + ("Duration: ").Length, ("00:00:00.00").Length);

                    //影片才需要做
                    if (fileType == 2)
                    {
                        //規則字串
                        string pattern = @"(\b[^0]\d+x[^0]\d+\b)";
                        //宣告 Regex 忽略大小寫
                        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);

                        //將比對後集合傳給 MatchCollection
                        MatchCollection matches = regex.Matches(result);

                        // 一一取出 MatchCollection 內容
                        foreach (Match match in matches)
                        {
                            videoResolutioin = match.Value.Trim();
                        }

                        string[] rArray = videoResolutioin.Split('x');
                        resolution.Width = rArray[0];
                        resolution.Height = rArray[1];

                        resolutionToJson = JsonConvert.SerializeObject(resolution);

                        videoAudioInfo.Resolution = resolutionToJson;
                    }
                }
                catch
                {
                    durationInfo = null;
                }

                ffmpeg.Close();
            }

            if (durationInfo != null)
            {
                duration = durationInfo.Split(':');
                //單位 1.時 2.分 3.秒 ，超過三個表示有問題！
                if (duration.Length > 3)
                {
                    videoAudioInfo.Duration = 0.0;
                }

                try
                {
                    //總秒數 = 小時*60*60  + 分*60 + 秒
                    videoAudioInfo.Duration = Convert.ToInt32(duration[0]) * 60 * 60 + Convert.ToInt32(duration[1]) * 60 + Convert.ToDouble(duration[2]);
                }
                catch { }
            }

            return videoAudioInfo;
        }

        /// <summary>
        /// 取得影片縮圖
        /// </summary>
        /// <param name="VideoPath"></param>
        /// <param name="Duration"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        private bool SaveVideoThumbnail(string VideoPath, double Duration, UserInfo _UserInfo)
        {
            try
            {
                var videoFileName = Path.GetFileNameWithoutExtension(VideoPath);
                var thumbnailFilePath = WebConfigurationManager.AppSettings["UploadFolder"] + "/" + _UserInfo.UserGroupId.ToString() + "/" + "origin-files";

                using (Process ffmpeg = new Process())
                {
                    //string outimg = mediaUploadPath + id.ToString() + ".png"; // 設定輸出路徑與檔名

                    string fileargs = "-ss " + Duration.ToString() + " -i ";
                    fileargs += " " + VideoPath + " -an -vframes 1 -y";
                    fileargs += "  " + (thumbnailFilePath + "/" + videoFileName + ".png") + "";

                    ffmpeg.StartInfo.UseShellExecute = false;
                    ffmpeg.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    ffmpeg.StartInfo.Arguments = fileargs;
                    ffmpeg.StartInfo.FileName = _FFmpegPath;//要執行檔案的位置
                    ffmpeg.Start(); // 執行 !
                    ffmpeg.WaitForExit();
                    ffmpeg.Close();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 檔案貼上
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult PasteFoldersorFiles(LibraryApiModel Model, UserInfo _UserInfo)
        {
            var fileIds = _DsDb.FileUpload.Where(o => Model.FileIdList.Contains(o.Id) && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null);
            var folderIds = _DsDb.LibraryFolder.Where(o => Model.FolderIdList.Contains(o.Id) && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null);

            if (Model.FileIdList == null && Model.FolderIdList == null)
            {
                return new NotFoundResult();
            }

            //判斷是否為剪下或是複製 //true cut false copy
            if (Model.Action)
            {
                //剪下動作

                //針對檔案
                if (Model.FileIdList != null)
                {
                    if (Model.TargetId == 0)
                    {
                        foreach (var file in fileIds)
                        {
                            file.FolderId = null;
                        }

                    }
                    else
                    {
                        foreach (var file in fileIds)
                        {
                            file.FolderId = Model.TargetId;
                        }
                    }
                }

                //針對資料夾
                if (Model.FolderIdList != null)
                {
                    //檢查是否將檔案移至子資料夾
                    int targetId = Model.TargetId;
                    if (Model.FolderIdList.Contains(targetId))
                    {
                        return new NotFoundResult();
                    }
                    else
                    {
                        try
                        {
                            for (int? i = 0; i != null; i++)
                            {
                                var parentId = _DsDb.LibraryFolder.Where(o => o.Id == targetId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).Select(o => o.ParentFolderId).SingleOrDefault();
                                if (parentId == null)
                                {
                                    break;
                                }
                                if (Model.FolderIdList.Contains(parentId.Value))
                                {
                                    return new NotFoundResult();
                                }
                                else
                                {
                                    targetId = parentId.Value;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            return new ExceptionResult(Message: "Error_0044");
                        }
                    }

                    if (Model.TargetId == 0)
                    {
                        foreach (var folderId in folderIds)
                        {
                            folderId.ParentFolderId = null;
                        }
                    }
                    else
                    {
                        foreach (var folderId in folderIds)
                        {
                            folderId.ParentFolderId = Model.TargetId;
                        }
                    }
                }
            }
            else
            {
                //複製動作

            }

            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            return new SuccessResult();
        }

        /// <summary>
        /// 取得所有Menu
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllMenu(UserInfo _UserInfo)
        {
            var menus = _DsDb.TextAsset.Where(o => o.AssetType == 12 && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).Select(o => new { AssetId = o.Id, Content = o.Content }).ToList();

            List<MenuData> data = new List<MenuData>();

            foreach (var m in menus)
            {
                var obj = JsonConvert.DeserializeObject<MenuData>(m.Content);
                obj.MediaType = 12;
                obj.AssetId = m.AssetId;
                data.Add(obj);
            }

            if (menus != null)
            {
                return new SuccessResult(Data: data);
            }

            return new NotFoundResult();
        }

        /// <summary>
        /// 刪除菜單
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteMenu(MenuInfo Model, UserInfo _UserInfo)
        {
            var asset = _DsDb.TextAsset.FirstOrDefault(o => o.Id == Model.AssetId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);
            var file = _DsDb.FileUpload.FirstOrDefault(o => o.Id == Model.MenuId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);

            TextAssetModels textModel = new TextAssetModels();

            if (asset != null)
            {
                textModel._DeleteRegionFrameTextAsset(Model.AssetId);

                asset.DeleteDate = DateTime.Now;
                asset.DeleteByUserId = _UserInfo.Id;

                try
                {
                    _DsDb.FileUpload.Remove(file);
                    _DsDb.SaveChanges();

                    //將刪除檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaAssetLog(asset, _UserInfo, 3);
                    _DsDb.SaveChanges();

                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();

            }
            else
            {
                return new NotFoundResult();
            }
        }

        public class TimerSettings
        {
            public bool Year { get; set; }
            public bool Month { get; set; }
            public bool Day { get; set; }
            public bool Hour { get; set; }
            public bool Minute { get; set; }
            public bool Second { get; set; }
            public bool Millisecond { get; set; }
        }

        public class TimerData
        {
            public string Name { get; set; }
            public int MediaType { get; set; }
            public int AssetId { get; set; }
            public string Color { get; set; }
            public DateTime DateTime { get; set; }
            public TimerSettings Settings { get; set; }
        }

        /// <summary>
        /// 取得所有倒數計時器
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllTimer(UserInfo _UserInfo)
        {
            var timers = _DsDb.TextAsset.Where(o => o.AssetType == 16 && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).Select(o => new { AssetId = o.Id, Content = o.Content }).ToList();

            List<TimerData> data = new List<TimerData>();

            foreach (var m in timers)
            {
                var obj = JsonConvert.DeserializeObject<TimerData>(m.Content);
                obj.MediaType = 16;
                obj.AssetId = m.AssetId;
                data.Add(obj);
            }

            if (timers != null)
            {
                return new SuccessResult(Data: data);
            }

            return new NotFoundResult();
        }

        /// <summary>
        /// 新增倒數計時器
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AddTimer(TimerInfo Model, UserInfo _UserInfo)
        {
            MediaLogModels MediaLogModels = new MediaLogModels();

            //表示新增
            if (Model.AssetId == 0)
            {
                TextAsset asset = new TextAsset()
                {
                    Name = Model.Name,
                    AssetType = 16,
                    Content = Model.TimerDetail,
                    CreateByUserId = _UserInfo.Id,
                    CreateDate = DateTime.Now,
                    UserGroupId = _UserInfo.UserGroupId
                };

                _DsDb.TextAsset.Add(asset);
            }
            else
            //表示編輯
            {
                List<int> campaignTextAssetUsedList = new List<int>();

                var editTextAsset = _DsDb.TextAsset.Where(o => o.Id == Model.AssetId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();
                editTextAsset.Name = Model.Name;
                editTextAsset.Content = Model.TimerDetail;
                editTextAsset.UpdateByUserId = _UserInfo.UserGroupId;
                editTextAsset.UpdateDate = DateTime.Now;

                TextAssetModels textAsset = new TextAssetModels();

                //找出那些有引用的campaign region
                var regionId = textAsset.FindRegionUsedwithTextAsset(Model.AssetId, _UserInfo);
                foreach (var campaignRegionId in regionId)
                {
                    int campaignId = _DsDb.CampaignRegion.Where(o => o.Id == campaignRegionId).Select(o => o.Campaign.Id).SingleOrDefault();
                    campaignTextAssetUsedList.Add(campaignId);
                }

                //找出scheduleSummary 中有用到此campaignId 的DeviceId
                List<ScheduleSummary> DeviceIdList = new List<ScheduleSummary>();
                //在ScheduleSummary中全部的 Campaign List 
                var campaigninScheduleSummary = _DsDb.ScheduleSummary.Where(o => o.CampaignId.HasValue && o.UserGroupId == _UserInfo.UserGroupId).ToList();

                foreach (var campaignId in campaigninScheduleSummary)
                {
                    foreach (var usedId in campaignTextAssetUsedList)
                    {
                        //比較 是否有相同的 CampaignId
                        if (campaignId.CampaignId == usedId)
                        {
                            //若為 screenGroup
                            if (campaignId.DeviceId == null)
                            {
                                var dinSg = _DsDb.Device.Where(o => o.ScreenGroupId == campaignId.ScreenGroupId).Select(o => o.Id).Distinct().ToList();
                                foreach (var d in dinSg)
                                {
                                    DeviceIdList.Add(new ScheduleSummary()
                                    {
                                        DeviceId = d
                                    });
                                }
                            }
                            else
                            {
                                //若為 一般device
                                DeviceIdList.Add(new ScheduleSummary()
                                {
                                    DeviceId = campaignId.DeviceId.Value
                                });

                            }

                        }
                    }
                }

                //修改內容狀態及派送狀態
                PublishModels PublishModels = new PublishModels();
                var ContentStatus = PublishModels.ChangeContentStatus(DeviceIdList, null);

                //將修改檔案資訊存入log
                MediaLogModels.MediaAssetLog(editTextAsset, _UserInfo, 2);
            }

            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            return new SuccessResult();
        }

        /// <summary>
        /// 刪除倒數計時器
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteTimer(TimerInfo Model, UserInfo _UserInfo)
        {
            var asset = _DsDb.TextAsset.FirstOrDefault(o => o.Id == Model.AssetId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null);

            TextAssetModels textModel = new TextAssetModels();

            if (asset != null)
            {
                textModel._DeleteRegionFrameTextAsset(Model.AssetId);

                asset.DeleteDate = DateTime.Now;
                asset.DeleteByUserId = _UserInfo.Id;

                try
                {
                    _DsDb.SaveChanges();

                    //將刪除檔案資訊存入log
                    MediaLogModels MediaLogModels = new MediaLogModels();
                    MediaLogModels.MediaAssetLog(asset, _UserInfo, 3);
                    _DsDb.SaveChanges();

                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();

            }
            else
            {
                return new NotFoundResult();
            }
        }
    }
}