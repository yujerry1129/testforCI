﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DsSite.Models.ViewModels;
using DsSite.Controllers.Api.Models;
using DsDbLib.Models;

namespace DsSite.Models.Modals
{
    public class ResolutionModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得DB現有Resolution設定值
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllResolution(UserInfo _UserInfo)
        {
            var data = _DsDb.ResolutionConfig
                .Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null)
                .ToList()
                .Select(o => new
                {
                    Id = o.Id,
                    Name = o.Name,
                    Width = o.Width,
                    Height = o.Height
                });

            return new SuccessResult(Data: data);
        }

        /// <summary>
        /// 新增解析度模版
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AddResolution(ResolutionApiModel Model, UserInfo _UserInfo)
        {
            //Thread.Sleep(1000);

            //return new ExceptionResult("some err occurs!");

            //Check legal data
            var err = Model.CheckModel();
            if (!string.IsNullOrEmpty(err))
            {
                return new ExceptionResult(Message: err);
            }
            
            var config = new ResolutionConfig()
            {
               
                Height = Model.Height,
                Name = Model.Name,
                Width = Model.Width,
                CreateDate = DateTime.Now,
                CreateByUserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId
            };

            _DsDb.ResolutionConfig.Add(config);

            _DsDb.SaveChanges();

            Model.ResolutionId = config.Id;
            
            return new SuccessResult(Data: Model);
        }
        
        /// <summary>
        /// 刪除解析度資料
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult RemoveResolution(ResolutionApiModel Model)
        {
            //Thread.Sleep(1000);

            var resolution = _DsDb.ResolutionConfig.FirstOrDefault(o => o.Id == Model.ResolutionId);

            if (resolution != null)
            {
                _DsDb.ResolutionConfig.Remove(resolution);
                _DsDb.SaveChanges();

                return new SuccessResult(Data: Model);
            }
            else
            {
                return new NotFoundResult();
            }

        }
    }
}