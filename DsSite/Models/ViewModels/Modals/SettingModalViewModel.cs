﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Models.ViewModels.Modals
{
    public class SettingModalViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RepeatPasssword { get; set; }
        public string LanguageCode { get; set; }
        public List<SelectListItem> LanguageListItem { get; set; }

        public SettingModalViewModel()
        {

        }

        /// <summary>
        /// Create model from UserInfo object
        /// </summary>
        public SettingModalViewModel(UserInfo UserInfo)
        {
            if (UserInfo != null)
            {
                Name = UserInfo.Name;
                Email = UserInfo.Email;
            }

            LanguageListItem = new List<SelectListItem>();

            LanguageListItem.Add(new SelectListItem()
            {
                Text = "English",
                Selected = UserInfo.LanguageCode == "en-us",
                Value = "en-us"
            });

            LanguageListItem.Add(new SelectListItem()
            {
                Text = "繁體中文",
                Selected = UserInfo.LanguageCode == "zh-tw",
                Value = "zh-tw"
            });
        }
    }
}