﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class SiteUserViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<SiteRoleViewModel> Roles { get; set; }
    }
}