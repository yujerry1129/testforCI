﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    [MetadataType(typeof(BIChartViewModel))]
    public class BIChartViewModel
    {
        [DisplayName("日期")]
        public string Date { get; set; }
        [DisplayName("年")]
        public List<int> Year { get; set; }
        public int? YearSelected { get; set; }
        [DisplayName("月")]
        public List<int> Month { get; set; }
        [DisplayName("日")]
        public List<int> Day { get; set; }
        [DisplayName("時間")]
        public string Time { get; set; }
        [DisplayName("開始")]
        public List<string> StartTime { get; set; }
        public string EndTimeSelected { get; set; }
        [DisplayName("結束")]
        public List<string> EndTime { get; set; }
        [DisplayName("位置")]
        public List<string> LocationInfo { get; set; }
        [DisplayName("性別")]
        public string Gender { get; set; }
        [DisplayName("年齡")]
        public List<string> Age { get; set; }
        [DisplayName("設備")]
        public List<string> Device { get; set; }
        [DisplayName("Campaign")]
        public List<string> Campaign { get; set; }
        [DisplayName("動作")]
        public int Action { get; set; }
    }
}