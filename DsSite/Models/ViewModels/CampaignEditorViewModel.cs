﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class CampaignEditorViewModel
    {
        public int CampaignId { get; set; }
        public bool HideEditor { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class CampaignInfo
    {
        public class CampaignList
        {
            public int CampaignId { get; set; }
            public string CampaignName { get; set; }
            public List<string> Tag { get; set; }
            public string TempTag { get; set; }
            public int Duration { get; set; }
        }
        public int Count { get; set; }
        public List<CampaignInfo.CampaignList> CampaignsList { get; set; }

    }


}