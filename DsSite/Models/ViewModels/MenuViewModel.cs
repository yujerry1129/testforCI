﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class MenuViewModel
    {
        public string Text { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? Order { get; internal set; }
    }
}