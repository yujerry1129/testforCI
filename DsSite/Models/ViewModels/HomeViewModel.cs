﻿using DsKit.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class HomeViewModel
    {
        public string ServerUrl { get; set; }
        public string SystemKey { get; set; }
        public string CustomerName { get; set; }
        public int DeviceCount { get; set; }
        public int DeviceLimit { get; set; }
        public int UploadFileCount { get; set; }
        public long FileStorageUsage { get; set; }
        public long StorageLimit { get; set; }
        public bool FaceDetectionEnabled { get; set; }
        public string Version { get; set; }
        public string PlayerVersion { get; set; }
    }
}