﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class AuthorizationManagementViewModel
    {
        public List<SiteRoleViewModel> Roles { get; set; }
        public List<SiteUserViewModel> Users { get; set; }
    }
}