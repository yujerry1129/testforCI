﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class LogViewModel
    {
        public class Device
        {
            public class Devices
            {
                public int deviceId { get; set; }
                public string deviceName { get; set; }
                public int dispatchStatus { get; set; }
                public string dispatchStatusName { get; set; }
                public int? locationId { get; set; }
                public string locationName { get; set; }
                public bool enableFaceDetection { get; set; }
            }
            public int Count { get; set; }
            public List<Device.Devices> DeviceList { get; set; }
        }
        public class FaceLogViewModel
        {
            public class FaceLogList
            {
                public long Id { get; set; }
                public DateTime LogTime { get; set; }
                public int Gender { get; set; }
                public int AgeId { get; set; }
                public int LookTime { get; set; }
                public int Distance { get; set; }
                public string Description { get; set; }

            }

            public int Count { get; set; }
            public List<FaceLogViewModel.FaceLogList> FaceList { get; set; }
        }

        public class DisplayLogViewModel
        {
            public class DisplayLogList
            {
                public long Id { get; set; }
                public DateTime LogTime { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
                public int FileType { get; set; }
                public string FileName { get; set; }
                public int DisplayType { get; set; }
                public string CampaignName { get; set; }
            }

            public int Count { get; set; }
            public List<DisplayLogViewModel.DisplayLogList> DisplayList { get; set; }
        }
    }

}