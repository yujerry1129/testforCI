﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class LibraryViewModel
    {
        public int FileSizeLimit { get; set; }
        public string SupportFileExt { get; set; }
    }
}