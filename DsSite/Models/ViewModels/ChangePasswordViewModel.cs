﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string Email { get; set; }
        public string TempPassword { get; set; }
        public string NewPassword { get; set; }
        public string RepeatPassword { get; set; }

    }
}