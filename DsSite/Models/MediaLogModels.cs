﻿using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class MediaLogModels : Controllers.Base.DsBaseController
    {

        public void MediaLog(FileUpload file, UserInfo _UserInfo, int action)
        {

            //將檔案資訊存入log
            var Medialog = new DsDbLib.Models.MediaLog()
            {
                Name = file.Name,
                FileType = file.FileType,
                MediaDuration = file.VideoDuration,
                Size = file.Size,
                ExtensionName = file.ExtensionName,
                UserId = _UserInfo.Id,
                TimeStamp = DateTime.Now,
                UserGroupId = _UserInfo.UserGroupId,
                Action = action
            };
            _DsDb.MediaLog.Add(Medialog);
            _DsDb.SaveChanges();
        }

        public void MediaAssetLog(TextAsset model, UserInfo _UserInfo, int action)
        {

            //將檔案資訊存入log
            var assetlog = new DsDbLib.Models.MediaLog()
            {
                Name = model.Name,
                FileType = model.AssetType,
                TextAssetContent = model.Content,
                UserId = _UserInfo.Id,
                TimeStamp = DateTime.Now,
                UserGroupId = _UserInfo.UserGroupId,
                Action = action

            };
            _DsDb.MediaLog.Add(assetlog);

            _DsDb.SaveChanges();
        }

        public int CheckAction(int check)
        {
            if (check == 0)
            {
                return 1;
            }
            return 2;

        }
    }
}