﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public int UserGroupId { get; set; }
        public string LanguageCode { get; set; }
        public bool IsLogin { get; set; }
    }
}