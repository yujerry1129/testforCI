﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class LanguageSet
    {
        public LanSet[] Sets { get; set; }
    }

    public class LanSet
    {
        public string id { get; set; }
        public Lan[] lan { get; set; }
    }

    public class Lan
    {
        public string text { get; set; }
        public string code { get; set; }
    }

}