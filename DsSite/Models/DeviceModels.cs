﻿using DsDbLib.Models;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace DsSite.Models
{
    public class DeviceModels : Controllers.Base.DsBaseController
    {
        private TimeSpan ConnectionTimeLimit = TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["ConnectionLimitSec"]));
        private TimeSpan MinusConnectionTimeLimit = TimeSpan.FromSeconds(-Convert.ToInt32(ConfigurationManager.AppSettings["ConnectionLimitSec"]));
        private String ImageThumbApiUrl = "/ace/api/library/file-thumbnail/";
        private String ImageApiUrl = "/ace/api/library/file-image/";

        /// <summary>
        /// 取得所有群組與設備
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllDevice(UserInfo _UserInfo)
        {
            List<ScreenGroupInfo> groups = new List<ScreenGroupInfo>();
            var deviceCount = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).Count();
            var currentUserGroup = _DsDb.UserGroup.FirstOrDefault(o => o.Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByAdminUserId == null);
            var GroupData = new UserGroupInfo
            {
                DeviceLimit = currentUserGroup.DeviceLimit,
                DeviceCount = deviceCount,
                DeviceRemaining = currentUserGroup.DeviceLimit - deviceCount,
                UsageRate = (deviceCount * 100 / currentUserGroup.DeviceLimit).ToString()
            };
            //未分派裝置(除未註冊的)
            var ungroupedDevice = GetWholeDeviceList(_UserInfo, null); //Null表示未分組裝置

            //組成群組 列出群組裡的裝置
            foreach (var group in _DsDb.ScreenGroup.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null))
            {
                var deviceInGroup = GetWholeDeviceList(_UserInfo, group.Id);

                //定義ScreenGroup資訊
                groups.Add(new ScreenGroupInfo()
                {
                    Id = group.Id,
                    Name = group.Name,
                    HorizontallyScreenCount = group.HorizontallyScreenCount,
                    VerticallyScreenCount = group.VerticallyScreenCount,
                    TagsName = GetScreenGroupTags(null, group.Id),
                    Devices = deviceInGroup
                });
            }

            //無任何設備或群組
            if (groups.Count == 0 && ungroupedDevice == null)
            {
                return new NotFoundResult();
            }

            return new SuccessResult(Data: new
            {
                GroupList = groups.OrderBy(o => o.Id),
                UngroupedDevice = ungroupedDevice,
                UserDeviceInfo = GroupData,
                UnregisteredDevice = GetUnregisteredDeviceList(_UserInfo)
            });
        }

        /// <summary>
        /// Get Device Information and Setting data
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApiResult GetDeviceDetailInfo(UserInfo _UserInfo, int id)
        {
            var currentUserGroup = _DsDb.UserGroup.FirstOrDefault(o => o.Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByAdminUserId == null);
            var now = DateTime.Now;
            var beforeOneHour = now.AddHours(-1);
            var connectbenchmark = now.Add(MinusConnectionTimeLimit);
            var currentDevice = _DsDb.Device.Where(o => o.Id == id && o.DeleteDate == null && o.ScreenGroupId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).SingleOrDefault();

            if (currentDevice != null)
            {
                var DeviceData = new DeviceInfo()
                {
                    Id = currentDevice.Id,
                    MachineName = currentDevice.MachineName,
                    Resolution = resolutionToString(_DsDb.DeviceScreen.Where(o => o.DeviceId == currentDevice.Id).ToList()),
                    IpAddress = currentDevice.IpAddressList,
                    DeviceName = currentDevice.Name,
                    MacAddress = currentDevice.MacAddress,
                    OperationSystem = currentDevice.OperationSystem,
                    DiskCapacity = ConverttoGB(currentDevice.MainDiskCapacity).ToString(),
                    DiskUsage = ConverttoGB(currentDevice.MainDiskUsage).ToString(),
                    DiskUsageRate = Math.Round((double)currentDevice.MainDiskUsage / (double)currentDevice.MainDiskCapacity * 100, 1).ToString(),
                    DailyShutDownTime = currentDevice.DailyShutDownTime,
                    LocationId = currentDevice.Location?.Id,
                    DispatchStatus = currentDevice.DispatchStatus,
                    ConnectionStatus = (currentDevice.LastConnectionTime.HasValue && currentDevice.LastConnectionTime.Value > connectbenchmark) ? true : false,
                    DefaultScreenId = currentDevice.DefaultScreenFileId,
                    DefaultScreenUrl = ImageApiUrl + currentDevice.DefaultScreenFileId,
                    EnableFaceDetection = currentUserGroup.EnableFaceDetection,
                    FaceDetectionType = currentDevice.FaceDetectionType,
                    FaceDetectionReportEnabled = currentDevice.FaceDetectionReportEnabled,
                    AutoUpdateEnabled = currentDevice.AutoUpdateEnabled,
                    AutoUpdateTime = currentDevice.AutoUpdateTime
                };

                return new SuccessResult(Data: new
                {
                    DeviceInfo = DeviceData,
                });
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 顯示群組底下的裝置
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetScreenGroupDetailInfo(ScreenGroupApiModel Model, UserInfo _UserInfo)
        {
            var screenGroup = FindScreenGroup(Model.Id, _UserInfo);

            if (screenGroup != null)
            {
                //群組底下的裝置
                var devices = GetWholeDeviceList(_UserInfo, screenGroup.Id);
                string timeServerName = _DsDb.Device.Where(o => o.Id == screenGroup.TimeServerId && o.UserGroup_Id == _UserInfo.UserGroupId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).Select(o => o.Name).SingleOrDefault();

                if (devices == null)
                {
                    return new NotFoundResult();
                }

                return new SuccessResult(Data: new ScreenGroupInfo()
                {
                    Id = screenGroup.Id,
                    Name = screenGroup.Name,
                    TimeServerName = timeServerName,
                    HorizontallyScreenCount = screenGroup.HorizontallyScreenCount,
                    VerticallyScreenCount = screenGroup.VerticallyScreenCount,
                    TimeServerId = screenGroup.TimeServerId,
                    Devices = devices,
                    DisconnectCount = screenGroup.DisconnectCount
                });
            }

            return new NotFoundResult();
        }

        /// <summary>
        /// 取得裝置資訊
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="ScreenGroupId"></param>
        /// <returns></returns>
        public List<DeviceInfo> GetWholeDeviceList(UserInfo _UserInfo, long? ScreenGroupId)
        {
            var now = DateTime.Now;
            var beforeOneHour = now.AddHours(-1);
            var connectbenchmark = now.Add(MinusConnectionTimeLimit);
            var currentUserGroup = _DsDb.UserGroup.FirstOrDefault(o => o.Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByAdminUserId == null);
            var DeviceList = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.ScreenGroupId == ScreenGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null);
            ConvertHelper convertHelper = new ConvertHelper();
            string MaxVersion = convertHelper.CompareVersion(_DsDb.VersionFile.Select(o => o.Version).ToList());
            var LatestVersion = _DsDb.VersionFile.Where(o => o.Version == MaxVersion).FirstOrDefault().Id;
            List<DeviceInfo> deviceDatail = new List<DeviceInfo>();

            foreach (var d in DeviceList)
            {
                //把Device塞入List
                deviceDatail.Add(new DeviceInfo()
                {
                    Id = d.Id,
                    DispatchStatus = d.DispatchStatus,
                    DeviceName = d.Name,
                    ConnectionStatus = (d.LastConnectionTime.HasValue && d.LastConnectionTime >= connectbenchmark) ? true : false,
                    DiskCapacity = ConverttoGB(d.MainDiskCapacity).ToString(),
                    DiskUsage = ConverttoGB(d.MainDiskUsage).ToString(),
                    DiskUsageRate = Math.Round((double)d.MainDiskUsage / (double)d.MainDiskCapacity * 100, 1).ToString(),
                    MachineName = d.MachineName,
                    IpAddress = d.IpAddressList,
                    Resolution = resolutionToString(_DsDb.DeviceScreen.Where(o => o.DeviceId == d.Id).ToList()),
                    OffsetX = d.OffsetX,
                    OffsetY = d.OffsetY,
                    LocationId = d.LocationId,
                    EnableFaceDetection = currentUserGroup.EnableFaceDetection,
                    FaceDetectionType = d.FaceDetectionType,
                    FaceDetectionReportEnabled = d.FaceDetectionReportEnabled,
                    DailyShutDownTime = d.DailyShutDownTime,
                    DefaultScreenUrl = ImageApiUrl + d.DefaultScreenFileId,
                    DefaultScreenId = d.DefaultScreenFileId,
                    TagsName = GetScreenGroupTags(d.Id, null),
                    VersionNumber = d.Version.Version,
                    AutoUpdateEnabled = d.AutoUpdateEnabled,
                    IsLatestVersion = (d.VersionId == LatestVersion),
                    IsNeedUpdate = d.IsNeedUpdate,
                    IsNeedRestart = d.IsNeedRestart,
                    AutoUpdateTime = d.AutoUpdateTime
                });
            }
            return deviceDatail;
        }

        /// <summary>
        /// 取得未註冊裝置列表
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public List<DeviceInfo> GetUnregisteredDeviceList(UserInfo _UserInfo)
        {
            var now = DateTime.Now;
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            var unregisteredDevice = _DsDb.Device.Where(o => o.UserGroup_Id == CurrentUserGroupId && o.DeleteByUserId == null && o.DeleteDate == null && o.CreateByUserId == null && o.CreateDate == null).ToList();

            var DeviceListData = unregisteredDevice.Select(d => new DeviceInfo()
            {
                Id = d.Id,
                DeviceName = d.Name,
                MachineName = d.MachineName,
                IpAddress = d.IpAddressList,
                Resolution = resolutionToString(_DsDb.DeviceScreen.Where(o => o.DeviceId == d.Id).ToList()),
                VersionNumber = d.Version.Version
            }).ToList();

            return DeviceListData;
        }

        /// <summary>
        /// 取出所有圖片檔
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetImageInfo(UserInfo _UserInfo)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            var ImageList = _DsDb.FileUpload.Where(m => m.FileType == 1 && m.UserGroup.Id == CurrentUserGroupId && m.DeleteDate == null && m.DeleteByUserId == null).Select(o => new
            {
                Id = o.Id,
                Name = o.Name,
                FileUrl = ImageThumbApiUrl + o.Id
            });
            return new SuccessResult(Data: new
            {
                ImageList = ImageList
            });
        }

        /// <summary>
        /// 取得位置列表
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetLocationList(UserInfo _UserInfo)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            var LocationList = _DsDb.Location.Where(l => l.UserGroupId == CurrentUserGroupId && l.DeleteDate == null && l.DeleteByUserId == null);

            return new SuccessResult(Data: new
            {
                LocationList = LocationList
            });
        }

        /// <summary>
        /// 註冊裝置，透過 License 做裝置數量的驗證
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceIds"></param>
        /// <returns></returns>
        public ApiResult RegisteDevice(UserInfo _UserInfo, List<int> DeviceIds)
        {
            License license = new License();
            var now = DateTime.Now;
            int CurrentUserid = _UserInfo.Id;
            int deviceLimit = _DsDb.UserGroup.FirstOrDefault(o => o.Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByAdminUserId == null).DeviceLimit;

            int currentRegisterdDevice = _DsDb.Device.Count(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteByUserId == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null);

            if (DeviceIds != null)
            {
                //與License 驗證 device 數量是否足夠
                if (license.CheckRegDevices(_UserInfo.UserGroupId, DeviceIds.Count()).Result)
                {
                    foreach (var deviceId in DeviceIds)
                    {
                        var DeviceData = _DsDb.Device.Where(d => d.Id == deviceId && d.DeleteDate == null && d.CreateByUserId == null).SingleOrDefault();
                        {
                            DeviceData.CreateByUserId = CurrentUserid;
                            DeviceData.CreateDate = DateTime.Now;
                        }
                    }
                    _DsDb.SaveChanges();
                    return new SuccessResult();
                }
                else
                {
                    //超出可使用的裝置數量
                    return new ExceptionResult(Message: "Error_0057");
                }
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 拒絕註冊不合法的裝置，拒絕註冊後會將裝置資料刪除
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceIds"></param>
        public void RefuseRegisterDevice(UserInfo _UserInfo, List<int> DeviceIds)
        {
            if (DeviceIds != null)
            {
                foreach (var deviceId in DeviceIds)
                {
                    var ScreenData = _DsDb.DeviceScreen.Where(s => s.DeviceId == deviceId && s.Device.UserGroup_Id == _UserInfo.UserGroupId).SingleOrDefault();
                    _DsDb.DeviceScreen.Remove(ScreenData);

                    var DeviceData = _DsDb.Device.Where(d => d.Id == deviceId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate == null && d.CreateByUserId == null).SingleOrDefault();
                    _DsDb.Device.Remove(DeviceData);
                }
                _DsDb.SaveChanges();
            }
        }

        /// <summary>
        /// 重新命名裝置名稱
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceId"></param>
        /// <param name="tbRename"></param>
        public ApiResult RenameDevice(UserInfo _UserInfo, int DeviceId, string tbRename)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            try
            { //Device name不可以重複
                if (_DsDb.Device.Any(o => o.Name == tbRename.Trim() && o.UserGroup_Id == CurrentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Id != DeviceId))
                {
                    //Name has been used.Please use another name
                    return new ExceptionResult(Message: "Error_0023");
                }
                else
                {
                    var device = _DsDb.Device.Where(d => d.Id == DeviceId && d.UserGroup_Id == CurrentUserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).SingleOrDefault();

                    if (device == null)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }
                    else
                    {
                        device.Name = tbRename;
                        device.UpdateByUserId = _UserInfo.Id;
                        device.UpdateDate = DateTime.Now;
                        //通知Player改變狀態
                        device.ConfigStatus = true;

                        _DsDb.SaveChanges();

                        return new SuccessResult();
                    }
                }
            }
            catch (Exception)
            {
                //Failed To Rename
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 重新命名群組名稱
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Name"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult RenameScreenGroup(UserInfo _UserInfo, long Id, string Name)
        {
            var group = FindScreenGroup(Id, _UserInfo);

            if (_DsDb.ScreenGroup.Any(o => o.Name == Name && o.DeleteDate == null && o.DeleteByUserId == null))
            {
                //Name is duplicate
                return new ExceptionResult(Message: "Error_0023");
            }
            else
            {
                if (group != null)
                {
                    var devicesinGroup = _DsDb.Device.Where(o => o.ScreenGroupId == group.Id && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).ToList();

                    group.Name = Name;
                    group.UpdateDate = DateTime.Now;
                    group.UpdateByUserId = _UserInfo.Id;

                    //通知Player改變狀態
                    foreach (var device in devicesinGroup)
                    {
                        device.ConfigStatus = true;
                    }

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }

                    return new SuccessResult();

                }
                else
                {
                    return new ExceptionResult(Message: "Error_0044");
                }
            }
        }

        /// <summary>
        /// 設定裝置的每日關機時間
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// /// <param name="screenGroupId"></param>
        /// <param name="AutoShutDownTime"></param>
        /// <returns></returns>
        public ApiResult SetDailyShutDownTime(UserInfo _UserInfo, int? id, long? screenGroupId, string AutoShutDownTime)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            List<Device> devices = new List<Device>();

            if (id != null)
            {
                devices = _DsDb.Device.Where(d => d.Id == id && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else if (screenGroupId != null)
            {
                devices = _DsDb.Device.Where(d => d.ScreenGroupId == screenGroupId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            try
            {
                TimeSpan dailyshutdowntime;
                if (AutoShutDownTime == null)
                {
                    foreach (var d in devices)
                    {
                        d.DailyShutDownTime = null;
                        d.UpdateByUserId = _UserInfo.Id;
                        d.UpdateDate = DateTime.Now;
                        //通知Player改變狀態
                        d.ConfigStatus = true;
                    }
                }
                else
                {
                    dailyshutdowntime = TimeSpan.Parse(AutoShutDownTime);

                    foreach (var d in devices)
                    {
                        d.DailyShutDownTime = dailyshutdowntime;
                        d.UpdateByUserId = _UserInfo.Id;
                        d.UpdateDate = DateTime.Now;
                        //通知Player改變狀態
                        d.ConfigStatus = true;
                    }
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
            catch (Exception)
            {
                //errmsg = "自動關機時間變更失敗";
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 設定DeviceScreen的預設畫面
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceId"></param>
        /// <param name="ScreenGroupId"></param>
        /// <param name="DefaultScreenId"></param>
        public ApiResult SettingDefaultScreen(UserInfo _UserInfo, int? DeviceId, long? ScreenGroupId, int? DefaultScreenId)
        {
            List<Device> devices = new List<Device>();

            if (DeviceId != null)
            {
                devices = _DsDb.Device.Where(d => d.Id == DeviceId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else if (ScreenGroupId != null)
            {
                devices = _DsDb.Device.Where(d => d.ScreenGroupId == ScreenGroupId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }

            if (devices.Count > 0 && (_DsDb.FileUpload.Any(o => o.Id == DefaultScreenId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null || DefaultScreenId == null)))
            {
                foreach (var device in devices)
                {
                    if (device.DefaultScreenFileId != DefaultScreenId)
                    {
                        //若Default Screen有變更，要修正Dispatch Status為2，重新派送
                        device.DispatchStatus = 2;
                        device.ContentStatus = 1;
                        device.DefaultScreenFileId = DefaultScreenId == null ? null : DefaultScreenId;

                        _DsDb.SaveChanges();

                        //將修改Screen資訊存入log
                        DefaultScreenLogModels DefaultScreenLogModels = new DefaultScreenLogModels();
                        DefaultScreenLogModels.ScreenLog(device, _UserInfo, 2);
                    }
                }
                return new SuccessResult();
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 臉部辨識設定: 是否開啟臉部辨識切換、偵測模式為單一或多數的設定
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="deviceId"></param>
        /// /// <param name="screenGroupId"></param>
        /// <param name="FaceDetectionType"></param>
        /// <returns></returns>
        public ApiResult FaceDetectionChange(UserInfo _UserInfo, int? deviceId, long? screenGroupId, int? FaceDetectionType)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            List<Device> devices = new List<Device>();
            if (deviceId != null)
            {
                devices = _DsDb.Device.Where(d => d.Id == deviceId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else if (screenGroupId != null)
            {
                devices = _DsDb.Device.Where(d => d.ScreenGroupId == screenGroupId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            try
            {
                foreach (var d in devices)
                {
                    d.FaceDetectionType = FaceDetectionType;
                    d.UpdateByUserId = _UserInfo.Id;
                    d.UpdateDate = DateTime.Now;
                    //通知Player改變狀態
                    d.ConfigStatus = true;
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
            catch (Exception)
            {
                //errmsg = "臉部辨識變更失敗";
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 臉部辨識設定：是否開啟臉部辨識報表的設定
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="deviceId"></param>
        /// /// <param name="screenGroupId"></param>
        /// <param name="FaceDetectionReportEnabled"></param>
        /// <returns></returns>
        public ApiResult FaceDetectionReportEnabledChange(UserInfo _UserInfo, int? deviceId, long? screenGroupId, bool FaceDetectionReportEnabled)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            List<Device> devices = new List<Device>();
            if (deviceId != null)
            {
                devices = _DsDb.Device.Where(d => d.Id == deviceId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else if (screenGroupId != null)
            {
                devices = _DsDb.Device.Where(d => d.ScreenGroupId == screenGroupId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            try
            {
                foreach (var d in devices)
                {
                    d.FaceDetectionReportEnabled = FaceDetectionReportEnabled;
                    d.UpdateByUserId = _UserInfo.Id;
                    d.UpdateDate = DateTime.Now;
                    //通知Player改變狀態
                    d.ConfigStatus = true;
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
            catch (Exception)
            {
                //errmsg = "臉部辨識報表變更失敗";
                return new ExceptionResult(Message: "Error_0044");

            }
        }

        /// <summary>
        /// 更新現有位置的名字、地點及描述
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        public ApiResult UpdateLocationInfo(UserInfo _UserInfo, DeviceApiModel DeviceModel)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            var location = _DsDb.Location.Where(d => d.Id == DeviceModel.LocationId && d.DeleteDate == null && d.DeleteByUserId == null).SingleOrDefault();

            if (DeviceModel.Name == null || string.IsNullOrEmpty(DeviceModel.Name))
            {
                //Location Name can not be empty.
                return new ExceptionResult(Message: "Error_0004");
            }
            else if (DeviceModel.Address == null || string.IsNullOrEmpty(DeviceModel.Address))
            {
                //Location Address can not be empty.
                return new ExceptionResult(Message: "Error_0046");
            }
            else if (DeviceModel.Latitude.ToString() == null || string.IsNullOrEmpty(DeviceModel.Latitude.ToString()) || DeviceModel.Longitude.ToString() == null || string.IsNullOrEmpty(DeviceModel.Longitude.ToString()))
            {
                //Location Latitude or Longitude can not be empty.
                return new ExceptionResult(Message: "Error_0047");
            }
            else
            {
                if (_DsDb.Location.Where(o => o.Name == DeviceModel.Name && o.Id != DeviceModel.LocationId && o.DeleteDate == null && o.DeleteByUserId == null && o.UserGroupId == CurrentUserGroupId).Count() > 0)
                {
                    //Location Name is duplicate.
                    return new ExceptionResult(Message: "Error_0023");
                }
                else
                {
                    //更新該位置的資訊
                    location.Name = DeviceModel.Name;
                    location.Description = DeviceModel.Description;
                    location.Address = DeviceModel.Address;
                    location.Latitude = DeviceModel.Latitude;
                    location.Longitude = DeviceModel.Longitude;

                    _DsDb.SaveChanges();

                    return new SuccessResult(Data: new
                    {
                        locationId = DeviceModel.LocationId,
                        Name = location.Name,
                        Description = location.Description,
                        Address = location.Address,
                        Latitude = location.Latitude,
                        Longitude = location.Longitude
                    });
                }
            }
        }

        /// <summary>
        /// (裝置與螢幕群組共用)設定將裝置或螢幕群組的位置
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        public ApiResult SetDeviceLocation(UserInfo _UserInfo, DeviceApiModel DeviceModel)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            List<Device> devices = new List<Device>();
            if (DeviceModel.DeviceId != null)
            {
                devices = _DsDb.Device.Where(o => o.UserGroup_Id == CurrentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Id == DeviceModel.DeviceId).ToList();
            }
            else if (DeviceModel.ScreenGroupId != null)
            {
                devices = _DsDb.Device.Where(o => o.UserGroup_Id == CurrentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.ScreenGroupId == DeviceModel.ScreenGroupId.Value).ToList();
            }
            else
            {
                //兩個id都為0，回傳錯誤
                return new ExceptionResult(Message: "Error_0044");
            }

            if (DeviceModel.LocationId.HasValue)
            {
                var location = _DsDb.Location.Where(d => d.Id == DeviceModel.LocationId && d.DeleteDate == null && d.DeleteByUserId == null).SingleOrDefault();
                //更新該裝置的位置資訊

                foreach (var d in devices)
                {
                    d.Location = location;
                    //通知Plaryer改變狀態
                    d.ConfigStatus = true;
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
            else
            {
                foreach (var d in devices)
                {
                    d.LocationId = null;
                    //通知Plaryer改變狀態
                    d.ConfigStatus = true;
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
        }

        /// <summary>
        /// (裝置與螢幕群組共用)新增位置，完成新增時會將裝置設定於新增後的位置
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceModel"></param>
        /// <returns></returns>
        public ApiResult AddLocation(UserInfo _UserInfo, DeviceApiModel DeviceModel)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            int newLocationId;
            List<Device> devices = new List<Device>();

            if (DeviceModel.DeviceId != null)
            {
                devices = _DsDb.Device.Where(o => o.Id == DeviceModel.DeviceId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.UserGroup_Id == CurrentUserGroupId).ToList();
            }
            else if (DeviceModel.ScreenGroupId != null)
            {
                devices = _DsDb.Device.Where(o => o.ScreenGroupId == DeviceModel.ScreenGroupId.Value && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.UserGroup_Id == CurrentUserGroupId).ToList();
            }
            else
            {
                //兩個id都為0，回傳錯誤
                return new ExceptionResult(Message: "Error_0044");
            }

            if (DeviceModel.Name == null || string.IsNullOrEmpty(DeviceModel.Name))
            {
                //Location Name can not be empty.
                return new ExceptionResult(Message: "Error_0004");
            }
            else if (DeviceModel.Address == null || string.IsNullOrEmpty(DeviceModel.Address))
            {
                //Location Address can not be empty.
                return new ExceptionResult(Message: "Error_0046");
            }
            else if (DeviceModel.Latitude.ToString() == null || string.IsNullOrEmpty(DeviceModel.Latitude.ToString()) || DeviceModel.Longitude.ToString() == null || string.IsNullOrEmpty(DeviceModel.Longitude.ToString()))
            {
                //Location Latitude or Longitude can not be empty.
                return new ExceptionResult(Message: "Error_0047");
            }
            else
            {
                if (_DsDb.Location.Where(o => o.Name == DeviceModel.Name && o.DeleteDate == null && o.DeleteByUserId == null && o.UserGroupId == CurrentUserGroupId).Count() > 0)
                {
                    //Location Name is duplicate.
                    return new ExceptionResult(Message: "Error_0023");
                }
                else
                {
                    //新增該位置的資訊
                    _DsDb.Location.Add(new Location()
                    {
                        Name = DeviceModel.Name,
                        Description = DeviceModel.Description,
                        Address = DeviceModel.Address,
                        Latitude = DeviceModel.Latitude,
                        Longitude = DeviceModel.Longitude,
                        CreateByUserId = _DsDb.User.Where(u => u.Id == _UserInfo.Id && u.DeleteDate == null).FirstOrDefault().Id,
                        CreateDate = DateTime.Now,
                        UserGroupId = CurrentUserGroupId
                    });

                    _DsDb.SaveChanges();

                    newLocationId = _DsDb.Location.OrderByDescending(o => o.Id).FirstOrDefault().Id;

                    foreach (var d in devices)
                    {
                        d.Location = _DsDb.Location.Where(o => o.Id == newLocationId && o.DeleteDate == null && o.DeleteByUserId == null).FirstOrDefault();
                    }

                    _DsDb.SaveChanges();
                    return new SuccessResult(Data: new
                    {
                        locationId = newLocationId,
                        Name = DeviceModel.Name,
                        Description = DeviceModel.Description,
                        Address = DeviceModel.Address,
                        Latitude = DeviceModel.Latitude,
                        Longitude = DeviceModel.Longitude
                    });
                }
            }
        }

        /// <summary>
        /// 刪除位置
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="DeviceModel"></param>
        public void DeleteLocation(UserInfo _UserInfo, DeviceApiModel DeviceModel)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            var location = _DsDb.Location.Where(d => d.Id == DeviceModel.LocationId && d.UserGroupId == CurrentUserGroupId && d.DeleteDate == null && d.DeleteByUserId == null).SingleOrDefault();

            var deviceswithlocation = _DsDb.Device.Where(o => o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.LocationId == DeviceModel.LocationId);

            //delete location
            foreach (var device in deviceswithlocation)
            {
                device.LocationId = null;
            }

            try
            {
                _DsDb.Location.Remove(location);
                _DsDb.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// (裝置與螢幕群組共用)取得處理過後的月份、總週數、1號是星期幾、月份有幾天、廣告行事曆
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="month"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public ApiResult SetCalendar(UserInfo _UserInfo, int? id, long? screenGroupId, string month, int num)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            var newmonth = DateTime.Parse(month).AddMonths(num);
            DateTime firstDateOfMonth = new DateTime(newmonth.Year, newmonth.Month, 1);
            int firstdateWeekdate = int.Parse(firstDateOfMonth.DayOfWeek.ToString("d"));
            DateTime lastDateOfMonth = newmonth.AddMonths(1).AddDays(-1);
            int lastdateWeekdate = int.Parse(lastDateOfMonth.DayOfWeek.ToString("d"));
            CultureInfo info = CultureInfo.CurrentCulture;
            int firstDateWeekNumber = info.Calendar.GetWeekOfYear(firstDateOfMonth, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
            int lastDateWeekNumber = info.Calendar.GetWeekOfYear(lastDateOfMonth, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
            int totalweek;
            int SetYear = newmonth.Year;
            int SetMonth = newmonth.Month;

            if (id == null && screenGroupId == null)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            if (firstdateWeekdate == 0)
            {
                totalweek = lastDateWeekNumber - firstDateWeekNumber + 2;
            }
            else if (lastdateWeekdate == 0)
            {
                totalweek = lastDateWeekNumber - firstDateWeekNumber;
            }
            else
            {
                totalweek = lastDateWeekNumber - firstDateWeekNumber + 1;
            }

            List<DispatchedCampaignInfo.CalendarDateInfo> campaignCalendarlist = GetDispatchedCampaignCalendar(_UserInfo, id, screenGroupId, newmonth);

            return new SuccessResult(Data: new
            {
                //處理過後的月份
                NewMonth = SetYear.ToString() + "/" + SetMonth.ToString(),
                //處理過後的月份總週數
                TotalWeek = totalweek,
                //處理過後的1號是星期幾
                FirstDateWeekDate = firstdateWeekdate,
                //處理過後的月份有幾天
                LastDateOfMonth = int.Parse(lastDateOfMonth.ToString("dd")),
                //處理過後的廣告行事曆
                CampaignCalendarList = campaignCalendarlist
            });
        }

        /// <summary>
        /// (裝置與螢幕群組共用)取出派送到指定裝置或螢幕群組的所有有效廣告
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="screenGroupId"></param>
        /// <returns></returns>
        public ApiResult GetDispatchedCampaignList(UserInfo _UserInfo, int? id, long? screenGroupId)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            List<ScheduleSummary> DeviceCampaign = new List<ScheduleSummary>();

            if (id != null)
            {
                DeviceCampaign = _DsDb.ScheduleSummary.Where(o => o.DeviceId == id && o.DeleteDate == null && o.UserGroupId == CurrentUserGroupId).OrderBy(o => o.PlayOrder).ToList();
            }
            else if (screenGroupId != null)
            {
                DeviceCampaign = _DsDb.ScheduleSummary.Where(o => o.ScreenGroupId == screenGroupId.Value && o.DeleteDate == null && o.UserGroupId == CurrentUserGroupId).OrderBy(o => o.PlayOrder).ToList();
            }
            else
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            var result = new List<DispatchedCampaignInfo>();
            foreach (var c in DeviceCampaign)
            {
                List<string> repeatWeekDayList = new List<string>();
                if (c.HasCalenderRule)
                {
                    if (c.RepeatMonday == true)
                        repeatWeekDayList.Add("Monday");
                    if (c.RepeatTuesday == true)
                        repeatWeekDayList.Add("Tuesday");
                    if (c.RepeatWednesday == true)
                        repeatWeekDayList.Add("Wednesday");
                    if (c.RepeatThurday == true)
                        repeatWeekDayList.Add("Thursday");
                    if (c.RepeatFriday == true)
                        repeatWeekDayList.Add("Friday");
                    if (c.RepeatSaturday == true)
                        repeatWeekDayList.Add("Saturday");
                    if (c.RepeatSunday == true)
                        repeatWeekDayList.Add("Sunday");
                }

                result.Add(new DispatchedCampaignInfo()
                {
                    ScheduleSummaryId = c.Id,
                    Playorder = c.PlayOrder,
                    CampaignId = c.CampaignId,
                    CampaignName = c.Campaign.Name,
                    HasCalendarRule = c.HasCalenderRule,
                    RepeatWeekDay = repeatWeekDayList,
                    HasFaceDetectionRule = c.HasFaceDetectionRule,
                    Spot = c.Spot,
                    Event = c.Event,
                    Startdate = ConvertDateString(c.StartDate),
                    Enddate = c.EndDate.HasValue ? ConvertDateString(c.EndDate) : "",
                    DailyStartTime = c.DailyStartTime,
                    DailyEndTime = c.DailyEndTime,
                    Gender = c.Gender.ToString(),
                    AgeRestriction = CombineAgeRestrictionName(_UserInfo, c.AgeRestriction),
                    HasMonthCalenderRule = c.HasMonthCalenderRule,
                    MonthDay = c.MonthDay
                });
            }

            return new SuccessResult(Data: new
            {
                DeviceCampaignList = result
            });
        }

        /// <summary>
        /// (裝置與螢幕群組共用)取出指定月份的廣告行事曆，輸出資料以日為單位 {日期,[一般廣告數,臉部辨識廣告數,插播廣告數]}
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="SelectedDate"></param>
        /// <returns></returns>
        public List<DispatchedCampaignInfo.CalendarDateInfo> GetDispatchedCampaignCalendar(UserInfo _UserInfo, int? id, long? screenGroupId, DateTime SelectedDate)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            DateTime firstDateOfMonth = new DateTime(SelectedDate.Year, SelectedDate.Month, 1);
            DateTime lastDateOfMonth = firstDateOfMonth.AddMonths(1).AddMilliseconds(-1);
            int monthdays = int.Parse(lastDateOfMonth.ToString("dd"));
            //0:General, 1:Face detection, 2:Spot
            List<DispatchedCampaignInfo.CalendarDateInfo> DispatchedCampaignCalendar = new List<DispatchedCampaignInfo.CalendarDateInfo>();

            if (id == null && screenGroupId == null)
            {
                return null;
            }

            List<ScheduleSummary> CurrentScreenCampaign = new List<ScheduleSummary>();

            if (id != null)
            {
                CurrentScreenCampaign = _DsDb.ScheduleSummary.Where(o => o.DeviceId == id && o.DeleteDate == null && o.UserGroupId == CurrentUserGroupId).ToList();
            }
            else if (screenGroupId != null)
            {
                CurrentScreenCampaign = _DsDb.ScheduleSummary.Where(o => o.ScreenGroupId == screenGroupId && o.DeleteDate == null && o.UserGroupId == CurrentUserGroupId).ToList();
            }
            else
            {
                return null;
            }

            //初始化
            for (int i = 0; i < int.Parse(lastDateOfMonth.ToString("dd")); i++)
            {
                int[] InitialCampaignCount = new int[] { 0, 0, 0, 0 };
                DispatchedCampaignCalendar.Add(new DispatchedCampaignInfo.CalendarDateInfo()
                {
                    DateNumber = i + 1,
                    CampaignCount = InitialCampaignCount
                });
            }

            foreach (var CampaignItem in CurrentScreenCampaign)
            {
                //只抓當月的有效Campaign
                if (CampaignItem.StartDate <= lastDateOfMonth)
                {
                    //取出此Campaign該月的起始日期
                    int Campaignstartdate;
                    if (CampaignItem.StartDate >= firstDateOfMonth)
                    {
                        Campaignstartdate = int.Parse(CampaignItem.StartDate.ToString("dd"));
                    }
                    else
                    {
                        Campaignstartdate = 1;
                    }

                    if (CampaignItem.EndDate == null)
                    {
                        //不停循環的廣告
                        if (CampaignItem.HasCalenderRule)
                        {
                            //循環播放的CalendarRepeatCampaign
                            bool[] repeatWeekDay = new bool[7];
                            repeatWeekDay[0] = CampaignItem.RepeatSunday == true;
                            repeatWeekDay[1] = CampaignItem.RepeatMonday == true;
                            repeatWeekDay[2] = CampaignItem.RepeatTuesday == true;
                            repeatWeekDay[3] = CampaignItem.RepeatWednesday == true;
                            repeatWeekDay[4] = CampaignItem.RepeatThurday == true;
                            repeatWeekDay[5] = CampaignItem.RepeatFriday == true;
                            repeatWeekDay[6] = CampaignItem.RepeatSaturday == true;

                            for (int i = Campaignstartdate; i <= int.Parse(lastDateOfMonth.ToString("dd")); i++)
                            {
                                DateTime thisDate = new DateTime(SelectedDate.Year, SelectedDate.Month, i);
                                if (repeatWeekDay[int.Parse(thisDate.DayOfWeek.ToString("d"))])
                                {
                                    if (CampaignItem.HasFaceDetectionRule)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[1]++;
                                    }
                                    else if (CampaignItem.Spot)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[2]++;
                                    }
                                    else if (CampaignItem.Event)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[3]++;
                                    }
                                    else
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[0]++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //循環播放的一般Campaign
                            for (int i = Campaignstartdate; i <= int.Parse(lastDateOfMonth.ToString("dd")); i++)
                            {
                                if (CampaignItem.HasFaceDetectionRule)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[1]++;
                                }
                                else if (CampaignItem.Spot)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[2]++;
                                }
                                else if (CampaignItem.Event)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[3]++;
                                }
                                else
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[0]++;
                                }
                            }
                        }

                    }
                    else if (CampaignItem.EndDate.Value >= firstDateOfMonth)
                    {
                        //指定日期播放
                        //取出此Campaign該月的結束日期
                        int Campaignenddate;
                        if (CampaignItem.EndDate.Value <= lastDateOfMonth)
                        {
                            Campaignenddate = int.Parse(CampaignItem.EndDate.Value.ToString("dd"));
                        }
                        else
                        {
                            Campaignenddate = int.Parse(lastDateOfMonth.ToString("dd"));
                        }
                        if (CampaignItem.HasCalenderRule)
                        {
                            //指定日期播放的CalendarRepeatCampaign
                            bool[] repeatWeekDay = new bool[7];

                            repeatWeekDay[0] = CampaignItem.RepeatSunday == true;
                            repeatWeekDay[1] = CampaignItem.RepeatMonday == true;
                            repeatWeekDay[2] = CampaignItem.RepeatTuesday == true;
                            repeatWeekDay[3] = CampaignItem.RepeatWednesday == true;
                            repeatWeekDay[4] = CampaignItem.RepeatThurday == true;
                            repeatWeekDay[5] = CampaignItem.RepeatFriday == true;
                            repeatWeekDay[6] = CampaignItem.RepeatSaturday == true;

                            for (int i = Campaignstartdate; i <= Campaignenddate; i++)
                            {
                                DateTime thisDate = new DateTime(SelectedDate.Year, SelectedDate.Month, i);
                                if (repeatWeekDay[int.Parse(thisDate.DayOfWeek.ToString("d"))])
                                {
                                    if (CampaignItem.HasFaceDetectionRule)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[1]++;
                                    }
                                    else if (CampaignItem.Spot)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[2]++;
                                    }
                                    else if (CampaignItem.Event)
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[3]++;
                                    }
                                    else
                                    {
                                        DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[0]++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //指定日期播放的一般Campaign
                            for (int i = Campaignstartdate; i <= Campaignenddate; i++)
                            {
                                if (CampaignItem.HasFaceDetectionRule)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[1]++;
                                }
                                else if (CampaignItem.Spot)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[2]++;
                                }
                                else if (CampaignItem.Event)
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[3]++;
                                }
                                else
                                {
                                    DispatchedCampaignCalendar.Where(o => o.DateNumber == i).FirstOrDefault().CampaignCount[0]++;
                                }
                            }
                        }
                    }
                }
            }
            return DispatchedCampaignCalendar;
        }

        /// <summary>
        /// 傳入指定日期後，取出裝置於該日期被安排播放哪些廣告
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="selectDate"></param>
        /// <returns></returns>
        public List<DispatchedCampaignInfo> GetSeletedDateDispatchedCampaign(UserInfo _UserInfo, int? id, long? screenGroupId, string selectDate)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;
            DateTime selectedDateStart = DateTime.Parse(selectDate);
            DateTime selectedDateEnd = DateTime.Parse(selectDate).AddDays(1).AddMilliseconds(-1);

            if (id == null && screenGroupId == null)
            {
                return null;
            }

            //取出當前裝置的當日有效廣告
            List<ScheduleSummary> ScheduleSummary = new List<ScheduleSummary>();

            if (id != null)
            {
                ScheduleSummary = _DsDb.ScheduleSummary.Where(o => o.DeviceId == id && o.StartDate <= selectedDateEnd && (o.EndDate == null ? true : o.EndDate >= selectedDateStart)).OrderBy(o => o.PlayOrder).ToList();
            }
            else if (screenGroupId != null)
            {
                ScheduleSummary = _DsDb.ScheduleSummary.Where(o => o.ScreenGroupId == screenGroupId.Value && o.StartDate <= selectedDateEnd && (o.EndDate == null ? true : o.EndDate >= selectedDateStart)).OrderBy(o => o.PlayOrder).ToList();
            }

            var SeletedDateDispatchedCampaign = new List<DispatchedCampaignInfo>();
            foreach (var c in ScheduleSummary)
            {
                List<string> repeatWeekDayList = new List<string>();
                if (c.HasCalenderRule)
                {
                    //循環播放的CalendarRepeatCampaign
                    bool[] repeatWeekDay = new bool[7];
                    repeatWeekDay[0] = c.RepeatSunday == true;
                    repeatWeekDay[1] = c.RepeatMonday == true;
                    repeatWeekDay[2] = c.RepeatTuesday == true;
                    repeatWeekDay[3] = c.RepeatWednesday == true;
                    repeatWeekDay[4] = c.RepeatThurday == true;
                    repeatWeekDay[5] = c.RepeatFriday == true;
                    repeatWeekDay[6] = c.RepeatSaturday == true;

                    if (c.RepeatMonday == true)
                        repeatWeekDayList.Add("Monday");
                    if (c.RepeatTuesday == true)
                        repeatWeekDayList.Add("Tuesday");
                    if (c.RepeatWednesday == true)
                        repeatWeekDayList.Add("Wednesday");
                    if (c.RepeatThurday == true)
                        repeatWeekDayList.Add("Thursday");
                    if (c.RepeatFriday == true)
                        repeatWeekDayList.Add("Friday");
                    if (c.RepeatSaturday == true)
                        repeatWeekDayList.Add("Saturday");
                    if (c.RepeatSunday == true)
                        repeatWeekDayList.Add("Sunday");

                    if (repeatWeekDay[int.Parse(selectedDateStart.DayOfWeek.ToString("d"))])
                    {
                        SeletedDateDispatchedCampaign.Add(new DispatchedCampaignInfo()
                        {
                            ScheduleSummaryId = c.Id,
                            Playorder = c.PlayOrder,
                            CampaignId = c.CampaignId,
                            CampaignName = c.Campaign.Name,
                            HasCalendarRule = c.HasCalenderRule,
                            RepeatWeekDay = repeatWeekDayList,
                            HasFaceDetectionRule = c.HasFaceDetectionRule,
                            Spot = c.Spot,
                            Event = c.Event,
                            Startdate = ConvertDateString(c.StartDate),
                            Enddate = c.EndDate.HasValue ? ConvertDateString(c.EndDate) : "",
                            DailyStartTime = c.DailyStartTime,
                            DailyEndTime = c.DailyEndTime,
                            Gender = c.Gender.ToString(),
                            AgeRestriction = CombineAgeRestrictionName(_UserInfo, c.AgeRestriction),
                            HasMonthCalenderRule = c.HasMonthCalenderRule,
                            MonthDay = c.MonthDay
                        });
                    }
                }
                else
                {
                    SeletedDateDispatchedCampaign.Add(new DispatchedCampaignInfo()
                    {
                        ScheduleSummaryId = c.Id,
                        Playorder = c.PlayOrder,
                        CampaignId = c.CampaignId,
                        CampaignName = c.Campaign.Name,
                        HasCalendarRule = c.HasCalenderRule,
                        HasFaceDetectionRule = c.HasFaceDetectionRule,
                        Spot = c.Spot,
                        Event = c.Event,
                        Startdate = ConvertDateString(c.StartDate),
                        Enddate = c.EndDate.HasValue ? ConvertDateString(c.EndDate) : "",
                        DailyStartTime = c.DailyStartTime,
                        DailyEndTime = c.DailyEndTime,
                        Gender = c.Gender.ToString(),
                        AgeRestriction = CombineAgeRestrictionName(_UserInfo, c.AgeRestriction),
                        HasMonthCalenderRule = c.HasMonthCalenderRule,
                        MonthDay = c.MonthDay
                    });
                }
            }

            return SeletedDateDispatchedCampaign;
        }

        /// <summary>
        /// 新增device tag
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="deviceId"></param>
        /// <param name="screenGroupId"></param>
        /// <returns></returns>
        public ApiResult InsertTag(List<String> tagName, List<int> deviceId, List<long> screenGroupId)
        {
            List<DeviceTag> Reslut = new List<DeviceTag>();

            if (deviceId != null)
            {
                //Device
                foreach (var d in deviceId)
                {
                    foreach (var t in tagName)
                    {
                        //判斷是否已經存在這個tag 了
                        if (_DsDb.DeviceTag.Where(o => o.TagName == t && o.DeviceId == d).Count() == 0)
                        {
                            Reslut.Add(new DeviceTag
                            {
                                DeviceId = d,
                                TagName = t.Trim(),
                                CreateDate = DateTime.Now
                            });
                        }
                    }
                }
            }

            if (screenGroupId != null)
            {
                //screenGroup
                foreach (var s in screenGroupId)
                {
                    foreach (var t in tagName)
                    {
                        //判斷是否已經存在這個tag 了
                        if (_DsDb.DeviceTag.Where(o => o.TagName == t && o.ScreenGroupId == s).Count() == 0)
                        {
                            Reslut.Add(new DeviceTag
                            {
                                ScreenGroupId = s,
                                TagName = t.Trim(),
                                CreateDate = DateTime.Now
                            });
                        }
                    }
                }
            }


            try
            {
                _DsDb.DeviceTag.AddRange(Reslut);
                _DsDb.SaveChanges();
                return new SuccessResult();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }


        }

        /// <summary>
        /// 刪除 Device Tag
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="deviceList"></param>
        /// <returns></returns>
        public ApiResult DeleteTag(List<string> tagName, List<int> deviceList, List<long> screenGroupList)
        {
            List<DeviceTag> deleteList = new List<DeviceTag>();
            if (deviceList != null)
            {
                foreach (var d in deviceList)
                {
                    var deleteObj = _DsDb.DeviceTag.Where(o => o.DeviceId == d && tagName.Contains(o.TagName)).ToList();
                    deleteList.AddRange(deleteObj);
                }
            }

            if (screenGroupList != null)
            {
                foreach (var s in screenGroupList)
                {
                    var deleteObj = _DsDb.DeviceTag.Where(o => o.ScreenGroupId == s && tagName.Contains(o.TagName)).ToList();
                    deleteList.AddRange(deleteObj);
                }
            }

            try
            {
                _DsDb.DeviceTag.RemoveRange(deleteList);
                _DsDb.SaveChanges();
                return new SuccessResult();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

        }

        /// <summary>
        /// 取所有device 以及含有的tag 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageRows"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="searchText"></param>
        /// <param name="searchType"></param>
        /// 
        /// <returns></returns>
        public ApiResult GetDeviceTag(int page, int pageRows, UserInfo _UserInfo, string searchText, int searchType)
        {
            //SearchType: 0 deviceName, 1 screenGroup, 2 tag
            int currentUserGroupId = _UserInfo.UserGroupId;
            int allPage = 0;
            List<DeviceTagInfo> tempDeviceList;
            List<DeviceTagInfo> tempScreenGroup;
            List<DeviceTagInfo> tempTatalList;
            List<DeviceTagInfo> deviceList;
            //SearchType 有deviceName screenGroup Tag 查詢方式
            //有查詢條件
            if (searchText != null)
            {
                if (searchType == 0)
                {
                    //device
                    tempDeviceList = _DsDb.Device.Where(o => o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Name.Contains(searchText))
                    .OrderBy(o => o.Id)
                    .Select(o => new DeviceTagInfo()
                    {
                        DeviceId = o.Id,
                        Name = o.Name
                    }).ToList();


                    tempTatalList = tempDeviceList;
                }
                else if (searchType == 1)
                {
                    //screengroup id

                    tempScreenGroup = _DsDb.ScreenGroup.Where(o => o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.Name.Contains(searchText))
                        .Select(o => new DeviceTagInfo()
                        {
                            ScreenGroupId = o.Id,
                            Name = o.Name
                        }).ToList();
                    tempTatalList = tempScreenGroup;
                }
                else //tag search
                {
                    //device
                    tempDeviceList = _DsDb.Device.Join(_DsDb.DeviceTag,
                        d => d.Id,
                        t => t.DeviceId,
                        (d, t) => new
                        {
                            DeviceId = d.Id,
                            DeviceName = d.Name,
                            CreateByUserId = d.CreateByUserId,
                            DeleteDate = d.DeleteDate,
                            UserGroup_Id = d.UserGroup_Id,
                            TempTag = t.TagName
                        })
                        .OrderBy(dt => dt.DeviceId)
                        .Where(dt => dt.TempTag.Contains(searchText) && dt.UserGroup_Id == currentUserGroupId && dt.DeleteDate == null && dt.CreateByUserId != null)
                        .Select(dt => new DeviceTagInfo()
                        {
                            DeviceId = dt.DeviceId,
                            Name = dt.DeviceName,
                            TempTag = dt.TempTag,
                        })
                        .ToList();
                    //sc
                    tempScreenGroup = _DsDb.ScreenGroup.Join(_DsDb.DeviceTag,
                        d => d.Id,
                        t => t.ScreenGroupId,
                        (d, t) => new
                        {
                            ScreenGroupId = d.Id,
                            DeviceName = d.Name,
                            DeleteDate = d.DeleteDate,
                            UserGroup_Id = d.UserGroup_Id,
                            CreateByUserId = d.CreateByUserId,
                            TempTag = t.TagName
                        })
                        .OrderBy(dt => dt.ScreenGroupId)
                        .Where(dt => dt.TempTag.Contains(searchText) && dt.UserGroup_Id == currentUserGroupId && dt.DeleteDate == null)
                        .Select(dt => new DeviceTagInfo()
                        {
                            ScreenGroupId = dt.ScreenGroupId,
                            Name = dt.DeviceName,
                            TempTag = dt.TempTag
                        })
                        .ToList();
                    //array合併
                    tempDeviceList.AddRange(tempScreenGroup);
                    tempTatalList = tempDeviceList;
                }

            }
            //無查詢條件 查詢所有device 以及 screengroup
            else
            {
                //device
                tempDeviceList = _DsDb.Device.Where(o => o.UserGroup_Id == currentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null)
                    .OrderBy(o => o.Id)
                    .Select(o => new DeviceTagInfo()
                    {
                        DeviceId = o.Id,
                        Name = o.Name
                    }).ToList();

                //screengroup id
                tempScreenGroup = _DsDb.ScreenGroup.Where(o => o.DeleteDate == null && o.UserGroup_Id == currentUserGroupId)
                    .Select(o => new DeviceTagInfo()
                    {
                        ScreenGroupId = o.Id,
                        Name = o.Name
                    }).ToList();
                //array合併
                tempDeviceList.AddRange(tempScreenGroup);
                tempTatalList = tempDeviceList;
            }
            //計算總頁數
            allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tempTatalList.Count()) / pageRows));
            // 將tag(array)塞入
            deviceList = tempTatalList.Skip((page - 1) * pageRows).Take(pageRows).ToList();
            foreach (var data in deviceList)
            {
                var tag = _DsDb.DeviceTag
                    .Where(o => o.DeviceId == data.DeviceId || o.ScreenGroupId == data.ScreenGroupId).Select(o => o.TagName)
                    .Distinct()
                    .ToList();
                if (tag != null)
                {
                    data.Tag = tag;
                }
            }

            return new SuccessResult(Data: new
            {
                DeviceList = deviceList,
                Count = allPage
            });
        }

        /// <summary>
        /// 帶group id 找群組
        /// </summary>
        /// <param name="groupId"></param>
        /// /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ScreenGroup FindScreenGroup(long groupId, UserInfo _UserInfo)
        {
            var screenGroup = _DsDb.ScreenGroup.Where(o => o.Id == groupId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null).SingleOrDefault();

            return screenGroup;
        }

        /// <summary>
        /// 帶device id找device(瀝除已分組的裝置)
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public Device FindDevice(int deviceId, UserInfo _UserInfo)
        {
            var device = _DsDb.Device.Where(o => o.Id == deviceId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.ScreenGroupId == null).SingleOrDefault();

            return device;
        }

        /// <summary>
        /// 刪除裝置
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="deviceId"></param>
        public void DeleteDevice(UserInfo _UserInfo, int deviceId)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            var device = _DsDb.Device.Where(o => o.Id == deviceId && o.UserGroup_Id == CurrentUserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).SingleOrDefault();

            //delete related Campaign
            _DsDb.CampaignToDeviceScreen.RemoveRange(_DsDb.CampaignToDeviceScreen.Where(c => c.DeviceId == deviceId));

            var devicescreen = _DsDb.DeviceScreen.Where(ds => ds.DeviceId == deviceId);
            foreach (var ds in devicescreen)
            {
                //delete related devicescreen
                _DsDb.DeviceScreen.Remove(ds);
            }

            //delete device tag
            var tag = _DsDb.DeviceTag.Where(o => o.DeviceId == deviceId);
            if (tag != null)
            {
                _DsDb.DeviceTag.RemoveRange(tag);
            }

            //delete device
            device.DeleteDate = DateTime.Now;
            device.DeleteByUserId = _UserInfo.Id;

            _DsDb.SaveChanges();
        }

        /// <summary>
        /// 刪除(解散裝置)群組
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult DeleteScreenGroup(ScreenGroupApiModel Model, UserInfo _UserInfo)
        {
            var group = FindScreenGroup(Model.Id, _UserInfo);

            if (group != null)
            {
                var devicesinGroup = _DsDb.Device.Where(o => o.ScreenGroupId == group.Id && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).ToList();
                var scheduleSummary = _DsDb.ScheduleSummary.Where(o => o.ScreenGroupId == group.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                var deviceTags = _DsDb.DeviceTag.Where(o => o.ScreenGroupId == group.Id).ToList();

                foreach (var device in devicesinGroup)
                {
                    //清除群組相關資訊
                    device.ScreenGroupId = null;
                    device.OffsetX = null;
                    device.OffsetY = null;

                    device.LocationId = null;
                    device.DefaultScreenFileId = null;
                    device.DailyShutDownTime = null;
                    device.ContentStatus = 1;
                    device.DispatchStatus = 2;
                    device.ConfigStatus = true;
                }

                group.Name = group.Name + DateTime.Now.ToString(); //重新命名
                group.DeleteDate = DateTime.Now;
                group.DeleteByUserId = _UserInfo.Id;

                try
                {
                    if (scheduleSummary.Count > 0)
                    {
                        _DsDb.ScheduleSummary.RemoveRange(scheduleSummary);
                    }

                    if (deviceTags.Count > 0)
                    {
                        _DsDb.DeviceTag.RemoveRange(deviceTags);
                    }

                    _DsDb.SaveChanges();
                }
                catch (Exception)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }

            return new NotFoundResult();
        }

        /// <summary>
        /// 批次刪除裝置或螢幕群組
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult BatchDeleteDevices(BatchChangeDeviceApiModel Model, UserInfo _UserInfo)
        {
            var now = DateTime.Now;

            if (Model.ScreenGroupList != null && Model.ScreenGroupList.Count > 0)
            {
                //Delete groups
                foreach (var screenGroupId in Model.ScreenGroupList)
                {
                    var deletedScreenGroup = FindScreenGroup(screenGroupId, _UserInfo);

                    var devicesinGroup = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).ToList();
                    var scheduleSummary = _DsDb.ScheduleSummary.Where(o => o.ScreenGroupId == screenGroupId && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                    var deviceTags = _DsDb.DeviceTag.Where(o => o.ScreenGroupId == screenGroupId).ToList();

                    foreach (var d in devicesinGroup)
                    {
                        //清除群組相關資訊
                        d.ScreenGroupId = null;
                        d.OffsetX = null;
                        d.OffsetY = null;

                        d.LocationId = null;
                        d.DefaultScreenFileId = null;
                        d.DailyShutDownTime = null;
                        d.ContentStatus = 0;
                        d.DispatchStatus = 0;
                        d.ConfigStatus = true;
                    }

                    deletedScreenGroup.Name = deletedScreenGroup.Name + DateTime.Now.ToString(); //重新命名
                    deletedScreenGroup.DeleteDate = DateTime.Now;
                    deletedScreenGroup.DeleteByUserId = _UserInfo.Id;

                    if (scheduleSummary.Count > 0)
                    {
                        _DsDb.ScheduleSummary.RemoveRange(scheduleSummary);
                    }

                    if (deviceTags.Count > 0)
                    {
                        _DsDb.DeviceTag.RemoveRange(deviceTags);
                    }
                }
            }

            if (Model.DeviceList != null && Model.DeviceList.Count > 0)
            {
                //Delete devices
                PublishModels PublishModels = new PublishModels();
                foreach (var deviceId in Model.DeviceList)
                {
                    //呼叫刪除裝置的方法
                    PublishModels.AdjustbyDeleteDevice(deviceId);
                    DeleteDevice(_UserInfo, deviceId);
                }
            }

            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            return new SuccessResult();

        }

        /// <summary>
        /// 新增/編輯群組(裝置資訊)
        /// 新增時傳入Id,Name,TimeServerId,HorizontallyScreenCount,VerticallyScreenCount,Devices,IsNeedUpdate,DisconnectCount,AutoUpdateEnabled,AutoUpdateTime
        /// 維護時傳入Id,Name,TimeServerId,HorizontallyScreenCount,VerticallyScreenCount,Devices,IsNeedUpdate,DisconnectCount
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult EditScreenGroup(ScreenGroupApiModel Model, UserInfo _UserInfo)
        {
            #region
            //群組的設定
            int? SGLocationId = null;
            int? SGDefaultScreenFileId = null;
            TimeSpan? SGDailyShutDownTime = null;
            string SGVersionBenchmark = "";
            bool SGIsNeedUpdate = false;
            int EditSGCount = Model.Devices.Count();

            //判斷是否有裝置
            if (EditSGCount == 0)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            //id = 0 為新增
            if (Model.Id == 0)
            {
                var groupData = new ScreenGroup()
                {
                    Name = Model.Name,
                    TimeServerId = Model.TimeServerId,
                    HorizontallyScreenCount = Model.HorizontallyScreenCount,
                    VerticallyScreenCount = Model.VerticallyScreenCount,
                    CreateDate = DateTime.Now,
                    CreateByUserId = _UserInfo.Id,
                    UserGroup_Id = _UserInfo.UserGroupId,
                    DisconnectCount = Model.DisconnectCount >= 0 && Model.DisconnectCount < EditSGCount ? Model.DisconnectCount : 0
                };

                var checkgroup = _DsDb.ScreenGroup.Where(o => o.Name == Model.Name && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();

                if (checkgroup.Count > 0)
                {
                    //Name is duplicate
                    return new ExceptionResult(Message: "Error_0023");
                }

                try
                {
                    _DsDb.ScreenGroup.Add(groupData);
                    _DsDb.SaveChanges();

                    foreach (var deviceingroup in Model.Devices)
                    {
                        //尋找要加入群組的裝置
                        var device = _DsDb.Device.Where(o => o.Id == deviceingroup.Id && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null).SingleOrDefault();
                        //尋找剛剛新增的群組
                        var group = _DsDb.ScreenGroup.Where(o => o.Name == groupData.Name && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null).SingleOrDefault();

                        //清除所有device設定
                        device.LocationId = null;
                        device.DefaultScreenFileId = null;
                        device.DailyShutDownTime = null;
                        device.ContentStatus = 1;
                        device.DispatchStatus = 2;
                        device.ConfigStatus = true;
                        device.IsNeedUpdate = Model.IsNeedUpdate;
                        device.AutoUpdateEnabled = Model.AutoUpdateEnabled;
                        if (Model.AutoUpdateTime != null)
                        {
                            device.AutoUpdateTime = TimeSpan.Parse(Model.AutoUpdateTime);
                            device.VersionUpdateTime = TimeSpan.Parse(Model.AutoUpdateTime);
                        }
                        else
                        {
                            device.AutoUpdateTime = null;
                            device.VersionUpdateTime = null;
                        }
                        device.ScreenGroupId = group.Id;
                        device.OffsetX = deviceingroup.OffsetX;
                        device.OffsetY = deviceingroup.OffsetY;

                        //判斷是否已有ScheduleSummary有則刪除
                        var scheduleSummary = _DsDb.ScheduleSummary.Where(o => o.DeviceId == device.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                        if (scheduleSummary.Count > 0)
                        {
                            _DsDb.ScheduleSummary.RemoveRange(scheduleSummary);
                        }
                    }

                    _DsDb.SaveChanges();

                }
                catch (Exception)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
                #endregion
            }
            else
            {
                // id不為0 則為編輯
                var editGroup = FindScreenGroup(Model.Id, _UserInfo);

                if (editGroup != null)
                {
                    editGroup.Name = Model.Name;
                    editGroup.TimeServerId = Model.TimeServerId;
                    editGroup.HorizontallyScreenCount = Model.HorizontallyScreenCount;
                    editGroup.VerticallyScreenCount = Model.VerticallyScreenCount;
                    editGroup.UpdateDate = DateTime.Now;
                    editGroup.UpdateByUserId = _UserInfo.Id;
                    editGroup.DisconnectCount = Model.DisconnectCount >= 0 && Model.DisconnectCount < EditSGCount ? Model.DisconnectCount : 0;

                    //舊的devices
                    var devicesinGroup = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.ScreenGroupId == Model.Id).ToList();

                    Device originDeviceSetting = new Device();
                    originDeviceSetting = devicesinGroup.FirstOrDefault(); //取第一筆當群組資料

                    Device newScreenGroupMember = new Device();
                    DeviceOffsetInfo newScreenGroupMemberBenchmark = new DeviceOffsetInfo();
                    newScreenGroupMemberBenchmark = Model.Devices.FirstOrDefault();
                    newScreenGroupMember = _DsDb.Device.Where(o => o.Id == newScreenGroupMemberBenchmark.Id).FirstOrDefault();

                    //寫入群組設定
                    SGLocationId = originDeviceSetting.LocationId;
                    SGDefaultScreenFileId = originDeviceSetting.DefaultScreenFileId;
                    SGDailyShutDownTime = originDeviceSetting.DailyShutDownTime;
                    SGVersionBenchmark = newScreenGroupMember.Version.Version;
                    foreach (var deviceingroup in Model.Devices)
                    {
                        if (!SGIsNeedUpdate)
                        {
                            var device = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Id == deviceingroup.Id).SingleOrDefault();
                            if (SGVersionBenchmark != device.Version.Version)
                                SGIsNeedUpdate = true;
                        }
                    }

                    foreach (var d in devicesinGroup)
                    {
                        var device = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Id == d.Id).SingleOrDefault();

                        //清除舊的裝置群組資訊
                        device.ScreenGroupId = null;
                        device.OffsetX = null;
                        device.OffsetY = null;
                        device.LocationId = null;
                        device.DefaultScreenFileId = null;
                        device.DailyShutDownTime = null;
                        device.ContentStatus = 1;
                        device.DispatchStatus = 2;
                        device.ConfigStatus = true; //通知Player修改
                        device.AutoUpdateEnabled = false;
                        device.AutoUpdateTime = null;
                        device.VersionUpdateTime = null;
                        device.UpdateDate = DateTime.Now;
                        device.UpdateByUserId = _UserInfo.Id;
                    }

                    foreach (var deviceingroup in Model.Devices)
                    {
                        //替換裝置的群組
                        var device = _DsDb.Device.Where(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null && o.Id == deviceingroup.Id).SingleOrDefault();

                        //寫入群組設定到所有device
                        device.LocationId = SGLocationId;
                        device.DefaultScreenFileId = SGDefaultScreenFileId;
                        device.DailyShutDownTime = SGDailyShutDownTime;
                        device.ContentStatus = 1;
                        device.DispatchStatus = 2;
                        device.ConfigStatus = true;
                        device.IsNeedUpdate = SGIsNeedUpdate;
                        device.AutoUpdateEnabled = Model.AutoUpdateEnabled;
                        if (Model.AutoUpdateTime != null)
                        {
                            device.AutoUpdateTime = TimeSpan.Parse(Model.AutoUpdateTime);
                            device.VersionUpdateTime = TimeSpan.Parse(Model.AutoUpdateTime);
                        }
                        else
                        {
                            device.AutoUpdateTime = null;
                            device.VersionUpdateTime = null;
                        }

                        device.ScreenGroupId = Model.Id;
                        device.OffsetX = deviceingroup.OffsetX;
                        device.OffsetY = deviceingroup.OffsetY;

                        device.UpdateDate = DateTime.Now;
                        device.UpdateByUserId = _UserInfo.Id;

                        //判斷是否已有ScheduleSummary有則刪除
                        var scheduleSummary = _DsDb.ScheduleSummary.Where(o => o.DeviceId == device.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                        if (scheduleSummary.Count > 0)
                        {
                            _DsDb.ScheduleSummary.RemoveRange(scheduleSummary);
                        }
                    }

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }

                    return new SuccessResult();
                }

                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 取得群組的Tag
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="screenGroupId"></param>
        /// <returns></returns>
        public List<string> GetScreenGroupTags(int? deviceId, long? screenGroupId)
        {
            List<string> tagsName = new List<string>();
            if (deviceId != null)
            {
                tagsName = _DsDb.DeviceTag.Where(o => o.DeviceId == deviceId).Select(o => o.TagName).ToList();
            }
            else if (screenGroupId != null)
            {
                tagsName = _DsDb.DeviceTag.Where(o => o.ScreenGroupId == screenGroupId).Select(o => o.TagName).ToList();
            }
            else
            {
                return null;
            }

            return tagsName;
        }

        /// <summary>
        /// 播放器自動更新設定
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="autoUpdateEnabled"></param>
        /// <param name="autoUpdateTime"></param>
        /// <returns></returns>
        public ApiResult AutoUpdateEnabledChange(UserInfo _UserInfo, int? id, long? screenGroupId, bool autoUpdateEnabled, string autoUpdateTime)
        {
            int CurrentUserGroupId = _UserInfo.UserGroupId;

            var versionUpdate = false;

            if (id == null && screenGroupId == null)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            List<Device> devices = new List<Device>();
            if (id != null)
            {
                devices = _DsDb.Device.Where(d => d.Id == id && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
            }
            else if (screenGroupId != null)
            {
                devices = _DsDb.Device.Where(d => d.ScreenGroupId == screenGroupId && d.UserGroup_Id == _UserInfo.UserGroupId && d.DeleteDate == null && d.DeleteByUserId == null && d.CreateDate != null && d.CreateByUserId != null).ToList();
                //關閉時才需判斷
                if (!autoUpdateEnabled)
                {
                    //timserver的device版本
                    var targetVersionId = _DsDb.Device.Where(d => d.Id == (_DsDb.ScreenGroup.FirstOrDefault().TimeServerId)).FirstOrDefault().VersionId;
                    //比對是否每台相同
                    versionUpdate = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId).Any(o => o.VersionId != targetVersionId);
                }
            }

            try
            {
                foreach (var d in devices)
                {
                    d.AutoUpdateEnabled = autoUpdateEnabled;
                    d.UpdateByUserId = _UserInfo.Id;
                    d.UpdateDate = DateTime.Now;
                    //通知Player改變狀態
                    d.ConfigStatus = true;
                    //判斷按鈕開關
                    if (autoUpdateEnabled)
                    {
                        if (autoUpdateTime != null)
                        {
                            //將時間轉成date格式
                            d.VersionUpdateTime = TimeSpan.Parse(autoUpdateTime);
                            d.AutoUpdateTime = TimeSpan.Parse(autoUpdateTime);
                        }
                        else
                        {
                            d.VersionUpdateTime = null;
                            d.AutoUpdateTime = null;
                        }
                    }
                    else
                    {
                        d.IsNeedUpdate = false;
                        //有ScreenGroup Device版本不一
                        if (versionUpdate)
                        {
                            d.IsNeedUpdate = true;
                        }
                        d.VersionUpdateTime = null;
                        d.AutoUpdateTime = null;
                    }
                }

                _DsDb.SaveChanges();

                return new SuccessResult();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 批次或個別更新裝置的Player version,輸入Type,DeviceList,ScreenGroupList,AutoUpdateTime
        /// 更新類型 0:單次更新 1:批次調整更新時間 2:批次取消更新
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult BatchUpdateDevicesVersion(BatchChangeDeviceApiModel Model, UserInfo _UserInfo)
        {
            ConvertHelper convertHelper = new ConvertHelper();
            string MaxVersion = convertHelper.CompareVersion(_DsDb.VersionFile.Select(o => o.Version).ToList());
            var latestVersionId = _DsDb.VersionFile.Where(o => o.Version == MaxVersion).FirstOrDefault().Id;
            var deviceList = new List<Device>();
            var screenGroupList = new List<ScreenGroup>();
            TimeSpan? autoUpdateTime = Model.AutoUpdateTime != null ? TimeSpan.Parse(Model.AutoUpdateTime) : (TimeSpan?)null;

            if (Model.ScreenGroupList != null && Model.ScreenGroupList.Count > 0)
            {
                foreach (var screenGroupId in Model.ScreenGroupList)
                {
                    var devicesInGroup = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                    foreach (var device in devicesInGroup)
                    {
                        deviceList.Add(device);
                    }
                    screenGroupList.Add(_DsDb.ScreenGroup.Where(o => o.Id == screenGroupId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault());
                }
            }

            if (Model.DeviceList != null && Model.DeviceList.Count > 0)
            {
                foreach (var deviceId in Model.DeviceList)
                {
                    deviceList.Add(FindDevice(deviceId, _UserInfo));
                }
            }

            //區分更新類型 0:單次更新 1:批次調整更新時間 2:批次取消更新
            if (Model.Type == 0)
            {
                //已經是最新版本的不處理
                var singleUpdateList = deviceList.Where(o => o.VersionId != latestVersionId).ToList();
                foreach (var device in singleUpdateList)
                {
                    device.UpdateByUserId = _UserInfo.Id;
                    device.UpdateDate = DateTime.Now;
                    device.ConfigStatus = true;
                    device.IsNeedUpdate = true;
                    device.VersionUpdateTime = autoUpdateTime;
                }
            }
            else if (Model.Type == 1)
            {
                foreach (var device in deviceList)
                {
                    device.UpdateByUserId = _UserInfo.Id;
                    device.UpdateDate = DateTime.Now;
                    device.ConfigStatus = true;
                    device.IsNeedUpdate = true;
                    device.AutoUpdateEnabled = true;
                    device.AutoUpdateTime = autoUpdateTime;
                    device.VersionUpdateTime = autoUpdateTime;
                }
            }
            else //將傳入裝置的更新全部取消
            {
                //取出現有更新設定的裝置
                var CancelAutoUpdateList = deviceList.Where(o => o.AutoUpdateEnabled == true || o.VersionUpdateTime != null).ToList();
                var screenGroupDeviceList = deviceList.Where(o => (o.AutoUpdateEnabled == true || o.VersionUpdateTime != null) && o.ScreenGroupId != null).ToList();

                foreach (var screenGroup in screenGroupList)
                {
                    if (screenGroupDeviceList.Where(o => o.ScreenGroupId == screenGroup.Id).Any())
                    {
                        var timeServerId = screenGroup.TimeServerId;
                        var VersionId = _DsDb.Device.Where(o => o.Id == timeServerId).SingleOrDefault().VersionId;
                        //將與Time Server版本不一致的列入更新裝置清單
                        if (screenGroupDeviceList.Any(o => o.ScreenGroupId == screenGroup.Id && o.VersionId != VersionId))
                        {
                            foreach (var device in screenGroupDeviceList.Where(o => o.ScreenGroupId == screenGroup.Id))
                            {
                                device.IsNeedUpdate = true;
                            }
                        }
                    }
                }

                foreach (var device in CancelAutoUpdateList)
                {
                    device.UpdateByUserId = _UserInfo.Id;
                    device.UpdateDate = DateTime.Now;
                    device.ConfigStatus = true;
                    device.AutoUpdateEnabled = false;
                    device.AutoUpdateTime = null;
                    device.VersionUpdateTime = null;
                    device.IsNeedUpdate = false;
                }
            }

            try
            {
                _DsDb.SaveChanges();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            return new SuccessResult();

        }
        /// <summary>
        /// 傳入Device Id，指定該台裝置進行重啟OS
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ApiResult RestartDeviceOS(UserInfo _UserInfo, int Id)
        {
            var device = _DsDb.Device.FirstOrDefault(o => o.Id == Id && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteDate == null && o.DeleteByUserId == null && o.CreateDate != null && o.CreateByUserId != null);

            device.IsNeedRestart = true;
            device.UpdateByUserId = _UserInfo.Id;
            device.UpdateDate = DateTime.Now;

            try
            {
                _DsDb.SaveChanges();
                return new SuccessResult();
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }
        /// <summary>
        /// 傳入年齡限制，將DB中對應的年齡區間名稱組成字串後回傳
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="AgeRestriction"></param>
        /// <returns></returns>
        public string CombineAgeRestrictionName(UserInfo _UserInfo, string AgeRestriction)
        {
            string AgeRestrictionName = "";
            if (AgeRestriction != null)
            {
                int CurrentUserGroupId = _UserInfo.UserGroupId;
                var AgeRangeDefinition = _DsDb.AgeRangeDefinition.Where(o => o.UserGroup_Id == CurrentUserGroupId);


                string[] AgeRestrictionList = AgeRestriction.Split(',');

                for (int i = 0; i < AgeRestrictionList.Length; i++)
                {
                    if (i != 0)
                    {
                        AgeRestrictionName = AgeRestrictionName + ",";
                    }
                    int ageId = int.Parse(AgeRestrictionList[i]);
                    AgeRestrictionName = AgeRestrictionName + AgeRangeDefinition.Where(a => a.AgeId == ageId && a.UserGroup_Id == CurrentUserGroupId).FirstOrDefault().Description;
                }
            }
            return AgeRestrictionName;
        }

        /// <summary>
        /// 轉換byte to GB
        /// </summary>
        /// <param name="usage"></param>
        /// <returns></returns>
        public double ConverttoGB(long usage)
        {
            return Math.Round(usage / (Math.Pow(1024, 3)), 2, MidpointRounding.AwayFromZero);
        }

        //多螢幕解析度轉換成字串
        private string resolutionToString(List<DeviceScreen> dsList)
        {
            if (dsList == null || dsList.Count < 1)
            {
                return string.Empty;
            }

            var result = dsList[0].Width + "*" + dsList[0].Height;

            for (int i = 1; i < dsList.Count; i++)
            {
                result += " , " + dsList[i].Width + "*" + dsList[i].Height;
            }
            return result;
        }
        /// <summary>
        /// Datetime?轉換格式為 YYYY/MM/DD HH:mm
        /// </summary>
        /// <param name="inDate"></param>
        /// <returns></returns>
        public string ConvertDateString(DateTime? inDate)
        {
            string outDate;
            if (inDate.HasValue)
            {
                string year = inDate.Value.Year.ToString();
                string month = inDate.Value.Month.ToString();
                string day = inDate.Value.Day.ToString();
                string hour = inDate.Value.Hour.ToString();
                string minite = string.Format("{0:00}", inDate.Value.Minute);
                outDate = year + "/" + month + "/" + day + " " + hour + ":" + minite;
            }
            else
            {
                outDate = "";
            }
            return outDate;
        }
    }
}
