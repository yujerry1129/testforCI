﻿using DsDbLib.Models;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static DsSite.Models.LibraryModels;

namespace DsSite.Models
{
    public class CampaignModels : Controllers.Base.DsBaseController
    {

        /// <summary>
        /// DB 連線close 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// CampaignEditor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public CampaignEditorViewModel CampaignEditor(int? id, UserInfo _UserInfo)
        {
            var model = new CampaignEditorViewModel();

            //Check if user can edit this campaign
            if (id == null)
            {
                //do add
                model.CampaignId = 0;
            }
            else if (_DsDb.Campaign.Any(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteByUserId == null && o.Id == id))
            {
                //do edit
                model.CampaignId = id.Value;
            }
            else
            {
                //user has no PERMISSION to edit the campaign or data NOT FOUND
                model.HideEditor = true;
                model.ErrorMessage = "You have no permission to edit the campaign or campaign had been deleted.";
            }
            return model;
        }

        /// <summary>
        /// 取得當前使用者的所有Campagin資訊
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="page"></param>
        /// <param name="pageRows"></param>
        /// <param name="searchText"></param>
        /// <param name="searchType"></param>
        /// <returns>回傳Campaign 的所有資料</returns>
        public ApiResult GetAllCampaign(UserInfo _UserInfo, int page, int pageRows, string searchText, string searchType) //campaignName tag
        {
            int currentUserGroupId = _UserInfo.UserGroupId;
            int allPage = 0;
            List<CampaignInfo.CampaignList> campaignList;

            if (searchText != null)
            {
                if (searchType == "campaignName")
                {
                    //campaignName
                    campaignList = _DsDb.Campaign
                        .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == currentUserGroupId && o.Name.Contains(searchText))
                        .Select(o => new CampaignInfo.CampaignList()
                        {
                            CampaignId = o.Id,
                            CampaignName = o.Name,
                            Duration = o.Duration
                        })
                        .ToList();
                }
                else
                {
                    //tag
                    campaignList = _DsDb.Campaign.Join(_DsDb.CampaignTag,
                        c => c.Id,
                        t => t.CampaignId,
                        (c, t) => new
                        {
                            CampaignId = c.Id,
                            CampaignName = c.Name,
                            Duration = c.Duration,
                            CreateByUserId = c.CreateByUserId,
                            DeleteDate = c.DeleteDate,
                            UserGroup_Id = c.UserGroup_Id,
                            TempTag = t.TagName
                        })
                        .Where(ct => ct.TempTag.Contains(searchText) && ct.DeleteDate == null && ct.UserGroup_Id == currentUserGroupId)
                        .Select(o => new CampaignInfo.CampaignList()
                        {
                            CampaignId = o.CampaignId,
                            CampaignName = o.CampaignName,
                            Duration = o.Duration,
                        })
                        .ToList();
                }
            }
            else
            {
                campaignList = _DsDb.Campaign
                      .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == currentUserGroupId)
                      .Select(o => new CampaignInfo.CampaignList()
                      {
                          CampaignId = o.Id,
                          CampaignName = o.Name,
                          Duration = o.Duration
                      })
                      .ToList();
            }
            allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(campaignList.Count()) / pageRows));
            foreach (var campaign in campaignList)
            {
                var tag = _DsDb.CampaignTag.Where(c => c.CampaignId == campaign.CampaignId)
                    .Select(o => o.TagName)
                    .Distinct()
                    .ToList();

                if (tag != null)
                {
                    campaign.Tag = tag;
                }
            }
            var model = new CampaignInfo();
            model.CampaignsList = campaignList.Skip((page - 1) * pageRows).Take(pageRows).ToList();
            model.Count = allPage;
            return new SuccessResult(Data: model);
        }

        /// <summary>
        /// 取得Campaign Detail
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult GetCampaign(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FindCampaign(_UserInfo, Model);

            if (cp == null)
            {
                return new NotFoundResult();
            }

            var data = new
            {
                Id = Model.CampaignId,
                Name = cp.Name,
                Width = cp.Width,
                Height = cp.Height,
                Duration = cp.Duration,
                Regions = cp.Regions.Select(o => new
                {
                    Id = o.Id,
                    Width = o.Width,
                    Height = o.Height,
                    Left = o.OffsetX,
                    Top = o.OffsetY,
                    Order = o.LayerOrder,
                    Frames = o.Frames.Where(p => p.DeleteByUserId == null).Select(p => new
                    {
                        FileId = p.FileId.HasValue ? p.FileId : p.TextAssetId,
                        //FileName = p.MediaFile?.Name, //todo: 取得文字名稱
                        FileName = _GetRegionFrameFileName(p),
                        FrameType = p.FrameType,
                        Order = p.Order,
                        Duration = p.Length,
                        Id = p.Id,
                        Content = p.Content
                    }).ToList()
                }).ToList()
            };
            return new SuccessResult(data);
        }



        /// <summary>
        /// 依照Model.CampaignId找Campaign 
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public Campaign FindCampaign(UserInfo _UserInfo, CampaignApiModel Model)
        {
            return _DsDb.Campaign.FirstOrDefault(o => o.Id == Model.CampaignId && o.UserGroup_Id == _UserInfo.UserGroupId && o.DeleteByUserId == null);
        }


        /// <summary>
        /// 取得定義的 RegionFrameFileName
        /// </summary>
        /// <param name="Frame"></param>
        /// <returns></returns>
        private string _GetRegionFrameFileName(RegionFrame Frame)
        {
            switch (Frame.FrameType)
            {
                case 1:
                case 2:
                case 3:
                case 9:
                    return Frame.MediaFile.Name;

                case 4:
                case 5:
                case 6:
                //RSS
                case 7:
                //Youtube
                case 8:
                case 12:
                case 16:
                    return Frame.TextAsset?.Name;
                case 10:
                    return "10";
                case 11:
                    return Frame.Content == "0" ? "110" : "111";
                case 13:
                    return "13";
            }
            return "unknown";
        }



        /// <summary>
        /// 取得 Camapign meta 資訊
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns>回傳取得資料或是失敗無資料</returns>
        public ApiResult GetCampaignMetaInformation(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FindCampaign(_UserInfo, Model);

            if (cp == null)
            {
                return new NotFoundResult();
            }

            var data = new
            {
                CreateByUserName = _DsDb.User.FirstOrDefault(o => o.Id == cp.CreateByUserId)?.Name,
                CreateDate = cp.CreateDate.ToShortDateString(),
                UpdateByUserName = _DsDb.User.FirstOrDefault(o => o.Id == cp.UpdateByUserId)?.Name,
                UpdateDate = cp.UpdateDate?.ToShortDateString(),
                DeviceCount = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.ScreenGroupId == null).Select(o => o.DeviceId).Distinct().Count(),
                ScreenGroupCount = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.DeviceId == null).Select(o => o.ScreenGroupId).Distinct().Count(),
            };

            return new SuccessResult(Data: data);
        }


        /// <summary>
        /// Delete Campaign
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public Campaign FlagCampaignStatus(UserInfo _UserInfo, CampaignApiModel Model)
        {
            return _DsDb.Campaign.FirstOrDefault(o => o.Id == Model.CampaignId && o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId);
        }

        /// <summary>
        /// Delete Campaign
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult DeleteCamapign(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FlagCampaignStatus(_UserInfo, Model);
            var tempCampaignId = Model.CampaignId;

            if (cp != null)
            {
                var name = cp.Name;
                cp.DeleteDate = DateTime.Now;
                cp.DeleteByUserId = _UserInfo.Id;
                cp.Name = name + DateTime.Now;

                //將刪除Campaign資訊存入log
                CampaignLogModels CampaignLogModels = new CampaignLogModels();
                CampaignLogModels.CampaignLog(cp, _UserInfo, 3);

                //Delete all region and frames
                if (cp.Regions.Count != 0)
                {
                    foreach (var region in cp.Regions)
                    {
                        if (region.Frames.Count != 0)
                        {
                            _DsDb.RegionFrame.RemoveRange(region.Frames);
                        }
                    }
                    _DsDb.CampaignRegion.RemoveRange(cp.Regions);
                }

                //刪除campaign tag
                var tag = _DsDb.CampaignTag.Where(o => o.CampaignId == tempCampaignId);
                _DsDb.CampaignTag.RemoveRange(tag);

                cp.DeleteDate = DateTime.Now;
                cp.DeleteByUserId = _UserInfo.Id;
                //_DsDb.Campaign.Remove(cp);


                PublishModels PublishModels = new PublishModels();
                var AdjustOrderResult = PublishModels.AdjustbyCampaignsOrder(Model.CampaignId, _UserInfo);

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }
                //return new SuccessResult();
                if (AdjustOrderResult.Result)

                    return new SuccessResult();
                else
                {
                    return AdjustOrderResult;
                }
            }
            else
            {
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Add Campaign
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult Add(UserInfo _UserInfo, CampaignApiModel Model)
        {
            //Thread.Sleep(3000);

            //if (Model.CampaignId != 0)
            //{
            //    return new ExceptionResult(Message: "todo");
            //}

            //Check data
            var err = Model.CheckModel();

            if (!string.IsNullOrEmpty(err))
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            //check name
            if (_DsDb.Campaign.Any(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.Name == Model.Name))
            {

                return new ExceptionResult(Message: "Error_0023");
            }

            var cp = new Campaign()
            {
                Name = Model.Name,
                Height = Model.Height,
                Width = Model.Width,
                Duration = Model.Duration,
                CreateDate = DateTime.Now,
                CreateByUserId = _UserInfo.Id,
                UserGroup_Id = _UserInfo.UserGroupId,
                Regions = new List<CampaignRegion>()
            };

            //Add regions to campaign
            var campaignId = cp.Id;
            if (Model.Regions != null)
            {
                foreach (var region in Model.Regions)
                {
                    var cpRegion = new CampaignRegion()
                    {
                        Campaign = cp,
                        Width = region.Width,
                        Height = region.Height,
                        OffsetX = region.OffsetX,
                        OffsetY = region.OffsetY,
                        LayerOrder = region.LayerOrder,
                        Frames = new List<RegionFrame>()
                    };

                    //Add frames to region
                    if (region.Frames != null)
                    {
                        foreach (var frame in region.Frames)
                        {
                            if (frame.FrameType == 1 || frame.FrameType == 2 || frame.FrameType == 3 || frame.FrameType == 9)
                            {
                                //save image or video or audio or flash
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = frame.FileId,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    Content = null
                                });
                            }
                            else if (frame.FrameType == 4 || frame.FrameType == 5 || frame.FrameType == 6 || frame.FrameType == 7 || frame.FrameType == 8 || frame.FrameType == 16)
                            {
                                //save text assets
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = null,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    Content = null,
                                    //
                                    TextAssetId = frame.FileId
                                });
                            }
                            else if (frame.FrameType == 10 || frame.FrameType == 11 || frame.FrameType == 12 || frame.FrameType == 13)
                            {
                                //save apps assets
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = null,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    TextAssetId = frame.FileId == null ? null : frame.FileId,
                                    Content = frame.ClockStyle.ToString(),
                                });
                            }
                        }
                    }
                    cp.Regions.Add(cpRegion);
                    //_DsDb.CampaignRegion.Add(cpRegion);
                }
            }
            _DsDb.Campaign.Add(cp);
            _DsDb.SaveChanges();


            //將新增Campaign資訊存入log
            CampaignLogModels CampaignLogModels = new CampaignLogModels();
            CampaignLogModels.CampaignLog(cp, _UserInfo, 1);

            return new SuccessResult(Data: new CampaignApiModel()
            {
                CampaignId = cp.Id
            });



        }


        /// <summary>
        /// Edit Campaign
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult Edit(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FlagCampaignStatus(_UserInfo, Model);
            //var cp = _DsDb.Campaign.FirstOrDefault(o => o.Id == Model.CampaignId && o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId);

            if (cp == null)
            {
                return new NotFoundResult();
            }

            //Check data
            var err = Model.CheckModel();

            if (!string.IsNullOrEmpty(err))
            {
                return new ExceptionResult(Message: "Error_0044");
            }

            if (_DsDb.Campaign.Any(o => o.UserGroup_Id == _UserInfo.UserGroupId && o.Name == Model.Name && o.Id != Model.CampaignId))
            {
                return new ExceptionResult(Message: "Error_0023");

            }
            //update camapign
            cp.Name = Model.Name;
            cp.Height = Model.Height;
            cp.Width = Model.Width;
            cp.Duration = Model.Duration;
            cp.UpdateDate = DateTime.Now;
            cp.UpdateByUserId = _UserInfo.Id;
            //cp.CreateDate = DateTime.Now;
            //cp.CreateByUserId = _UserInfo.Id;
            //cp.UserGroup_Id = _UserInfo.UserGroupId;
            //cp.Regions = new List<CampaignRegion>();

            //先把campaign原有的region跟frame通通刪光
            foreach (var region in cp.Regions)
            {
                _DsDb.RegionFrame.RemoveRange(region.Frames);
            }

            _DsDb.CampaignRegion.RemoveRange(cp.Regions);

            //
            cp.Regions = new List<CampaignRegion>();

            //Add regions to campaign
            var campaignId = cp.Id;
            if (Model.Regions != null)
            {
                foreach (var region in Model.Regions)
                {
                    var cpRegion = new CampaignRegion()
                    {
                        Campaign = cp,
                        Width = region.Width,
                        Height = region.Height,
                        OffsetX = region.OffsetX,
                        OffsetY = region.OffsetY,
                        LayerOrder = region.LayerOrder,
                        Frames = new List<RegionFrame>()
                    };

                    //Add frames to region
                    if (region.Frames != null)
                    {
                        foreach (var frame in region.Frames)
                        {
                            if (frame.FrameType == 1 || frame.FrameType == 2 || frame.FrameType == 3 || frame.FrameType == 9)
                            {
                                //save image or video
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = frame.FileId,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    Content = null
                                });
                            }
                            else if (frame.FrameType == 4 || frame.FrameType == 5 || frame.FrameType == 6 || frame.FrameType == 7 || frame.FrameType == 8 || frame.FrameType == 16)
                            {
                                //save text assets
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = null,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    Content = null,
                                    //
                                    TextAssetId = frame.FileId
                                });
                            }
                            else if (frame.FrameType == 10 || frame.FrameType == 11 || frame.FrameType == 12 || frame.FrameType == 13)
                            {
                                //save apps assets
                                cpRegion.Frames.Add(new RegionFrame()
                                {
                                    FrameType = frame.FrameType,
                                    CreateByUserId = _UserInfo.Id,
                                    CreateDate = DateTime.Now,
                                    Order = frame.Order,
                                    Length = frame.Length,
                                    FileId = null,
                                    //
                                    ContentUrl = null,
                                    ThumbnameUrl = null,
                                    ContentMeta = null,
                                    DataId = Guid.NewGuid(),
                                    Offset = 0,
                                    TextAssetId = frame.FileId == null ? null : frame.FileId,
                                    Content = frame.ClockStyle.ToString(),
                                });
                            }

                        }
                    }

                    cp.Regions.Add(cpRegion);
                    //_DsDb.CampaignRegion.Add(cpRegion);
                }
            }
            List<ScheduleSummary> DeviceIdList = CampaignUsedDeviceIdList(_UserInfo, Model);
            PublishModels PublishModels = new PublishModels();
            PublishModels.ChangeContentStatus(DeviceIdList, null);
            _DsDb.SaveChanges();

            //將編輯Campaign資訊存入log
            CampaignLogModels CampaignLogModels = new CampaignLogModels();
            CampaignLogModels.CampaignLog(cp, _UserInfo, 2);

            return new SuccessResult(Data: new CampaignApiModel()
            {
                CampaignId = cp.Id
            });
        }

        /// <summary>
        /// 被Device使用的Campaign清單 提供前端使用的回傳
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult CampaignUsedList(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FlagCampaignStatus(_UserInfo, Model);
            List<string> deviceName = new List<string>();
            List<string> screenGroupName = new List<string>();
            if (cp == null)
            {
                return new NotFoundResult();
            }
            else
            {
                var devices = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.Device.DeleteDate == null && o.DeviceId != null).Select(o => o.Device.Name).Distinct().ToList();
                var screenGroups = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.Device.DeleteDate == null && o.DeviceId == null).Select(o => o.ScreenGroup.Name).Distinct().ToList();

                if (devices.Count != 0)
                {
                    foreach (var dName in devices)
                    {
                        deviceName.Add(dName);
                    }
                }
                if (screenGroups.Count != 0)
                {
                    foreach (var sName in screenGroups)
                    {
                        screenGroupName.Add(sName);
                    }
                }
                var data = new
                {
                    DeviceName = deviceName,
                    ScreenGroupName = screenGroupName
                };

                if (deviceName.Count == 0 && screenGroupName.Count() == 0)
                {
                    return new SuccessResult(Data: null);
                }
                else
                {
                    return new SuccessResult(Data: data);
                }
            }

        }

        /// <summary>
        /// 使用此Campaign的Device Id清單，傳入CampaignId 提供 dispatch 狀態寫入
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public List<ScheduleSummary> CampaignUsedDeviceIdList(UserInfo _UserInfo, CampaignApiModel Model)
        {
            var cp = FlagCampaignStatus(_UserInfo, Model);
            List<ScheduleSummary> deviceId = new List<ScheduleSummary>();

            var dList = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.DeviceId != null).Select(o => o.Device.Id).Distinct().ToList();
            var sgList = _DsDb.ScheduleSummary.Where(o => o.CampaignId == cp.Id && o.DeviceId == null).Select(o => o.ScreenGroupId).Distinct().ToList();
            var dinSgList = _DsDb.Device.Where(o => sgList.Contains(o.ScreenGroupId)).Select(o => o.Id).Distinct().ToList();

            if (dList.Count != 0 || sgList.Count != 0)
            {
                foreach (var dId in dList)
                {
                    deviceId.Add(new ScheduleSummary() { DeviceId = dId });
                }
                foreach (var d in dinSgList)
                {

                    deviceId.Add(new ScheduleSummary() { DeviceId = d });

                }
            }

            return deviceId;

        }
        /// <summary>
        /// Insert campaign tag to campaignTag
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="campaignList"></param>
        /// <param name="tagList"></param>
        /// <returns></returns>
        public ApiResult InsertTag(UserInfo _UserInfo, List<int> campaignList, List<string> tagList)
        {
            List<CampaignTag> InsertList = new List<CampaignTag>();

            if (campaignList != null)
            {
                //Device
                foreach (var d in campaignList)
                {
                    foreach (var t in tagList)
                    {
                        //判斷是否已經存在這個tag 了
                        if (_DsDb.CampaignTag.Where(o => o.TagName == t && o.CampaignId == d).Count() == 0)
                        {
                            InsertList.Add(new CampaignTag
                            {
                                CampaignId = d,
                                TagName = t.Trim(),
                                CreateDate = DateTime.Now
                            });
                        }
                    }
                }
            }

            try
            {
                _DsDb.CampaignTag.AddRange(InsertList);
                _DsDb.SaveChanges();
                return new SuccessResult();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }


        /// <summary>
        /// Delete campaign tag from campaignTag
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="campaignList"></param>
        /// <param name="tagList"></param>
        /// <returns></returns>
        public ApiResult DeleteTag(UserInfo _UserInfo, List<int> campaignList, List<string> tagList)
        {
            List<CampaignTag> deleteList = new List<CampaignTag>();
            if (campaignList != null)
            {
                foreach (var d in campaignList)
                {
                    var deleteObj = _DsDb.CampaignTag.Where(o => o.CampaignId == d && tagList.Contains(o.TagName)).ToList();
                    deleteList.AddRange(deleteObj);
                }
            }

            try
            {
                _DsDb.CampaignTag.RemoveRange(deleteList);
                _DsDb.SaveChanges();
                return new SuccessResult();
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }







        /// <summary>
        /// save as campaign///
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>


        public ApiResult AddOther(UserInfo _UserInfo, CampaignApiModel Model)
        {


            ///////查詢campaign並將資料取出
            var cp = FindCampaign(_UserInfo, Model);

            if (cp == null)
            {
                return new NotFoundResult();
            }

            var data = new CampaignApiModel
            {
                CampaignId = 0,
                Name = Model.Name,
                Width = cp.Width,
                Height = cp.Height,
                Duration = cp.Duration,
                Regions = cp.Regions.Select(o => new RegionApiModel()
                {
                    RegionId = o.Id,
                    Width = o.Width,
                    Height = o.Height,
                    OffsetX = o.OffsetX,
                    OffsetY = o.OffsetY,
                    LayerOrder = o.LayerOrder,
                    Frames = o.Frames.Where(p => p.DeleteByUserId == null).Select(p => new FrameApiModel()
                    {
                        FileId = p.FileId.HasValue ? p.FileId : p.TextAssetId,
                        //FileName = p.MediaFile?.Name, //todo: 取得文字名稱
                        //FileName = _GetRegionFrameFileName(p),
                        FrameType = p.FrameType,
                        Order = p.Order,
                        Length = p.Length,
                        FrameId = p.Id
                    }).ToList()
                }).ToList()
            };

            var reslut = Add(_UserInfo, data);

            return reslut;

        }
        /// <summary>
        /// replace campaign
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult ReplaceCampaign(UserInfo _UserInfo, CampaignSaveAsApiModel Model)
        {
            var data = new CampaignApiModel
            {
                CampaignId = Model.ReplaceCampaignId,
            };
            var cp = FindCampaign(_UserInfo, data);
            data = new CampaignApiModel
            {
                CampaignId = Model.SaveAsCampaignId,
                Name = Model.SaveAsCampaignName,
                Width = cp.Width,
                Height = cp.Height,
                Duration = cp.Duration,
                Regions = cp.Regions.Select(o => new RegionApiModel()
                {
                    RegionId = o.Id,
                    Width = o.Width,
                    Height = o.Height,
                    OffsetX = o.OffsetX,
                    OffsetY = o.OffsetY,
                    LayerOrder = o.LayerOrder,
                    Frames = o.Frames.Where(p => p.DeleteByUserId == null).Select(p => new FrameApiModel()
                    {
                        FileId = p.FileId.HasValue ? p.FileId : p.TextAssetId,
                        //FileName = p.MediaFile?.Name, //todo: 取得文字名稱
                        //FileName = _GetRegionFrameFileName(p),
                        FrameType = p.FrameType,
                        Order = p.Order,
                        Length = p.Length,
                        FrameId = p.Id
                    }).ToList()
                }).ToList()
            };
            var result = Edit(_UserInfo, data);
            return result;

        }


        public ApiResult GetAllCampaignInfo(UserInfo _UserInfo, string searchText)
        {
            int currentUserGroupId = _UserInfo.UserGroupId;
            List<CampaignListInfo.CampaignList> campaignList;

            if (searchText != null)
            {
                //campaignName
                campaignList = _DsDb.Campaign
                    .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == currentUserGroupId && o.Name.Contains(searchText))
                    .Select(o => new CampaignListInfo.CampaignList()
                    {
                        CampaignId = o.Id,
                        CampaignName = o.Name,
                        Duration = o.Duration,
                        UpdateDate = o.UpdateDate == null ? o.CreateDate : o.UpdateDate,
                        UpdatebyUserName = _DsDb.User.FirstOrDefault(v => v.Id == o.UpdateByUserId).Name == null ?
                        _DsDb.User.FirstOrDefault(u => u.Id == o.CreateByUserId).Name : _DsDb.User.FirstOrDefault(v => v.Id == o.UpdateByUserId).Name

                    })
                    .ToList();



            }
            else
            {
                campaignList = _DsDb.Campaign
                      .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == currentUserGroupId)
                      .Select(o => new CampaignListInfo.CampaignList()
                      {
                          CampaignId = o.Id,
                          CampaignName = o.Name,
                          Duration = o.Duration,
                          UpdateDate = o.UpdateDate == null ? o.CreateDate : o.UpdateDate,
                          UpdatebyUserName = _DsDb.User.FirstOrDefault(v => v.Id == o.UpdateByUserId).Name == null ?
                          _DsDb.User.FirstOrDefault(u => u.Id == o.CreateByUserId).Name : _DsDb.User.FirstOrDefault(v => v.Id == o.UpdateByUserId).Name

                      })
                      .ToList();
            }


            var model = new CampaignListInfo();
            model.CampaignsList = campaignList.ToList();
            return new SuccessResult(Data: model);


        }
    }






}