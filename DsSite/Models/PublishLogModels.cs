﻿using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class PublishLogModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 寫入新增PublishLog資訊
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="action"></param>
        public void PublishLogAdd(PublishApiModel Model, UserInfo _UserInfo, int action)
        {
            //取得 Campaigns           
            var Campaigns = new List<PublishCampaignApiModel>();
            foreach (var c in Model.Campaigns)
            {
                var campaign = _DsDb.Campaign
                   .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == c.Id)
                   .Select(o => new PublishCampaignApiModel()
                   {
                       Id = o.Id,
                       Name = o.Name,
                       Duration = o.Duration,
                   }).ToList();

                Campaigns.AddRange(campaign);

            }
            //取得 Screens 
            var Screens = new List<Object>();

            if (Model.Screens != null)
            {
                foreach (var s in Model.Screens)
                {
                    var device = _DsDb.Device
                       .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == s.Id)
                       .Select(o => new
                       {
                           Id = o.Id,
                           DeviceName = o.Name,

                       }).ToList();

                    Screens.AddRange(device);
                }
            }
            if (Model.ScreenGroups != null && Model.ScreenGroups.Count() > 0)
            {

                foreach (var sg in Model.ScreenGroups)
                {
                    var deviceList = _DsDb.Device.Where(o => o.ScreenGroupId == sg.Id)
                        .ToList();
                    var device = deviceList.Select(o => new
                    {
                        Id = o.Id,
                        DeviceName = o.Name,
                        ScreenGroupId = o.ScreenGroup.Id,
                        ScreenGroupName = o.ScreenGroup.Name

                    }).ToList();
                    Screens.AddRange(device);
                }
            }

            //取得 SelectedType
            var SelectedType = new List<SelectedTypeApiModel>();
            SelectedType.Add(Model.SelectedType);

            //取得 FaceRestriction
            var FaceRestriction = new List<Object>();
            if (Model.FaceRestriction != null)
            {

                var gender = new
                {
                    Male = Model.FaceRestriction.Male,
                    Female = Model.FaceRestriction.Female,
                };
                FaceRestriction.Add(gender);
                var ageList = Model.FaceRestriction.AgeIdList;
                foreach (var aId in ageList)
                {
                    var range = _DsDb.AgeRangeDefinition
                        .Where(o => o.Id == aId && o.UserGroup_Id == _UserInfo.UserGroupId)
                        .Select(o => new
                        {
                            Description = o.Description,
                            MinValue = o.MinValue,
                            MaxValue = o.MaxValue

                        });

                    FaceRestriction.AddRange(range);

                }

            }
            else
            {
                FaceRestriction = null;
            }

            //取得 TimeRestriction
            var TimeRestriction = new List<TimeRestrictionApiModel>();
            if (Model.TimeRestriction.DailyStartTime == null)
            {
                Model.TimeRestriction.DailyStartTime = new TimeSpan(0, 0, 0);
            }
            if (Model.TimeRestriction.DailyEndTime == null)
            {
                Model.TimeRestriction.DailyEndTime = new TimeSpan(23, 59, 59);
            }
            TimeRestriction.Add(Model.TimeRestriction);


            //將檔案資訊存入log
            var Publishlog = new PublishLog()
            {
                ScheduleSummaryId = Model.ScheduleSummaryId,
                StartDate = Model.StartDate,
                EndDate = Model.EndDate,
                SelectType = JsonConvert.SerializeObject(SelectedType),
                Campaigns = JsonConvert.SerializeObject(Campaigns),
                Screens = JsonConvert.SerializeObject(Screens),
                FaceRestriction = JsonConvert.SerializeObject(FaceRestriction),
                TimeRestriction = JsonConvert.SerializeObject(TimeRestriction),
                UserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                TimeStamp = DateTime.Now,
                Action = action
            };
            _DsDb.PublishLog.Add(Publishlog);
            _DsDb.SaveChanges();
        }

        /// <summary>
        /// 寫入編輯修改PublishLog資訊
        /// </summary>
        /// <param name="scheduleSummaryId"></param>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="action"></param>
        public void PublishLogEdit(long scheduleSummaryId, PublishApiModel Model, UserInfo _UserInfo, int action)
        {
            var deviceId = 0;
            if (Model.Screens != null)
                deviceId = Model.Screens[0].Id;

            var Publication = _DsDb.ScheduleSummary.FirstOrDefault(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId);


            //取得 Campaigns
            var Campaigns = _DsDb.Campaign
                .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == Publication.CampaignId)
                .Select(o => new PublishCampaignApiModel()
                {
                    Id = o.Id,
                    Name = o.Name,
                    Duration = o.Duration,

                }).ToList();

            //取得 Screens
            var Screens = new List<Object>();

            if (Model.Screens != null)
            {
                var device = _DsDb.Device
                       .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == deviceId)
                       .Select(o => new
                       {
                           Id = o.Id,
                           DeviceName = o.Name,

                       }).ToList();

                Screens.AddRange(device);

            }
            if (Model.ScreenGroups != null)
            {

                foreach (var sg in Model.ScreenGroups)
                {
                    var deviceList = _DsDb.Device.Where(o => o.ScreenGroupId == sg.Id)
                        .ToList();
                    var device = deviceList.Select(o => new
                    {
                        Id = o.Id,
                        DeviceName = o.Name,
                        ScreenGroupId = o.ScreenGroup.Id,
                        ScreenGroupName = o.ScreenGroup.Name

                    }).ToList();
                    Screens.AddRange(device);
                }
            }

            bool general = false;
            if (Publication.HasFaceDetectionRule == false && Publication.Spot == false)
            {
                general = true;
            }

            //取得 SelectedType
            var SelectedType = _DsDb.ScheduleSummary
                .Where(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId)
                .Select(o => new SelectedTypeApiModel
                {
                    General = general,
                    Face = o.HasFaceDetectionRule,
                    Spot = o.Spot,

                }).ToList();

            //取得 FaceRestriction
            bool male, female;
            switch (Publication.Gender)
            {
                case 0:
                    male = true;
                    female = true;
                    break;
                case 1:
                    male = true;
                    female = false;
                    break;
                case 2:
                    male = false;
                    female = true;
                    break;
                default:
                    male = false;
                    female = false;
                    break;
            }

            var FaceRestriction = new List<Object>();
            if (Model.FaceRestriction != null)
            {

                var gender = new
                {
                    Male = male,
                    Female = female,
                };
                FaceRestriction.Add(gender);
                var ageList = Model.FaceRestriction.AgeIdList;
                foreach (var aId in ageList)
                {
                    var range = _DsDb.AgeRangeDefinition
                        .Where(o => o.Id == aId && o.UserGroup_Id == _UserInfo.UserGroupId)
                        .Select(o => new
                        {
                            Description = o.Description,
                            MinValue = o.MinValue,
                            MaxValue = o.MaxValue

                        });

                    FaceRestriction.AddRange(range);

                }

            }
            else
            {
                FaceRestriction = null;
            }

            //取得 TimeRestriction
            var TimeRestriction = _DsDb.ScheduleSummary
            .Where(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId)
            .Select(o => new TimeRestrictionApiModel
            {
                RepeatMon = o.RepeatMonday,
                RepeatTue = o.RepeatTuesday,
                RepeatWed = o.RepeatWednesday,
                RepeatThu = o.RepeatThurday,
                RepeatFri = o.RepeatFriday,
                RepeatSat = o.RepeatSaturday,
                RepeatSun = o.RepeatSunday,
                DailyStartTime = o.DailyStartTime,
                DailyEndTime = o.DailyEndTime
            }).ToList();

            //將檔案資訊存入log
            var Publishlog = new PublishLog()
            {
                ScheduleSummaryId = scheduleSummaryId,
                StartDate = Publication.StartDate,
                EndDate = Publication.EndDate,
                SelectType = JsonConvert.SerializeObject(SelectedType),
                Campaigns = JsonConvert.SerializeObject(Campaigns),
                Screens = JsonConvert.SerializeObject(Screens),
                FaceRestriction = JsonConvert.SerializeObject(FaceRestriction),
                TimeRestriction = JsonConvert.SerializeObject(TimeRestriction),
                UserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                TimeStamp = DateTime.Now,
                Action = action
            };
            _DsDb.PublishLog.Add(Publishlog);
            _DsDb.SaveChanges();


        }

        /// <summary>
        /// 寫入刪除PublishLog資訊
        /// </summary>
        /// <param name="scheduleSummaryId"></param>
        /// <param name="deviceId"></param>
        /// <param name="screenGroupId"></param>
        /// <param name="_UserInfo"></param>
        /// <param name="action"></param>
        public void PublishLogDelete(long scheduleSummaryId, int? deviceId, long? screenGroupId, UserInfo _UserInfo, int action)
        {

            var Publication = _DsDb.ScheduleSummary.FirstOrDefault(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId && o.DeviceId == deviceId);

            //取得 Campaigns
            var Campaigns = _DsDb.Campaign
                .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == Publication.CampaignId)
                .Select(o => new PublishCampaignApiModel()
                {
                    Id = o.Id,
                    Name = o.Name,
                    Duration = o.Duration,

                }).ToList();

            //取得 Screens
            var Screens = new List<Object>();
            if (deviceId != null)
            {
                var device = _DsDb.Device
                       .Where(o => o.DeleteByUserId == null && o.UserGroup_Id == _UserInfo.UserGroupId && o.Id == deviceId)
                       .Select(o => new
                       {
                           Id = o.Id,
                           DeviceName = o.Name,

                       }).ToList();

                Screens.AddRange(device);

            }
            if (screenGroupId != null)
            {
                var ScreenGroups = _DsDb.Device.Where(o => o.ScreenGroupId == screenGroupId).ToArray();

                foreach (var sg in ScreenGroups)
                {
                    var device = ScreenGroups.Select(o => new
                    {
                        Id = o.Id,
                        DeviceName = o.Name,
                        ScreenGroupId = o.ScreenGroup.Id,
                        ScreenGroupName = o.ScreenGroup.Name

                    }).ToList();
                    Screens.AddRange(device);
                }
            }


            bool general = false;
            if (Publication.HasFaceDetectionRule == false && Publication.Spot == false)
            {
                general = true;
            }

            //取得 SelectedType
            var SelectedType = _DsDb.ScheduleSummary
                .Where(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId)
                .Select(o => new SelectedTypeApiModel
                {
                    General = general,
                    Face = o.HasFaceDetectionRule,
                    Spot = o.Spot,

                }).ToList();

            //取得 FaceRestriction
            bool male, female;
            switch (Publication.Gender)
            {
                case 0:
                    male = true;
                    female = true;
                    break;
                case 1:
                    male = true;
                    female = false;
                    break;
                case 2:
                    male = false;
                    female = true;
                    break;
                default:
                    male = false;
                    female = false;
                    break;
            }

            var FaceRestriction = new List<Object>();
            var summary = _DsDb.ScheduleSummary
             .Where(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId)
            .FirstOrDefault();

            if (summary.AgeRestriction != null)
            {
                var gender = new
                {
                    Male = male,
                    Female = female,
                };
                FaceRestriction.Add(gender);
                var ageList = summary.AgeRestriction.Split(',');

                foreach (var a in ageList)
                {
                    var aId = Convert.ToInt32(a);
                    var range = _DsDb.AgeRangeDefinition
                        .Where(o => o.Id == aId && o.UserGroup_Id == _UserInfo.UserGroupId)
                        .Select(o => new
                        {
                            Description = o.Description,
                            MinValue = o.MinValue,
                            MaxValue = o.MaxValue

                        });

                    FaceRestriction.AddRange(range);

                }

            }
            else
            {
                FaceRestriction = null;
            }

            //取得 TimeRestriction
            var TimeRestriction = _DsDb.ScheduleSummary
            .Where(o => o.Id == scheduleSummaryId && o.UserGroupId == _UserInfo.UserGroupId)
            .Select(o => new TimeRestrictionApiModel
            {
                RepeatMon = o.RepeatMonday,
                RepeatTue = o.RepeatTuesday,
                RepeatWed = o.RepeatWednesday,
                RepeatThu = o.RepeatThurday,
                RepeatFri = o.RepeatFriday,
                RepeatSat = o.RepeatSaturday,
                RepeatSun = o.RepeatSunday,
                DailyStartTime = o.DailyStartTime,
                DailyEndTime = o.DailyEndTime
            }).ToList();

            //將檔案資訊存入log
            var Publishlog = new PublishLog()
            {
                ScheduleSummaryId = scheduleSummaryId,
                StartDate = Publication.StartDate,
                EndDate = Publication.EndDate,
                SelectType = JsonConvert.SerializeObject(SelectedType),
                Campaigns = JsonConvert.SerializeObject(Campaigns),
                Screens = JsonConvert.SerializeObject(Screens),
                FaceRestriction = JsonConvert.SerializeObject(FaceRestriction),
                TimeRestriction = JsonConvert.SerializeObject(TimeRestriction),
                UserId = _UserInfo.Id,
                UserGroupId = _UserInfo.UserGroupId,
                TimeStamp = DateTime.Now,
                Action = action
            };
            _DsDb.PublishLog.Add(Publishlog);
            _DsDb.SaveChanges();


        }
    }
}