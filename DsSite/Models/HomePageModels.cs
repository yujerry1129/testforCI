﻿using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using DsKit.Helpers;

namespace DsSite.Models
{
    public class HomePageModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 取得所有home所需的info
        /// </summary>
        /// <param name="_UserInfo">使用者資訊</param>
        /// <returns>Json</returns>
        public ApiResult GetHomeInfo(UserInfo _UserInfo)
        {
            var userAdminRole = _DsDb.SiteUserToRole.Where(o => o.User_Id == _UserInfo.Id && (o.Role_Id == 1 || o.Role_Id == 3)).Count();
            var userGroup = _DsDb.UserGroup.FirstOrDefault(o => o.Id == _UserInfo.UserGroupId);
            var uploadFiles = _DsDb.FileUpload.Where(o => o.DeleteDate == null && o.UserGroup.Id == userGroup.Id).ToList();
            ApiResult model = new ApiResult();
            ConvertHelper convertHelper = new ConvertHelper();

            if (userGroup != null)
            {
                HomeViewModel HomeViewModel = new HomeViewModel()
                {
                    CustomerName = _UserInfo.Name,
                    DeviceCount = _DsDb.Device.Count(o => o.UserGroup.Id == userGroup.Id && o.DeleteDate == null && o.CreateByUserId != null),
                    DeviceLimit = userGroup.DeviceLimit,
                    UploadFileCount = uploadFiles.Count(),
                    FileStorageUsage = (uploadFiles.Select(o => o.Size)).ToList().Sum(),//所有上傳檔案總容量
                    SystemKey = userAdminRole > 0 ? userGroup.SystemKey : null,
                    ServerUrl = userAdminRole > 0 ? WebConfigurationManager.AppSettings["SiteRoot"] : null,
                    StorageLimit = userGroup.StorageLimit,
                    FaceDetectionEnabled = userGroup.EnableFaceDetection,
                    Version = WebConfigurationManager.AppSettings["SiteVersion"],
                    PlayerVersion = convertHelper.CompareVersion(_DsDb.VersionFile.Select(o => o.Version).ToList())
                };
                if (HomeViewModel != null)
                {
                    model.Data = HomeViewModel;
                    model.Result = true;
                    return model;
                }
                else
                {
                    model.Result = false;
                    return model;
                }
            }
            else
            {
                model.Result = false;
                return model;
            }
        }

    }
}