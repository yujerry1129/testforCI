﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class DsAuthFilterModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 檢查user是否有權限存取controller確認
        /// </summary>
        /// <param name="UserId">UserId</param>
        /// <param name="controllerName">controllerName</param>
        /// <returns>true/false</returns>
        public bool CheckAuth(int UserId, string controllerName)
        {
            //檢查user是否有權限存取controller
            var check = (
                    //
                    from user in _DsDb.SiteUserToRole
                    where user.User_Id == UserId
                    join role in _DsDb.SiteRoleToMenu on user.Role_Id equals role.SiteRole_Id
                    select role.Menu.ControllerName
                //
                ).ToList().Any(o => o.Equals(controllerName, StringComparison.OrdinalIgnoreCase));
            return check;
        }
    }
}