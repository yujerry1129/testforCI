﻿using DsDbLib.Models;
using DsKit.Helpers;
using DsSite.Controllers.Api.Models;
using DsSite.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using static DsSite.Controllers.Api.AccountController;

namespace DsSite.Models
{

    public class AccountModels : Controllers.Base.DsBaseController
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// 尋找使用者
        /// </summary>
        /// <param name="Account">email 帳戶</param>
        /// <param name="Password">密碼</param>
        /// <returns>使用者資料 </returns>
        public User FindUser(string Account)
        {


            var user = _DsDb.User.FirstOrDefault(o => o.Email == Account && o.DeleteDate == null && o.UserGroup.EffectiveEndDate >= DateTime.Now);
            return user;
        }

        /// <summary>
        /// 尋找隸屬使用者群組
        /// </summary>
        /// <param name="UserGroupId">使用者群組Id</param>
        /// <returns>回傳使用者群組資訊</returns>
        public UserGroup FindUserGroupId(int UserGroupId)
        {
            var user = _DsDb.UserGroup.FirstOrDefault(o => o.Id == UserGroupId && o.DeleteDate == null);

            return user;
        }

        /// <summary>
        /// 更新使用者登入時間
        /// </summary>
        /// <param name="user"></param>
        public void updateLoginInfo(User user)
        {
            user.LastLoginTime = DateTime.Now;
            user.TempPassword = null;
            user.TempPasswordExpiredTime = null;

            _DsDb.SaveChanges();
        }

        /// <summary>
        /// reset密碼
        /// </summary>
        /// <param name="user">使用者</param>
        /// <param name="newPassword">暫時密碼</param>
        public void reSetPassword(User user, String newPassword)
        {
            user.HashedPassword = newPassword;
            user.TempPassword = null;
            user.TempPasswordExpiredTime = null;

            _DsDb.SaveChanges();
        }

        public void setTempPassword(User user, string tmpPwd, int ExpiredTime)
        {
            user.TempPassword = tmpPwd;
            user.TempPasswordExpiredTime = DateTime.Now.AddMinutes(ExpiredTime);

            _DsDb.SaveChanges();
        }

        /// <summary>
        /// 更新密碼
        /// </summary>
        /// <param name="user">使用者</param>
        /// <param name="newPwd">新密碼</param>
        public void updatePassword(User user, string newPwd)
        {
            user.HashedPassword = newPwd;
            user.TempPassword = null;
            user.TempPasswordExpiredTime = null;

            _DsDb.SaveChanges();
        }

        /// <summary>
        /// 新增/編輯帳戶
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="siteData"></param>
        public ApiResult AddEditAccount(UserInfo _UserInfo, SetRole siteData)
        {
            var result = new ApiResult();
            var adminId = _DsDb.User.Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).Select(o => o.CreateByAdminUserId).First();
            //指派角色清單
            var siteRoleList = new List<SiteUserToRole>();

            if (adminId != 0)
            {
                //user_id = 0 為新增
                if (siteData.Id == 0)
                {
                    var userData = new User
                    {
                        Name = siteData.Name,
                        Email = siteData.Email,
                        HashedPassword = siteData.HashedPassword,
                        CreateDate = DateTime.Now,
                        UserGroupId = _UserInfo.UserGroupId,
                        CreateByAdminUserId = adminId
                    };

                    var checkUser = _DsDb.User.Where(o => o.Email == siteData.Email && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).ToList();
                    if (checkUser.Count > 0)
                    {
                        result.Result = false;
                        result.Message = "Error_0060";
                        return result;
                    }

                    try
                    {
                        _DsDb.User.Add(userData);
                        _DsDb.SaveChanges();

                        if (siteData.Roles.Count != 0)
                        {
                            foreach (var role in siteData.Roles)
                            {
                                var roleId = _DsDb.SiteRole.Where(o => o.Name == role).Select(o => o.Id).SingleOrDefault();
                                var userId = _DsDb.User.Where(o => o.Email == userData.Email).SingleOrDefault();
                                siteRoleList.Add(new SiteUserToRole { User_Id = userId.Id, Role_Id = roleId });
                            }
                        }

                        _DsDb.SiteUserToRole.AddRange(siteRoleList);
                        _DsDb.SaveChanges();
                        return new SuccessResult();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Error_0044");
                    }
                }
                else
                {
                    // user_id不為0 則為編輯
                    var editUser = _DsDb.User.Where(o => o.Id == siteData.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();

                    if (editUser != null)
                    {
                        editUser.Name = siteData.Name;
                        editUser.Email = siteData.Email;
                        editUser.UpdateDate = DateTime.Now;
                        editUser.UpdateByAdminUserId = adminId;

                        try
                        {
                            if (siteData.Roles.Count != 0)
                            {
                                int roleId = 0;
                                foreach (var role in siteData.Roles)
                                {
                                    roleId = _DsDb.SiteRole.Where(o => o.Name == role).Select(o => o.Id).SingleOrDefault();
                                    siteRoleList.Add(new SiteUserToRole { User_Id = siteData.Id, Role_Id = roleId });
                                }
                                var userRole = _DsDb.SiteUserToRole.Where(o => o.User_Id == siteData.Id).ToList();
                                _DsDb.SiteUserToRole.RemoveRange(userRole);
                            }
                            _DsDb.SiteUserToRole.AddRange(siteRoleList);
                            _DsDb.SaveChanges();
                            return new SuccessResult();
                        }
                        catch (Exception ex)
                        {
                            return new ExceptionResult(Message: "Error_0044");
                        }
                    }
                }

            }

            return new NotFoundResult();

        }

        /// <summary>
        /// 取得所有帳戶資料
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="PageRows"></param>
        /// <param name="DeviceName"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllAccounts(int Page, int PageRows, string DeviceName, UserInfo _UserInfo)
        {
            List<_User> userInfoList = new List<_User>();
            int allPage = 0;
            var users = _DsDb.User.ToList();
            if (DeviceName != null)
            {
                allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(_DsDb.User
                    .Where(o => o.Email != _UserInfo.Email && o.Name.Contains(DeviceName) && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null)
                    .Count()) / PageRows));
                users = _DsDb.User.Where(o => o.Email != _UserInfo.Email && o.Name.Contains(DeviceName) && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null)
                    .OrderBy(o => o.Id).Skip((Page - 1) * PageRows).Take(PageRows).ToList();
            }
            else
            {
                allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(_DsDb.User
                     .Where(o => o.Email != _UserInfo.Email && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null)
                     .Count()) / PageRows));
                users = _DsDb.User.Where(o => o.Email != _UserInfo.Email && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null)
                    .OrderBy(o => o.Id).Skip((Page - 1) * PageRows).Take(PageRows).ToList();
            }
            //搜尋除了本身使用者之外所有使用者帳號

            if (users.Count > 0)
            {
                foreach (var u in users)
                {
                    var siteRoles = _DsDb.SiteUserToRole.Where(o => o.User_Id == u.Id).Select(o => o.SiteRole.Name).Distinct().ToList();
                    userInfoList.Add(new _User
                    {
                        Id = u.Id,
                        Name = u.Name,
                        Email = u.Email,
                        LastLoginTime = u.LastLoginTime == null ? null : u.LastLoginTime.Value.ToString("yyyy/MM/dd HH:mm"),
                        CreateDate = u.CreateDate.ToString("yyyy/MM/dd HH:mm"),
                        SiteRole = siteRoles.Count == 0 ? null : siteRoles,
                        HashedPassword = u.HashedPassword
                    });
                }

                var result = new ApiResult();

                //大於0表示有搜尋到資料
                if (users.Count > 0)
                {
                    result.Result = true;
                    result.Data = new { UserInfoList = userInfoList, PageCount = allPage };

                    return result;
                }
                else
                {
                    //回傳No Data
                    return new NotFoundResult();
                }
            }

            return new NotFoundResult();

        }

        /// <summary>
        /// 刪除帳戶
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ApiResult Delete(UserInfo _UserInfo, SetRole data)
        {
            var adminId = _DsDb.User.Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).Select(o => o.CreateByAdminUserId).First();
            if (data.Users != null)
            {
                foreach (var user in data.Users)
                {
                    var siteRole = _DsDb.SiteUserToRole.Where(o => o.User_Id == user.Id).ToList();
                    if (siteRole.Count > 0)
                    {
                        _DsDb.SiteUserToRole.RemoveRange(siteRole);
                    }

                    var siteUser = _DsDb.User.Where(o => o.Id == user.Id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();

                    siteUser.DeleteDate = DateTime.Now;
                    siteUser.DeleteByAdminUserId = adminId;
                }

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();

            }

            return new NotFoundResult();

        }

        /// <summary>
        /// 刪除帳戶角色
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="roleData"></param>
        /// <returns></returns>
        public ApiResult DeleteRole(UserInfo _UserInfo, SetRole roleData)
        {
            if (roleData.Id != 0 || roleData.Role != null)
            {
                var role = _DsDb.SiteUserToRole.Where(o => o.SiteRole.Name == roleData.Role && o.User_Id == roleData.Id).SingleOrDefault();

                try
                {
                    _DsDb.SiteUserToRole.Remove(role);

                    _DsDb.SaveChanges();

                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 轉移帳戶為Admin
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApiResult Transfer(UserInfo _UserInfo, int id)
        {
            var user = _DsDb.User.Where(o => o.Id == id && o.UserGroupId == _UserInfo.UserGroupId && o.DeleteDate == null).SingleOrDefault();

            if (user != null)
            {
                var setRoleToAdmin = _DsDb.SiteUserToRole.Where(o => o.User_Id == user.Id).ToList();

                //找到舊的Admin並刪除
                var originAdmin = _DsDb.SiteUserToRole.Where(o => o.Role_Id == 1 && o.User_Id == _UserInfo.Id).SingleOrDefault();

                var newRole = new SiteUserToRole
                {
                    User_Id = id,
                    Role_Id = 1 //1為admin
                };

                try
                {
                    _DsDb.SiteUserToRole.RemoveRange(setRoleToAdmin);
                    _DsDb.SiteUserToRole.Remove(originAdmin);
                    _DsDb.SiteUserToRole.Add(newRole);
                    _DsDb.SaveChanges();

                    //登出
                    FormsAuthentication.SignOut();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult(Data: true);
            }

            return new NotFoundResult();

        }

        /// <summary>
        /// 取得所有角色資料
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllRoles(UserInfo _UserInfo)
        {
            //取得所有角色資料不包含Admin
            var roles = _DsDb.SiteRole.Where(o => o.Name != "admin").Select(o => o.Name).ToList();

            if (roles.Count > 0)
            {
                return new SuccessResult(Data: roles);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 取得角色對應表(Menu)清單
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetRolesInfo(UserInfo _UserInfo)
        {
            List<SiteRoleViewModel> roleModel = new List<SiteRoleViewModel>();

            var roles = from role in _DsDb.SiteRole
                        join text in _DsDb.SiteText on role.LanKey equals text.LanKey
                        where text.CultureCode == _UserInfo.LanguageCode
                        select new SiteRoleViewModel()
                        {
                            Id = role.Id,
                            Name = role.Name,
                            Description = text.LanText,
                            Menus = _DsDb.SiteRoleToMenu.Where(o => o.SiteRole_Id == role.Id).Select(o => new MenuViewModel()
                            {
                                Action = o.Menu.ActionName,
                                Controller = o.Menu.ControllerName
                            }).ToList()
                        };

            if (roles != null)
            {
                return new SuccessResult(Data: roles.ToList());
            }

            return new NotFoundResult();

        }

        /// <summary>
        /// 給予帳戶角色
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ApiResult SetRoleToUser(UserInfo _UserInfo, SetRole data)
        {
            var siteRoleList = new List<SiteUserToRole>();
            if (data.Roles.Count != 0 || data.Users.Count != 0)
            {
                foreach (var role in data.Roles)
                {
                    var roleId = _DsDb.SiteRole.Where(o => o.Name == role).Select(o => o.Id).SingleOrDefault();
                    foreach (var user in data.Users)
                    {
                        //判斷是否重複指派
                        if (_DsDb.SiteUserToRole.Where(o => o.User_Id == user.Id && o.Role_Id == roleId).Any() == false)
                        {
                            siteRoleList.Add(new SiteUserToRole { User_Id = user.Id, Role_Id = roleId });
                        }
                    }
                }
                try
                {
                    _DsDb.SiteUserToRole.AddRange(siteRoleList);
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }

                return new SuccessResult();
            }

            return new NotFoundResult();

        }

        public Object GetUserInfo(UserInfo _UserInfo)
        {
            var result = _DsDb.User.Where(o => o.Name == _UserInfo.Name && o.Email == _UserInfo.Email && o.DeleteDate == null).SingleOrDefault();
            return Json(new
            {
                Name = result.Name,
                Email = result.Email
            });
        }
    }

}