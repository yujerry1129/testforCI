﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models
{
    public class EmailServiceModels : Controllers.Base.DsBaseController
    {
        public DsDbLib.Models.EmailService FindEmailService()
        {
            return _DsDb.EmailService.FirstOrDefault();
        }
    }
}