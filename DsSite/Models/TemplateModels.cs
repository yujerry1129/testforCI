﻿using DsDbLib.Models;
using DsSite.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsSite.Models.Modals
{
    public class TemplateModels : Controllers.Base.DsBaseController
    {
        /// <summary>
        /// 讀取當前DB所有Template模版
        /// </summary>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult GetAllTemplate(UserInfo _UserInfo)
        {
            var data = _DsDb.LayoutTemplate
                .Where(o => o.UserGroupId == _UserInfo.UserGroupId && o.DeleteByUserId == null)
                .ToList()
                .Select(o => new TemplateApiModel()
                {
                    TemplateId = o.Id,
                    Name = o.Name,
                    Width = o.Width,
                    Height = o.Height,
                    Regions = o.TemplateRegions.Select(p => new TemplateRegionApiModel()
                    {
                        Id = p.Id,
                        Height = p.Height,
                        Width = p.Width,
                        Left = p.OffsetX,
                        Top = p.OffsetY,
                        Order = p.LayerOrder
                    }).ToList()
                });

            if (data.Count() > 0)
            {
                return new SuccessResult(Data: data);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        /// <summary>
        /// 新增Template模版
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult AddTemplate(TemplateApiModel Model, UserInfo _UserInfo)
        {

            var template = new LayoutTemplate()
            {
                Name = Model.Name,
                Width = Model.Width,
                Height = Model.Height,
                CreateByUserId = _UserInfo.Id,
                CreateDate = DateTime.Now,
                UserGroupId = _UserInfo.UserGroupId,
                TemplateRegions = Model.Regions.Select(o => new LayoutTemplateRegion()
                {
                    Width = o.Width,
                    Height = o.Height,
                    OffsetX = o.Left,
                    OffsetY = o.Top,
                    LayerOrder = o.Order,
                    CreateByUserId = _UserInfo.Id,
                    CreateDate = DateTime.Now
                }).ToList()
            };

            try
            {
                _DsDb.LayoutTemplate.Add(template);
                _DsDb.SaveChanges();

                Model.TemplateId = template.Id;
                return new SuccessResult(Data: Model);
            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0044");
            }
        }

        /// <summary>
        /// 刪除Template模版
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="_UserInfo"></param>
        /// <returns></returns>
        public ApiResult RemoveTemplate(TemplateApiModel Model, UserInfo _UserInfo)
        {
            var template = _DsDb.LayoutTemplate.FirstOrDefault(o => o.Id == Model.TemplateId && o.UserGroupId == _UserInfo.UserGroupId);

            if (template != null)
            {
                //Remove all regions
                _DsDb.LayoutTemplateRegion.RemoveRange(template.TemplateRegions);
                _DsDb.LayoutTemplate.Remove(template);

                try
                {
                    _DsDb.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new ExceptionResult(Message: "Error_0044");
                }
            }

            return new SuccessResult();
        }
    }
}