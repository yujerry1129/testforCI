﻿using DsDbLib.Models;
using Newtonsoft.Json;
using System;


namespace DsSite.Models
{
    public class CampaignLogModels : Controllers.Base.DsBaseController
    {
        public void CampaignLog(Campaign cp, UserInfo _UserInfo, int action)
        {
            //將Campaign資訊存入log
            var Campaignlog = new DsDbLib.Models.CampaignLog()
            {
                CampaignName = cp.Name,
                CampaignId = cp.Id,
                CampaignDuration = cp.Duration,
                Width = cp.Width,
                Height = cp.Height,
                Regions = JsonConvert.SerializeObject(cp.Regions),
                UserId = _UserInfo.Id,
                TimeStamp = DateTime.Now,
                UserGroupId = _UserInfo.UserGroupId,
                Action = action
            };
            _DsDb.CampaignLog.Add(Campaignlog);
            _DsDb.SaveChanges();
        }
    }
}