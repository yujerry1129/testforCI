﻿var __languages = [
    {
        id: "save",
        lan: [
            { text: "Save", code: "en-us" },
            { text: "儲存", code: "zh-tw" }
        ]
    },
    {
        id: "device",
        lan: [
            { text: "Device", code: "en-us" },
            { text: "裝置", code: "zh-tw" }
        ]
    }
]