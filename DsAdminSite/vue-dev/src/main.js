import Vue from 'vue'
import App from './App.vue'
import GroupIndex from './components/group-index.vue'
import PlayerVersionIndex from './components/player-version-index.vue'

Vue.config.silent = true

new Vue({
  el: '#app',
  components: {
    GroupIndex,
    PlayerVersionIndex,
  }
})