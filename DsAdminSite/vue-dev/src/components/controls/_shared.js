exports.GetSizeString = (size, fix) => {
    if (!size) {
        size = 0;
    }

    if (!fix) {
        fix = 2;
    }

    //byte
    if (size < 1024) {
        return size + ' byte(s)';
    }

    //kb
    size = size % 1024 === 0 ? size / 1024 : (size / 1024).toFixed(2);
    if (size < 1024) {
        return size + ' KB';
    }

    //mb
    size = size % 1024 === 0 ? size / 1024 : (size / 1024).toFixed(2);
    if (size < 1024) {
        return size + ' MB';
    }

    //gb
    size = size % 1024 === 0 ? size / 1024 : (size / 1024).toFixed(2);
    return size + ' GB';
}

exports.GetDateString = (date) => {
    var d = new Date(date);
    var n = d.getFullYear() + "/" + (parseInt(parseInt(d.getMonth()) + 1) < 10 ? "0" + parseInt(parseInt(d.getMonth()) + 1) : parseInt(parseInt(d.getMonth()) + 1)) +
        "/" + (d.getDate() < 10 ? "0" + d.getDate() : d.getDate()) + " " + (d.getHours() < 10 ? "0" + d.getHours() : d.getHours()) + ":" + (d.getMinutes() < 10 ? "0" +
            d.getMinutes() : d.getMinutes());
    return n
}