﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DsDbLib;
using DsDbLib.Models;

namespace DsAdminSite.Controllers.Api.Base
{
    public class DsBaseApiController : ApiController
    {
        protected DsDb _DsDb = new DsDb();

        protected UserGroup _UserGroup;

        //use db
        public DsDb Db
        {
            get { return _DsDb; }
        }
        public int UserId
        {
            get
            {
                return _DsDb.AdminUser.First().Id;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _DsDb.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
