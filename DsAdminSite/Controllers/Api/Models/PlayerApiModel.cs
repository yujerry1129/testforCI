﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsAdminSite.Controllers.Api.Models
{
    public class PlayerApiModel
    {
       
            public int Id { get; set; }
            public Version Version { get; set; }
            public DateTime CreateDate { get; set; }
            public long UpdateFileSize { get; set; }
            public string UpdateFileMD5Hash { get; set; }
            public string ChangeLog { get; set; }
        
    }
}