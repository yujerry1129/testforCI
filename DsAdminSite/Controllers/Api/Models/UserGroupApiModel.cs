﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DsAdminSite.Controllers.Api.Models
{
    public class AddEditUserApiModel
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public int DeviceLimit { get; set; }
        public long StorageLimit { get; set; }
        public string SystemKey { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public Boolean EnableFaceDetection { get; set; }
        public Boolean WatchDogShutDownTime { get; set; }
        public Boolean EnableWatchDogLog { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string UserPassword { get; set; }
        public string DMIKey { get; set; }
        public string DMIValue { get; set; }
        public int Page { get; set; }
        public int PageRows { get; set; }
        public string SearchText { get; set; }
        public string SearchType { get; set; }
        public string CheckModel()
        {
            if (string.IsNullOrEmpty(GroupName))
            {
                return "Group name is required";
            }
            if (DeviceLimit <= 0)
            {
                return "DeviceLimit must be at least 1 ";
            }
            if (StorageLimit <= 0)
            {
                return "StorageLimit must be at least 1";
            }
            if (EffectiveStartDate == null)
            {
                return "Please choose effective start date";
            }
            if (EffectiveEndDate == null)
            {
                return "Please choose effective end date";
            }
            if (string.IsNullOrEmpty(UserName))
            {
                return "User name is required";
            }
            if (string.IsNullOrEmpty(Email))
            {
                return "User account is required";
            }
            if (string.IsNullOrEmpty(UserPassword))
            {
                return "User password is required";
            }
            if (string.IsNullOrEmpty(DMIKey))
            {
                return "DMI key is required";
            }
            if (string.IsNullOrEmpty(DMIValue))
            {
                return "DMI value is required";
            }
            return null;
        }
    }
    public class GroupInfo
    {
        public class GroupList
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int DeviceLimit { get; set; }
            public int AccountCount { get; set; }
            public int DeviceCount { get; set; }
            public long UploadLimit { get; set; }
            public DateTime? EffectStartDate { get; set; }
            public DateTime? EffectEndDate { get; set; }
            public Boolean FaceDetection { get; set; }
            public Boolean AutoShutDown { get; set; }
            public Boolean WatchDog { get; set; }
            public List<UserSettingApiModel> UserSetting { get; set; }
        }
        public int Count { get; set; }
        public List<GroupInfo.GroupList> GroupInfoList { get; set; }
    }
    public class UserSettingApiModel
    {
        public string Name { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string DmiKey { get; set; }
        public string DmiValue { get; set; }
    }
}