﻿using DsAdminSite.Controllers.Api.Models;
using DsAdminSite.Models;
using System;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsAdminSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/player")]
    public class PlayerController : Base.DsBaseApiController
    {
        PlayerModels playerModels = new PlayerModels();

        /// <summary>
        /// 取得所有版本資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getallversions")]
        public ApiResult GetAllVersions()
        {
            var data = playerModels.GetAllVersions();

            return data;
        }

        /// <summary>
        /// 上傳新版player
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("upload")]
        public ApiResult UploadPlayerVersion()
        {

            var Date = DateTime.Now;
            var request = HttpContext.Current.Request;
            //拿掉副檔名
            var version = Path.GetFileNameWithoutExtension(request.Headers["filename"]);
            var folderPath = WebConfigurationManager.AppSettings["UploadPlayer"] + "/VersionRecord";
            var filePath = WebConfigurationManager.AppSettings["UploadPlayer"] + "/VersionRecord/" + version + "/" + version + ".msi";

            //VersionRecord 資料夾是否存在
            if (Directory.Exists(folderPath) == false)
            {
                Directory.CreateDirectory(folderPath);
            }
            //VersionRecord +版本資料夾是否存在
            if (Directory.Exists(folderPath + "/" + version) == false || (File.Exists(folderPath + "/" + version + "/" + "AcePlayerSetup-2.2.17.msi") == false))
            {
                Directory.CreateDirectory(folderPath + "/" + version);
                try
                {
                    //存到C:/ DSupload/VersionRecord/版本
                    using (var fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
                    {
                        request.InputStream.CopyTo(fs);
                    }
                }
                catch (Exception e)
                {
                    return new ExceptionResult(Data: " 存失敗" + e);

                }

                //判斷zip是否存在 若存在先刪掉重壓一份
                if (System.IO.File.Exists(@"C:\DsUpload\AcePlayerSetup.zip"))
                {
                    try
                    {
                        System.IO.File.Delete(@"C:\DsUpload\AcePlayerSetup.zip");
                    }
                    catch (System.IO.IOException e)
                    {
                        return new ExceptionResult(Data: " 若有刪除失敗" + e);
                    }
                }

                //壓縮檔案
                ZipFile.CreateFromDirectory(folderPath + "/" + version, @"C:\DsUpload\AcePlayerSetup.zip");
            }
            else
            {
                return new ExceptionResult(Data: "版本重複");
            }

            var result = playerModels.UploadData(HttpUtility.UrlDecode(request.Headers["ChangeLog"]), request.Headers["UpdateFileSize"], request.Headers["Version"], filePath);

            return result;
        }

    }
}
