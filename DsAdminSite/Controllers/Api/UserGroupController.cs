﻿using DsAdminSite.Controllers.Api.Models;
using DsAdminSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DsAdminSite.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/usergroup")]
    public class UserGroupController : Base.DsBaseApiController
    {
        UserGroupModels UserGroupModels = new UserGroupModels();
        /// <summary>
        /// get all user group data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getallgroups")]
        public ApiResult GetAllGroups(AddEditUserApiModel Model)
        {
            var data = UserGroupModels.GetAllGroups(UserId, Model.Page, Model.PageRows, Model.SearchText, Model.SearchType);
            //Todos
            return data;
        }
        /// <summary>
        /// add or edit user group
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addeditusergroup")]
        public ApiResult AddEditUserGroup(AddEditUserApiModel Model)
        {
            var data = UserGroupModels.AddEditUserGroup(UserId, Model);
            return data;
        }
        /// <summary>
        /// delete user group
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteusergroup")]
        public ApiResult DeleteUserGroup(AddEditUserApiModel Model)
        {
            var data = UserGroupModels.DeleteUserGroup(UserId, Model);
            return data;
        }

    }
}
