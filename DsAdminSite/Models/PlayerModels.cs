﻿using DsAdminSite.Controllers.Api.Models;
using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using static DsAdminSite.Controllers.Api.Models.PlayerApiModel;

namespace DsAdminSite.Models
{
    public class PlayerModels : Controllers.Api.Base.DsBaseApiController
    {
        public ApiResult GetAllVersions()
        {

            var versions = SortVersion();
            var tempVersionList = new List<object>();
            foreach (var version in versions)
            {
                tempVersionList.Add(new
                {
                    Id = version.Id,
                    Version = version.Version.Major + "." + version.Version.Minor + "." + version.Version.Build,
                    CreateDate = version.CreateDate,
                    UpdateFileSize = version.UpdateFileSize,
                    UpdateFileMD5Hash = version.UpdateFileMD5Hash,
                    ChangeLog = version.ChangeLog

                });
            }
            if (versions != null)
            {
                return new SuccessResult(Data: tempVersionList);
            }

            return new NotFoundResult();
        }

        public List<PlayerApiModel> SortVersion()
        {
            var versionList = _DsDb.VersionFile.ToList();
            List<PlayerApiModel> vList = new List<PlayerApiModel>();

            foreach (var version in versionList)
            {
                vList.Add(new PlayerApiModel
                {
                    Id = version.Id,
                    Version = Version.Parse(version.Version),
                    CreateDate = version.CreateDate,
                    UpdateFileSize = version.UpdateFileSize,
                    UpdateFileMD5Hash = version.UpdateFileMD5Hash,
                    ChangeLog = version.ChangeLog
                });
            }
            BubbleSort(vList);

            return vList;
        }


        public void Swap(List<PlayerApiModel> data, int i, int j)
        {
            var tmp = data[i];
            data[i] = data[j];
            data[j] = tmp;
        }

        public List<PlayerApiModel> BubbleSort(List<PlayerApiModel> data)
        {
            var flag = true;
            for (var i = 0; i < data.Count() - 1 && flag; i++)
            {
                flag = false;
                for (var j = 0; j < data.Count() - i - 1; j++)
                {
                    if (data[j + 1].Version.Major > data[j].Version.Major)
                    {
                        Swap(data, j + 1, j);
                        flag = true;
                    }
                    else if (data[j + 1].Version.Minor > data[j].Version.Minor)
                    {
                        Swap(data, j + 1, j);
                        flag = true;
                    }
                    else if (data[j + 1].Version.Build >= data[j].Version.Build)
                    {
                        Swap(data, j + 1, j);
                        flag = true;
                    }
                }
            }
            return data;
        }


        public ApiResult UploadData(string changLog, string tempSize, string tempVersion, string path)
        {
            var hash = "";
            var size = Convert.ToInt64(tempSize);
            //取版本號
            var version = tempVersion.Split('-').LastOrDefault();
            //拿掉副檔名
            version = Path.GetFileNameWithoutExtension(version);

            //取得目標檔案的MD5編碼
            MD5 md5Hash = new MD5CryptoServiceProvider();

            var tragetFile = new System.IO.FileStream(path, System.IO.FileMode.Open);

            byte[] byteData = md5Hash.ComputeHash(tragetFile);

            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < byteData.Length; i++)
            {
                sBuilder.Append(byteData[i].ToString("x2"));
            }

            md5Hash.Clear();
            tragetFile.Dispose();//停止占用檔案
            hash = sBuilder.ToString();

            // 尋找是否有相同版本號，有則蓋過去(針對 player msi 檔案毀損遺失 在不進版下的更新方法)
            var findVersion = _DsDb.VersionFile.Where(o => o.Version == version).FirstOrDefault();

            if (findVersion != null)
            {
                //update 此版本
                findVersion.ChangeLog = changLog;
                findVersion.UpdateFileSize = size;
                findVersion.UpdateFileMD5Hash = hash;
                findVersion.CreateDate = DateTime.Now;
                try
                {
                    _DsDb.SaveChanges();
                    SuccessResult result = new SuccessResult();
                    result.Data = true;
                    return result;
                }
                catch
                {
                    return new ExceptionResult(Data: false);
                }
            }
            else
            {
                //寫入新的一筆至 DB
                List<VersionFile> versionfile = new List<VersionFile>();
                versionfile.Add(new VersionFile
                {
                    ChangeLog = changLog,
                    Version = version,
                    UpdateFileSize = size,
                    UpdateFileMD5Hash = hash,
                    CreateDate = DateTime.Now
                });
                try
                {
                    _DsDb.VersionFile.AddRange(versionfile);
                    _DsDb.SaveChanges();
                    SuccessResult result = new SuccessResult();
                    result.Data = true;
                    return result;
                }
                catch
                {
                    return new ExceptionResult(Data: false);
                }

            }

        }
    }
}