﻿using DsAdminSite.Controllers.Api.Models;
using DsAdminSite.Models;
using DsDbLib;
using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DsKit.Helpers;


namespace DsAdminSite.Models
{
    public class UserGroupModels : Controllers.Api.Base.DsBaseApiController
    {
        /// <summary>
        /// Get all user group data
        /// </summary>
        /// <returns></returns>
        public ApiResult GetAllGroups(int userId, int page, int pageRows, string searchText, string searchType)
        {
            int allPage = 0;
            List<GroupInfo.GroupList> groupInfoList = new List<GroupInfo.GroupList>();
            if (searchText != null)
            {
                groupInfoList = _DsDb.UserGroup.Where(o => o.DeleteByAdminUserId == null && o.DeleteDate == null && o.Name.Contains(searchText) && userId == o.CreateByAdminUserId)
                    .Select(o => new GroupInfo.GroupList()
                    {
                        Id = o.Id,
                        Name = o.Name,
                        DeviceLimit = o.DeviceLimit,
                        AccountCount = _DsDb.User.Count(t => t.UserGroupId == o.Id && t.DeleteDate == null),
                        DeviceCount = _DsDb.Device.Count(c => c.UserGroup_Id == o.Id && c.DeleteDate == null),
                        UploadLimit = o.StorageLimit,
                        EffectStartDate = o.EffectiveStartDate,
                        EffectEndDate = o.EffectiveEndDate,
                        FaceDetection = o.EnableFaceDetection,
                        AutoShutDown = (o.WatchDogShutDownTime == 180) ? true : false,
                        WatchDog = o.EnableWatchDogLog,
                        UserSetting = _DsDb.ProductRule.Where(p => p.DeleteByAdminUserId == null && p.DeleteDate == null && p.UserGroupId == o.Id)
                        .Select(p => new UserSettingApiModel()
                        {
                            Name = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).Name,
                            Account = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).Email,
                            Password = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).HashedPassword,
                            DmiKey = _DsDb.DMIDictionay.FirstOrDefault(v => v.ProductRuleId == p.Id).DMIKey,
                            DmiValue = _DsDb.DMIDictionay.FirstOrDefault(v => v.ProductRuleId == p.Id).DMIValue,
                        }).ToList()
                    }).ToList();
            }
            else
            {
                groupInfoList = _DsDb.UserGroup.Where(o => o.DeleteByAdminUserId == null && o.DeleteDate == null && userId == o.CreateByAdminUserId)
                 .Select(o => new GroupInfo.GroupList()
                 {
                     Id = o.Id,
                     Name = o.Name,
                     DeviceLimit = o.DeviceLimit,
                     AccountCount = _DsDb.User.Count(t => t.UserGroupId == o.Id && t.DeleteDate == null),
                     DeviceCount = _DsDb.Device.Count(c => c.UserGroup_Id == o.Id && c.DeleteDate == null),
                     UploadLimit = o.StorageLimit,
                     EffectStartDate = o.EffectiveStartDate,
                     EffectEndDate = o.EffectiveEndDate,
                     FaceDetection = o.EnableFaceDetection,
                     AutoShutDown = (o.WatchDogShutDownTime == 180) ? true : false,
                     WatchDog = o.EnableWatchDogLog,
                     UserSetting = _DsDb.ProductRule.Where(p => p.DeleteByAdminUserId == null && p.DeleteDate == null && p.UserGroupId == o.Id)
                       .Select(p => new UserSettingApiModel()
                       {
                           Name = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).Name,
                           Account = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).Email,
                           Password = _DsDb.User.FirstOrDefault(t => t.UserGroupId == o.Id).HashedPassword,
                           DmiKey = _DsDb.DMIDictionay.FirstOrDefault(v => v.ProductRuleId == p.Id).DMIKey,
                           DmiValue = _DsDb.DMIDictionay.FirstOrDefault(v => v.ProductRuleId == p.Id).DMIValue,
                       }).ToList()
                 }).ToList();
            }
            foreach (var group in groupInfoList)
            {
                group.EffectStartDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)group.EffectStartDate, TimeZoneInfo.Local);
                group.EffectEndDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)group.EffectEndDate, TimeZoneInfo.Local);
            }
            allPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(groupInfoList.Count()) / pageRows));
            var model = new GroupInfo();
            model.GroupInfoList = groupInfoList.Skip((page - 1) * pageRows).Take(pageRows).ToList();
            model.Count = allPage;
            return new SuccessResult(Data: model);
        }


        /// <summary>
        /// Find user group detail
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public UserGroup FlagUserGroupStatus(int userId, AddEditUserApiModel Model)
        {
            return _DsDb.UserGroup.FirstOrDefault(o => o.Id == Model.Id && o.DeleteByAdminUserId == null && o.DeleteDate == null);
        }

        /// <summary>
        /// Add or edit user group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult AddEditUserGroup(int userId, AddEditUserApiModel Model)
        {
            License License = new License();


            if (Model.Id == 0)
            {
                var data = License.CheckAdminCreateGroup(Model.DeviceLimit);
                if (data.Result)
                {
                    if (_DsDb.UserGroup.Any(o => o.Name == Model.GroupName))
                    {
                        return new ExceptionResult(Message: "Group name is repeated use");
                    }
                    if (_DsDb.User.Any(o => o.Email == Model.Email && o.DeleteDate == null))
                    {
                        return new ExceptionResult(Message: "Email is repeated use");
                    }

                    var hashCode = Guid.NewGuid();

                    var userGroup = new UserGroup()
                    {
                        Name = Model.GroupName,
                        DeviceLimit = Model.DeviceLimit,
                        StorageLimit = Model.StorageLimit,
                        SystemKey = hashCode.ToString(),
                        EffectiveStartDate = TimeZoneInfo.ConvertTime(Model.EffectiveStartDate, TimeZoneInfo.Local, TimeZoneInfo.Utc),
                        EffectiveEndDate = TimeZoneInfo.ConvertTime(Model.EffectiveEndDate, TimeZoneInfo.Local, TimeZoneInfo.Utc),
                        EnableFaceDetection = Model.EnableFaceDetection,
                        WatchDogShutDownTime = Model.WatchDogShutDownTime ? (int?)180 : null,
                        EnableWatchDogLog = Model.EnableWatchDogLog,
                        CreateByAdminUserId = userId,
                        CreateDate = DateTime.Now
                    };

                    _DsDb.UserGroup.Add(userGroup);

                    var user = new User()
                    {
                        Name = Model.UserName,
                        Email = Model.Email,
                        HashedPassword = Model.UserPassword,
                        CreateDate = DateTime.Now,
                        UserGroup = userGroup,
                        CreateByAdminUserId = userId
                    };

                    _DsDb.User.Add(user);

                    var productRule = new ProductRule()
                    {
                        CreateDate = DateTime.Now,
                        CreateByAdminUserId = userId,
                        UserGroup = userGroup
                    };

                    _DsDb.ProductRule.Add(productRule);

                    var dmiDictionary = new DMIDictionary()
                    {
                        DMIKey = "any",
                        DMIValue = "any",
                        CreateDate = DateTime.Now,
                        CreateByAdminUserId = userId,
                        ProductRule = productRule

                    };

                    _DsDb.DMIDictionay.Add(dmiDictionary);

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Please contact system admin");
                    }

                    //新增AgeRangeDefinition,DistanceRangeDefinition , LooktimeRangeDefinition, 
                    List<AgeRangeDefinition> ageList = new List<AgeRangeDefinition>();
                    List<DistanceRangeDefinition> distanceList = new List<DistanceRangeDefinition>();
                    List<LooktimeRangeDefinition> looktimeList = new List<LooktimeRangeDefinition>();

                    ageList.Add(new AgeRangeDefinition { Description = "兒童", MinValue = 0, MaxValue = 13, UserGroup_Id = userGroup.Id, AgeId = 0 });
                    ageList.Add(new AgeRangeDefinition { Description = "青年", MinValue = 13, MaxValue = 18, UserGroup_Id = userGroup.Id, AgeId = 1 });
                    ageList.Add(new AgeRangeDefinition { Description = "壯年", MinValue = 18, MaxValue = 35, UserGroup_Id = userGroup.Id, AgeId = 2 });
                    ageList.Add(new AgeRangeDefinition { Description = "老年", MinValue = 35, MaxValue = 65, UserGroup_Id = userGroup.Id, AgeId = 3 });

                    distanceList.Add(new DistanceRangeDefinition { Description = "1公尺以下", MinValue = 0, MaxValue = 100, UserGroup_Id = userGroup.Id });
                    distanceList.Add(new DistanceRangeDefinition { Description = "1~2公尺", MinValue = 100, MaxValue = 200, UserGroup_Id = userGroup.Id });
                    distanceList.Add(new DistanceRangeDefinition { Description = "2~3公尺", MinValue = 200, MaxValue = 300, UserGroup_Id = userGroup.Id });
                    distanceList.Add(new DistanceRangeDefinition { Description = "3公尺以上", MinValue = 300, MaxValue = 99999999, UserGroup_Id = userGroup.Id });

                    looktimeList.Add(new LooktimeRangeDefinition { Description = "1秒以下", MinValue = 0, MaxValue = 10, UserGroup_Id = userGroup.Id });
                    looktimeList.Add(new LooktimeRangeDefinition { Description = "1~5秒", MinValue = 10, MaxValue = 50, UserGroup_Id = userGroup.Id });
                    looktimeList.Add(new LooktimeRangeDefinition { Description = "5~10秒", MinValue = 50, MaxValue = 100, UserGroup_Id = userGroup.Id });
                    looktimeList.Add(new LooktimeRangeDefinition { Description = "10秒以上", MinValue = 100, MaxValue = 99999999, UserGroup_Id = userGroup.Id });

                    _DsDb.AgeRangeDefinition.AddRange(ageList);
                    _DsDb.DistanceRangeDefinition.AddRange(distanceList);
                    _DsDb.LooktimeRangeDefinition.AddRange(looktimeList);

                    //新增預設Template
                    var layoutTemplateHorizontal = new LayoutTemplate()
                    {
                        CreateDate = DateTime.Now,
                        CreateByUserId = user.Id,
                        UserGroupId = userGroup.Id,
                        Width = 1920,
                        Height = 1080,
                        Name = "Default"
                    };

                    _DsDb.LayoutTemplate.Add(layoutTemplateHorizontal);

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult("Please contact system admin");
                    }

                    var layoutTemplateHorizontalRegion = new LayoutTemplateRegion()
                    {
                        CreateDate = DateTime.Now,
                        CreateByUserId = user.Id,
                        Width = 1720,
                        Height = 880,
                        OffsetX = 100,
                        OffsetY = 100,
                        LayerOrder = 1,
                        TemplateId = layoutTemplateHorizontal.Id
                    };

                    _DsDb.LayoutTemplateRegion.Add(layoutTemplateHorizontalRegion);

                    var layoutTemplateVertical = new LayoutTemplate()
                    {
                        CreateDate = DateTime.Now,
                        CreateByUserId = user.Id,
                        UserGroupId = userGroup.Id,
                        Width = 1080,
                        Height = 1920,
                        Name = "Default2"
                    };

                    _DsDb.LayoutTemplate.Add(layoutTemplateVertical);

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult("Please contact system admin");
                    }

                    var layoutTemplateVerticalRegion = new LayoutTemplateRegion()
                    {
                        CreateDate = DateTime.Now,
                        CreateByUserId = user.Id,
                        Width = 880,
                        Height = 1720,
                        OffsetX = 100,
                        OffsetY = 100,
                        LayerOrder = 1,
                        TemplateId = layoutTemplateVertical.Id
                    };

                    _DsDb.LayoutTemplateRegion.Add(layoutTemplateVerticalRegion);

                    var UserRule = new SiteUserToRole()
                    {
                        User_Id = user.Id,
                        Role_Id = 1
                    };

                    _DsDb.SiteUserToRole.Add(UserRule);

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Please contact system admin");
                    }

                    return new SuccessResult();
                }
                else
                {
                    return new ExceptionResult(Message: "Number of devices is over limit");
                }
            }
            else
            {
                var data = License.CheckAdminEditGroup(Model.DeviceLimit, Model.Id);
                if (data.Result)
                {
                    var uerGroup = FlagUserGroupStatus(userId, Model);
                    var user = _DsDb.User.FirstOrDefault(o => o.UserGroupId == Model.Id && o.DeleteDate == null && o.DeleteByAdminUserId == null);
                    var productRuleId = _DsDb.ProductRule.FirstOrDefault(o => o.UserGroupId == Model.Id && o.DeleteDate == null && o.DeleteByAdminUserId == null).Id;
                    var dmiDictionary = _DsDb.DMIDictionay.FirstOrDefault(o => o.ProductRuleId == productRuleId);

                    uerGroup.Name = Model.GroupName;
                    uerGroup.DeviceLimit = Model.DeviceLimit;
                    uerGroup.StorageLimit = Model.StorageLimit;
                    uerGroup.EffectiveStartDate = TimeZoneInfo.ConvertTime(Model.EffectiveStartDate, TimeZoneInfo.Local, TimeZoneInfo.Utc);
                    uerGroup.EffectiveEndDate = TimeZoneInfo.ConvertTime(Model.EffectiveEndDate, TimeZoneInfo.Local, TimeZoneInfo.Utc);
                    uerGroup.EnableFaceDetection = Model.EnableFaceDetection;
                    uerGroup.WatchDogShutDownTime = Model.WatchDogShutDownTime ? (int?)180 : null;
                    uerGroup.EnableWatchDogLog = Model.EnableWatchDogLog;
                    uerGroup.UpdateByAdminUserId = userId;
                    uerGroup.UpdateDate = DateTime.Now;

                    user.Name = Model.UserName;
                    user.Email = Model.Email;
                    user.HashedPassword = Model.UserPassword;
                    user.UpdateDate = DateTime.Now;
                    user.UpdateByAdminUserId = userId;

                    dmiDictionary.DMIKey = "any";
                    dmiDictionary.DMIValue = "any";
                    dmiDictionary.UpdateDate = DateTime.Now;
                    dmiDictionary.UpdateByAdminUserId = userId;

                    try
                    {
                        _DsDb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionResult(Message: "Please contact system admin");
                    }

                    return new SuccessResult();
                }
                else
                {
                    return new ExceptionResult(Message: "Number of devices is over limit");
                }
            }

        }
        /// <summary>
        /// Delete user group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ApiResult DeleteUserGroup(int userId, AddEditUserApiModel Model)
        {
            var cp = FlagUserGroupStatus(userId, Model);


            foreach (var user in cp.User)
            {
                user.Email = user.Email + DateTime.Now;
                user.DeleteDate = DateTime.Now;
                user.DeleteByAdminUserId = userId;
            }
            foreach (var productrule in cp.ProductRules)
            {
                productrule.DeleteDate = DateTime.Now;
                productrule.DeleteByAdminUserId = userId;
                _DsDb.DMIDictionay.RemoveRange(productrule.DMIDictionaries);
            }
            cp.Name = cp.Name + DateTime.Now;
            cp.DeleteByAdminUserId = userId;
            cp.DeleteDate = DateTime.Now;
            _DsDb.SaveChanges();
            return new SuccessResult();
        }
    }
}