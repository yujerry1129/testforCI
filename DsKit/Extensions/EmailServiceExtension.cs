﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DsDbLib.Models;
using System.Net.Mail;
using System.Net;

namespace DsKit.Extensions
{
    public static class EmailServiceExtension
    {
        public static SmtpClient CreateSmtpClient(this EmailService obj)
        {
            SmtpClient smtp = new SmtpClient()
            {
                Host = obj.SmtpServerAddress,
                Port = obj.SmtpPort,
                EnableSsl = obj.EnableSSL,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = obj.AllowAnonymous
            };

            if (obj.AllowAnonymous == false)
            {
                smtp.Credentials = new NetworkCredential(obj.SmtpAccount, obj.SmtpPassword);
            }

            return smtp;
        }
    }
}
