﻿using DsDbLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsKit.Extensions
{
    public static class FileUploadExtension
    {
        public static string GetSavedFileName(this FileUpload obj)
        {
            if (obj == null)
            {
                return "";
            }

            return obj.HashCode + obj.ExtensionName;
        }
    }
}
