﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsKit.Helpers
{
    public class RandomHelper
    {
        public static string GetRandomNumberString(int Length)
        {
            var sb = new StringBuilder();

            var seed = DateTime.Now.Millisecond;
            
            for (int i = 0; i < Length; i++)
            {
                sb.Append(((new Random(seed + i)).Next() % 10).ToString());
            }

            return sb.ToString();
        }
    }
}
