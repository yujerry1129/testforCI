﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsKit.Helpers
{
    public class NamingHelper
    {
        /// <summary>
        /// 名稱轉換UserGroup_Name
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        public static string EncodeName(string Name, int UserGroupId)
        {
            return UserGroupId.ToString() + "_" + Name;
        }

        public static string DecodeName(string Name)
        {
            char split = '_';
            var position = Name.IndexOf(split, 0) > 0 ? Name.IndexOf(split, 0) : -1;

            return Name.Substring(position + 1).Trim();
        }
    }
}
