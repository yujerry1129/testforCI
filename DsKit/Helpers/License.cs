﻿using DsDbLib;
using DsKit.Models;
using Rhino.Licensing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Xml;

namespace DsKit.Helpers
{
    public class License
    {
        DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
        //doc.Load(AppDomain.CurrentDomain.BaseDirectory + "license.xml");       

        /// <summary>
        /// //檢驗publicKey及license檔是否合法(檔案、時間有效)
        /// </summary>
        /// <returns></returns>
        public ApiResult CheckLicense()
        {
            try
            {
                var publicKey = File.ReadAllText(dir.Parent.FullName + "\\publicKey.xml");
                var licensePath = dir.Parent.FullName + "\\license.xml";
                //new LicenseValidator(publicKey, licensePath).AssertValidLicense();

                //檢驗Lic檔是否為原始檔
                var valuesValidated = new LicenseValidator(publicKey, licensePath);
                valuesValidated.DisableFutureChecks();
                //valuesValidated.AssertValidLicense();

                if (valuesValidated.TryLoadingLicenseValuesFromValidatedXml())
                {
                    //讀Lic檔傳出xml檔給下一步使用
                    var license = File.ReadAllText(licensePath);
                    XmlDocument licenseData = new XmlDocument();
                    licenseData.Load(licensePath);

                    if (CheckTime(licenseData))
                    {
                        return new SuccessResult()
                        {
                            Data = licenseData,
                            Message = "Leagal license"
                        };
                    }
                    return new ExceptionResult(Message: "Error_0061");

                }
                return new ExceptionResult(Message: "Error_0062");

            }
            catch (Exception ex)
            {
                return new ExceptionResult(Message: "Error_0063");
            }
        }

        /// <summary>
        /// 檢查Lic檔是否仍在有效期內
        /// </summary>
        /// <param name="licenseData"></param>
        /// <returns></returns>
        Boolean CheckTime(XmlDocument licenseData)
        {
            try
            {
                DateTime? now = DateTime.MaxValue;
                //1.取得網路端的UTC時間
                DateTime netUTC = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(GetUtcTimeStamp());
                //如果年分是1970，表示網路回應有問題，無法取得正確時間
                if (netUTC.Year == 1970)
                {
                    //2.取得客戶本機端的UTC時間               
                    DateTime clientUTC = DateTime.UtcNow;

                    //3.取得客戶最後登入時間
                    DsDb _DsDb = new DsDb();
                    DateTime? lastLoginTime = _DsDb.User.Select(o => o.LastLoginTime).Max();
                    DateTime loginTimeUTC = TimeZoneInfo.ConvertTime((DateTime)lastLoginTime, TimeZoneInfo.Local, TimeZoneInfo.Utc);

                    now = (clientUTC > loginTimeUTC ? clientUTC : loginTimeUTC);

                }
                else { now = netUTC; }
                //三者取大
                //now = (netUTC > clientUTC ? netUTC : clientUTC) > loginTimeUTC ? (netUTC > clientUTC ? netUTC : clientUTC) : loginTimeUTC;

                XmlNode licenseNode = licenseData.SelectSingleNode("license");
                if (licenseNode != null)
                {
                    XmlNode licenceDetail = licenseNode;
                    var expiration = Convert.ToDateTime(licenceDetail.Attributes[1].Value);

                    if (now <= expiration)
                    { return true; }
                }
                licenseNode = null;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 檢驗客戶端資料
        /// </summary>
        /// <returns></returns>
        public ApiResult CheckClient()
        {
            try
            {
                var licenseVerified = CheckLicense();

                if (licenseVerified.Result)
                {
                    XmlDocument licenseData = new XmlDocument();
                    licenseData = (XmlDocument)licenseVerified.Data;
                    XmlNode licenseNode = licenseData.SelectSingleNode("license");
                    var licenseErrorMsg = "";

                    if (licenseNode != null)
                    {
                        XmlNode licenceDetail = licenseNode;
                        var macAddress = licenceDetail.Attributes[4].Value;

                        if (!CheckMacAddress(macAddress))
                        {
                            licenseErrorMsg = "Error_0064";
                            return new ExceptionResult(Message: licenseErrorMsg);
                        }
                        licenseNode = null;
                        return new SuccessResult();
                    }
                    return new ExceptionResult(Message: "Error_0065");
                }
                return new ExceptionResult(Message: licenseVerified.Message);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 取得網路上UTC校時調整秒數
        /// </summary>
        /// <returns></returns>
        static double GetUtcTimeStamp()
        {
            System.Net.HttpWebRequest oWRq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.timeanddate.com/scripts/ts.php?ut=1443508040915");
            //設定網路對接逾時秒數
            oWRq.Timeout = 1000;
            try
            {
                System.Net.HttpWebResponse oWRp = (System.Net.HttpWebResponse)oWRq.GetResponse();
                using (System.IO.StreamReader oSR = new System.IO.StreamReader(oWRp.GetResponseStream(), System.Text.Encoding.UTF8))
                {
                    string cUTC = oSR.ReadToEnd();
                    if (cUTC.IndexOf(".") != -1 | cUTC.IndexOf(" ") != -1)
                    {
                        return System.Convert.ToDouble(cUTC.Split(' ')[0]);
                    }
                    else
                    {
                        return 0.0;
                    }
                }
            }
            catch
            {
                return 0.0;
            }
        }

        /// <summary>
        /// 取得MacAddress
        /// </summary>
        /// <returns></returns>
        static string GetMacAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            List<string> macList = new List<string>();
            foreach (var nic in nics)
            {   // 因為電腦中可能有很多的網卡(包含虛擬的網卡)，
                // 只抓取 Ethernet 網卡的 MAC
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {   //nic傳回媒體存取控制 (MAC) 或實體位址，此配接器
                    macList.Add(nic.GetPhysicalAddress().ToString());
                }
            }
            return macList[0];
        }

        /// <summary>
        /// 檢驗MacAddress
        /// </summary>
        /// <param name="MacAddress"></param>
        /// <returns></returns>
        static Boolean CheckMacAddress(string MacAddress)
        {
            try
            {
                List<string> macLists = new List<string>();
                foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
                    if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        macLists.Add(nic.GetPhysicalAddress().ToString());

                foreach (var mac in macLists)
                {
                    if (mac == MacAddress)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// 檢驗DB內所有DeviceLimit總和是否大於LicLimit(T/F)
        /// </summary>
        /// <param name="licenseData"></param>
        /// <returns>(T/F),檢驗後的DBLimit</returns>
        public ApiResult CheckAllGroupLimitSum(XmlDocument licenseData)
        {
            try
            {
                XmlNode licenseNode = licenseData.SelectSingleNode("license");
                var licenseErrorMsg = "";

                if (licenseNode != null)
                {
                    XmlNode licenceDetail = licenseNode;
                    var deviceLimit = Convert.ToInt32(licenceDetail.Attributes[6].Value);
                    deviceLimit = (deviceLimit == -1 ? int.MaxValue : deviceLimit);

                    DsDb _DsDb = new DsDb();
                    //檢查DB內所有Group的DeviceLimit總和是否大於LicLimit
                    //var currentGroup = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null);
                    var gpDeviceLimitSum = _DsDb.UserGroup.Where(o => o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).Sum(o => o.DeviceLimit);
                    if (gpDeviceLimitSum > deviceLimit)
                    {
                        licenseErrorMsg = "Error_0066";
                        return new ExceptionResult(Message: licenseErrorMsg);
                    }
                    licenseNode = null;
                    return new SuccessResult(Data: deviceLimit);
                }
                return new ExceptionResult(Message: "Error_0065");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 檢驗當前群組的DBDeviceLimit是否大於LicLimit
        /// 檢驗當前群組的Device總和是否大於當前群組的BDeviceLimit
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="licLimit"></param>
        /// <param name="regDevices"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckExistDevices(int userGroupId, int licLimit, int regDevices)
        {
            DsDb _DsDb = new DsDb();
            var licenseErrorMsg = "";

            //檢查當前群組的DeviceLimit是否大於licLimit
            var currentGroupDeviceLimit = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).DeviceLimit;
            if (currentGroupDeviceLimit > licLimit)
            {
                licenseErrorMsg = "Error_0067";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            //檢查當前群組內Device總和是否大於當前群組的DeviceLimit
            var currentGroupDevices = _DsDb.Device.Where(o => o.UserGroup_Id == userGroupId && o.DeleteByUserId == null && o.DeleteDate == null && o.CreateDate != null).Count();
            if (currentGroupDevices > currentGroupDeviceLimit)
            {
                licenseErrorMsg = "Error_0068";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            if (currentGroupDevices + regDevices > currentGroupDeviceLimit)
            {
                licenseErrorMsg = "Error_0069";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            return new SuccessResult()
            {
                Result = true,
                Message = "Check Pass"
            };
        }

        /// <summary>
        /// 註冊裝置時的Lic檢驗
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="regDevices"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckRegDevices(int userGroupId, int regDevices)
        {
            var licenseErrorMsg = "";
            var licenseVerified = CheckLicense();

            if (licenseVerified.Result)
            {
                XmlDocument licenseData = new XmlDocument();
                licenseData = (XmlDocument)licenseVerified.Data;
                var checkGroupSum = CheckAllGroupLimitSum(licenseData);
                if (checkGroupSum.Result)
                {
                    var DBLicLimit = Convert.ToInt32(checkGroupSum.Data);
                    var checkDevices = CheckExistDevices(userGroupId, DBLicLimit, regDevices);

                    if (checkDevices.Result)
                    {
                        return new SuccessResult()
                        {
                            Result = true,
                            Message = checkDevices.Message
                        };
                    }
                    licenseErrorMsg = checkDevices.Message;
                    return new ExceptionResult(Message: licenseErrorMsg);
                }
                licenseErrorMsg = checkGroupSum.Message;
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            return new ExceptionResult(Message: licenseVerified.Message);
        }

        /// <summary>
        /// Admin新增群組時的Lic檢驗
        /// </summary>
        /// <param name="limit"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckAdminCreateGroup(int limit)
        {
            try
            {
                var licenseVerified = CheckLicense();
                if (licenseVerified.Result)
                {
                    XmlDocument licenseData = new XmlDocument();
                    licenseData = (XmlDocument)licenseVerified.Data;
                    XmlNode licenseNode = licenseData.SelectSingleNode("license");
                    var licenseErrorMsg = "";

                    if (licenseNode != null)
                    {
                        XmlNode licenceDetail = licenseNode;
                        var deviceLimit = Convert.ToInt32(licenceDetail.Attributes[6].Value);
                        deviceLimit = (deviceLimit == -1 ? int.MaxValue : deviceLimit);

                        DsDb _DsDb = new DsDb();
                        //檢驗DB內所有Group的DeviceLimit總和+本次新增群組的Limit是否大於LicLimit
                        var groupLimitSum = _DsDb.UserGroup.Where(o => o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).Select(o => o.DeviceLimit).DefaultIfEmpty(0).Sum();
                        if (groupLimitSum + limit <= deviceLimit) 
                        {
                            return new SuccessResult()
                            {
                                Result = true,
                                Message = "裝置數量限制檢驗成功,可新增群組"
                            };
                        }
                        licenseErrorMsg = "Error_0066";
                        licenseNode = null;
                        return new ExceptionResult(Message: licenseErrorMsg);
                    }
                    return new ExceptionResult(Message: "Error_0065");
                }
                return new ExceptionResult(Message: licenseVerified.Message);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Admin修改群組裝置數量的Lic檢驗
        /// </summary>
        /// <param name="newLimit"></param>
        /// <param name="userGroupId"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckAdminEditGroup(int newLimit, int userGroupId)
        {
            try
            {
                var licenseVerified = CheckLicense();
                if (licenseVerified.Result)
                {
                    XmlDocument licenseData = new XmlDocument();
                    licenseData = (XmlDocument)licenseVerified.Data;
                    XmlNode licenseNode = licenseData.SelectSingleNode("license");
                    var licenseErrorMsg = "";

                    if (licenseNode != null)
                    {
                        XmlNode licenceDetail = licenseNode;
                        var deviceLimit = Convert.ToInt32(licenceDetail.Attributes[6].Value);
                        deviceLimit = (deviceLimit == -1 ? int.MaxValue : deviceLimit);

                        DsDb _DsDb = new DsDb();
                        //檢驗向上調整時，新的GroupLimitSum是否大於LicLimit
                        var groupLimitSum = _DsDb.UserGroup.Where(o => o.DeleteDate == null && o.CreateDate != null).Sum(o => o.DeviceLimit);
                        var currentGroupDeviceLimit = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).DeviceLimit;
                        if ((groupLimitSum - currentGroupDeviceLimit + newLimit) > deviceLimit)
                        {
                            licenseErrorMsg = "Error_0066";
                            return new ExceptionResult(Message: licenseErrorMsg);
                        }
                        //檢驗向下調整時，新的GroupLimit是否大於當前Group內所有的合法Device
                        var clientDevices = _DsDb.Device.Where(o => o.CreateByUserId == userGroupId && o.DeleteByUserId == null && o.DeleteDate == null && o.CreateDate != null).Count();
                        if (clientDevices > currentGroupDeviceLimit)
                        {
                            licenseErrorMsg = "Error_0068";
                            return new ExceptionResult(Message: licenseErrorMsg);
                        }
                        licenseNode = null;
                        return new SuccessResult()
                        {
                            Result = true,
                            Message = "裝置數量限制檢驗成功,可修改"
                        };
                    }
                    return new ExceptionResult(Message: "");
                }
                return new ExceptionResult(Message: licenseVerified.Message);
            }
            catch (Exception)
            {
                return new ExceptionResult(Message: "");
            }
        }

        /// <summary>
        /// 檢驗DB內所有StorageLimit總和是否大於LicLimit(T/F)
        /// </summary>
        /// <param name="licenseData"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckAllGroupStorageLimitSum(XmlDocument licenseData)
        {
            try
            {
                var licenseErrorMsg = "";

                XmlNode licenseNode = licenseData.SelectSingleNode("license");
                if (licenseNode != null)
                {
                    XmlNode licenceDetail = licenseNode;
                    var storageLimit = Convert.ToInt64(licenceDetail.Attributes[5].Value);
                    //storageLimit = (storageLimit == -1 ? Int64.MaxValue : storageLimit);

                    DsDb _DsDb = new DsDb();
                    //檢查DB內所有Group的StorageLimit總和是否大於LicLimit
                    //var currentGroup = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null);
                    var gpStorageLimitSum = _DsDb.UserGroup.Where(o => o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).Sum(o => o.StorageLimit);
                    if (gpStorageLimitSum > storageLimit)
                    {
                        licenseErrorMsg = "Error_0070";
                        return new ExceptionResult(Message: licenseErrorMsg);
                    }
                    licenseNode = null;
                    return new SuccessResult(Data: storageLimit);
                }
                return new ExceptionResult(Message: "Error_0065");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 檢驗當前群組的DBStorageLimit是否大於LicLimit
        /// 檢驗當前User的UsageStorage總和是否大於當前群組DBStorageLimit
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="licLimit"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public ApiResult CheckUsageStorage(int userGroupId, long licLimit, long fileSize)
        {
            DsDb _DsDb = new DsDb();
            var licenseErrorMsg = "";

            //檢查當前Group的StorageLimit是否大於licLimit
            var currentGroupStorageLimit = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null).StorageLimit;
            if (currentGroupStorageLimit > licLimit)
            {
                licenseErrorMsg = "Error_0071";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            //檢查當前Group內StorageUsage總和+新增檔案是否大於當前群組的StorageLimit
            var currentGroupUsage = _DsDb.FileUpload.Where(o => o.UserGroupId == userGroupId && o.DeleteByUserId == null && o.DeleteDate == null && o.CreateDate != null).Sum(o => o.Size);
            if (currentGroupUsage > currentGroupStorageLimit)
            {
                licenseErrorMsg = "Error_0072";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            if (currentGroupUsage + fileSize > currentGroupStorageLimit)
            {
                licenseErrorMsg = "Error_0073";
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            return new SuccessResult()
            {
                Result = true,
                Message = "Check Pass"
            };
        }

        /// <summary>
        /// 上傳檔案時的Lic檢驗
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="fileSize"></param>
        /// <returns>(T/F)</returns>
        public ApiResult CheckStorage(int userGroupId, long fileSize)
        {
            var licenseErrorMsg = "";

            var licenseVerified = CheckLicense();
            if (licenseVerified.Result)
            {
                XmlDocument licenseData = new XmlDocument();
                licenseData = (XmlDocument)licenseVerified.Data;
                var checkGroupSum = CheckAllGroupStorageLimitSum(licenseData);

                if (checkGroupSum.Result)
                {
                    var DBLicLimit = Convert.ToInt64(checkGroupSum.Data);
                    var checkStorage = CheckUsageStorage(userGroupId, DBLicLimit, fileSize);

                    if (checkStorage.Result)
                    {
                        return new SuccessResult()
                        {
                            Result = true,
                            Message = checkStorage.Message
                        };
                    }
                    licenseErrorMsg = checkStorage.Message;
                    return new ExceptionResult(Message: licenseErrorMsg);
                }
                licenseErrorMsg = checkGroupSum.Message;
                return new ExceptionResult(Message: licenseErrorMsg);
            }
            return new ExceptionResult(Message: licenseVerified.Message);
        }


        /// <summary>
        /// 提供Player驗證EffectiveDate
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <returns></returns>
        public ApiResult CheckEffectiveDate(int userGroupId)
        {
            try
            {
                var licenseErrorMsg = "";

                var licenseVerified = CheckLicense();
                if (licenseVerified.Result)
                {
                    XmlDocument licenseData = new XmlDocument();
                    licenseData = (XmlDocument)licenseVerified.Data;
                    XmlNode licenseNode = licenseData.SelectSingleNode("license");

                    if (licenseNode != null)
                    {
                        XmlNode licenceDetail = licenseNode;
                        var expiration = Convert.ToDateTime(licenceDetail.Attributes[1].Value);
                        DateTime deadLine = Convert.ToDateTime(expiration);

                        DateTime? now = DateTime.MaxValue;
                        DsDb _DsDb = new DsDb();

                        //1.取得網路端的UTC時間
                        DateTime netUTC = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(GetUtcTimeStamp());
                        //如果年分是錯的，表示網路回應有問題，無法取得正確時間
                        if (netUTC.Year == 1970)
                        {
                            //2.取得客戶本機端的UTC時間               
                            DateTime clientUTC = DateTime.UtcNow;

                            //3.取得客戶最後登入時間
                            DateTime? lastLoginTime = _DsDb.User.Select(o => o.LastLoginTime).Max();
                            DateTime loginTimeUTC = TimeZoneInfo.ConvertTime((DateTime)lastLoginTime, TimeZoneInfo.Local, TimeZoneInfo.Utc);

                            now = (clientUTC > loginTimeUTC ? clientUTC : loginTimeUTC);
                            //licenseErrorMsg = "無法取得網路UTC";
                        }
                        else { now = netUTC; }

                        var currentGroup = _DsDb.UserGroup.SingleOrDefault(o => o.Id == userGroupId && o.DeleteByAdminUserId == null && o.DeleteDate == null && o.CreateDate != null);

                        if (currentGroup.EffectiveStartDate <= now && now <= currentGroup.EffectiveEndDate)
                        {
                            return new SuccessResult()
                            {
                                Result = true,
                                Message = "Check Pass"
                            };
                        }
                        licenseErrorMsg = "Error_0074";
                        licenseNode = null;
                        return new ExceptionResult(Message: licenseErrorMsg);
                    }
                    return new ExceptionResult(Message: "Error_0065");
                }
                return new ExceptionResult(Message: licenseVerified.Message);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}