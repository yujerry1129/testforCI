﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace DsKit.Helpers
{
    public class UserHelper
    {
        public static string GetUserName(HttpContextBase Context)
        {
            if(Context == null || Context.User == null)
            {
                return null;
            }

            var user = Context.User;

            if (user.Identity.IsAuthenticated)
            {
                var ticket = (user.Identity as FormsIdentity).Ticket;
                
                //Get user name in cookie
                return ticket.Name;
            }
            else
            {
                return null;
            }
        }

        public static string GetUserData(HttpContextBase Context)
        {
            if (Context == null || Context.User == null)
            {
                return null;
            }

            var user = Context.User;

            if (user.Identity.IsAuthenticated)
            {
                var ticket = (user.Identity as FormsIdentity).Ticket;

                //Get user data string in cookie
                return ticket.UserData;
            }
            else
            {
                return null;
            }
        }
    }
}
