﻿using DsDbLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DsKit.Helpers
{
    public class ConvertHelper
    {
        public static decimal GetGibabyteFromBytes(long Bytes, int Decimals)
        {
            return Math.Round(((decimal)Bytes / (1024 * 1024 * 1024)), Decimals);
        }
        public string CompareVersion(List<string> VersionList)
        {
            var versionList = VersionList;
            var maxVersion = "1.0.0";
            foreach (var version in versionList)
            {
                if (Version.Parse(version) >= Version.Parse(maxVersion))
                {
                    maxVersion = version;
                }
            }
            return maxVersion;
        }
    }
}
