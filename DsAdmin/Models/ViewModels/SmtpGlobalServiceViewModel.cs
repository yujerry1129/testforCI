﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DsAdmin.Models.ViewModels
{
    public class SmtpGlobalServiceViewModel
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("寄件者位置")]
        public string SenderAddress { get; set; }

        [Required]
        [DisplayName("寄件者姓名")]
        public string SenderName { get; set; }

        [DisplayName("啟用SSL")]
        public bool EnableSSL { get; set; }

        [DisplayName("SMTP Port")]
        public int SmtpPort { get; set; }

        [DisplayName("SMTP伺服器位置")]
        public string SmtpServerAddress { get; set; }

        [DisplayName("允許匿名存取")]
        public bool AllowAnonymous { get; set; }

        [DisplayName("SMTP帳號")]
        public string SmtpAccount { get; set; }

        [DisplayName("SMTP密碼")]
        public string SmtpPassword { get; set; }
    }
}