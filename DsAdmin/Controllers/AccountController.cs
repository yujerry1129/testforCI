﻿using DsDbLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DsAdmin.Controllers
{
    public class AccountController : Controller
    {
        private DsDb db = new DsDb();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string Account, string Password, string ReturnUrl)
        {
            var user = db.AdminUser.FirstOrDefault(o => o.LoginAccount == Account);

            if (user != null && Password == user.LoginPassword)
            {
                //Log in success
                
                //Create token
                var ticket = new FormsAuthenticationTicket(1, user.LoginAccount, DateTime.Now, DateTime.Now.Add(FormsAuthentication.Timeout), true, "user data");
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
                {
                    HttpOnly = true,
                };

                Response.Cookies.Add(cookie);

                if (!string.IsNullOrEmpty(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                //Login fail
                ViewBag.ErrorMessage = "Login failed";
                return View();
            }
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}