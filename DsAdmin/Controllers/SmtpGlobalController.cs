﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DsAdmin.Models;
using DsAdmin.Models.ViewModels;
using DsDbLib;
using DsDbLib.Models;
using System.Web.Helpers;
using System.Net.Mail;
using DsKit.Extensions;

namespace DsAdmin.Controllers
{
    [Authorize]
    public class SmtpGlobalController : Controller
    {
        private DsDb db = new DsDb();

        // GET: SmtpGlobalServiceViewModels
        public ActionResult Index()
        {
            var data = db.EmailService.Select(o => new SmtpGlobalServiceViewModel()
            {
                AllowAnonymous = o.AllowAnonymous,
                EnableSSL = o.EnableSSL,
                Id = o.Id,
                SenderAddress = o.SenderAddress,
                SenderName = o.SenderName,
                SmtpAccount = o.SmtpAccount,
                SmtpPassword = o.SmtpPassword,
                SmtpPort = o.SmtpPort,
                SmtpServerAddress = o.SmtpServerAddress
            });

            ViewBag.ServiceCount = db.EmailService.Count();

            return View(data);
        }

        // GET: SmtpGlobalServiceViewModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var emailService = db.EmailService.Find(id);

            var smtpGlobalServiceViewModel = new SmtpGlobalServiceViewModel()
            {
                SmtpServerAddress = emailService.SmtpServerAddress,
                SmtpPort = emailService.SmtpPort,
                AllowAnonymous = emailService.AllowAnonymous,
                EnableSSL = emailService.EnableSSL,
                Id = emailService.Id,
                SenderAddress = emailService.SenderAddress,
                SenderName = emailService.SenderName,
                SmtpAccount = emailService.SmtpAccount,
                SmtpPassword = emailService.SmtpPassword
            };

            if (smtpGlobalServiceViewModel == null)
            {
                return HttpNotFound();
            }
            return View(smtpGlobalServiceViewModel);
        }

        // GET: SmtpGlobalServiceViewModels/Create
        public ActionResult Create()
        {
            if (db.EmailService.Count() >= 1)
            {
                return Content("Global email service alreay exists!");
            }
            return View();
        }

        // POST: SmtpGlobalServiceViewModels/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SmtpGlobalServiceViewModel model)
        {
            if (ModelState.IsValid && db.EmailService.Count() == 0)
            {
                var emailService = new EmailService()
                {
                    SenderAddress = model.SenderAddress,
                    SmtpPassword = model.SmtpPassword,
                    AllowAnonymous = model.AllowAnonymous,
                    CreateDate = DateTime.Now,
                    EnableSSL = model.EnableSSL,
                    SenderName = model.SenderName,
                    SmtpAccount = model.SmtpAccount,
                    SmtpPort = model.SmtpPort,
                    SmtpServerAddress = model.SmtpServerAddress,
                    UpdateDate = null,
                    //UserGroup = null
                };

                db.EmailService.Add(emailService);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: SmtpGlobalServiceViewModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var emailService = db.EmailService.Find(id);

            if (emailService == null)
            {
                return HttpNotFound();
            }

            var model = new SmtpGlobalServiceViewModel()
            {
                SmtpServerAddress = emailService.SmtpServerAddress,
                SmtpPort = emailService.SmtpPort,
                AllowAnonymous = emailService.AllowAnonymous,
                EnableSSL = emailService.EnableSSL,
                Id = emailService.Id,
                SenderAddress = emailService.SenderAddress,
                SenderName = emailService.SenderName,
                SmtpAccount = emailService.SmtpAccount,
                SmtpPassword = emailService.SmtpPassword
            };

            return View(model);
        }

        // POST: SmtpGlobalServiceViewModels/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SmtpGlobalServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(smtpGlobalServiceViewModel).State = EntityState.Modified;

                var emailService = db.EmailService.Find(model.Id);

                if (emailService == null)
                {
                    return HttpNotFound();
                }
                
                emailService.AllowAnonymous = model.AllowAnonymous;
                emailService.EnableSSL = model.EnableSSL;
                emailService.SenderAddress = model.SenderAddress;
                emailService.SenderName = model.SenderName;
                emailService.SmtpAccount = model.SmtpAccount;
                emailService.SmtpPassword = model.SmtpPassword;
                emailService.SmtpPort = model.SmtpPort;
                emailService.SmtpServerAddress = model.SmtpServerAddress;
                emailService.UpdateDate = DateTime.Now;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: SmtpGlobalServiceViewModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var emailService = db.EmailService.Find(id);

            var smtpGlobalServiceViewModel = new SmtpGlobalServiceViewModel()
            {
                SmtpServerAddress = emailService.SmtpServerAddress,
                SmtpPort = emailService.SmtpPort,
                AllowAnonymous = emailService.AllowAnonymous,
                EnableSSL = emailService.EnableSSL,
                Id = emailService.Id,
                SenderAddress = emailService.SenderAddress,
                SenderName = emailService.SenderName,
                SmtpAccount = emailService.SmtpAccount,
                SmtpPassword = emailService.SmtpPassword
            };

            if (smtpGlobalServiceViewModel == null)
            {
                return HttpNotFound();
            }
            return View(smtpGlobalServiceViewModel);
        }

        // POST: SmtpGlobalServiceViewModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var emailService = db.EmailService.FirstOrDefault(o => o.Id == id);

            if (emailService != null)
            {
                db.EmailService.Remove(emailService);
                db.SaveChanges();
            }
            else
            {
                return HttpNotFound();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult SendTestEmail(int? id)
        {
            var emailService = db.EmailService.FirstOrDefault(o => o.Id == id);

            if (emailService == null)
            {
                return HttpNotFound();
            }

            var model = new SmtpGlobalServiceViewModel()
            {
                SmtpServerAddress = emailService.SmtpServerAddress,
                SmtpPort = emailService.SmtpPort,
                AllowAnonymous = emailService.AllowAnonymous,
                EnableSSL = emailService.EnableSSL,
                Id = emailService.Id,
                SenderAddress = emailService.SenderAddress,
                SenderName = emailService.SenderName,
                SmtpAccount = emailService.SmtpAccount,
                SmtpPassword = emailService.SmtpPassword
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult SendTestEmail(int? id, string title, string body, string sendTo)
        {
            string TestMailMessage = "";

            var emailService = db.EmailService.FirstOrDefault(o => o.Id == id);

            if (emailService == null)
            {
                return HttpNotFound();
            }

            var model = new SmtpGlobalServiceViewModel()
            {
                SmtpServerAddress = emailService.SmtpServerAddress,
                SmtpPort = emailService.SmtpPort,
                AllowAnonymous = emailService.AllowAnonymous,
                EnableSSL = emailService.EnableSSL,
                Id = emailService.Id,
                SenderAddress = emailService.SenderAddress,
                SenderName = emailService.SenderName,
                SmtpAccount = emailService.SmtpAccount,
                SmtpPassword = emailService.SmtpPassword
            };

            try
            {
                //Send mail
                var smtp = emailService.CreateSmtpClient();

                using (var message = new MailMessage())
                {
                    message.From = new MailAddress(emailService.SmtpAccount, emailService.SenderName);

                    foreach (var to in sendTo.Split(';'))
                    {
                        message.To.Add(to);
                    }

                    message.Subject = "test title";
                    message.Body = "test body";
                    message.IsBodyHtml = true;

                    smtp.Send(message);
                }

                TestMailMessage = "Send Success!";
            }
            catch (Exception ex)
            {
                TestMailMessage = ex.Message;
            }

            ViewBag.TestMailMessage = TestMailMessage;


            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
